import {
    GET_EBOOK_DETAIL_SUCCEED, 
    GET_EBOOK_LIST_SUCCEED, 
    ADD_EBOOK_SUCCEED, 
    UPDATE_EBOOK_SUCCEED, 
    DELETE_EBOOK_SUCCEED, 
    FAILED_PROCESS_EBOOK
} from '../constants/ActionTypes';

export default function reducer(state = {
    ebookList: null,
    totalEbook: 0,
    ebookData: null,
    succeed: null,
    error: null
}, action){
    switch(action.type){
        case GET_EBOOK_LIST_SUCCEED: {
            return {
                ...state,
                ebookList: action.payload.data,
                succeed: null
            }
        }
        case GET_EBOOK_DETAIL_SUCCEED: {
            return {
                ...state,
                ebookData: action.payload,
                succeed: null
            }
        }
        case ADD_EBOOK_SUCCEED: {
            return {
                ...state,
                succeed: 'Ebook berhasil ditambahkan',
                error: null
            }
        }
        case UPDATE_EBOOK_SUCCEED: {
            return {
                ...state,
                succeed: 'Ebook berhasil diupdate',
                error: null
            }
        }
        case DELETE_EBOOK_SUCCEED: {
            return {
                ...state,
                succeed: 'Ebook berhasil dihapus',
                error: null
            }
        }
        case FAILED_PROCESS_EBOOK: {
            return{
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}