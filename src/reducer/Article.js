import {
    API_URL,
    GET_ARTICLE_DETAIL_SUCCEED,
    GET_ARTICLE_LIST_SUCCEED,
    ADD_ARTICLE_SUCCEED, 
    UPDATE_ARTICLE_SUCCEED,
    DELETE_ARTICLE_SUCCEED,
    FAILED_PROCESS_ARTICLE
} from '../constants/ActionTypes';

export default function reducer(state = {
    articleList: null,
    articleData: null,
    succeed: null,
    error: null,
    totalData: 0
}, action){
    switch(action.type){
        case GET_ARTICLE_LIST_SUCCEED: {
            return{
                ...state,
                articleList: action.payload.data,
                totalData: action.payload.total_data,
                succeed: null
            }
        }
        case GET_ARTICLE_DETAIL_SUCCEED: {
            return {
                ...state,
                articleData: action.payload,
                succeed: null
            }
        }
        case ADD_ARTICLE_SUCCEED: {
            return {
                ...state,
                succeed: "Artikel berhasil ditambahkan",
                error: null
            }
        }
        case UPDATE_ARTICLE_SUCCEED: {
            return {
                ...state,
                succeed: "Artikel berhasil diupdate",
                error: null
            }
        }
        case DELETE_ARTICLE_SUCCEED: {
            return {
                ...state,
                succeed: "Artikel berhasil dihapus",
                error: null
            }
        }
        case FAILED_PROCESS_ARTICLE: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}