import {
    UPDATE_DYNAMIC_WEB_CONTENT_SUCCEED,
    GET_DYNAMIC_WEB_CONTENT_SUCCEED,
    FAILED_PROCESS_DYNAMIC_WEB_CONTENT
} from '../constants/ActionTypes';

export default function reducer(state = {
    contentData: null,
    succeed: null,
    error: null
}, action){
    switch(action.type){
        case GET_DYNAMIC_WEB_CONTENT_SUCCEED: {
            return {
                ...state,
                contentData: action.payload.data,
                succeed: null,
                error: null
            }
        }
        case UPDATE_DYNAMIC_WEB_CONTENT_SUCCEED: {
            return {
                ...state,
                contentData: action.payload,
                succeed: "Web konten berhasil diupdate",
                error: null
            }
        }
        case FAILED_PROCESS_DYNAMIC_WEB_CONTENT: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}