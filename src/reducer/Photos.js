import {
    GET_PHOTOS_SUCCEED,
    ADD_PHOTOS_SUCCEED,
    DELETE_PHOTOS_SUCCEED,
    FAILED_PROCESS_PHOTOS
} from '../constants/ActionTypes';

export default function reducer(state = {
    albumn: null,
    photosList: null,
    succeed: null,
    error: null
}, action){
    switch(action.type){
        case GET_PHOTOS_SUCCEED: {
            return {
                albumn: action.payload.album,
                photosList: action.payload.photos,
                succeed: null
            }
        }
        case ADD_PHOTOS_SUCCEED: {
            return {
                succeed: "Berhasil menambahkan foto dalam album",
                error: null
            }
        }
        case DELETE_PHOTOS_SUCCEED: {
            return {
                succeed: 'Berhasil menghapus foto dalam album',
                error: null
            }
        }
        case FAILED_PROCESS_PHOTOS: {
            return {
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}