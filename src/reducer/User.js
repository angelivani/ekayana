
import {
	USER_LOGIN_SUCCEED,
	USER_LOGIN_FAILED,
	ADD_USER_SUCCEED,		
	FAILED_PROCESS_USER,
	GET_USER_LIST,
	UPDATE_USER_LIST_SUCCEED,
	RESET_USER_PROPS,
	FAILED_UPDATE_USER
} from '../constants/ActionTypes';

export default function reducer(state = {
	userList: {},
	succeed: null,
	accessToken: null,
	fetched: false,
	error: null,
	updateError: null
}, action){
	switch(action.type){
		case USER_LOGIN_SUCCEED: {
			return{
				...state,
				fetched: true,
				error: null,
				accessToken: action.payload.data.accessToken,
				user: action.payload.data.user_data
			}
		}
		case USER_LOGIN_FAILED:{
			return{
				...state,
				fetched: false,
				error: action.payload
			}
		}
		case ADD_USER_SUCCEED: {
			return {
				...state,
				succeed: "User berhasil ditambahkan",
				error: null
			}
		}
		case GET_USER_LIST: {
			return {
				...state,
				userList: action.payload,
				error: null
			}
		}
		case UPDATE_USER_LIST_SUCCEED: {
			return {
				...state,
				succeed: "Data user berhasil diupdate",
				error: null
			}
		}
		case RESET_USER_PROPS: {
			return {
				...state,
				succeed: null,
				error: null,
				updateError: null
			}
		}	
		case FAILED_PROCESS_USER: {
			return {
				...state,
				succeed: null,
				error: action.payload
			}
		}
		case FAILED_UPDATE_USER: {
			return {
				...state,
				succeed: null,
				updateError: action.payload
			}
		}	
		default: {
			return state;
		}
	}
}