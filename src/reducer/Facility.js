import {
    ADD_FACILITY_SUCCEED,
    UPDATE_FACILITY_SUCCEED,
    DELETE_FACILITY_SUCCEED,
    GET_FACILITY_LIST_SUCCEED,
    GET_FACILITY_DETAIL_SUCCEED,
    FAILED_PROCESS_FACILITY
} from '../constants/ActionTypes';

export default function reducer(state = {
    facilityList: null,
    facilityData: null,
    succeed: null,
    error: null,
    totalData: 0
}, action){
    switch(action.type){
        case GET_FACILITY_LIST_SUCCEED: {
            return {
                ...state,
                facilityList: action.payload.data,
                totalData: action.payload.total_data,
                succeed: null,
                error: null
            }
        }
        case GET_FACILITY_DETAIL_SUCCEED: {
            return {
                ...state,
                facilityData: action.payload,
                error: null
            }
        }
        case ADD_FACILITY_SUCCEED: {
            return {
                ...state,
                succeed: "Fasilitas berhasil ditambahkan",
                error: null
            }
        }
        case UPDATE_FACILITY_SUCCEED: {
            return {
                ...state,
                succeed: "Fasilitas berhasil diupdate",
                error: null
            }
        }
        case DELETE_FACILITY_SUCCEED: {
            return {
                ...state,
                succeed: "Fasilitas berhasil dihapus",
                error: null
            }
        }
        case FAILED_PROCESS_FACILITY: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}
