import {
    GET_PINNED_EVENT_SUCCEED,
    GET_EVENT_LIST_SUCCEED,
    GET_EVENT_DETAIL_SUCCEED,
    ADD_EVENT_SUCCEED,
    UPDATE_EVENT_SUCCEED,
    DELETE_EVENT_SUCCEED,
    PIN_EVENT_SUCCEED,
    UNPIN_EVENT_SUCCEED,
    FAILED_PROCESS_EVENT
} from '../constants/ActionTypes';

export default function reducer(state = {
    eventPinnedList: null,
    eventData: null,
    eventList: null,
    succeed: null,
    error: null,
    totalData: 0
}, action){
    switch(action.type){
        case GET_PINNED_EVENT_SUCCEED: {
            return{
                ...state,
                eventPinnedList: action.payload.data,
                succeed: null,
                error: null
            }
        }
        case GET_EVENT_LIST_SUCCEED: {
            return {
                ...state,
                eventList: action.payload.data,
                totalData: action.payload.total_data,
                succeed: null
            }
        }
        case GET_EVENT_DETAIL_SUCCEED: {
            return {
                ...state,
                eventData: action.payload,
                succeed: null
            }
        }
        case ADD_EVENT_SUCCEED: {
            return {
                ...state,
                succeed: "Event berhasil ditambahkan",
                error: null
            }
        }
        case UPDATE_EVENT_SUCCEED: {
            return {
                ...state,
                succeed: "Event berhasil diupdate",
                error: null
            }
        }
        case DELETE_EVENT_SUCCEED: {
            return {
                ...state,
                succeed: "Event berhasil dihapus",
                error: null
            }
        }
        case PIN_EVENT_SUCCEED: {
            return {
                ...state,
                succeed: "Event berhasil diset menjadi prioritas",
                error: null
            }
        }
        case UNPIN_EVENT_SUCCEED: {
            return {
                ...state,
                succeed: "Event berhasil diunpin",
                error: null
            }
        }
        case FAILED_PROCESS_EVENT: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}