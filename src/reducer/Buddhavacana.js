import {
    GET_BUDDHAVACANA_LIST, 
    GET_BUDDHAVACANA_DETAIL, 
    ADD_BUDDHAVACANA_SUCCEED, 
    UPDATE_BUDDHAVACANA_SUCCEED, 
    FAILED_PROCESS_BUDDHAVACANA,
    DELETE_BUDDHAVACANA_SUCCEED
} from '../constants/ActionTypes';

export default function reducer(state = {
    buddhavacanaList: null,
    totalData: 0,
    buddhavacanaData: null,
    succeed: null,
    error: null
}, action){
    switch(action.type){
        case GET_BUDDHAVACANA_LIST: {
            return {
                ...state,
                buddhavacanaList: action.payload.data,
                totalData: action.payload.total_data,
                succeed: null
            }
        }
        case GET_BUDDHAVACANA_DETAIL: {
            return {
                ...state,
                buddhavacanaData: action.payload,
                succeed: null
            }
        }
        case ADD_BUDDHAVACANA_SUCCEED: {
            return {
                ...state,
                succeed: 'Buddhavacana berhasil ditambahkan',
                error: null
            }
        }
        case UPDATE_BUDDHAVACANA_SUCCEED: {
            return {
                ...state,
                succeed: 'Buddhavacana berhasil diupdate',
                error: null
            }
        }
        case DELETE_BUDDHAVACANA_SUCCEED: {
            return {
                ...state,
                succeed: 'Buddhavacana berhasil dihapus',
                error: null
            }
        }
        case FAILED_PROCESS_BUDDHAVACANA: {
            return{
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}