import {
    GET_LEADER_LIST_SUCCEED,
    GET_LEADER_DETAIL_SUCCEED,
    ADD_LEADER_SUCCEED,
    UPDATE_LEADER_SUCCEED,
    DELETE_LEADER_SUCCEED,
    FAILED_PROCESS_LEADER
} from '../constants/ActionTypes';

export default function reducer(state = {
    leaderList: null,
    leaderData: null,
    succeed: null,
    error: null,
    totalData: 0
}, action){
    switch(action.type){
        case GET_LEADER_LIST_SUCCEED: {
            return {
                ...state,
                leaderList: action.payload.data,
                totalData: action.payload.total_data,
                succeed: null
            }
        }
        case GET_LEADER_DETAIL_SUCCEED: {
            return {
                ...state,
                leaderData: action.payload,
                succeed: null
            }
        }
        case ADD_LEADER_SUCCEED: {
            return {
                ...state,
                succeed: "Leader berhasil ditambahkan",
                error: null
            }
        }
        case UPDATE_LEADER_SUCCEED: {
            return {
                ...state,
                succeed: "Leader berhasil diupdate",
                error: null
            }
        }
        case DELETE_LEADER_SUCCEED: {
            return {
                ...state,
                succeed: "Leader berhasil dihapus",
                error: null
            }
        }
        case FAILED_PROCESS_LEADER: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}