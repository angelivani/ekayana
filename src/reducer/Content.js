import {
    UPDATE_WEB_CONTENT_SUCCEED,
    GET_WEB_CONTENT_SUCCEED,
    FAILED_PROCESS_WEB_CONTENT,
} from '../constants/ActionTypes';

export default function reducer(state = {
    webContentData: null,
    succeed: null,
    error: null,
}, action){
    switch(action.type){
        case GET_WEB_CONTENT_SUCCEED: {
            return {
                ...state,
                webContentData: action.payload.data,
                succeed: null
            }
        }
        case UPDATE_WEB_CONTENT_SUCCEED: {
            return {
                ...state,
                succeed: "Konten berhasil diupdate",
                error: null
            }
        }
        case FAILED_PROCESS_WEB_CONTENT: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}