import {
  USER_SUBSCRIBE,
  USER_UNSUBSCRIBE,
  FAILED_SUBSCRIBE,
  FAILED_UNSUBSCRIBE
} from '../constants/ActionTypes';

export default function reducer(state = {
  succeed: null,
  error: null
}, action){
  switch(action.type){
      case USER_SUBSCRIBE: {
        return {
            ...state,
            succeed: action.payload,
            error: null
        }
      }
      case USER_UNSUBSCRIBE: {
        return {
          ...state,
          succeed: action.payload,
          error: null
        }
      }
      case FAILED_SUBSCRIBE: {
        return {
          ...state,
          succeed: null,
          error: action.payload
        }
      }
      case FAILED_UNSUBSCRIBE: {
        return {
          ...state,
          succeed: null,
          error: action.payload
        }
      }
      default: {
          return state;
      }
  }
}