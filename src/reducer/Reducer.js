import {combineReducers} from 'redux';
import user from './User';
import buddhavacana from './Buddhavacana';
import activity from './Activity';
import event from './Event';
import albumn from './Albumn';
import photos from './Photos';
import ebook from './Ebook';
import article from './Article';
import facility from './Facility';
import leader from './Leader';
import content from './Content';
import dynamiccontent from './DynamicContent';
import subscribe from './Subscribe';

export default combineReducers({
    user,
    buddhavacana,
    activity,
    event,
    albumn,
    photos,
    ebook,
    article,
    facility,
    leader,
    content,
    dynamiccontent,
    subscribe
})