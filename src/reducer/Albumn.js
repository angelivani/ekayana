import {
    API_URL,
    GET_ALBUMN_LIST_SUCCEED,
    GET_ALBUMN_DETAIL_SUCCEED,
    ADD_ALBUMN_SUCCEED, 
    UPDATE_ALBUMN_SUCCEED,
    DELETE_ALBUMN_SUCCEED,
    FAILED_PROCESS_ALBUMN
} from '../constants/ActionTypes';

export default function reducer(state = {
    albumnList: null,
    albumnData: null,
    photoList: null,
    succeed: null,
    error: null,
}, action){
    switch(action.type){
        case GET_ALBUMN_LIST_SUCCEED: {
            return{
                ...state,
                albumnList: action.payload,
                succeed: null
            }
        }
        case GET_ALBUMN_DETAIL_SUCCEED: {
            return {
                ...state,
                albumnData: action.payload.album,
                photoList: action.payload.photos,
                succeed: null
            }
        }
        case ADD_ALBUMN_SUCCEED: {
            return {
                ...state,
                succeed: action.payload
            }
        }
        case UPDATE_ALBUMN_SUCCEED: {
            return {
                ...state,
                succeed: action.payload,
                error: null
            }
        }
        case DELETE_ALBUMN_SUCCEED: {
            return {
                ...state,
                succeed: action.payload,
                error: null
            }
        }
        case FAILED_PROCESS_ALBUMN: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}