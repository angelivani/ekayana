import {
    GET_ACTIVITY_LIST_SUCCEED, 
    GET_ACTIVITY_DETAIL_SUCCEED, 
    ADD_ACTIVITY_SUCCEED, 
    UPDATE_ACTIVITY_SUCCEED, 
    DELETE_ACTIVITY_SUCCEED,
    FAILED_PROCESS_ACTIVITY
} from '../constants/ActionTypes';

export default function reducer(state = {
    activityData: null,
    activityList: null,
    succeed: null,
    error: null,
}, action){
    switch(action.type){
        case GET_ACTIVITY_LIST_SUCCEED: {
            return {
                ...state,
                activityList: action.payload,
                succeed: null
            }
        }
        case GET_ACTIVITY_DETAIL_SUCCEED: {
            return {
                ...state,
                activityData: action.payload,
                succeed: null
            }
        }
        case ADD_ACTIVITY_SUCCEED: {
            return {
                ...state,
                succeed: "Aktivitas berhasil ditambahkan",
                error: null
            }
        }
        case UPDATE_ACTIVITY_SUCCEED: {
            return {
                ...state,
                succeed: "Aktivitas berhasil diupdate",
                error: null
            }
        }
        case DELETE_ACTIVITY_SUCCEED: {
            return {
                ...state,
                succeed: "Aktivitas berhasil dihapus",
                error: null
            }
        }
        case FAILED_PROCESS_ACTIVITY: {
            return {
                ...state,
                succeed: null,
                error: action.payload
            }
        }
        default: {
            return state;
        }
    }
}