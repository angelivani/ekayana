export const reflections = {
    "01": {
        "01": {
            "content": [
                "Bangunlah, jangan lengah! Tempuhlah kehidupan yang benar. Orang yang menjalani kehidupan benar akan berbahagia di dunia ini maupun di alam sana.",
                "Ikutilah kehidupan yang benar, dan jangan menyimpang. Orang yang menjalani kehidupan benar akan berbahagia di dunia ini maupun di alam sana."
            ],
            "source": "Narada Mahathera, <em>Dhammapada - Sabda-sabda Buddha Gotama</em>, hal. 76"
        },
        "02": {
            "content": [
                "Seseorang tidak disebut bijaksana, hanya karena dia banyak bicara. Ia yang tenang, ramah, dan bebas dari rasa takutlah, yang disebut orang bijaksana.",
                "Seseorang tidak disebut ahli dalam Dharma, hanya karena ia banyak bicara. Ia yang meskipun hanya mendengar sedikit, tapi dapat melihat Kesunyataan dalam batinnya, yang sesungguhnya ahli dalam Dharma. Ia selalu berpegang teguh pada Dharma."
            ],
            "source": "Narada Mahathera, <em>Dhammapada - Sabda-sabda Buddha Gotama</em>, hal. 112"
        },
        "03": {
            "content": [
                "Meditasi dan belas kasih atau pelayanan kasih, berjalan seiring: tujuannya adalah Nirwana. Dan ini bisa dicapai dalam sekejap. Bila engkau ‘menyadari’ sepatah kata yang diucapkan Buddha, selubung pun terlepas, belenggu yang mengikatmu patah, dan engkau akan melihat Rahasia yang kini telah terbuka!",
                "Terbekahilah sungguh seorang Bodhisattwa. Dalam belas-kasihnya, Bodhisattwa mempersembahkan dirinya kepada kita, mengabdi dan menderita bagi umat manusia, bagi semua makhluk yang sengsara dalam kesakitan. Ia menyadari adanya sesuatu yang lebih berharga ketimbang kesucian diri sendiri, dan itu adalah: ‘penyucian’ orang-orang dan makhluk lainnya."
            ],
            "source": "T. L. Vaswani, <em>Mengikuti Jejak Buddha</em>, hal. 9"
        },
        "04": {
            "content": [
                "Seorang Bodhisattwa menyadari kekuatan <em>dunia sebelah-dalam</em>, karena ia pun tumbuh di dalam keheningan dan meditasi. Ia tidak menyerah kepada gangguan dari dunia luar, karena disadarinya betapa segala sesuatu bersifat sementara dan kosong. Tapi disadari pula: bahwa hal ini menjadi relatif dalam pandangan orang awam. Sungguh, Bodhisattwa memiliki belas-kasih yang mendalam terhadap mereka.",
                "Dalam keheningan dan meditasi ia belajar memusatkan pikiran, berlatih mengendalikan diri, dan perlahan-lahan mencapai kedamaian, ‘ketenangan yang dilandasi pengertian’. Begitulah ia lalu terbebas dari kesombongan dan egoisme, ia pun menjadi lembut dan sabar. Segala sesuatu, kini disadarinya sebagai subjek yang selalu berubah. Semua hal, terlihat olehnya bersifat sementara dan kosong, seperti mimpi, secercah kilat atau awan yang datang dan pergi.",
                "Bodhisattwa dengan demikian menjadi seorang Pembuka Jalan, sebuah titian ke Pantai Seberang, seorang pendiri jembatan."
            ],
            "source": "T. L. Vaswani, <em>Mengikuti Jejak Buddha</em>, hal. 57-58"
        },
        "05": {
            "content": [
                "Kunci kebahagiaan duniawi adalah <em>caga</em> (kemurahan hati), yang diwujudkan dalam tindakan yang aktif: yaitu <em>dana</em>, dan pasif: <em>sila</em>. Berdana artinya melepas, sedikit-demi-sedikit mengendurkan keterikatan (<em>upadana</em>) terhadap harta-benda. Menjaga sila (perilaku) berarti menahan, mengendalikan keenam indra. Dengan dana dan sila, secara perlahan namun pasti, seseorang mengikis akar-akar keserakahan (<em>lobha</em>), kebencian (<em>dosa</em>), dan kebodohan (<em>moha</em>) dari batinnya."
            ],
            "source": "J. L. Young, <em>Segenggam Daun Kebenaran</em>, hal. 24"
        },
        "06": {
            "content": [
                "Salah apabila dikatakan bahwa ‘dengan berbuat begini, seseorang akan menerima akibat begitu’. Itu adalah determinisme absolut, yang dikenal dengan istilah ‘takdir’. Berdasarkan hukum kausalitas Buddha, maka Hukum Karma adalah hukum sebab-akibat yang bersyarat.",
                "Seseorang yang melakukan perbuatan baik, diibaratkan sedang mengumpulkan tetes-tetes air ke dalam mangkuk. Bila ia mengerjakan sesuatu yang buruk, maka ia seakan-akan menaruh garam ke dalam mangkuk airnya. Akibat dari perbuatan buruknya itu, diibaratkan sebagai rasa asin. Semakin banyak tetes air yang dikumpulkannya, makin tidak terasa asin dari sejumput garam yang ditaruhnya."
            ],
            "source": "J. L. Young, <em>Segenggam Daun Kebenaran</em>, hal. 40"
        },
        "07": {
            "content": [
                "Adalah sifat lelaki untuk meletakkan dirinya lebih tinggi daripada wanita. Mengetahui hal ini, Buddha memelopori perubahan radikal dan mengangkat harkat wanita dengan anjuran sederhana: para suami hendaknya menghormati dan menghargai istri mereka. Seorang suami harus setia kepada istrinya. Artinya ia mesti memenuhi dan menjalankan kewajibannya terhadap istri, di dalam kehidupan berumah-tangga. Hanya dengan cara demikian, kebersamaan di dalam perkawinan dapat dipertahankan."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Perkawinan yang Bahagia</em>, hal. 29"
        },
        "08": {
            "content": [
                "Pemenuhan kebutuhan materi adalah hal kedua yang terpenting setelah kasih sayang dan perhatian orangtua. Kita mengenal banyak orangtua dari keluarga yang kurang begitu tercukupi secara materi tetapi telah membesarkan anak-anak mereka dengan baik serta dengan kasih sayang yang berkecukupan. Sebaliknya, banyak keluarga kaya yang telah mencukupi segala kebutuhan materi dari anak-anaknya tetapi menjauhkan mereka dari kasih sayang orangtua. Anak-anak seperti itu akan tumbuh tanpa perkembangan psikologis dan moral yang baik."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Perkawinan yang Bahagia</em>, hal. 41"
        },
        "09": {
            "content": [
                "Agar doa kita memberikan kesembuhan bagi orang yang sakit atau agar kondisi keuangan keluarga bertambah baik atau agar kenalan dilahirkan kembali ke alam yang lebih baik, tergantung pula kepada orang yang kita doakan dalam membuat sebab yang diperlukan. Jika ia telah membuat sebab tersebut, doa kita menyediakan kondisi bagi benih dari perbuatan baiknya di masa lalu untuk menjadi masak dan membawa hasil. Tetapi, jika orang tersebut belum menciptakan benih dengan perbuatan baiknya di masa lampau maka akan sulit bagi doa kita untuk dikabulkan. Kita dapat menebarkan pupuk dan menyiramnya dengan air, tetapi jika petani tersebut tidak menanam benih, tak akan ada yang tumbuh."
            ],
            "source": "Thubten Chodron, <em>Agama Buddha dan Saya</em>, hal. 29-30"
        },
        "10": {
            "content": [
                "Adanya orang yang berlatih tidak sempurna tidak berarti agama Buddha tidak sempurna. Ini berarti atau mereka tidak melatihnya dengan benar atau latihan mereka belum cukup kuat. Orang hidup dengan harmonis dan mencoba saling menerima kelemahan yang lain adalah penting dalam siklus religius. Tugas kita bukanlah mencoba menunjuk dengan jari kita dan berkata, “Mengapa tidak berlatih dengan lebih baik? Mengapa engkau tidak mengendalikan emosimu?” Tugas kita adalah untuk memikirkan, “Mengapa aku tidak berlatih dengan lebih baik sehingga perbuatan mereka tidak akan membuatku marah?” dan “Apa yang bisa kulakukan untuk membantu mereka?”"
            ],
            "source": "Thubten Chodron, <em>Agama Buddha dan Saya</em>, hal. 79-80"
        },
        "11": {
            "content": [
                "Sepanjang kita masih tergantung pada kata-kata dan aksara, membiarkan diri kita diperbudak mereka dan bukannya memanfaatkannya, kita tidak akan bisa menyadari apa yang mengatasi kata-kata dan aksara. Pada saat yang sama kita akan mencampuradukkan kedua jenis pengetahuan itu, berpikir bahwa kita mengetahui sesuatu padahal kita hanya mendengar atau membaca tentangnya. Karena alasan ini Zen mendesak bahwa mesti ada “tidak bergantung kepada kata-kata dan aksara”."
            ],
            "source": "Maha Sthavira Sangharakshita, <em>Zen Inti Sari Ajaran</em>, hal. 47"
        },
        "12": {
            "content": [
                "Dengan latihan adalah mungkin untuk memusatkan pikiran ke sebelah dalam dengan periode waktu yang semakin panjang. Ini pada akhirnya akan menghasilkan peralihan pusat perhatian dari dunia luar ke pikiran itu sendiri, sedemikian rupa sehingga saat kita berhubungan dengan kegiatan dunia luar suatu tingkat ingatan sebelah dalam dan mawas diri berlangsung."
            ],
            "source": "Maha Sthavira Sangharakshita, <em>Zen Inti Sari Ajaran</em>, hal. 57"
        },
        "13": {
            "content": [
                "Sebenarnya tidak banyak rahasia dalam hidup ini. Dari jenis perkataan yang kita ucapkan, orang dapat mengetahui siapa adanya kita. Orang bijaksana tidak akan menggunakan kata-kata bodoh. Orang bodoh tidak akan berkata-kata seperti layaknya seorang petapa. Orang bajik tidak akan menggunakan kata-kata yang kasar dan tajam. Orang yang bengis mungkin saja dapat menipu orang dengan kata-kata manis dan lemah-lembut, tetapi cepat atau lambat sifat aslinya akan terbuka dengan sendirinya. Seperti kata pepatah, “Kucing akan selalu keluar dari karungnya.”"
            ],
            "source": "Sumangalo Mahathera, <em>Buddhadharma untuk Anak</em>, hal. 38"
        },
        "14": {
            "content": [
                "Jika kita berbuat baik, kita akan membawa kebahagiaan pada orang lain dan pada diri sendiri. Melakukan sesuatu yang salah sering membawa kenikmatan dan kebahagiaan palsu, tetapi kebahagiaan sejati tidak pernah datang dari perbuatan yang tidak baik. Oleh karena itu, kita mesti berusaha membawa keseimbangan dalam hidup kita dengan menyingkirkan semua yang tidak baik dan menggantikannya dengan perbuatan, perkataan, dan pikiran baik."
            ],
            "source": "Sumangalo Mahathera, <em>Buddhadharma untuk Anak</em>, hal. 93"
        },
        "15": {
            "content": [
                "Masyarakat Buddhis dari tradisi mana pun memiliki benda-benda suci seperti patung, lukisan, stupa, dan sebagainya, tak lain karena rasa bakti itu merupakan bagian penting dari praktik Dharma. Dari latihan bakti ini akan lahir kerendahan hati, yang membikin harmonis hubungan kita dengan orang lain. Untuk ini Buddha telah mengajarkan bahwasanya empat pahala akan diperoleh mereka yang menghormati orang yang lebih tua: “panjang umur, kecantikan, kebahagiaan, dan kekuatan” (Dhammapada 109). Siapa yang tidak menginginkannya?"
            ],
            "source": "Bhikkhu Khantipalo, <em>Saya Seorang Buddhis - Bagaimana Menjadi Buddhis Sejati</em>, hal. 5"
        },
        "16": {
            "content": [
                "Empat kondisi luhur: cinta kasih, belas kasih, simpati atas kebahagiaan orang lain, dan keseimbangan batin, memberikan dua berkah—harmoni di dalam batin dan hubungan yang harmonis dengan orang lain. Arti penting empat hal ini tidak bisa diabaikan dalam praktik seorang Buddhis. Dengan empat hal itu seorang Buddhis melatih hati dan emosinya, dan dari sudut pandang Buddhisme, sikap lembut dan tidak agresif lebih penting meski pun pengetahuan Dharma yang kita miliki masih sedikit. Orang yang hanya dipenuhi oleh teori Dharma tentang tindakan welas asih (tidak menyakiti), tapi tidak disertai praktik, hanya akan menumbuhkan kesombongan dan mempertebal pandangan salah (ditthi)."
            ],
            "source": "Bhikkhu Khantipalo, <em>Saya Seorang Buddhis - Bagaimana Menjadi Buddhis Sejati</em>, hal. 58"
        },
        "17": {
            "content": [
                "Kita harus jatuh bangun ketika belajar berjalan. Lihatlah bayi: saya tak pernah menemukan bayi yang langsung dapat berjalan jauh tanpa jatuh. Bayi-bayi itu pun belajar berjalan sambil meraih dan memegang benda-benda yang bisa menunjangnya, dengan jatuh dan berusaha bangun sendiri. Begitu pula meditasi. Kita belajar bijaksana dengan mengamati kebodohan, dengan membuat kesalahan, merenungkannya, dan melepaskannya!",
                "Jika kita terlalu memikirkannya maka kita akan putus asa. Jika bayi-bayi itu juga berpikir macam-macam, maka mereka tidak akan pernah belajar berjalan. Karena lihatlah bayi yang sedang belajar berjalan itu, bukankah semua usaha mereka nampak seperti sia-sia?"
            ],
            "source": "Ajahn Sumedho, <em>Hidup Saat Ini</em>, hal. 23"
        },
        "18": {
            "content": [
                "Betapa banyak penderitaan yang dialami manusia dalam hidupnya, karena kita mengharapkan hidup ini lain dari kenyataannya! Kita memiliki gagasan romantis ini saat kita menjumpai orang yang kita sukai, jatuh cinta padanya, dan ingin hidup bahagia bersamanya, sekarang dan selama-lamanya. Dan tentang kematian? Anda berpikir, “Ah, mungkin kami akan mati bersama-sama.” Tapi, itu hanya harapan, kan? Ada harapan, dan adalah kekecewaan saat dia yang anda cintai mati mendahuluimu, atau lari bersama bajingan atau <em>salesman door-to-door</em>."
            ],
            "source": "Ajahn Sumedho, <em>Hidup Saat Ini</em>, hal. 38"
        },
        "19": {
            "content": [
                "Ada kalanya orang menganggap dengan bersuara lantang dan bersikap bengis saat bertengkar akan tampil sebagai pemenang. Tapi sebenarnya tidak demikian. Orang yang sedang diliputi kemarahan—sebelum kemarahan membakar pihak lawannya—maka kemarahan itu sudah membakar dirinya sendiri. Seperti orang yang meludah sambil menghadap ke angkasa. Sebelum ludah tiba di angkasa, ludah sudah jatuh dan mengenai diri sendiri. Sama halnya seperti menabur debu dengan melawan arah angin. Sasaran tidak kena, malah debu berbalik mengotori diri sendiri. Artinya, sebelum niat mencelakai orang lain kesampaian, kita sendiri sudah celaka."
            ],
            "source": "Master Hsing Yun, <em>Bagaimana Menambah Kebahagiaan Hidup</em>, hal. 26"
        },
        "20": {
            "content": [
                "Yang dimaksud dengan berhati lurus adalah menjaga hati dan pikiran agar tidak mudah goyah oleh godaan. Bagi yang berkepribadian lemah dan berjiwa rapuh, akan mudah disusupi godaan-godaan pada kesenangan duniawi. Mata kita hanya akan tertarik pada benda-benda yang indah; telinga kita hanya akan mendengar suara yang merdu; dan lidah kita hanya akan mau mencicipi makanan yang lezat. Tubuh menjadi manja, dan pikiran mengembara ke mana-mana tanpa dapat dikendalikan. Orang-orang tua dulu mengatakan bahwa perang yang tidak ada habisnya adalah perang melawan diri sendiri. Musuh yang paling sulit ditaklukkan adalah keinginan diri sendiri. Jika kita mampu menundukkan berbagai keinginan dan tuntutan yang datang dari dalam diri kita, maka kitalah pemenang dalam peperangan itu."
            ],
            "source": "Master Hsing Yun, <em>Bagaimana Menambah Kebahagiaan Hidup</em>, hal. 39"
        },
        "21": {
            "content": [
                "Buddha telah membabarkan ajaranNya dengan sempurna. Tapi kita sama sekali belum melatihnya, kecuali dalam kata-kata. Apa yang kita ucapkan belum selaras dengan pikiran kita—kita cenderung berkata kosong. Dan ajaran Buddha bukanlah sesuatu untuk dibicarakan atau direka-reka. Ia sungguh suatu pengetahuan tentang hakikat sesungguhnya dari kehidupan. Itulah mengapa Buddha mengatakan, “Tathagata hanyalah Penunjuk Jalan.” Beliau tidak dapat melatihnya untuk anda, karena kebenaran adalah sesuatu yang tidak dapat diungkapkan dengan kata-kata atau dioperkan kepada orang lain. Anda harus menemukannya sendiri."
            ],
            "source": "Ajahn Chah, <em>Meditasi - Jalan Menuju Kebebasan</em>, hal. 20"
        },
        "22": {
            "content": [
                "Kita mesti merenungkan apa yang akan kita lakukan sekarang. Mengapa kita hidup pada saat ini, untuk apa kita bekerja? Di dunia ini orang bekerja untuk mendapat ini atau itu, tapi para biksu mengajarkan sesuatu yang lebih mendalam. Apa pun yang kita lakukan, kita tidak mengharapkan imbalan. Kita bekerja tanpa pamrih. Jika anda melakukan sesuatu untuk memperoleh imbalan, maka anda akan menderita. Cobalah ini pada diri sendiri! Anda ingin membuat pikiran tenang kemudian anda duduk dan mencoba menenangkannya—yang anda peroleh hanya penderitaan! Cobalah! Jalan kami lebih halus—kami lakukan, kemudian melepas."
            ],
            "source": "Ajahn Chah, <em>Meditasi - Jalan Menuju Kebebasan</em>, hal. 74"
        },
        "23": {
            "content": [
                "Jika kesunyataan pertama menjelaskan adanya penderitaan dalam kehidupan ini, kesunyataan ketiga menguraikan adanya kegembiraan dan kedamaian. Jika sementara orang beranggapan bahwa agama Buddha berpikir dengan cara yang terlalu pesimis, itu karena mereka terlalu menekankan pada kebenaran pertama, bukan yang ketiga. Aliran Mahayana telah sangat berusaha menekankan kebenaran yang ketiga ini. Aliran Mahayana, misalnya, akan berbicara mengenai pepohonan yang hijau, rumpun bambu, dan bulan bulat penuh sebagai manifestasi dari Dharmakaya."
            ],
            "source": "Thich Nhat Hanh, <em>Empat Belas Pedoman</em>, hal. 11"
        },
        "24": {
            "content": [
                "Dalam masa-masa melatih diri, kita berkesempatan mempraktikkan keheningan atau setidaknya mengurangi kata-kata sembilan per sepuluh bagian. Latihan seperti ini sungguh sangat bermanfaat. Tidak saja kita dapat belajar mengendalikan kata-kata kita, tetapi kita dapat merefleksikan dan dengan lebih jelas melihat diri kita, orang-orang di sekitar kita, dan kehidupan. Marilah kita manfaatkan kesempatan yang ditawarkan oleh keheningan untuk melihat dan tersenyum pada bunga-bunga, rerumputan, alang-alang, burung-burung, dan saudara manusia kita. Pernahkah anda melakukan praktik keheningan, misalnya, selama lima hari? Jika jawabannya ya, maka engkau pasti mengetahui manfaat dari latihan seperti itu. Dengan keheningan, sekulum senyum dan kata-kata yang benar, kita dapat menumbuhkan kedamaian dalam diri kita dan dunia."
            ],
            "source": "Thich Nhat Hanh, <em>Empat Belas Pedoman</em>, hal. 24-25"
        },
        "25": {
            "content": [
                "Bagaimana pun, tidak ada kehidupan tanpa penderitaan; selalu harus ada usia tua, sakit, dan kematian. Harus ada perpisahan dengan yang dicintai, pertemuan dengan yang dibenci, serta hal-hal lain yang tidak menyenangkan. Tapi kita dapat melatih batin kita agar dapat awas terhadap penderitaan, untuk mengatasinya dan melepaskan segala keterikatan. Latihan pelepasan ini semata-mata merupakan sarana dengan apa yang kita dapat menempatkan batin di atas penderitaan—hidup di tengah penderitaan tanpa menderita dan hidup di tengah kebahagiaan tanpa terhanyut ke dalamnya."
            ],
            "source": "Ajahn Yantra Amaro, <em>Menuju Kebahagiaan Sejati</em>, hal. 18-19"
        },
        "26": {
            "content": [
                "Segala kesenangan dan “kebahagiaan” itu tak lebih dari nafsu keinginan (yang terpuasi), seperti halnya kesedihan dan “penderitaan” juga timbul dari nafsu keinginan (yang tak terpuasi). Dalam hal ini berhubungan erat dengan kebiasaan. Bila kita, misalnya, terbiasa menenggang rasa dan hidup dalam masyarakat yang sopan, kita serta merta akan “menderita” bila masuk dalam kelompok dengan cara bergaul yang kasar. Begitu pun saat pertama melakukan latihan atau praktik Dharma, mungkin akan terasa kurang menyenangkan bagi anda."
            ],
            "source": "Ajahn Yantra Amaro, <em>Menuju Kebahagiaan Sejati</em>, hal. 45-46"
        },
        "27": {
            "content": [
                "Naskah-naskah Buddhis tertentu menggolongkan semua kegiatan manusia sebagai berikut: 1. Bergerak, 2. Berdiri tenang, 3. Duduk, 4. Berbaring, 5. Berbicara, 6. Bekerja. Indra juga dibagi-bagi dalam enam kategori: 1. Melihat, 2. Mendengar, 3. Mencium, 4. Mengecap, 5. Menyentuh, 6. Berpikir. Zen bisa hadir dalam kesemuanya kapan pun juga. Sebenarnya, sebelum Zen meliputi kesemuanya, Zen itu sendiri masih dangkal. Tapi dari sudut pandang seorang pemula, tempat terbaik untuk memulai latihan Zen adalah di wihara atau tempat lainnya yang sunyi. Lagipula kita harus memulai latihan ini dalam posisi tradisional dan dengan teknik-teknik biasa. Bila dasarnya telah terbentuk, kita bisa mulai membangun dari sana."
            ],
            "source": "Shindai Sekiguchi, <em>Zen Pedoman Bagi Pemula</em>, hal. 87"
        },
        "28": {
            "content": [
                "Zen ibaratnya udara; ia ada di sekeliling kita, kita bernapas dengannya, hidup dalam lingkungannya, tapi kita tidak bisa menggenggamnya dengan tangan dan kita tidak perlu mencarinya. Misalnya, kekuatan sebuah lukisan atau sepenggal musik hanya akan terlihat jika si pengamat atau pendengar benar-benar menyatu di dalamnya sehingga melupakan yang lain: pada saat-saat seperti ini, ia berada dalam keadaan Zen seperti yang dialami oleh para biksu di wihara."
            ],
            "source": "Shindai Sekiguchi, <em>Zen Pedoman Bagi Pemula</em>, hal. 105-106"
        },
        "29": {
            "content": [
                "Seandainya semua makhluk mengetahui seperti Aku (Tathagata) mengetahui tentang manfaat berdana, mereka tidak akan menikmati semua yang mereka miliki tanpa membaginya dengan makhluk lain (yang membutuhkan) juga tidak akan membiarkan noda kekikiran menggoda dan menetap di dalam batinnya. Bahkan jika apa yang mereka miliki merupakan sedikit makanan terakhir yang dipunyai, mereka tidak akan menikmati tanpa membaginya (berdana), seandainya ada makhluk lain yang layak mendapatkannya."
            ],
            "source": "Shravasti Dhammika, <em>Buddhavacana – Renungan Harian dari Kitab Suci Agama Buddha</em>, hal. 9"
        },
        "30": {
            "content": [
                "Bila siapa pun menyakiti wajahmu, memukulmu dengan kepalan tangan, melemparimu dengan gumpalan tanah, memukulmu dengan tongkat atau melukaimu dengan pedang, engkau harus mengesampingkan semua keinginan dan pertimbangan duniawi serta melatih dirimu dengan cara seperti ini, “Batinku tidak akan tergoyahkan. Tiada perkataan jahat yang akan kuucapkan. Aku akan hidup dengan welas asih untuk kebaikan makhluk lain, dengan kemurahan hati, tanpa rasa dendam.” Demikianlah engkau harus melatih dirimu."
            ],
            "source": "Shravasti Dhammika, <em>Buddhavacana – Renungan Harian dari Kitab Suci Agama Buddha</em>, hal. 182"
        },
        "31": {
            "content": [
                "Kata-kata dan konsep mengandaikan segala sesuatu dalam kategori hitam dan putih. Orang baik, orang jahat, bodoh, cerdas—implikasi penggunaan bahasa ialah bahwa segala sesuatu ada dalam kategori yang definitif dan bebas. “Ini adalah orang dungu. Dia tidak dapat melakukan sesuatu.” Melihat kenyataan adalah melihat keberadaan segala sesuatu tidak melalui fantasi yang terkotak-kotak dalam kategori hitam atau putih. Segala sesuatu akan menjadi lebih terbuka dan dinamis. Seseorang mungkin tidak bisa mengerjakan sesuatu sekarang, tetapi itu tidak berarti dia selalu bodoh. Dia mempunyai banyak sifat lain yang menunjukkan kelebihannya."
            ],
            "source": "Alexander Berzin &amp; Thubten Chodron, <em>Dua Sisi Kesunyataan</em>, hal. 9"
        }
    },
    "02": {
        "01": {
            "content": [
                "Inti pelaksanaan Dharma bukanlah penampakan luar, melainkan motivasi dari dalam diri kita. Dharma yang sebenarnya, bukanlah wihara yang besar, upacara-upacara besar yang muluk, pakaian yang megah dan mewah, dan ritual yang rumit. Itu semua hanyalah sarana yang dapat membantu perkembangan batin kita jika digunakan dengan tepat, dengan motivasi yang benar. Kita tidak dapat menghakimi motivasi orang lain, dan sebaiknya kita tidak menghabiskan waktu untuk menilai perbuatan orang lain."
            ],
            "source": "Alexander Berzin &amp; Thubten Chodron, <em>Dua Sisi Kesunyataan</em>, hal. 81"
        },
        "02": {
            "content": [
                "Hidup sederhana sebagai suatu ungkapan <em>metta</em> menuntut perubahan cara pandang dan perilaku seseorang di dalam dunia yang dibangun di atas konsep-konsep milik, perburuan kesenangan, dan kompetisi ini. Seorang yang hidupnya sederhana akan memiliki perangai yang halus. Meski tetap efektif dan efisien, indra-indranya terkendali, dan sikapnya moderat dalam segala hal. Pembinaan batin melalui meditasi akan membentuk sikap yang wajar dan tidak ambisius: karena itulah ia disebut “indranya tenang”."
            ],
            "source": "Asadhananda, <em>Metta</em>, hal. 18"
        },
        "03": {
            "content": [
                "Dalam latihan <em>metta</em>, kita harus memahami emosi-emosi yang dapat mementahkan <em>metta</em>. Emosi-emosi itu ada yang sangat mirip dengan <em>metta</em> — <em>Visuddhimagga</em> menyebutnya musuh-musuh terdekat, yaitu nafsu birahi, keserakahan, dan cinta duniawi. Bila kita melihat sisi baik atau keindahan suatu objek dengan batin penuh nafsu, maka kita akan terhanyut. Jika kita berkata, “Aku mencintai barang ini, aku mengasihi orang itu,” seringkali itu berarti, “Aku ingin memperolehnya, memilikinya.” Dan itu bukanlah <em>metta</em>, melainkan keterikatan dan kepentingan pribadi yang berkedok cinta kasih. Keduanya jelas berbeda!"
            ],
            "source": "Asadhananda, <em>Metta</em>, hal. 25"
        },
        "04": {
            "content": [
                "Ketika anda duduk bermeditasi, jangan berpikir dan berharap untuk menjadi suci. Dengan menyingkirkan angan-angan ini dan itu, meskipun hanya beberapa menit, duduk diam, sementara batin kita secara aktif mengada dengan apa yang muncul di dalam maupun di luar diri, batin kita akan berubah menjadi seperti cermin. Kita melihat segala sesuatunya dengan jelas. Kita melihat diri sendiri: yang berupaya untuk menjadi baik, untuk menjadi yang pertama, atau menjadi yang terakhir. Kita melihat kemarahan sendiri, kegelisahan kita, sifat membesar-besarkan diri sendiri, dan apa yang sering kita sebut spiritualitas. Dan spiritualitas yang sesungguhnya adalah hidup dan mengada bersama <em>semua</em> itu."
            ],
            "source": "John Welwood (ed.), <em>Cuci Piringmu Sehabis Makan</em>, hal. 46"
        },
        "05": {
            "content": [
                "Menjadi diri sendiri memang teramat sulit. Seseorang dapat terus berpura-pura, mengenakan topeng, dengan mudah. Tetapi menjadi diri sendiri merupakan urusan yang kompleks; karena seseorang selalu berubah, tidak pernah sama setiap detiknya, selalu menampilkan sisi yang baru, kedalaman, dan wajah yang baru. Ia tidak dapat sekaligus menjadi semua itu pada saat yang sama, karena setiap saat membawa perubahan tersendiri. Jadi kalau seseorang itu cukup cerdas, ia akan melepaskan segala keterikatan. "
            ],
            "source": "John Welwood (ed.), <em>Cuci Piringmu Sehabis Makan</em>, hal. 83"
        },
        "06": {
            "content": [
                "Merenungkan dengan baik ajaran-ajaran yang mereka dengar dengan pikiran tak terikat,<br />Kemauan mereka untuk pencerahan diarahkan tanpa nafsu meraih keuntungan,<br />Mencari pengetahuan dan kekuatan, ajaran para Buddha yang menyucikan,<br />Mereka bekerja di jalan supraduniawi, bebas dari khayalan dan kecongkakan.",
                "Berlaku sesuai dengan kata-kata, teguh dalam ucapan benar,<br />Mereka tidak menodai keluarga para Buddha, tekun mempelajari pencerahan;<br />Tidak terikat terhadap perbuatan duniawi, mengharapkan kebaikan bagi dunia,<br />Tak kenal lelah dalam pekerjaan mulia, mereka menanjak semakin tinggi."
            ],
            "source": "Hudaya Kandahjaya (penerjemah), <em>Avatamsaka Sutra (Sutra Karangan Bunga) Kitab Dua Puluh Enam</em>, hal. 24"
        },
        "07": {
            "content": [
                "Pencarian kebaikan bagi semua makhluk adalah derma;<br />Disiplin adalah penghentian samsara, toleransi adalah tak melukai;<br />Semangat adalah keteguhan yang terus membesar dalam pelaksanaan mereka;<br />Ketaktergangguan dalam Jalan adalah meditasi terhadap hal-hal yang bajik.",
                "Penerimaan akan ketanpa-asalan, tak memihak, adalah kebijaksanaan agung;<br />Pengabdian adalah keterampilan dalam cara, bersumpah adalah mencari keberhasilan yang lebih besar;<br />Di sini ketakhancuran adalah kekuatan, pengetahuan adalah kegembiraan dalam bimbingan:<br />Begitulah mereka memperoleh sifat-sifat pencerahan dari saat ke saat."
            ],
            "source": "Hudaya Kandahjaya (penerjemah), <em>Avatamsaka Sutra (Sutra Karangan Bunga) Kitab Dua Puluh Enam</em>, hal. 88"
        },
        "08": {
            "content": [
                "Segalanya tidak kekal, sehingga kapan saja orang bisa mati. Orang harus berpikir bahwa pada saat kematian menjemput, ia harus pergi sendiri, tanpa bantuan dari sahabat maupun pengikut. Karenanya, jika orang memiliki kesempatan untuk mempraktikkan perbuatan bajik melalui tubuh, suara, dan pikiran, maka perbuatan itu harus dilakukan seketika itu juga. Seperti dinyatakan Candrakirti di dalam <em>Madhyamakavatara</em>.",
                "Jika orang tidak memegang (kebajikan-kebajikan) ini tatkala ia masih memiliki apa yang diperlukan dan berada di bawah kekuatan sendiri, (maka) setelah jatuh ke dalam jurang dan berada di bawah kekuatan lain, bagaimana ia bisa bangkit?"
            ],
            "source": "Arya Nagarjuna, <em>Surat Seorang Sahabat</em>, hal. 73-74"
        },
        "09": {
            "content": [
                "Pandangan benar adalah dahan yang meneliti dengan utuh. Pikiran benar adalah dahan yang membuat hal-hal lain diketahui. Pekerjaan, perkataan, dan perbuatan benar adalah dahan-dahan yang menyebabkan orang lain memiliki keyakinan. Perhatian, samadhi, dan usaha benar adalah dahan-dahan yang merupakan pembersih berbagai kotoran. Ini adalah delapan ranting jalan. Orang mesti mempraktikkan Jalan Mulia Berunsur Delapan ini untuk mencapai keadaan Nirwana yang damai."
            ],
            "source": "Arya Nagarjuna, <em>Surat Seorang Sahabat</em>, hal. 107"
        },
        "10": {
            "content": [
                "Banyak yang menganggap bahwa tujuan-tujuan duniawi dengan kenyamanan materi yang dihasilkannya dapat membawa kebahagiaan dan ketenangan batin. Mereka yang tidak menyadari kesementaraan akan menganggap materi itu permanen dan dapat memberikan kebahagiaan abadi. Ketika materi (atau perasaan puas) itu menampakkan kesementaraannya, muncullah penderitaan. Orang-orang akan mencari sesuatu yang baru, atau membuat standar kepuasan baru, untuk menggantikan ‘kebahagiaan’ yang hilang itu."
            ],
            "source": "Geshe Rabten, <em>Tujuh Butir Transformasi Pikiran</em>, hal. 25"
        },
        "11": {
            "content": [
                "Segala metode dalam beragam tradisi yang diajarkan oleh Buddha Shakyamuni memiliki satu tujuan: menghancurkan sikap kasihan pada diri sendiri dan pandangan salah tentang ‘aku’. Jika kita makin mendekati tujuan itu, maka latihan yang dijalani benar adanya; tapi jika hasil latihan itu menuju arah sebaliknya, maka kita harus menilai kembali semuanya. Kita harus menyeimbangkan neraca latihan dengan menerapkan metode yang benar dan berlatih lebih giat."
            ],
            "source": "Geshe Rabten, <em>Tujuh Butir Transformasi Pikiran</em>, hal. 65"
        },
        "12": {
            "content": [
                "Bila kalian berlatih dan tubuh terasa lelah, pikiran kacau dan kesal, itulah sesungguhnya kesempatan untuk mulai berjuang dengan diri sendiri. Jika kalian menentang kelelahan dan kekesalan itu, maka kalian hanya akan bertambah lelah dan kesal. Akan timbul perasaan putus asa. Lebih baik katakan pada diri sendiri, “Ini adalah kebodohan, tapi kebodohan adalah Kebuddhaan. Saya tidak akan menentangnya. Saya tidak akan marah. Saya tidak akan melawan keadaan saya.”"
            ],
            "source": "Chan Master Sheng Yen, <em>Pedang Pusaka Kebijaksanaan - Ulasan tentang Senandung Pencerahan</em>, hal. 31"
        },
        "13": {
            "content": [
                "Hakikat Buddha tidak dapat diciptakan dengan latihan. Ia selalu ada. Jika hakikat Buddha adalah sesuatu yang dapat diciptakan, maka ia tentunya juga dapat dihancurkan. Lalu untuk apa orang berlatih jika ia telah memiliki hakikat Buddha? Latihan tidak menciptakan seorang Buddha. Latihan menolong kita menyadari atau membuktikan keberadaan hakikat Buddha itu, yang selalu ada di sana. Jika seseorang bertanya, “Di mana atau apa sebenarnya Buddha itu?”, jawablah dengan sebuah pertanyaan, “Di mana atau apa yang sebenarnya bukan Buddha?” "
            ],
            "source": "Chan Master Sheng Yen, <em>Pedang Pusaka Kebijaksanaan - Ulasan tentang Senandung Pencerahan</em>, hal. 36"
        },
        "14": {
            "content": [
                "Berlatih seperti membersihkan debu di atas cermin. Sebelum pencerahan, kita membuat perbedaan antara cermin pikiran dan debu penderitaan. Sesudah pencerahan saat cermin bebas dari debu, kita sadar bahwa pikiran atau debu itu tidak ada. Cermin hanyalah sebuah bayangan diri yang semu, dan debu adalah kekecewaan di mana diri yang semu ini terikat. Cermin sejati tidak memantulkan suatu diri ataupun penderitaan; sesungguhnya cermin sejati bukanlah cermin.",
                "Tatkala kita berlatih, cermin dan debu berdampingan. Kita menyadari adanya suatu diri karena kekesalan. Hakikat Buddha hanya ada ketika ada kekesalan. Karena memiliki kekesalan, kita mengamati hakikat Buddha. Selama latihan, jangan peduli dengan pikiran yang menjengkelkan dan kacau. Mereka sama dengan batin Buddha.",
                "Inilah intisari Senandung Pencerahan. Selama berabad-abad Yung-chia memberitahu kita: bangkit menuju pencerahan, jangan mengejarnya. Gunakan pikiran dengan tepat dalam metode dan berusahalah sekuat tenaga."
            ],
            "source": "Chan Master Sheng Yen, <em>Pedang Pusaka Kebijaksanaan - Ulasan tentang Senandung Pencerahan</em>, hal. 48"
        },
        "15": {
            "content": [
                "Pandanglah sesamamu dengan hati welas asih, dan nyatakan semua teorimu yang abstrak dalam tindakan. Setiap saat, milikilah semangat: “Jika bukan kita yang menolong mereka, siapa lagi?” Apabila dapat melakukan hal itu, bahkan dunia yang buruk ini dapat diubah menjadi Tanah Suci Buddha Amitabha. (Tanah Suci adalah alam kelahiran-kembali bagi makhluk-makhluk yang dalam hidupnya mengembangkan perhatian-penuh dengan terus merapalkan nama Buddha Amitabha.)"
            ],
            "source": "Dharma Master Cheng Yen, <em>Menyelam ke Dasar Batin</em>, hal. 17"
        },
        "16": {
            "content": [
                "Setiap gerak pikiran menghasilkan karma. Membuka mulut atau menggerakkan lidah, mengangkat tangan atau menendang — setiap gerakan dengan mudah dapat berubah menjadi karma buruk. Buddha tidak mengajarkan kita untuk mengingat-ingat kesalahan yang lalu, tetapi untuk menyesali dan tidak mengulanginya. Sempurnakan dirimu dari waktu ke waktu, capailah kebebasan dan ketenangan batin."
            ],
            "source": "Dharma Master Cheng Yen, <em>Menyelam ke Dasar Batin</em>, hal. 70"
        },
        "17": {
            "content": [
                "Umat awam merasakan kesulitan dalam menjalankan konsep “ketidakmelekatan” dan “tanpa-pribadi”. Dengan tujuan menjangkau umat biasa, Buddha mengajarkan dengan cara yang cukup mereka kenal. Jadi, ajaran untuk berjuang demi Alam Sukhavati kelihatan menarik bagi kita sebagai motivator dan tampaknya membentuk suatu jenis kemelekatan. Walaupun demikian, latihan pembacaan berulang-ulang “Amitabha” secara bertahap akan menuntun kita ke pembebasan berupa tidak adanya “pribadi”. Lagipula, Alam Sukhavati-nya umat Buddha tidak ada di suatu tempat tertentu. Saat pikiran kita bebas dari cengkeraman konsep “pribadi”, kita berada di Alam Sukhavati."
            ],
            "source": "Dr. Yutang Lin, <em>Pembacaan Berulang-ulang Amitabha</em>, hal. 24"
        },
        "18": {
            "content": [
                "Yang sangat bermanfaat bagi kita sebagai praktisi ialah memahami dan mempertahankan makna betapa terbatas dan kecilnya kita dalam hal waktu dan tenaga yang dimiliki. Jika pikiran kita benar-benar telah terbuka, kita akan menyadari bahwa masing-masing dari antara kita sangat kecil dan sangat tidak berarti dari segi materi; dunia juga tidak jauh berbeda dengan ada atau tanpa diri kita. Jika pikiran anda terbuka seperti pikiran Buddha, anda siap bekerja demi kebaikan orang lain. Anda tidak lagi bisa diabaikan karena anda berhubungan dengan banyak orang dan segala sesuatu yang anda lakukan mempunyai pengaruh yang bertahan lama. Bahkan setelah anda meninggal dunia, hasil kerja anda akan terus bermanfaat bagi orang lain. Guru saya telah meninggal, kendatipun demikian, kita masih merasakan manfaat dari ajaran-ajarannya."
            ],
            "source": "Dr. Yutang Lin, <em>Pembacaan Berulang-ulang Amitabha</em>, hal. 51-52"
        },
        "19": {
            "content": [
                "Ia yang tidak menaklukkan keinginan dan khayalan hanya mengucapkan kata-kata yang hampa dan kosong.<br />Ia yang tidak tahu cara-cara yang terampil dan mendalam akan gagal, betapa pun giat upayanya.<br />Ia yang tidak memiliki kunci kepada makna Dharma yang mendalam akan menjauh dari Jalan, betapa pun besar semangatnya.<br />Ia yang tidak mengumpulkan kebajikan dan hanya mencari pembebasannya sendiri akan dilahirkan kembali."
            ],
            "source": "Lobsang P. Lhalungpa, <em>Riwayat Hidup Milarepa</em>, hal. 286"
        },
        "20": {
            "content": [
                "Realisasi tidak muncul dari perkataan belaka<br />Pengertian tidak berasal dari dugaan belaka.<br />Aku meminta mereka yang berjuang untuk Pencerahan<br />Agar bermeditasi dengan tekun dan penuh upaya.<br />Upaya dan kesabaran akan mengatasi kesulitan yang terbesar sekali pun<br />Semoga tiada rintangan bagi orang-orang yang mencari Pencerahan."
            ],
            "source": "Lobsang P. Lhalungpa, <em>Riwayat Hidup Milarepa</em>, hal. 190"
        },
        "21": {
            "content": [
                "Kebajikan adalah keadaan yang asli dari tubuh, ucapan, dan pikiran. Bila keaslian itu hilang dari pikiran kita, maka ia juga akan hilang dari tindakan tubuh dan ucapan kita, karena keduanya ada di bawah pengaruh pikiran. Karenanya, mereka yang sungguh-sungguh hendak memupuk kebajikan dan menghayati ajaran Buddha, harus melatih pikirannya dengan bermeditasi."
            ],
            "source": "Ajahn Thate Desaransi, <em>Samadhi</em>, hal. 16"
        },
        "22": {
            "content": [
                "Air dalam keadaan aslinya adalah zat yang jernih dan murni, namun jika ditambahkan pewarna ke dalamnya ia akan berubah dan memiliki warna yang sesuai; jika dimasukkan pewarna merah, ia akan menjadi merah; jika hitam, ia hitam. Meskipun begitu, air tidak akan kehilangan kemurnian dan warna alamiahnya. Zat warna adalah faktor eksternal yang berbeda secara hakiki dari zat cair itu. Demikianlah orang bijaksana akan menyaring semua air berwarna (batin yang penuh noda), sehingga kembali ke warna aslinya (batin yang murni dan bercahaya)."
            ],
            "source": "Ajahn Thate Desaransi, <em>Samadhi</em>, hal. 49"
        },
        "23": {
            "content": [
                "Melalui cara kita berpikir dan melalui cara kita memercayai sesuatu, dunia kita terbentuk. Di abad pertengahan, setiap orang hanya menerima gagasan yang diberikan, atas dasar rasa takut, bahwa hanya ada satu cara untuk percaya; jika anda mempunyai kepercayaan yang lain, anda adalah musuh. Kondisi seperti itu merupakan lonceng kematian bagi pola pemikiran bebas dan kreatif. Banyak hal yang sebenarnya mampu dilihat orang, tidak lagi terlihat karena mereka tidak memercayainya. Begitu mereka mulai memercayai dan berpikir dengan cara tertentu, terdapat banyak sekali hal yang tidak mampu lagi mereka dengar, lihat, cium, atau sentuh, karena semua itu berada di luar sistem pola pikir mereka."
            ],
            "source": "Pema Chodron, <em>Kebijakan Sejati</em>, hal. 51"
        },
        "24": {
            "content": [
                "Saya terperanjat membaca kutipan pada papan pengumuman kemarin, bunyinya, “Latihan sehari-hari hanyalah untuk mengembangkan sikap penerimaan dan keterbukaan sepenuhnya terhadap semua situasi, emosi, dan masyarakat.” Anda membacanya, anda mendengarnya, dan mungkin saya bahkan telah membicarakannya, tetapi pada dasarnya, apakah maknanya? Tatkala membacanya, anda seperti mengetahui apa artinya, tetapi ketika anda mencoba melakukannya, usaha anda kelihatan bertentangan dengan pernyataan itu, maka pandangan awal anda atas makna tersebut akan buyar; anda menemukan sesuatu yang segar dan baru, yang belum pernah anda sadari sebelumnya. Pengalaman pribadi akan Dharma berarti hidup dengannya, mengujinya, berusaha menemukan apa arti sesungguhnya dalam hal anda kehilangan pekerjaan, ditinggal kekasih, sekarat karena penyakit kanker. “Terbukalah dan terimalah semua situasi dan masyarakat.” Bagaimana anda melakukannya? Barangkali ini adalah nasihat terburuk yang pernah diberikan pada anda, tetapi anda harus menemukan caranya sendiri."
            ],
            "source": "Pema Chodron, <em>Kebijakan Sejati</em>, hal. 131"
        },
        "25": {
            "content": [
                "Berbicara tentang pembentukan kehidupan, sebuah pertanyaan sudah dipertanyakan banyak orang sejak dahulu, yaitu, “Yang mana muncul dahulu, ayam atau telur?”<br />Agama Buddha tidak mempermasalahkan mana yang muncul terlebih dahulu, juga tidak mempertentangkan asal mula atau akhir. Agama Buddha membicarakan tentang “lingkaran”. “Lingkaran” ini tidak memiliki awal atau akhir."
            ],
            "source": "Master Hsing Yun, <em>Karakteristik &amp; Esensi Agama Buddha</em>, hal. 18"
        },
        "26": {
            "content": [
                "Beberapa orang mengatakan bahwa agama Buddha menganjurkan pengurangan emosi. Akan tetapi, pada kenyataannya, agama Buddha menempatkan banyak penekanan pada emosi. Yang ditolak oleh agama Buddha adalah emosi diri dan nafsu keinginan diri. Seseorang seharusnya mengubah emosi diri menjadi kebijaksanaan. Emosi yang disarankan dalam agama Buddha adalah pengabdian, bukan pemilikan. Yang diharapkan ialah cinta kasih untuk memberi, bukan meminta."
            ],
            "source": "Master Hsing Yun, <em>Karakteristik &amp; Esensi Agama Buddha</em>, hal. 63"
        },
        "27": {
            "content": [
                "Bagaimana guru-guru Chan berlatih dan mencapai kesadaran? Mereka berlatih mencapai kesadaran dengan hidup di dalam persamuhan Sanggha dan berlatih di setiap saat mereka terjaga dalam kehidupan setiap hari. Orang-orang suci jaman dahulu pernah mengatakan, “Mengumpulkan kayu bakar dan membawa air, semua adalah Chan.” Dalam kehidupan setiap hari, kita dapat berlatih pada saat mengenakan pakaian atau sedang makan; kita dapat berlatih pada saat sedang berjalan atau tidur; kita dapat berlatih bahkan ketika kita pergi ke kamar kecil."
            ],
            "source": "Master Hsing Yun, <em>Karakteristik &amp; Esensi Ajaran Zen</em>, hal. 12"
        },
        "28": {
            "content": [
                "Tao Ch’ien bertanya kepada Master Chan, Chao Chou, “Bagaimana kita harus bermeditasi untuk memperoleh penyadaran?”<br />Setelah mendengar pertanyaan ini, Chao Chou berdiri dan berkata, “Saya ingin buang air.”<br />Setelah berjalan dua langkah, Chao Chou berhenti dan berkata, “Lihat, hal mudah seperti ini pun masih harus dilakukan sendiri.”<br />Prinsip yang sama digunakan untuk mencari Dharma — tidak seorang pun dapat menolong dalam hal ini."
            ],
            "source": "Master Hsing Yun, <em>Karakteristik &amp; Esensi Ajaran Zen</em>, hal. 32"
        },
        "29": {
            "content": [
                "Latihan maupun pencerahan tergantung pada diri kita sendiri. Ketika bercermin diri atau mengamati diri sendiri, kita seharusnya bertindak seperti Awalokiteswara. Kita seharusnya mengamati diri kita dan melihat apakah kita ada atau tidak. Ini adalah semangat Chan. Chan adalah kebijaksanaan. Kadang-kadang kita melihat bahwa Awalokiteswara juga memegang sebuah tasbih. Seseorang bertanya, “Kita menggunakan tasbih untuk membaca nama Awalokiteswara. Nama siapakah yang dibacakan oleh Awalokiteswara tersebut?”<br />“Juga Awalokiteswara.”<br />“Mengapa Awalokiteswara masih perlu membaca nama Awalokiteswara?”<br />“Oleh karena bergantung pada diri sendiri lebih bisa diandalkan daripada bergantung kepada orang lain.”"
            ],
            "source": "Master Hsing Yun, <em>Pembahasan Tiga Sutra Agama Buddha</em>, hal. 15-16"
        }
    },
    "03": {
        "01": {
            "content": [
                "Suatu kali, Sariputra berkata kepada Buddha, “Tanah-tanah Buddha di sepuluh penjuru, semuanya sangat bersih. Mengapa Dunia Saha kita begitu kotor dan tidak murni?”",
                "“Kamu tidak dapat memahami dunia tempat aku tinggal,” jawab Buddha. Saat berbicara, Buddha menekan bumi dengan jari kaki-Nya. Tiba-tiba, dunia menjadi cemerlang, bersih, dan indah sekali. Buddha berkata, “Inilah dunia yang Aku tinggali.”",
                "Orang-orang mungkin berada di tempat yang sama pada saat yang sama dan melakukan hal yang sama, tetapi mereka bisa saja memiliki perasaan yang berbeda karena perbedaan-perbedaan dunia internal mereka."
            ],
            "source": "Master Hsing Yun, <em>Pembahasan Tiga Sutra Agama Buddha</em>, hal. 77"
        },
        "02": {
            "content": [
                "Pertama kali, kita melatih perbuatan dan ucapan agar bebas dari hal-hal yang tidak bermanfaat, yang merupakan kebajikan. Sebagian orang mengira bahwa untuk memiliki kebajikan, anda harus menghafal istilah-istilah Pali dan membaca paritta siang malam, tetapi yang sebenarnya harus anda lakukan ialah mengusahakan agar perbuatan dan ucapan anda tidak tercela, dan itulah kebajikan. Itu tidak terlalu sukar untuk dipahami. Ibarat memasak makanan — memasukkan sedikit bahan ini dan sedikit bumbu itu, hingga rasanya sesuai dan masakan akan menjadi lezat. Setelah terasa lezat, anda tidak perlu lagi menambahkan apa-apa. Bahan dan bumbu yang tepat telah dimasukkan. Dengan cara yang sama, memperhatikan perbuatan dan ucapan kita agar benar akan menghasilkan kebajikan yang “lezat”, kebajikan yang tepat."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (1)</em>, hal. 11"
        },
        "03": {
            "content": [
                "Semua murid saya perlakukan seperti anak saya. Saya hanya memikirkan sifat cinta kasih dan kesejahteraan mereka. Jika saya membuat anda menderita, itu juga untuk kepentingan anda belaka. Saya tahu di antara anda ada yang terpelajar dan sangat berpendidikan. Orang-orang yang memiliki sedikit ilmu dan pengetahuan duniawi dapat berlatih dengan mudah. Akan tetapi, orang yang memiliki banyak pengetahuan bagaikan seseorang yang mempunyai rumah yang besar untuk dibersihkan. Mereka harus melakukan banyak hal. Akan tetapi, setelah rumah itu dibersihkan, mereka akan memiliki ruang tinggal yang nyaman dan luas. Bersabarlah. Kesabaran dan ketabahan dibutuhkan dalam latihan kita."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (1)</em>, hal. 34"
        },
        "04": {
            "content": [
                "Mencari kedamaian bagaikan mencari kura-kura yang berjenggot. Anda tidak akan menemukannya. Akan tetapi, jika hati anda siap, kedamaian akan muncul dan mencari anda."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (2)</em>, hal. 12"
        },
        "05": {
            "content": [
                "Betapa pun anda menyukai sesuatu, anda harus meyakinkan diri anda bahwa hal itu tidaklah kekal. Ibarat tunas bambu: mungkin saja kelihatan enak dimakan, tetapi anda harus berkata pada diri anda, “Belum tentu!” Jika anda ingin membuktikannya, cobalah makan tunas bambu setiap hari. Akhirnya, anda akan mengeluh, “Ini tidak terasa enak lagi!” Selanjutnya, anda akan lebih menyukai makanan yang lain dan yakin bahwa makanan itu enak. Akan tetapi, anda akan menemukan bahwa makanan itu juga “belum tentu” enak. Segala sesuatu bersifat “tidak tentu”."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (2)</em>, hal. 26"
        },
        "06": {
            "content": [
                "Sebenarnya, tahukah anda, kita manusia, cara kita melakukan sesuatu, cara kita hidup, cara kita bertingkah laku, semuanya bagaikan seorang anak kecil. Seorang anak belum mengetahui apa-apa. Jika seorang dewasa mengamati tingkah laku anak kecil, caranya bermain dan melompat-lompat, tindakan anak itu terlihat tidak banyak menyiratkan banyak tujuan. Jika pikiran tidak terlatih, pikiran akan menjadi seperti anak kecil. Kita berbicara dengan tidak berhati-hati dan bertindak tanpa kebijaksanaan. Kita bisa mundur, tetapi kita tidak mengetahuinya. Seorang anak kecil tidak tahu apa-apa sehingga ia hanya bermain sebagaimana anak-anak yang lain. Pikiran kita yang penuh dengan ketidaktahuan juga sama. Itulah sebabnya, Buddha mengajarkan kita untuk melatih pikiran."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (3)</em>, hal. 3"
        },
        "07": {
            "content": [
                "Ketika manusia memasuki anak sungai Dharma, semuanya adalah sama. Walaupun mereka bisa berasal dari tempat yang berbeda, mereka berada dalam keharmonisan, mereka bersatu. Bagaikan sungai dan anak sungai yang mengalir menuju laut. Begitu memasuki laut, mereka memiliki rasa dan warna yang sama. Demikian pula manusia."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan - Meditasi dalam Segala Objek Kehidupan (3)</em>, hal. 40"
        },
        "08": {
            "content": [
                "Pada bulan Agustus 1981 saya singgah di Universiti Sains Malaysia dan memperoleh kehormatan mengunjungi Sekretariat Keluarga Mahasiswa Buddhis (<em>Buddhist Society Room</em>). Dapat saya amati juga pada saat itu bahwa banyak kelompok mahasiswa yang lain juga diberi kesempatan memiliki ruangannya sendiri-sendiri. Yang mengejutkan saya adalah bahwa empat hingga lima ruangan ditempati oleh beberapa kelompok agama Kristen. Bukannya merasa iri di dalam hati, batin saya malah menjadi ringan dan lapang rasanya. Bagi saya, papan nama yang cuma ditulis “Keluarga Mahasiswa Buddhis” sangat simbolis sifatnya—ia melambangkan kesatuan."
            ],
            "source": "Piyasilo Mahatera, <em>Jalan Tunggal - Studi Perbandingan Mengenai Mahayana dan Theravada</em>, hal. 1"
        },
        "09": {
            "content": [
                "Jika orang benar-benar berminat pada perkembangan spiritual—dan tidak terlalu banyak pada aspek-aspek kultural dan historis—maka ia tidak perlu berafiliasi pada salah satu aliran atau sekte itu, karena tidakkah semua aliran itu cuma suatu ekspresi kultural dan bentuk luar belaka? Di samping itu, sektarianisme sungguh-sungguh merupakan kepercayaan atas pandangan salah akan adanya sesuatu diri (<em>sakkaya ditthi</em>/<em>satkaya drsti</em>)—rintangan pertama dalam jalan menuju Pencerahan."
            ],
            "source": "Piyasilo Mahatera, <em>Jalan Tunggal - Studi Perbandingan Mengenai Mahayana dan Theravada</em>, hal. 54"
        },
        "10": {
            "content": [
                "Tanpa api dan pedang, melainkan dengan keteguhan, Buddha Dharma telah menemukan dan akan menemukan jalannya kepada berjuta-juta hati dari berjuta-juta makhluk hidup. Tidak seperti api, Terang Dharma yang penuh dengan <em>Viriya</em>, tidak pernah membawa kehancuran, melainkan hanya akan membawa terang di dalam batin manusia, tidak akan membakar tubuh mereka.",
                "Ashin Jinarakkhita<br />Bulan Waisaka Purnama 2550 (24 Mei 1956)"
            ],
            "source": "Edij Juangari, <em>Menabur Benih Dharma di Nusantara - Riwayat Singkat Bhikkhu Ashin Jinarakkhita</em>, hal. 99-100"
        },
        "11": {
            "content": [
                "Pada tahun itu pula, Dalai Lama berkunjung ke Indonesia. Dalam kunjungannya itu, Dalai Lama sempat bertemu dengan Bhante Ashin Jinarakkhita. Mereka bercakap-cakap dengan akrab dalam bahasa Inggris.",
                "“To what sect of Buddhism do you belong?” demikian Dalai Lama bertanya pada Bhante Ashin Jinarakkhita.",
                "“I am just a servant of the Buddha,” ujar Bhante Ashin Jinarakkhita."
            ],
            "source": "Edij Juangari, <em>Menabur Benih Dharma di Nusantara - Riwayat Singkat Bhikkhu Ashin Jinarakkhita</em>, hal. 203"
        },
        "12": {
            "content": [
                "Ketahuilah wahai Rasa Sakit!<br />Kamu bisa mengakibatkan hal-hal yang terburuk sekalipun,<br />menghancurkan tubuh saya ini,<br />membuat saya kurus sehingga tinggal kulit pembalut tulang,<br />meremukkan saya hingga menjadi bubur,<br />mengiris-iris saya menjadi lapisan tipis,<br />mencincang saya hingga menjadi potongan kecil,<br />menggerus saya hingga menjadi serbuk,<br />dan membuat tepung halus dari tulang-tulang saya.",
                "Tetapi, ketahuilah wahai Rasa Sakit!<br />kamu tidak akan bisa mengganggu pikiran ini!<br />Karena pikiran ini berada di atasmu<br />ia bisa menertawakanmu,<br />ia bisa menyanyi dan menari,<br />ia bisa berenang dan berlari,<br />ia bisa menonton dan menunggu,<br />ia bisa bermeditasi.",
                "Ketahuilah wahai Rasa Sakit!<br />pikiran ini tenang<br />hening dan jernih<br />sadar sepenuhnya dan selalu tenang<br />di luar lingkaran pengaruhmu.<br />Jadi kamu bisa melakukan yang kamu inginkan<br />kamu bisa merenggut tubuh ini<br />kamu bisa menaklukkannya dengan sepenuhnya<br />tetapi pikiran ini, wahai Rasa Sakit<br />kamu tidak akan pernah bisa menguasainya!"
            ],
            "source": "Visuddhacara, <em>Minum Teh Menjalankan Kehidupan - Menerapkan Perhatian Penuh dalam Kehidupan Sehari-hari</em>, hal. 64"
        },
        "13": {
            "content": [
                "Mengenakan sepatu ini<br />saya memperhatikan sepenuhnya<br />saya mengetahui sensasi sentuhan<br />antara kaki dan sepatu.",
                "Mengenakan sepatu ini<br />saya memperhatikan sepenuhnya<br />saya mengenakan sepatu perhatian-penuh.",
                "Dengan sepatu ini saya akan mengambil<br />langkah-langkah<br />perhatian-penuh<br />langkah-langkah<br />kedamaian<br />langkah-langkah<br />kasih sayang dan welas asih."
            ],
            "source": "Visuddhacara, <em>Minum Teh Menjalankan Kehidupan - Menerapkan Perhatian Penuh dalam Kehidupan Sehari-hari</em>, hal. 88"
        },
        "14": {
            "content": [
                "Penderitaan pada dasarnya adalah kekosongan, sementara karma negatif pada dasarnya kehilangan sifat sejati. Hanya apabila kesadaran tenang, dan ketika tidak ada lagi pemikiran yang mengkhayal, kita akan mencapai pencerahan sempurna. Ketika kita bisa mempertahankan suatu pikiran sejati yang tidak kacau, sifat Kebuddhaan akan bersinar dengan segera."
            ],
            "source": "Master Hsing Yun, <em>Kisah Chan dari Hsing Yun</em>, hal. 27"
        },
        "15": {
            "content": [
                "Apakah benda yang paling berkekuatan di dunia? Toleransi. Buddha pernah mengatakan, “Seseorang praktisi agama Buddha yang tidak dapat menoleransi hinaan, caci maki, cemooh, serta tidak menganggapnya sebagai embun yang manis, bukanlah orang yang kuat.<br />Kekerasan dapat membangkitkan rasa takut dari orang-orang, tetapi tidak dapat mendatangkan rasa hormat. Hanya sikap toleransi yang dapat menyentuh hati orang yang keras kepala."
            ],
            "source": "Master Hsing Yun, <em>Kisah Chan dari Hsing Yun</em>, hal. 62"
        },
        "16": {
            "content": [
                "Dharma ada di mana?<br />Dharma ada di dalam Buddha.<br />Di sini, di saat ini juga!",
                "Sanggha ada di mana?<br />Sanggha ada di dalam Dharma.",
                "Buddha, Dharma, dan Sanggha, ada di dalam pikiran kita, asal kita melihatnya dengan jernih."
            ],
            "source": "Ajahn Chah, <em>Bodhinyana - Ajaran-ajaran Yang Ariya Ajahn Chah</em>, hal. 32"
        },
        "17": {
            "content": [
                "Baik sebatang pohon, sebuah gunung, atau seekor hewan, mereka semua adalah Dharma, semuanya adalah Dharma. Dharma ada di mana? Sederhananya, yang bukan Dharma tidak ada. Dharma adalah Alam. Ini disebut “<em>Sacca-Dhamma</em>”, Dharma Sejati. Jika seseorang memahami Dharma, ia memahami Alam. Dengan berpaling kepada Alam, ia mengetahui Dharma. "
            ],
            "source": "Ajahn Chah, <em>Bodhinyana - Ajaran-ajaran Yang Ariya Ajahn Chah</em>, hal. 33"
        },
        "18": {
            "content": [
                "Pencerahan sejati adalah melihat ke dalam sifat diri anda sendiri dan mencapai ketenangan pikiran. Sifat ketenangan pikiran adalah kosong. Kekosongan berarti bahwa pikiran tidak ada, maka jika seseorang menganggap ia telah memperoleh pencerahan, tetapi ia merasa bergembira, atau ia yakin ia telah menyelesaikan sesuatu yang besar, maka ia belum melihat sifat dirinya."
            ],
            "source": "Chan Master Sheng-Yen, <em>Belajar dari Kawanan Sapi</em>, hal. 7"
        },
        "19": {
            "content": [
                "Sebelum pencerahan dan setelah pencerahan adalah berbeda. Sebelum pencerahan anda hanya mengetahui tentang diri-sendiri. Setelah pencerahan, anda adalah sifat-diri. Ketika anda tiba di gunung, anda menyatu dengan gunung. Masih adakah gunung untuk kembali? Tidak. Pada keadaan ini anda tidak mengetahui di mana gunung tersebut. Mengapa? Karena anda adalah gunung."
            ],
            "source": "Chan Master Sheng-Yen, <em>Belajar dari Kawanan Sapi</em>, hal. 26"
        },
        "20": {
            "content": [
                "Konsep Buddhayana hanya bertujuan untuk memperlihatkan pentingnya menghindari pengelompokan, sektarianisme, dan bentuk-bentuk perpecahan yang serupa itu. Kekuatan Buddhayana terletak dalam semangat persatuan tersebut. Dalam sejarah orang Barat terdapat istilah “menyebar dan kuasailah”. Hal itu merupakan ketamakan, kebencian, dan kebodohan yang menguasai kita, karena ketiga hal itulah pandangan dan batin kita terganggu; dan memecah belah kita dalam keengganan, iri hati, kebencian, cemburu, dan banyak pula kondisi mental yang tidak baik lainnya, sehingga kita kehilangan pandangan benar dan tidak dapat melihat kesunyataan lagi."
            ],
            "source": "Suryananda (ed.), <em>“Memahami Buddhayana”</em>, hal. 15"
        },
        "21": {
            "content": [
                "Bodhisattwa merupakan perwujudan gaya aktif Kebuddhaan dan pada tingkat tertentu mungkin berwujud seorang suci, tidak tergantung pada suku-bangsa, agama, atau tanah-airnya, dan bahkan tidak tergantung status yang dimilikinya."
            ],
            "source": "Suryananda (ed.), <em>“Memahami Buddhayana”</em>, hal. 44"
        },
        "22": {
            "content": [
                "Di dalam Borobudur, jalan ke pencerahan sempurna diperlihatkan melalui banyak cara. Strukturnya, relief-reliefnya dan semua simbolisme lainnya dimaksudkan untuk menjadi sumber pengetahuan, inspirasi, dan teladan. Mereka juga menjadi perlengkapan ritual dan proses bagi para <em>Bodhisattwa</em> untuk berjuang mendapatkan kebijakan dan cinta kasih yang semakin luhur dan pada akhirnya pencerahan sempurna."
            ],
            "source": "Hudaya Kandahjaya, <em>The Master Key for Reading Borobudur Symbolism, Kunci Induk untuk Membaca Simbolisme Borobudur</em>, hal. 99"
        },
        "23": {
            "content": [
                "Dalam agama Buddha, ada ungkapan yang menyatakan bahwa realitas, dharmadhatu, Kebuddhaan, atau pencerahan memiliki banyak pintu dan seseorang bebas memilih pintu yang membuatnya dapat masuk ke dalamnya. Secara metaforis, Borobudur mencerminkan ungkapan ini."
            ],
            "source": "Hudaya Kandahjaya, <em>The Master Key for Reading Borobudur Symbolism, Kunci Induk untuk Membaca Simbolisme Borobudur</em>, hal. 105"
        },
        "24": {
            "content": [
                "Seorang petapa pengelana, setelah mendengar mengenai Buddha, berkelana ke segala penjuru untuk mencari Beliau. Suatu malam, ia menginap di sebuah rumah yang juga diinapi Buddha, tetapi dengan tanpa mengetahui penampilan fisik Buddha, ia tidak memperhatikan kehadiran Beliau. Pagi berikutnya ia bangun dan melanjutkan perjalanannya, masih untuk mencari Buddha. Untuk mencari kedamaian dan pencerahan tanpa pemahaman yang benar adalah seperti ini.",
                "Oleh karena kurangnya pemahaman akan kebenaran penderitaan dan penghapusannya, semua faktor yang berhubungan dengan jalan akan menjadi salah—tekad salah, ucapan salah tindakan salah, latihan salah akan konsentrasi dan ketenangan. Kesukaan dan kebencian anda bukanlah pembimbing yang bisa diandalkan juga dalam hal ini, walaupun orang bodoh mungkin menganggapnya sebagai bahan pertimbangan yang terbaik. Yah, ini bagaikan berkelana ke kota tertentu—anda secara tidak tahu telah memulai dengan jalan yang salah, dan karena itu adalah jalan yang menyenangkan anda melaluinya dengan penuh kesenangan. Akan tetapi jalan itu tidak akan mengantarkan anda pada tempat tujuan yang anda inginkan."
            ],
            "source": "Ajahn Chah, <em>Telaga Jernih di Hutan - Meditasi Pemahaman dari Ajahn Chah (1)</em>, hal. 35"
        },
        "25": {
            "content": [
                "Usaha yang benar bukanlah melakukan sesuatu agar sesuatu benar-benar terjadi dengan cara tertentu. Yang diinginkan ialah usaha untuk waspada dan bangkit pada setiap momen, usaha untuk menanggulangi kemalasan dan kotoran batin, usaha untuk membuat setiap kegiatan dalam kehidupan sehari-hari kita sebagai meditasi."
            ],
            "source": "Ajahn Chah, <em>Telaga Jernih di Hutan - Meditasi Pemahaman dari Ajahn Chah (1)</em>, hal. 69"
        },
        "26": {
            "content": [
                "T: Apakah dianjurkan untuk banyak membaca atau belajar kitab suci sebagai bagian dari latihan?",
                "J: Dharma dari Buddha tidak ditemukan di dalam buku-buku. Jika anda benar-benar ingin memahami apa yang dibicarakan Buddha, anda tidak perlu bergelut dengan buku. Awasilah pikiran anda. Periksalah untuk melihat bagaimana perasaan dan pemikiran muncul dan berlalu. Jangan terikat pada apa pun, cukuplah memberikan perhatian-penuh pada apa pun yang bisa diamati. Inilah cara untuk mencapai kebenaran Buddha. Bersikap wajarlah. Segala sesuatu yang anda lakukan dalam kehidupan ini ialah kesempatan untuk berlatih. Itu semua adalah Dharma. Ketika anda mengerjakan pekerjaan rumah tangga anda, kerjakanlah dengan perhatian-penuh. Jika anda sedang mengosongkan tong sampah atau membersihkan toliet, janganlah merasa bahwa anda melakukannya untuk orang lain. Ada Dharma dalam mengosongkan tong sampah. Jangan mengira bahwa anda berlatih hanya ketika sedang duduk tenang, dengan kaki bersilang. Beberapa di antara anda telah mengeluh bahwa tidak mempunyai cukup waktu untuk berlatih. Apakah terdapat cukup waktu untuk bernapas? Inilah meditasi anda: perhatian-penuh, kealamian, dalam segala sesuatu yang anda kerjakan."
            ],
            "source": "Ajahn Chah, <em>Telaga Jernih di Hutan - Meditasi Pemahaman dari Ajahn Chah (2)</em>, hal. 116-167"
        },
        "27": {
            "content": [
                "Beberapa murid menanyai Ajahn Chah, mengapa ia jarang sekali membicarakan tentang Nirwana tetapi mengajarkan tentang kebijaksanan dalam kehidupan sehari-hari. Guru-guru yang lain sering sekali membicarakan tentang pencapaian Nirwana, tentang berkah khususnya dan pentingnya di dalam latihan mereka.",
                "Ajahn Chah menjawab bahwa beberapa orang menikmati hidangan yang lezat dan kemudian terus menerus memuji kelezatan itu kepada setiap orang yang mereka temui. Yang lain akan makan dan menikmati hidangan yang sama, tetapi, setelah itu, tidak merasa perlu untuk pergi memberitahukan orang lain tentang makanan yang telah disantapnya itu."
            ],
            "source": "Ajahn Chah, <em>Telaga Jernih di Hutan - Meditasi Pemahaman dari Ajahn Chah (2)</em>, hal. 90"
        },
        "28": {
            "content": [
                "Cara yang terbaik untuk menolong orangtua kita untuk mendapatkan kelahiran yang baik adalah dengan mendorong mereka untuk berbuat hal-hal yang positif dan menghentikan perbuatan yang merugikan pada saat mereka masih hidup."
            ],
            "source": "Thubten Chodron, <em>Tradisi dan Harmoni - Menelusuri Jejak-jejak Agama Buddha</em>, hal. 55"
        },
        "29": {
            "content": [
                "Bagaimana sikap sektarian membahayakan kemajuan batin kita? Dalam agama Buddha, dua kualitas utama yang ingin kita kembangkan adalah kebijaksanaan dan welas asih. Apabila kita terikat dengan agama kita, dan ada orang yang tidak sependapat dengan yang kita yakini, kita akan menganggapnya penghinaan pribadi. Kita menjadi marah dan agresif, mempertahankan keyakinan kita dan menyerang yang lain. Pada saat itu, kita berhenti mencari kebenaran dan mulai mempertahankan agama kita atas dasar yang sederhana karena itu adalah agamaku."
            ],
            "source": "Thubten Chodron, <em>Tradisi dan Harmoni - Menelusuri Jejak-jejak Agama Buddha</em>, hal. 65"
        },
        "30": {
            "content": [
                "Tidak ada yang lebih terhormat dari pencerahan, tidak ada yang lebih indah dari kebajikan. Mereka yang mempunyai kebajikan yang cerah tetap mempunyai kebajikan itu meskipun mereka itu orang biasa, sedangkan mereka yang tidak mempunyai kebajikan yang cerah, tetap tidak memilikinya meskipun mereka adalah raja.",
                "Ada orang yang mati kelaparan di zaman dahulu kala tapi dikenang dan dihormati karena kebajikannya; ada yang menjadi raja, namun dicaci sampai sekarang karena tidak memiliki kebajikan.",
                "Sehingga, mereka yang belajar, merasa cemas jika tidak memiliki kebajikan, mereka tidak merasa cemas jika tidak memperoleh jabatan atau pun kekuasaan."
            ],
            "source": "Thomas Clearly, <em>Thomas Clearly, Dua Angin - Seni Kepemimpinan Zen (1)</em>, hal. 1"
        },
        "31": {
            "content": [
                "Jalan itu seperti gunung; semakin jauh engkau mendaki, semakin tinggi gunung itu. Jalan itu seperti bumi; semakin jauh engkau melangkah, semakin luas ia menghampar. Murid-murid yang dangkal menghabiskan seluruh kekuatan mereka, kemudian berhenti. Hanya mereka yang memiliki tekad untuk meraih pencerahan yang dapat mencapai kedalaman dan ketinggiannya. Sementara untuk yang lain, siapa yang mau berurusan dengan mereka?"
            ],
            "source": "Thomas Clearly, <em>Thomas Clearly, Dua Angin - Seni Kepemimpinan Zen (1)</em>, hal. 65"
        }
    },
    "04": {
        "01": {
            "content": [
                "Ia yang mampu melihat ujung sehelai rambut yang jatuh tidak dapat melihat alis matanya sendiri, ia yang mampu mengangkat tiga puluh ribu pon tidak mampu mengangkat tubuhnya sendiri. Ini seperti murid yang cerdas yang mencela orang lain namun tidak mengenal dirinya sendiri."
            ],
            "source": "Thomas Clearly, <em>Dua Angin - Seni Kepemimpinan Zen (2)</em>, hal. 35"
        },
        "02": {
            "content": [
                "Ketika energi murid lebih besar dari tekadnya, ia menjadi orang yang kecil dan rendah. Jika tekadnya mengendalikan energinya, ia menjadi orang yang benar dan sejati. Jika energi dan tekadnya seimbang, mereka menjadi petapa yang cerah.",
                "Ada orang yang keras kepala dan kasar, dan tidak akan menerima nasihat apa pun—energi merekalah yang membuat mereka seperti itu. Orang yang lurus dan benar, tak akan goyah dan mantap hingga mati—tekadlah yang membuat mereka seperti itu."
            ],
            "source": "Thomas Clearly, <em>Dua Angin - Seni Kepemimpinan Zen (2)</em>, hal. 63"
        },
        "03": {
            "content": [
                "Dalam kepemimpinan terdapat tiga jangan: jika banyak yang harus dikerjakan, jangan takut; jika tidak ada yang dikerjakan, jangan terburu-buru; dan jangan membicarakan pendapat tentang benar atau salah.<br />Pemimpin yang berhasil di dalam tiga jangan ini tidak akan dibingungkan ataupun dikotori oleh objek-objek luar."
            ],
            "source": "Thomas Clearly, <em>Dua Angin - Seni Kepemimpinan Zen (3)</em>, hal. 76"
        },
        "04": {
            "content": [
                "Untuk melatih dirimu dalam berurusan dengan perkumpulan, adalah perlu menggunakan kebijaksanaan. Untuk menyingkirkan kotoran batin dan membuang emosi yang berlebihan, engkau pertama-tama harus sadar. Jika engkau berpaling dari kesadaran dan bercampur dengan debu, maka pikiranmu akan terselubungi. Ketika kebijaksanaan dan kedunguan tidak dikenali, masalah menjadi kusut."
            ],
            "source": "Thomas Clearly, <em>Dua Angin - Seni Kepemimpinan Zen (3)</em>, hal. 61"
        },
        "05": {
            "content": [
                "Seseorang seharusnya bertanya kepada dirinya sendiri dan memastikan dua hal: Saya gembira saya dilahirkan; dan saya dilahirkan untuk tujuan tertentu. Sekarang jika seseorang menyimpulkan bahwa ia gembira dilahirkan untuk menjalankan tugas tertinggi yang mungkin bagi seorang manusia, maka posisinya menjadi agak paradoks, jika tujuan nyata dari kehidupannya ialah pembebasan dari kelahiran kembali, maka ia dilahirkan untuk tidak mengalami kelahiran kembali lagi, dan seharusnya tidak dilahirkan sejak awal! Mengapa ia harus gembira ia dilahirkan dan dengan demikian diberikan kesempatan untuk menapaki jalan menuju Nirwana? Jika pembebasan dari kelahiran adalah hal yang sedemikian baiknya, mengapa harus ada kelahiran dari awal?"
            ],
            "source": "Buddhadasa Bhikkhu, <em>Mengapa Kita Dilahirkan?</em>, hal. 18"
        },
        "06": {
            "content": [
                "Maksud sebenarnya dari “neraka” ialah kecemasan (secara harafiah, artinya “bara batiniah”). Kecemasan membakar seseorang bagaikan api. Jika seseorang berada dalam kegelisahan, terbakar oleh kecemasan, ia disebut sebagai makhluk neraka. Baik seorang biksu, pemula, umat awam, umat perumahtangga, atau apa pun, jika ia terbakar nyala api kecemasan (“bara batiniah”), terbakar melalui keterlibatan diri dalam “aku”, “milikku”, inilah neraka. "
            ],
            "source": "Buddhadasa Bhikkhu, <em>Mengapa Kita Dilahirkan?</em>, hal. 65"
        },
        "07": {
            "content": [
                "Introspeksilah diri anda dan lihatlah apakah anda mengetahui “kenyataan sebagaimana adanya” itu. Bahkan walaupun anda mengetahui siapakah diri anda, apakah kehidupan itu, apa sebenarnya tugas, mata pencaharian, uang, harta kekayaan, kehormatan, dan ketenaran itu, beranikah anda menyatakan bahwa anda mengetahui segala-galanya? Jika kita benar-benar mengetahui “kenyataan sebagaimana adanya”, kita tidak akan pernah bertindak salah; dan jika kita senantiasa bertindak tepat, pastilah kita tidak akan mengalami penderitaan. Sebagaimana kenyataannya, kita masih buta akan sifat sejati segala sesuatu, sehingga tingkah laku kita kurang lebih masih tidak tepat, dan penderitaan pun muncul sebagai akibatnya. Latihan agama Buddha dirancang untuk mengajarkan kepada kita bagaimana sebenarnya kondisi segala sesuatu. Mengetahui ini dengan sejelas-jelasnya maka ia telah mencapai Buah Hasil dari Jalan, mungkin juga Buah Hasil akhir, Nirwana, karena pengetahuan yang satu inilah yang menghancurkan kotoran batin. "
            ],
            "source": "Buddhadasa Bhikkhu, <em>Buku Petunjuk Bagi Umat Manusia</em>, hal. 20-21"
        },
        "08": {
            "content": [
                "Setiap orang bisa mengamati bahwa di mana ada nafsu keinginan, akan ada ketegangan pula; dan bila kita dipaksa untuk bertindak sesuai dengan nafsu keinginan, kita akan menderita lagi sesuai dengan tindakan kita. Setelah mendapatkan hasilnya, kita tidak mampu untuk mengakhiri nafsu keinginan kita, sehingga kita meneruskan untuk melahirkan nafsu keinginan. Alasan bagi kita untuk harus terus-menerus mengalami ketegangan ialah bahwa kita masih belum bebas tuntas dari nafsu keinginan, melainkan masih merupakan budaknya. Jadi, bisa dikatakan bahwa seorang jahat melakukan kejahatan karena ia menginginkan untuk melakukan kejahatan, dan mengalami jenis penderitaan yang sesuai dengan sifat seseorang yang jahat; dan seseorang yang baik berkeinginan untuk melakukan kebaikan, dan tidak luput pula dari jenis penderitaan lain, jenis penderitaan yang sesuai bagi orang baik. Akan tetapi, jangan salah sangka bahwa ini adalah himbauan kepada kita untuk meninggalkan perbuatan baik. Ini hanyalah merupakan ajaran kepada kita untuk menyadari bahwa ada tingkat penderitaan yang sedemikian halusnya sehingga umat awam tidak bisa mendeteksinya. Kita haruslah bertindak sesuai dengan nasihat Buddha, “Jika kita mau membebaskan diri dari penderitaan secara sepenuhnya, melakukan kebajikan saja belumlah cukup. Masih perlu dilakukan hal-hal yang berada di atas dan melampaui perbuatan kebajikan, hal-hal yang akan membebaskan pikiran dari kondisi perbudakan dan penjajahan oleh jenis nafsu keinginan yang mana pun. “Ini adalah intisari utama dari ajaran Buddha. Ini tidak bisa diimbangi atau dilampaui oleh ajaran agama apa pun di dunia, sehingga haruslah diingat dengan baik-baik. Keberhasilan menaklukkan ketiga jenis nafsu keinginan ini berarti mencapai pembebasan sepenuhnya dari penderitaan.”"
            ],
            "source": "Buddhadasa Bhikkhu, <em>Buku Petunjuk Bagi Umat Manusia</em>, hal. 42-43"
        },
        "09": {
            "content": [
                "Gelombang lautan tidak pernah berhenti naik dan turun, dan dengan cara yang sama, pikiran salah kita tidak pernah berhenti. Ia berlanjut terus, satu pikiran salah setelah pikiran salah yang lain, berputar dan berputar, terus dan terus, dalam arus yang tidak pernah berhenti. Dan tidak satu pun di antara mereka yang mau tertinggal di belakang; mereka semua berpacu ke depan. Mengapa orang menjadi bingung oleh pikiran salah seperti itu? Karena ia tidak memiliki kebijaksanaan. Jika ia bijaksana, tidak akan ada gelombang di dalam air.<br />Angin boleh bertiup, tapi sia-sia ia berhembus.<br />Karena tiada riak di dalam air."
            ],
            "source": "Tripitaka Master Hsuan Hua, <em>Maha Karuna Dharani - Dharani Hati mengenai Welas Asih Agung, Tak Terbatas, Sempurna, Utuh, Luhur dari Bodhisattwa Seribu Tangan Seribu Mata Yang Mendengarkan Suara Dunia</em>, hal. 68"
        },
        "10": {
            "content": [
                "Satu objek lewat, ia memantulkannya.<br />Objek pergi, cermin jernih.",
                "Cermin tidak melekat. Orang yang memiliki kebijaksanaan akan memperhatikan segala sesuatu, kemudian membiarkannya berlalu; hatinya tidak melekat pada objek-objek itu. Meskipun ia tidak menyimpannya, objek-objek itu selalu terwujud. Dan meskipun objek-objek itu terus-menerus terwujud, ia tidak menyimpannya."
            ],
            "source": "Tripitaka Master Hsuan Hua, <em>Maha Karuna Dharani - Dharani Hati mengenai Welas Asih Agung, Tak Terbatas, Sempurna, Utuh, Luhur dari Bodhisattwa Seribu Tangan Seribu Mata Yang Mendengarkan Suara Dunia</em>, hal. 71"
        },
        "11": {
            "content": [
                "Ketika Biksuni Chiyono mempelajari Zen di bawah bimbingan Bukko dari Engaku, selama jangka waktu yang panjang ia belum bisa mencapai buah hasil meditasi.<br />Pada suatu malam bulan purnama, ia membawa sebuah ember tua yang dikelilingi oleh bambu. Bambu itu patah dan alas ember itu terlepas, dan pada saat itulah Chiyono mencapai pencerahan!<br />Untuk mengingatnya, dia menulis puisi ini:",
                "Dengan segala cara saya berusaha untuk merawat ember itu<br />Sejak untaian bambu itu aus dan akan patah<br />Hingga akhirnya alasnya terlepas dari ember itu.<br />Tiada lagi air di dalam ember!<br />Tiada lagi bulan di air!"
            ],
            "source": "Paul Reps, <em>Daging Zen Tulang Zen - Bunga Rampai Karya Tulis Zen dan Pra-Zen (1)</em>, hal. 57"
        },
        "12": {
            "content": [
                "Seorang murid Tendai, sebuah aliran filsafat dalam agama Buddha, datang ke tempat tinggal Gasan yang menjadi penganut dan bertindak sebagai seorang murid. Ketika ia pergi beberapa bulan kemudian, Gasan memperingatinya, “Mempelajari kebenaran secara spekulasi adalah cara yang bermanfaat untuk mengumpulkan bahan ceramah. Tetapi ingatlah bahwa jika anda tidak bermeditasi secara konstan, cahaya kebenaran anda mungkin akan mengalir ke luar.”"
            ],
            "source": "Paul Reps, <em>Daging Zen Tulang Zen - Bunga Rampai Karya Tulis Zen dan Pra-Zen (1)</em>, hal. 90"
        },
        "13": {
            "content": [
                "Daibai bertanya kepada Baso, “Apakah Buddha itu?”<br />Baso berkata, “Pikiran inilah Buddha.”"
            ],
            "source": "Paul Reps, <em>“Daging Zen Tulang Zen 2”</em>, hal. 63"
        },
        "14": {
            "content": [
                "Dua orang biksu sedang berargumentasi tentang bendera. Seorang berkata, “Bendera itu bergerak.”<br />Yang lain berkata, “Anginlah yang bergerak.”<br />Sesepuh keenam kebetulan sedang lewat. Ia memberitahukan mereka, “Bukan angin, bukan bendera; pikiranlah yang bergerak.”"
            ],
            "source": "Paul Reps, <em>Daging Zen Tulang Zen - Bunga Rampai Karya Tulis Zen dan Pra-Zen (2)</em>, hal. 61"
        },
        "15": {
            "content": [
                "Kita harus berhati-hati sehingga tidak mencampuradukkan <em>pengetahuan</em> tentang kejahatan dan <em>melakukan</em> perbuatan jahat. Melalui pengetahuan yang sempurna, Buddha dapat mengetahui siapa yang salah dan siapa yang tidak salah. Tapi karena Beliau telah menghilangkan sama sekali gagasan untuk melakukan perbuatan jahat, maka keinginan untuk melakukan perbuatan jahat pun turut lenyap. Demikianlah, kita harus memperkuat keyakinan terhadap Buddha, merenungkan sifat-sifat-Nya, seperti kemampuan untuk mengetahui perbuatan baik dan buruk yang tidak dibarengi hadirnya keinginan untuk melakukan perbuatan jahat."
            ],
            "source": "Sayagyi U Ba Khin, <em>Empat Kesempurnaan</em>, hal. 26"
        },
        "16": {
            "content": [
                "Kesabaran itu sedalam lautan; pantai lautan kebencian;<br />palang pintu alam penderitaan;<br />tangga menuju alam dewa dan brahma;<br />landasan bagi segala sifat mulia;<br />penyucian tertinggi bagi tubuh, ucapan, dan pikiran."
            ],
            "source": "Sayagyi U Ba Khin, <em>Empat Kesempurnaan</em>, hal. 60"
        },
        "17": {
            "content": [
                "Awalokiteswara pada kenyataannya menjadi satu-satunya Bodhisattwa Mahayana yang diterima oleh umat Buddha Therawada.<br />Salah satu alasannya adalah bahwa Awalokiteswara merupakan penjelmaan dari karuna Buddha.<br />Kebijaksanaan (prajna) dan belas kasih (karuna) membentuk dua 'sayap' pencerahan Buddha: Penyadaran dan ajaran-Nya tentang Dharma adalah aspek 'Kebijaksanaan' sementara hidup dan tindakan-Nya adalah aspek 'Karuna'.<br />Aspek Kebijaksanaan dijelmakan menjadi Bodhisattwa, yaitu Prajnaparamita dan Manjusri.<br />Sedangkan aspek Karuna diwujudkan dalam bentuk Bodhisattwa Awalokiteswara yang menjadi jalan tertinggi dalam mencapai pembebasan spiritual."
            ],
            "source": "Piyasilo Mahathera, <em>Avalokitesvara - Asal, Perwujudan, dan Makna</em>, hal. 9"
        },
        "18": {
            "content": [
                "Awalokiteswara memberikan jalur 'komunikasi' khusus dengan Buddha, terutama bila dibutuhkan keyakinan dalam kehidupan sehari-hari.<br />Orang-orang yang percaya hanya perlu memohon secara langsung kepada Awalokiteswara dalam salah satu bentuk dari bentuknya yang tak terhingga banyaknya.<br />Orang-orang dapat mengutarakan kesulitannya, Awalokiteswara, yang sering diartikan sebagai 'Dia yang menjawab jerit tangis dunia', akan selalu siap mendengar dengan penuh perhatian.<br />Orang-orang percaya bahwa Awalokiteswara yang merupakan perwujudan dari 'karuna' akan dipenuhi oleh hati Maha Penyayang dan kenudian berubah menjadi Maha Pengasih di mana cinta kasih adalah tindakannya."
            ],
            "source": "Piyasilo Mahathera, <em>Avalokitesvara - Asal, Perwujudan, dan Makna</em>, hal. 13-14"
        },
        "19": {
            "content": [
                "Sebagai seorang manusia, terdapat banyak sekali nafsu keinginan dan keterikatan di dalam kehidupan kita. Ini bisa mengakibatkan penderitaan, baik terhadap diri sendiri maupun orang lain.<br />Jika nafsu keinginan tersebut tidak dipenuhi, kita menjadi tidak senang. Tetapi walaupun jika kita mendapatkan apa yang kita inginkan, kebahagiaan yang muncul hanyalah bersifat sementara, karena suatu nafsu keinginan yang baru akan muncul.<br />Dari waktu ke waktu, yang kita kerjakan adalah mencoba memuaskan nafsu keinginan kita yang tiada batas.<br />Ternyata tidaklah mungkin kita dapat memuaskan nafsu keinginan yang tidak ada habisnya itu.<br />Kesalahannya adalah karena kita berharap untuk menemukan kebahagiaan di luar diri kita, tidak menyadari bahwa itu berasal dari dalam diri kita sendiri.<br />Jadi yang perlu dirubah adalah cara kita memandang dunia ini. Kita harus belajar menerima nafsu keinginan kita dan tidak terseret olehnya. Hanya dengan menjinakkan pikiran, pencarian tanpa akhir akan pemuasan diri bisa ditaklukkan."
            ],
            "source": "Akong Tulku Rinpoche, <em>Menjinakkan Harimau - Ajaran Tibet untuk Meningkatkan Mutu Kehidupan Sehari-hari</em>, hal. 5, 7"
        },
        "20": {
            "content": [
                "Kalau anda mengikuti pikiran berkelana, berarti anda membatasi kesadaran; pikiran anda dibatasi pada rangkaian pikiran itu. Bila anda tidak mengikuti pikiran berkelana, pikiran anda bebas dan terbuka. Katakan pada pikiran bahwa ia bisa pergi ke mana yang ia suka, tetapi anda tak akan turut serta. Di saat ini tubuh akan relaks dan pikiran akan bebas karena anda tidak membatasinya dalam cara apa pun. Inilah waktu yang paling menyenangkan. Anda tidak perlu berbuat apa pun. Baik pikiran dan tubuh dalam kondisi istirahat."
            ],
            "source": "Chan Master Sheng Yen, <em>Kebijakan Zen - Pengetahuan dan Tindakan</em>, hal. 4"
        },
        "21": {
            "content": [
                "Pencerahan terdalam adalah ketika anda tidak hanya melihat kekosongan, tetapi lebih berada di tengah kekosongan itu. Tingkat pengalaman pencerahan dapat dibandingkan dengan pengalaman seseorang akan anggur yang meningkat secara bertahap. Tingkat pertama adalah orang yang belum pernah melihat atau mencicipi anggur. Pada tingkat selanjutnya orang itu telah melihat dan mengetahui seperti apa anggur itu, tetapi belum mencobanya. Kemudian, orang itu mencicipi dan sekarang mengetahui rasanya. Lalu, kalau masih tertarik, orang itu mungkin menginginkan cicipan yang lain, atau anggur segelas penuh. Terakhir adalah tingkatan di mana orang itu lompat ke dalam tong anggur. Pada titik ini sudah tiada lagi pemisahan dari anggur. Berbicara tentang rasa haus sudah tidak relevan lagi."
            ],
            "source": "Chan Master Sheng Yen, <em>Kebijakan Zen - Pengetahuan dan Tindakan</em>, hal. 45"
        },
        "22": {
            "content": [
                "Janganlah kecewa akan diri anda.<br />Jika seseorang mencapai keberhasilan atau mewarisi harta jutaan dolar, janganlah menghabiskan waktu anda dengan bermurung diri memikirkan mengapa bukan anda yang mendapatkan keberuntungan tersebut.<br />Janganlah iri hati atas keberhasilan rekan anda dengan melakukan hal-hal yang memboroskan waktu anda.<br />Misalnya dengan melihat sisi jelek dari pencapaiannya dan bahkan mengutarakannya kepada rekan anda. Ini akan merugikan bagi anda sendiri."
            ],
            "source": "Chogyam Trungpa, <em>Melatih Pikiran dan Mengembangkan Kasih Sayang</em>, hal. 216"
        },
        "23": {
            "content": [
                "Jangan pindahkan beban sapi jantan kepada sapi betina.",
                "Mudah sekali untuk berkata, “Itu bukan kesalahanku, semua itu kesalahanmu, selalu saja kesalahan kamu.”<br />Seseorang harus memikirkan permasalahannya secara pribadi, jujur, dan sejati.<br />Sapi jantan mempunyai kemampuan lebih untuk membawa beban, jadi makna semboyan ini adalah agar anda tidak memindahkan beban berat anda kepada seseorang yang kemampuannya lebih rendah daripada anda.<br />Memindahkan beban dari sapi jantan kepada sapi betina berarti anda tidak mau menyelesaikan masalah anda sendiri, dan anda tidak mau memikul tanggung jawab.<br />Melakukan hal ini adalah suatu perbuatan jahat.<br />Kita boleh meminta orang lain untuk membantu kita, tetapi kita tidak boleh melemparkan tanggung jawab kita kepada mereka. Janganlah memindahkan beban sapi jantan kepada sapi betina."
            ],
            "source": "Chogyam Trungpa, <em>Melatih Pikiran dan Mengembangkan Kasih Sayang</em>, hal. 192"
        },
        "24": {
            "content": [
                "Peduli pekerjaan berarti memberikan kekuatan penuh dari pikiran dan perasaan. Sikap peduli ini akan tumbuh menjadi daya dorong yang kuat, memungkinkan kita untuk menjalankan tugas yang berat dan kompleks.<br />Peduli terhadap pekerjaan, menyukainya, bahkan mencintainya, tampaknya sangat berarti daripada kita memandang pekerjaan hanya sebagai suatu cara untuk mencari nafkah.<br />Jika kita memandang pekerjaan sebagai suatu cara untuk memperdalam dan memperkaya pengalaman hidup, maka kita akan menemukan sikap peduli tersebut dan membangkitkannya di dalam hati orang-orang yang ada di sekitar kita, memanfaatkan setiap aspek pekerjaan untuk belajar dan tumbuh."
            ],
            "source": "Tarthang Tulku, <em>Sukses dalam Bekerja – Bagaimana Menggunakan Keterampilan Bekerja</em>, hal. 12"
        },
        "25": {
            "content": [
                "Ketika kita menemukan masalah secara langsung dan menghadapinya, kita menemukan cara-cara baru untuk menjalani kehidupan. Kita membangun kekuatan dan kepercayaan diri untuk menggeluti kesukaran pada masa mendatang.<br />Hidup menjadi suatu tantangan yang bermakna yang menuntun kita pada pengetahuan yang lebih luas dan membangun mental yang kuat.<br />Semakin banyak yang kita pelajari, semakin dewasa kita tumbuh, dan semakin banyak tantangan yang kita temui, maka semakin banyak kekuatan dan kewaspadaan yang kita raih.<br />Sering kali kita tidak berusaha bahkan menentang untuk berubah.<br />Mencoba mencegah perubahan ibarat berenang melawan arus sungai yang mengalir, sangat melelahkan dan bisa menimbulkan frustasi.<br />Ketika kita hidup dalam keharmonisan dengan proses perubahan, kita melakukan sesuatu yang berharga dalam menjalani kehidupan."
            ],
            "source": "Tarthang Tulku, <em>Sukses dalam Bekerja – Bagaimana Menggunakan Keterampilan Bekerja</em>, hal. 54"
        },
        "26": {
            "content": [
                "Suatu ketika Buddha berbicara kepada beberapa biksu waktu mereka duduk di hutan kecil yang ditumbuhi pohon-pohon simsapa.<br />Buddha mengumpulkan beberapa lembar daun simsapa di tangannya, lalu berkata kepada para biksu, “Mana yang lebih banyak, daun-daun yang ada di tanganku atau daun-daun yang ada di pohon?”<br />“Lebih banyak yang ada di pohon,” jawab para biksu.<br />“Seperti itulah perbandingan antara kebenaran yang telah Aku ajarkan kepadamu dengan kebenaran yang tidak Aku ajarkan,” kata Buddha, “Semua yang tidak Aku nyatakan itu tidak berguna bagi kehidupan suci, dan tidak akan membantu bagi kemajuan spiritual kalian. Apa yang telah Aku ajarkan adalah hakikat dari ketidakbahagiaan, dan bagaimana untuk mengatasinya. Hal-hal tersebut sudah cukup untuk membimbing dalam pencapaian kesucian dan Nirwana.”"
            ],
            "source": "Dr. H. Saddhatissa, <em>Kisah Hidup Buddha</em>, hal. 136"
        },
        "27": {
            "content": [
                "Buddha menjelaskan sebuah parabel kepada Malunkyaputta yang mendesak Beliau untuk menjelaskan tentang rahasia alam semesta,<br />“Bayangkanlah seseorang yang terkena anak panah beracun. Teman-temannya akan segera mengobatinya, tetapi sebelum mereka melakukan pengobatan, orang itu berkata, 'Aku tidak mau anak panah ini diambil sebelum aku tahu siapa orang yang memanahku, kasta apa, namanya siapa, apakah dia tinggi atau pendek, dari desa atau kota mana ia datang. Dan aku harus tahu busur jenis apa serta jenis anak panah yang dipakai.' Malunkyaputta, orang itu akan mati sebelum ia mengetahui hal-hal tersebut. Demikian pula dalam kasus seseorang yang tidak akan mengabdikan dirinya pada kehidupan suci sebelum tahu jawaban atas pertanyaan tentang alam semesta.”<br />Buddha melanjutkan, “Kehidupan suci tidak bergantung pada persoalan-persoalan semacam itu. Fakta-fakta esensial dari kehidupan jauh lebih penting diketahui, yaitu kelahiran, usia tua, kesakitan, kematian, kesedihan, penderitaan, dan penghentian dari semua itu, serta jalan yang mengarah kepada penghentian itu. Inilah dasar bagi kehidupan spiritual.”"
            ],
            "source": "Dr. H. Saddhatissa, <em>Kisah Hidup Buddha</em>, hal. 134-135"
        },
        "28": {
            "content": [
                "Hal positif mendasar dari ajaran Buddha adalah mencari kebebasan mutlak.<br />Banyak orang yang ragu-ragu dalam mencari kebebasan mutlak.<br />Sebagai gambaran, suatu ketika seorang pengemis ditanya apa yang akan paling dinikmatinya bila dia menjadi raja, dengan segera ia menjawab, “Sebagai seorang raja, saya ingin makan donat dan kue-kue sepuas hati saya.”<br />Umat Buddha bertujuan mendapatkan kebebasan mutlak yaitu mata, telinga, hidung, lidah, badan, dan pikiran bebas dari kotoran dan halangan, sehingga akhirnya mencapai Kebuddhaan.<br />Bila tujuan anda bukanlah pembebasan mutlak, karena anda berpikir bahwa yang dituntut oleh agama Buddha tersebut tidak masuk akal, maka garis pemikiran anda tersebut dapat dikatakan sama dengan kepuasan yang ingin dicapai oleh pengemis tersebut di atas.<br />Ajaran Buddha didasarkan kepada kebebasan mutlak dan kesamaan sejati, merupakan ajaran yang rasional, bebas, objektif, nyata, sempurna, positif, pragmatis, dan dapat diimplementasikan pada semua tingkat."
            ],
            "source": "Wang Chi Biu, <em>Pandangan Ilmiah dalam Agama Buddha</em>, hal. 100-105"
        },
        "29": {
            "content": [
                "Agama Buddha tidak berisi ketakhyulan, tetapi justru menghilangkan ketakhyulan karena ajarannya mutlak logis, tidak berat sebelah dan logis.<br />Selain itu ajaran Buddha selaras dengan ilmu pengetahuan.<br />Terlepas dari fungsinya yang luar biasa dan aplikasinya yang luas, secara keseluruhan, <em>'way of living'</em> secara praktis hanya dapat diwujudkan oleh pengalaman diri sendiri."
            ],
            "source": "Wang Chi Biu, <em>Pandangan Ilmiah dalam Agama Buddha</em>, hal. 139"
        },
        "30": {
            "content": [
                "Jika kita merenungkan berbagai macam nasib makhluk di dunia, tampak bahwa seakan-akan segala sesuatu di dunia ini tidaklah adil.<br />Mengapa ada orang yang kaya dan berkuasa sedangkan yang lain miskin dan tertindas?<br />Mengapa ada orang yang sepanjang hidupnya sehat, sementara yang lain sejak lahir telah sakit-sakitan?<br />Mengapa seseorang terberkahi paras yang menawan dan cerdas, sedang yang lainnya berparas buruk dan dungu?<br />Mengapa ada yang buta, idiot atau bisu-tuli, sementara yang lainnya tidak?<br />Mengapa seorang anak dilahirkan sebagai anak seorang penjahat, sementara ada yang dilahirkan dari orangtua yang mulia sehingga dapat mengenyam pendidikan moral yang baik?<br />Mengapa seseorang tanpa bersusah payah bisa sukses dalam usahanya, sedangkan yang lain selalu gagal mewujudkan rencananya?<br />Mengapa ada yang menikmati panjang usia, namun ada yang meninggal pada awal kehidupannya, bahkan sebelum sempat dilahirkan?<br />Begitu banyak dijumpai ketidakadilan dan diskriminasi di antara sesama manusia.<br />Apakah ketidakadilan ini terjadi secara kebetulan atau memang sudah direncanakan oleh “sesuatu”?<br />Kalau “sesuatu” itu murah hati mengapa merencanakan dan menciptakan keadaan-keadaan yang tidak mengenakkan bagi manusia?<br />Suatu sosok yang maha murah seharusnya sanggup berbuat sesuatu untuk mengatasi ketidakadilan ini.<br />Dalam ajaran Buddha, ketidakadilan ini dijelaskan sebagai sesuatu yang logis sesuai dengan Hukum Karma dan Tumimbal Lahir."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Tumimbal lahir - Percayakah Anda?</em>, hal. 20-21"
        }
    },
    "05": {
        "01": {
            "content": [
                "Buddha menganggap kesejahteraan ekonomi sebagai suatu syarat bagi kenyamanan manusia, tetapi pengembangan moral dan spiritual adalah syarat bagi kehidupan yang bahagia, damai, dan memuaskan.",
                "Dighajanu pernah bertanya kepada Buddha bagaimana caranya agar umat awam yang menjalani hidup berumah tangga dapat berbahagia di dunia dan selamanya. Buddha menjelaskan kepadanya bahwa ada empat hal yang mendukung kebahagiaan manusia di dunia, yaitu:",
                "(1) Ia sebaiknya terampil, efisien, bersungguh-sungguh, dan bersemangat dalam menjalani profesinya.<br />(2) Ia sebaiknya melindungi penghasilan yang diperolehnya dengan benar.<br />(3) Ia sebaiknya memiliki teman-teman yang jujur, dan bijaksana.<br />(4) Ia sebaiknya hidup sepadan dengan penghasilannya, tidak terlalu banyak atau terlalu sedikit.",
                "Dan agar bahagia selamanya, maka ia harus mempunyai keyakinan, melakukan kebajikan, murah hati, dan mengembangkan kebijaksanaan."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Keyakinan Umat Buddha - Buku Standar Wajib Baca</em>, hal. 206-207"
        },
        "02": {
            "content": [
                "Perlunya Toleransi",
                "Orang-orang saat ini diracuni oleh nafsu untuk memperoleh ketenaran, kekayaan, dan kekuasaan. Mereka kecanduan akan pemuasan indra. Orang melewati hari-hari mereka dalam ketakutan, kecurigaan, dan ketidaknyamanan.<br />Orang menjadi sulit untuk hidup berdampingan dengan sesamanya. Karena itu diperlukan toleransi dan pengertian di dunia ini agar memungkinkan terciptanya perdamaian dunia.<br />Orang-orang membicarakan agama dan berjanji untuk menyediakan jalan pintas menuju surga, tetapi mereka tidak tertarik untuk mempraktikkannya.<br />Jika umat Kristen hidup sesuai dengan khotbah di Bukit, jika umat Buddha mengikuti Jalan Mulia Berunsur Delapan, jika umat Muslim benar-benar mengikuti konsep persaudaraan, dan jika umat Hindu membentuk hidupnya dalam kesatuan, pasti akan ada kedamaian dan harmoni di dunia ini. Intoleransi yang dilakukan atas nama agama itu adalah paling memalukan dan tercela."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Keyakinan Umat Buddha - Buku Standar Wajib Baca</em>, hal. "
        },
        "03": {
            "content": [
                "Kaishu dan teman-temannya menyeberangi sebuah sungai yang deras dalam hujan lebat. Perahu mereka terombang-ambing dengan hebat.<br />Semua teman-teman guru itu menjadi ketakutan dan pucat pasi, beberapa dari mereka sebisa-bisa berdoa meminta pertolongan kepada Awalokiteswara, Dewi Pengasih.<br />Hanya Kaishu yang duduk dengan tenang bermeditasi.<br />Ketika pada akhirnya perahu itu kandas, teman-temannya mengeluh tentang batu runcing. Kaishu menegur mereka dengan kata-kata berikut: “Seorang Zen lebih baik tidak jadi apa-apa jika ia tidak bisa menolong dirinya sendiri. Sang Dewi pasti menertawakan kelemahanmu.” "
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Membuka Kemungkinan-kemungkinan - Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 58"
        },
        "04": {
            "content": [
                "Seorang pria yang kehilangan kapaknya, mencurigai anak tetangganya. Anak itu berjalan seperti pencuri, terlihat seperti pencuri, dan berbicara seperti pencuri.<br />Tetapi kemudian orang itu menemukan kapaknya ketika ia menggali di lembah.<br />Dan saat berikutnya ketika ia melihat anak tetangganya itu, anak itu berjalan seperti anak-anak lainnya, terlihat seperti anak-anak lainnya, dan berbicara seperti anak-anak lainnya."
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Membuka Kemungkinan-kemungkinan - Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 123"
        },
        "05": {
            "content": [
                "Umat Buddha yang memahami ajaran Buddha tidak akan pernah menilai agama lain adalah salah. Seseorang yang bertujuan murni mempelajari agama lain dengan wawasan yang luas pasti akan sependapat.<br />Hal pertama yang hendaknya diperhatikan dalam mempelajari agama lain adalah sampai mana agama tersebut memiliki persamaan.<br />Semua agama mengakui bahwa keadaan manusia saat ini tidaklah memuaskan. Semua sependapat bahwa dibutuhkan suatu perubahan sikap dan perilaku untuk memperbaiki keadaan ini.<br />Semua agama mengajarkan nilai cinta kasih, kebajikan, kesabaran, kemurahan hati, tanggung jawab sosial, dan semua menerima adanya sesuatu yang bersifat mutlak.<br />Sikap intoleransi beragama, keangkuhan, dan pembenaran diri sendiri akan muncul ketika melekat secara sempit kepada satu-satunya cara pandang mereka."
            ],
            "source": "Shravasti Dhammika, <em>Anda Bertanya Kami Menjawab – Alat Bantu Pengenalan Buddhisme</em>, hal. 13"
        },
        "06": {
            "content": [
                "Buddha mengajarkan bahwa apa pun yang terjadi pasti karena satu atau lebih penyebabnya, dan bukan disebabkan oleh karena: kebetulan, nasib, atau untung-untungan.<br />Orang yang tertarik pada keberuntungan selalu mencoba untuk mendapatkan sesuatu, biasanya banyak rezeki atau kesehatan.<br />Buddha mengajarkan bahwa mengembangkan hati dan pikiran ke arah yang baik adalah jauh lebih bermanfaat."
            ],
            "source": "Shravasti Dhammika, <em>Anda Bertanya Kami Menjawab - Alat Bantu Pengenalan Buddhisme</em>, hal. 84"
        },
        "07": {
            "content": [
                "Ada kebenaran yang agung dalam pernyataan, “Jika engkau ingin mengenal seorang guru Zen, engkau harus bertanya kepada teman hidup mereka.” Kita dikenal dari kebanyakan perilaku pribadi kita, perhatian kita, ucapan kita dan perasaan-perasaan. Hidup di dalam harmoni dan kedamaian batin berarti meliputi setiap pikiran dan tindakan kita."
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Menghidupkan Kebenaran Kita – Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 89"
        },
        "08": {
            "content": [
                "Disebutkan bahwa segera sesudah pencerahan-Nya, Buddha bertemu dengan seorang laki-laki di jalan, yang terpesona dengan pancaran yang luar biasa dan kedamaian dari kehadirannya. Orang itu berhenti dan bertanya, “Temanku, engkau ini siapa? Apakah engkau makhluk surga atau dewa?”<br />“Bukan,” jawab Buddha.<br />“Wah, kalau begitu, engkau gandarwa atau yaksa?”<br />Lagi Buddha menjawab, “Bukan.”<br />“Apakah engkau seorang manusia?”<br />“Bukan.”<br />“Jika demikian, temanku, engkau siapa?”<br />Buddha menjawab, “Aku orang yang telah sadar.”"
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Menghidupkan Kebenaran Kita – Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 132"
        },
        "09": {
            "content": [
                "Kebahagiaan sejati adalah keadaan internal - kualitas pikiran - dan karenanya harus dicari di dalam diri sendiri.<br />Kebahagiaan tidak bergantung pada hal-hal eksternal, walaupun kemapanan materi sampai tingkat tertentu tetap diperlukan sebagai dasar bagi pengembangan internal.<br />Kita hanya membutuhkan empat jenis kebutuhan jasmani: makanan yang bermanfaat, pakaian, tempat tinggal, dan obat-obatan.<br />Sejalan dengan ini, kita memiliki empat kebutuhan batin: pengetahuan benar, kebajikan, penjagaan pintu indra, dan meditasi.",
                "Ini semua adalah dua set keperluan dasar untuk menjalani hidup sederhana mengarah kepada kepuasan dan kedamaian pikiran untuk mencapai kebajikan dan nilai-nilai yang lebih tinggi."
            ],
            "source": "Robert Bogoda, <em>Hidup Sederhana Hidup Bahagia</em>, hal. 19"
        },
        "10": {
            "content": [
                "Pikiran secara terus menerus akan tertarik kepada objek indra yang menyenangkan dan menolak objek yang tidak menyenangkan.<br />Pikiran mengembara ke sana-sini sehingga menjadi terpecah dan bingung.<br />Dengan mengendalikan indra dan menjaga pintu-pintu indra, pikiran akan menjadi teduh dan mantap, dan sebagai hasilnya kita mengalami kebahagiaan sepenuhnya.<br />Waktu, energi, dan dana itu terbatas, sedangkan keinginan itu tidak terbatas. Karena itu harus memiliki skala prioritas.<br />Seorang umat Buddha harus dapat membedakan dan mengetahui apa yang benar-benar penting bagi hidup yang bermanfaat; apa yang diinginkan tetapi tidak mendesak; apa yang sepele dan dapat diabaikan; dan apa yang merusak.<br />Dengan membuat perbedaan ini, seseorang harus mengupayakan hal-hal yang memiliki nilai tinggi dalam skala prioritas dan mengelakkan hal-hal yang bernilai rendah.<br />Hal ini akan memungkinkan seseorang menghindari kesia-siaan dan kecemasan yang tidak perlu dan membantu meningkatkan hidup yang seimbang dan bersahaja."
            ],
            "source": "Robert Bogoda, <em>Hidup Sederhana Hidup Bahagia</em>, hal. 38-39"
        },
        "11": {
            "content": [
                "Nasarudin kini menjadi seorang yang sudah tua, yang melihat kembali kehidupannya.<br />Ia duduk bersama teman-temannya di kedai teh, menceritakan kisahnya.<br />“Ketika aku masih muda, aku bersemangat sekali - aku ingin menyadarkan setiap orang. Aku berdoa kepada Allah agar memberikan kekuatan untuk mengubah dunia.<br />Di tengah-tengah hidupku, suatu hari aku bangun dan menyadari hidupku sudah berlalu setengahnya, dan aku tak mengubah seorang pun. Maka, aku berdoa kepada Allah agar memberiku kekuatan untuk mengubah mereka yang dekat di sekitarku, yang begitu banyak membutuhkannya.<br />Aduh, sekarang aku sudah tua, dan doaku lebih sederhana. 'Allah', pintaku, 'tolong berikan padaku kekuatan untuk mengubah paling tidak diriku sendiri'.”"
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Menemukan Jalan – Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 12"
        },
        "12": {
            "content": [
                "Seorang petani memohon kepada seorang biksu Tendai untuk membacakan sutra buat istrinya yang telah meninggal dunia. Setelah pembacaan doa selesai, si petani bertanya, “Kau pikir istriku akan meraih pahala dengan ini?”<br />“Bukan hanya istrimu, semua makhluk hidup akan menarik manfaat dari pembacaan sutra,” jawab biksu tadi.<br />“Jika engkau berkata semua makhluk hidup akan beruntung,” kata si petani, “istriku mungkin sangat lemah dan yang lainnya akan mengambil keuntungan darinya, mendapatkan keuntungan yang seharusnya ia miliki. Jadi tolong, bacakan sutra hanya untuknya.”<br />Biksu itu menjelaskan bahwa merupakan harapan seorang Buddhis untuk memberikan pemberkahan dan menginginkan pahala bagi setiap makhluk hidup.<br />“Itu ajaran yang bagus,” si petani menyimpulkan, “tetapi tolong buat suatu kecualian. Aku punya seorang tetangga yang kasar dan kejam padaku. Tolong kecualikan dia dari semua makhluk hidup.”"
            ],
            "source": "Christina Feldman &amp; Jack Kornfield, <em>Menemukan Jalan – Kisah-kisah Spirit, Kisah-kisah Batin</em>, hal. 36"
        },
        "13": {
            "content": [
                "Menurut ajaran Buddha, perbuatan baik atau buruk yang didasari kehendak, bukanlah “akibat” dari perbuatan sebelumnya, tetapi adalah “perbuatan” itu sendiri.<br />Kehendak tidak dipengaruhi oleh buah karma lampau.<br />Misal kita melihat sesuatu yang indah, maka kesadaran melihat yang indah adalah karma baik yang berbuah.<br />Namun reaksi kita terhadap aktivitas melihat bukanlah buah karma. Jika anda melekat terhadap sesuatu yang indah tersebut, maka reaksi anda itu adalah karma buruk.<br />Jika reaksi anda sesudah melihat yang indah tersebut muncul pikiran tentang sifat sejati dari benda, yaitu tidak kekal dan akan mengikuti kaidah alam: muncul dan lenyap, maka reaksi anda adalah karma baik.<br />Apa pun yang kita hadapi atau alami dalam hidup ini adalah hasil dari karma lampau, namun reaksi anda terhadapnya bukanlah akibat karma. Reaksi anda adalah karma baru."
            ],
            "source": "Sayadaw U Silananda, <em>Kamma - Hukum Sebab Akibat, Anatta - Doktrin Tiada Inti Diri</em>, hal. 35-37"
        },
        "14": {
            "content": [
                "Ajaran Buddha tak diragukan lagi sejalan dengan ilmu pengetahuan, tetapi keduanya harus dianggap sebagai ajaran yang sejajar, karena yang satu terutama berkaitan dengan kebenaran material, sementara yang satunya membatasi diri pada kebenaran moral dan spiritual.<br />Dharma yang diajarkan bukanlah untuk dilestarikan dalam buku, bukan pula untuk menjadi subjek pelajaran dari sudut sejarah atau sastra.<br />Dharma harus dipelajari dan dipraktikkan dalam perjalanan hidup sehari-hari, tanpa praktik seseorang tidak dapat menghargai kebenaran dan tidak mungkin dapat mencapai tujuan akhir, yaitu terbebas dari kelahiran dan kematian."
            ],
            "source": "Narada Mahathera, <em>Ajaran Buddha Secara Ringkas - Pedoman Pokok Ajaran Buddha</em>, hal. 23"
        },
        "15": {
            "content": [
                "Siapakah “aku” ini?<br />“Aku” adalah sumber segala masalah, kecemasan, kegelisahan, dan ketidakbahagiaan.<br />Ketika “aku” melekat pada hal-hal yang diinginkan, maka timbullah keserakahan.<br />Ketika “aku” menolak hal-hal yang tidak diinginkan, maka timbullah niat jahat, kemarahan, atau kebencian.<br />Ketika “aku” putus asa, maka timbullah kesakitan, kesedihan, kecemasan, kesengsaraan yang bisa berakhir dengan pengakhiran hidup.<br />Ketika “aku” menggelembung, maka timbullah kesombongan.<br />Ketika “aku” meluas dan menyatu dengan semua, maka timbullah rasa tidak mementingkan diri, belas kasih tanpa batas, cinta kasih universal, dan harmoni sempurna.<br />Ketika “aku” tiada, maka timbullah keseimbangan batin sempurna tanpa kemelekatan dan penolakan."
            ],
            "source": "Narada Mahathera, <em>Ajaran Buddha Secara Ringkas - Pedoman Pokok Ajaran Buddha</em>, hal. 139-140"
        },
        "16": {
            "content": [
                "<em>Moralitas: prasyarat meraih pengetahuan sejati.</em> Menurut Buddhisme, untuk mendapatkan pengetahuan sejati diperlukan meditasi penembusan untuk memahami sifat-dasar realitas. Namun tanpa pelaksanaan sila yang ketat, meditasi sejati tidak dapat dicapai. Dan tanpa meditasi sejati, maka kearifan tertinggi untuk memahami realitas tunggal mustahil diraih. Intinya, pengetahuan sejati itu tidak bisa terlepas dari landasan moralitas (sila) yang benar."
            ],
            "source": "Ivan Taniputra Dipl. Ing, <em>Sains Modern dan Buddhisme - Menelaah Persamaan Buddhisme dan Sains dalam Bidang Kosmologi, Fisika Kuantum, Geologi, Matematika, dan Psikologi<em>, hal. 98"
        },
        "17": {
            "content": [
                "Sains merupakan sebuah instrumen belaka, yang secara intrinsik bersifat netral, tidak baik dan juga tidak jahat."
            ],
            "source": "Ivan Taniputra Dipl. Ing, <em>Sains Modern dan Buddhisme - Menelaah Persamaan Buddhisme dan Sains dalam Bidang Kosmologi, Fisika Kuantum, Geologi, Matematika, dan Psikologi<em>, hal, 7"
        },
        "18": {
            "content": [
                "Karena perang diawali dari pikiran manusia, maka dari sana pula benteng perdamaian harus dibangun."
            ],
            "source": "Dr. K. Sri Dhammanda, <em>Meditasi untuk Siapa Saja - Panduan Praktis Pengembangan Mental</em>, hal. 96"
        },
        "19": {
            "content": [
                "Melalui meditasi, anda dapat menjadi tuan bagi nafsu indra anda."
            ],
            "source": "Dr. K. Sri Dhammanda, <em>Meditasi untuk Siapa Saja - Panduan Praktis Pengembangan Mental</em>, hal. 35"
        },
        "20": {
            "content": [
                "Pikiran terikat pada masa lampau, ditimbulkan oleh kegelisahan.<br />Pikiran berkelana ke masa depan, disebabkan oleh harapan.<br />Pikiran yang menolak terhadap sesuatu, disebabkan oleh kebencian."
            ],
            "source": "Matara Sri Nanarama Mahathera, <em>Tujuh Tingkat Kesucian dan Pengertian Langsung</em>, hal. 6"
        },
        "21": {
            "content": [
                "Pengembangan spiritual atau keadaan pikiran yang luhur adalah lebih penting daripada kesenangan duniawi."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Ajaran Buddha di Mata Cendekiawan - Opini Tokoh Besar Dunia tentang Buddhisme</em>, hal. 80"
        },
        "22": {
            "content": [
                "Perbedaan antara Buddha dengan orang biasa adalah seperti perbedaan antara orang waras dengan orang gila."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Ajaran Buddha di Mata Cendekiawan - Opini Tokoh Besar Dunia tentang Buddhisme</em>, hal. 4"
        },
        "23": {
            "content": [
                "Bagaikan lengan yang dililit kuat oleh seekor ular, demikian juga pandangan salah."
            ],
            "source": "Acharya Buddharakkhita, <em>8 Jayamangala - Berkah Kejayaan Buddha</em>, hal. 119"
        },
        "24": {
            "content": [
                "Sekalipun menyimpang dari kebenaran, namun merasa dirinya sebagai acuan pengetahuan."
            ],
            "source": "Acharya Buddharakkhita, <em>8 Jayamangala - Berkah Kejayaan Buddha</em>, hal. 81"
        },
        "25": {
            "content": [
                "Mereka yang percaya bahwa uang dapat melakukan segala sesuatu seringkali bersedia melakukan segalanya demi uang."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Santapan Batin - Untaian Senyum Pencerahan</em>, hal. 327"
        },
        "26": {
            "content": [
                "Buddha Dharma pada hakikatnya bersifat humanistik. Bukan ajaran tentang hantu-hantu, roh-roh, atau dewa-dewa."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Santapan Batin - Untaian Senyum Pencerahan</em>, hal. 136"
        },
        "27": {
            "content": [
                "Kebenaran mutlak bisa dinyatakan dengan kata-kata? Tidak, meskipun dalam kenyataannya ia hanya dapat disadari dan dialami."
            ],
            "source": "<em>Intisari Ajaran Buddha - Wawancara dengan Pewaris Takhta Sakya</em>, hal. 71"
        },
        "28": {
            "content": [
                "Setiap perbuatan yang dilandasi keserakahan, kebencian, dan kebodohan, akan menimbulkan penderitaan."
            ],
            "source": "<em>Intisari Ajaran Buddha - Wawancara dengan Pewaris Takhta Sakya</em>, hal. 2"
        },
        "29": {
            "content": [
                "Salah satu kaidah emas untuk menjalani kehidupan mulia adalah dengan menerapkan penghidupan yang seimbang-tanpa menjadi ekstrem dalam segala hal. Buddha tidak menganjurkan kita untuk menyiksa tubuh dan pikiran demi agama. Kita bisa menjalankan agama dengan rasional. Jangan berlebih-lebihan dalam segala hal."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Be Happy - Mengatasi Takut dan Cemas dari Akarnya dan Berbahagia dalam Segala Situasi</em>, hal. 296"
        },
        "30": {
            "content": [
                "Gembok pernikahan itu begitu berat sehingga perlu dua orang untuk memikulnya."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Be Happy - Mengatasi Takut dan Cemas dari Akarnya dan Berbahagia dalam Segala Situasi</em>, hal. 258"
        },
        "31": {
            "content": [
                "Tujuan agama bukanlah untuk mendirikan tempat ibadah yang megah, melainkan untuk mengembangkan sifat-sifat positif seperti tenggang rasa, kemurahan hati, dan cinta kasih."
            ],
            "source": "H.H. Dalai Lama, <em>Belas Kasih dan Kebijaksanaan</em>, hal. 199"
        }
    },
    "06": {
        "01": {
            "content": [
                "“Terlepas dari segala perbedaan filosofi, yang paling penting adalah memiliki pikiran yang damai dan disiplin serta hati yang hangat.”"
            ],
            "source": "H.H. Dalai Lama, <em>Belas Kasih dan Kebijaksanaan</em>, hal. 198"
        },
        "02": {
            "content": [
                "Tidak ada api sepanas nafsu, tak ada cengkeraman sekuat kebencian, tak ada jerat seketat kebodohan, tak ada arus sederas keinginan."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Hidup dan Masalahnya</em>, hal. 329"
        },
        "03": {
            "content": [
                "Berbahagialah hidup dengan tidak membenci di antara membenci; di antara yang membenci, kita hidup dalam kebajikan."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Hidup dan Masalahnya</em>, hal. 328"
        },
        "04": {
            "content": [
                "Barangsiapa berbuat jahat terhadap orang yang baik, yang suci dan tidak bersalah; kejahatan itu akan berbalik menimpanya, seperti debu yang ditebarkan menentang angin."
            ],
            "source": "K. Sri Dhammananda, <em>Hidup dan Masalahnya</em>, hal. 326"
        },
        "05": {
            "content": [
                "Karena tidak melulu dogmatis, namun didasarkan pengalaman, ajaran Buddha tidak pernah gentar menghadapi perkembangan ilmu pengetahuan."
            ],
            "source": "Shravasti Dhammika &amp; Handaka Vijjananda, <em>Tak Kenal Maka Tak Sayang</em>, hal. 269"
        },
        "06": {
            "content": [
                "Seseorang seharusnya tidak hanya menghormati agamanya sendiri dan mencela agama lain."
            ],
            "source": "Shravasti Dhammika &amp; Handaka Vijjananda, <em>Tak Kenal Maka Tak Sayang</em>, hal. 262"
        },
        "07": {
            "content": [
                "Satu-satunya penggolongan umat manusia adalah berdasarkan kualitas perilaku moralnya."
            ],
            "source": "Shravasti Dhammika &amp; Handaka Vijjananda, <em>Tak Kenal Maka Tak Sayang</em>, hal. 250"
        },
        "08": {
            "content": [
                "Ketika sebab-sebab penderitaan hadir, maka penderitaan juga hadir. Ketika sebab-sebab penderitaan dihilangkan, maka penderitaan pun hilang."
            ],
            "source": "Thich Nhat Hanh, <em>Jalur Tua Awan Putih - Menelusuri Jejak Langkah Buddha (Buku Pertama)</em>, hal. 268"
        },
        "09": {
            "content": [
                "Manusia bukan saja terperangkap oleh penyakit dan ketidakadilan kondisi-kondisi sosial, melainkan juga oleh berbagai kesedihan dan nafsu yang mereka ciptakan dalam hati dan pikiran masing-masing."
            ],
            "source": "Thich Nhat Hanh, <em>Jalur Tua Awan Putih - Menelusuri Jejak Langkah Buddha (Buku Pertama)</em>, hal. 98"
        },
        "10": {
            "content": [
                "Kebenaran tetap merupakan kebenaran apakah orang memercayainya atau tidak."
            ],
            "source": "Thich Nhat Hanh, <em>Jalur Tua Awan Putih - Menelusuri Jejak Langkah Buddha (Buku Pertama)</em>, hal. 44"
        },
        "11": {
            "content": [
                "Keberadaan yang berkondisi itu tidak kekal.<br />Keberadaan yang berkondisi tidak memiliki inti.<br />Keberadaan yang berkondisi itu merupakan penderitaan."
            ],
            "source": "Mahasthavira Sangharakshita, <em>Jalan Mulia Berunsur Delapan - Jalan Menuju Lenyapnya Penderitaan</em>, hal. 26–27"
        },
        "12": {
            "content": [
                "Jika keinginan tidak diatasi, maka roda kelahiran dan kematian tak dapat dihindari, dan penderitaan tak akan pernah berakhir. Selama kita menjadi korban keinginan diri kita sendiri, kita tak akan pernah menjadi bebas."
            ],
            "source": "Master Hsing Yun, <em>Di Mana Benih Buddha Anda?</em>, hal. 8"
        },
        "13": {
            "content": [
                "Buddha mengamati Sronakotivimsa selama beberapa saat, lalu berkata, “Jika dawai kecapi ditarik terlalu kencang, apa yang akan terjadi saat engkau memainkannya?”<br />Sronakotivimsa menjawab, “Dawai itu akan putus.”<br />“Dan jika dawai itu ditata terlalu longgar,” tanya Buddha, “Apa yang akan terjadi?”<br />“Jika dawainya terlalu longgar, maka tidak akan muncul nada apa pun,” jawab Sronakotivimsa.<br />“Begitulah, Sronakotivimsa,” kata Buddha, sambil tersenyum dengan penuh pertimbangan dan pengertian. “Berlatih dalam Dharma diibaratkan seperti menata seutas dawai kecapi. Jika kita berlatih terlalu kuat, seakan-akan kita tengah menata dawai itu terlalu kencang. Dawai itu akan putus. Sebaliknya, jika kita terlalu santai dalam latihan Dharma, dan melalaikan waktu tanpa usaha memajukan diri kita sendiri, maka seolah-olah kita tengah menata dawai itu terlalu longgar. Tak ada hasil apa pun yang akan muncul dari usaha kita yang amat kecil itu.”"
            ],
            "source": "Master Hsing Yun, <em>Di Mana Benih Buddha Anda?</em>, hal. 31-32"
        },
        "14": {
            "content": [
                "Inti dari ajaran Buddha tentang kekayaan adalah untuk mendapatkan harta dengan jujur dan mempraktikkan aspek-aspek positif dari moralitas Buddhis di dunia ini: dengan penggunaan kekayaan yang penuh dengan pertimbangan dan kedermawanan, kita dapat belajar ketidakmelekatan, belas kasih, kemurahan hari, dan pikiran yang jernih. Pada akhirnya, kekayaan yang terbesar adalah pengetahuan Kebuddhaan kita. Sekilas cahaya dari kekayaan spiritual yang mengagumkan yang ada di dalam diri anda lebih bernilai dibandingkan dengan semua kekayaan materi yang ada di jagad raya ini."
            ],
            "source": "Master Hsing Yun, <em>Menjadi Baik - Etika Buddhis Sehari-hari</em>, hal. 111"
        },
        "15": {
            "content": [
                "Pikiran tercerahkan adalah jalan tengah yang mencari manfaat bagi diri sendiri dan orang lain sekaligus. Seperti seorang ibu harus makan dan merawat dirinya sendiri agar bisa merawat anaknya, maka pikiran tercerahkan mencari kemajuannya sendiri bahkan ketika ia mengerahkan energi yang besar untuk menolong makhluk lain."
            ],
            "source": "Master Hsing Yun, <em>Menjadi Baik - Etika Buddhis Sehari-hari</em>, hal. 182"
        },
        "16": {
            "content": [
                "Melihat ketidakkekalan akan menjadikan kita mengerti tentang sifat penderitaan atau tidak memuaskan, dan sifat tanpa aku dari fenomena."
            ],
            "source": "Visuddhacara, <em>Halo Meditasi - Perhatian Murni &amp; Cinta Kasih dalam Setiap Detak Hidup</em>, hal. 75"
        },
        "17": {
            "content": [
                "Karena kita bermeditasi dan menemukan kebahagiaan melalui pemeliharaan dan perbaikan, kita akan menikmati bentuk-bentuk pikiran baik yang terus semakin baik."
            ],
            "source": "Visuddhacara, <em>Halo Meditasi - Perhatian Murni &amp; Cinta Kasih dalam Setiap Detak Hidup</em>, hal. 40"
        },
        "18": {
            "content": [
                "“Kemarahan tidak pernah dapat diatasi dengan kemarahan. Hanya dengan kasih, kemarahan dapat diatasi. Ini adalah hukum abadi,” demikian sabda Buddha. Dalam kesempatan lain Beliau menasihatkan; “Taklukkan orang yang marah dengan kasih.”"
            ],
            "source": "Visuddhacara, <em>Meredam Marah Menebar Metta</em>, hal. 5"
        },
        "19": {
            "content": [
                "Ketenangan dan kesadaran penuh adalah penjaga yang pertama dan terbaik untuk melawan kemarahan dan semua pikiran yang buruk. Karena dengan kesadaran penuh kemarahan dapat diketahui kehadirannya.<br />Kapan pun kita marah, kita semestinya tidak bertindak atau pun mengatakan sesuatu."
            ],
            "source": "Visuddhacara, <em>Meredam Marah Menebar Metta</em>, hal. 30"
        },
        "20": {
            "content": [
                "“Hanya dengan kearifan atau pengertian, akan kita peroleh cinta kasih. Semua penderitaan teratasi manakala kita memperoleh kebijaksanaan. Jalur pembebasan sejati adalah jalan menuju pengertian. Pengertian adalah prajna. Pengertian semacam ini hanya bisa hadir dengan melihat sifat dasar segala sesuatu secara mendalam. Jalur yang melatih sila, konsentrasi, dan pengertian ini merupakan jalan yang membawa pada pembebasan”."
            ],
            "source": "Thich Nhat Hanh, <em>Jalur Tua Awan Putih 2</em>, hal. 62"
        },
        "21": {
            "content": [
                "“Kasih sayang adalah buah pengertian. Berlatih jalan menuju Keadaan Sadar adalah untuk memahami sejatinya wajah kehidupan. Wajah sejati tersebut adalah ketidakkekalan. Segala sesuatu tidak abadi dan tanpa diri yang terpisah. Segala sesuatu suatu hari pasti berlalu."
            ],
            "source": "Thich Nhat Hanh, <em>Jalur Tua Awan Putih 2</em>, hal. 131"
        },
        "22": {
            "content": [
                " “Ananda… jangan sedih. Di dunia ini, di mana ada pertemuan, akan ada perpisahan. Di mana ada kelahiran, akan ada kematian. Setiap bunga yang mekar tentu akan layu. Daun yang hijau akan jadi kuning. Praktikkanlah Dharma! Janganlah bersedih.” "
            ],
            "source": "Lin Shi Min, <em>Sepuluh Siswa - Dhamma untuk Anak</em>, hal. 111"
        },
        "23": {
            "content": [
                "Buddha mengajarkan kesetaraan. Pintu ajaran Buddha selalu terbuka bagi siapa saja."
            ],
            "source": "Lin Shi Min, <em>Sepuluh Siswa - Dhamma untuk Anak</em>, hal. 94"
        },
        "24": {
            "content": [
                "Semua perbuatan buruk, berat atau ringan, akan menghasilkan buah suatu hari, bagaikan benih yang telah ditanam di tanah, akan berakar dan tumbuh suatu hari."
            ],
            "source": "Lin Shi Min, <em>Sepuluh Siswa - Dhamma untuk Anak</em>, hal. 24"
        },
        "25": {
            "content": [
                "Kedua orangtua harus memiliki sifat berbudi agar keturunan mereka juga menjadi orang berbudi."
            ],
            "source": "Ashin Janakabhivamsa, <em>Abhidhamma Sehari-hari - Filosofi Tertinggi Buddhis dalam Terapan Etika</em>, hal. 254"
        },
        "26": {
            "content": [
                "Setiap perbuatan baik membawa hasilnya masing-masing dalam takaran yang setimpal."
            ],
            "source": "Ashin Janakabhivamsa, <em>Abhidhamma Sehari-hari - Filosofi Tertinggi Buddhis dalam Terapan Etika</em>, hal. 232"
        },
        "27": {
            "content": [
                "Dalam kehidupan terdapat penderitaan karena kelahiran, usia tua, sakit dan mati; penderitaan karena tidak mendapatkan apa yang diinginkan, tidak dapat menghindar dari yang tidak diinginkan; dan keterikatan terhadap kehidupan. Inilah Kebenaran sejati mengenai penderitaan."
            ],
            "source": "Dorothy C. Donath, <em>Pengenalan Agama Buddha – Theravada-Mahayana-Vajrayana</em>, hal. 37"
        },
        "28": {
            "content": [
                "Buddha tidak dapat ditemukan dengan mencari. Maka sadarilah BATINMU SENDIRI."
            ],
            "source": "Dorothy C. Donath, <em>Pengenalan Agama Buddha – Theravada-Mahayana-Vajrayana</em>, hal. 145"
        },
        "29": {
            "content": [
                "Empat landasan Perhatian atau Praktik Perhatian Murni adalah satu-satunya jalan untuk pemurnian makhluk…. Di sini Buddha berkata, “Inilah satu-satunya jalan.” "
            ],
            "source": "Sayadaw U Silananda, <em>Satu-satunya Jalan - Empat Landasan Perhatian Murni</em>, hal. 2"
        },
        "30": {
            "content": [
                "Para biksu, mereka yang mengabaikan Empat Landasan Perhatian Murni ini, berarti telah mengabaikan Jalan Mulia yang mengantar ke penghancuran total penderitaan. Mereka yang telah menjalankan Empat Landasan Murni ini, berarti telah menjalankan Jalan Mulia yang menghantar ke penghancuran total penderitaan."
            ],
            "source": "Sayadaw U Silananda, <em>Satu-satunya Jalan - Empat Landasan Perhatian Murni</em>, hal. 41"
        },
        "31": {
            "content": [
                "Perbedaan antara orang biasa dan orang yang tercerahkan ialah bahwa orang biasa salah memahami “aku” sebagai sesuatu yang nyata sementara yang tercerahkan tahu bahwa diri itu tidak nyata."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Pesan-pesan Kebenaran</em>, hal. 125"
        }
    },
    "07": {
        "01": {
            "content": [
                "Melatih atau mengontrol pikiran sepenuhnya tidak hanya merupakan hal yang paling penting, tetapi juga merupakan proses yang paling sederhana."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Pesan-pesan Kebenaran</em>, hal. 133"
        },
        "02": {
            "content": [
                "Pertanyaannya: Apakah kisah anda seperti di atas bakal pendek atau panjang? Pendek dan manis? Panjang dan melelahkan? Kenapa bisa begitu?<br />Orang yang tidak rendah hati tidak akan belajar karena ia menganggap telah mengetahui semuanya. Ia yang terpelajar akan rendah hati—bahkan Buddha juga bersifat rendah hati meskipun Beliau mengetahui semuanya."
            ],
            "source": "Shen Shian, <em>Cerah Setiap Hari - 366 Hari Renungan Harian</em>, hal. 65"
        },
        "03": {
            "content": [
                "Hanya pada saat benar-benar hening anda bisa mendengar apa yang sedang berlangsung di balik kepala anda—ini adalah prinsip dasar meditasi dan refleksi hening."
            ],
            "source": "Shen Shian, <em>Cerah Setiap Hari - 366 Hari Renungan Harian</em>, hal. 85"
        },
        "04": {
            "content": [
                "Tahapan tersulit dari perjalanan untuk bebas dari rasa bersalah adalah meyakinkan diri kita bahwa kita layak untuk dimaafkan."
            ],
            "source": "Ajahn Bhram, <em>Membuka Pintu Hati – 108 Cerita tentang Kebahagiaan Sejati</em>, hal. 27"
        },
        "05": {
            "content": [
                "Keinginan tak ada batasnya. Rasa berkecukupan adalah satu-satunya saat tatkala hati anda merasa cukup."
            ],
            "source": "Ajahn Bhram, <em>Membuka Pintu Hati – 108 Cerita tentang Kebahagiaan Sejati</em>, hal. 245"
        },
        "06": {
            "content": [
                "Karena pikiranlah, hidup kita menjadi bahagia atau menderita."
            ],
            "source": "Buntario Tigris, <em>1 Menit yang Mengubah Hidup Anda</em>, hal. 39"
        },
        "07": {
            "content": [
                "Napas kita adalah yang paling handal yang diberikan oleh alam untuk melatih kesadaran, napas senantiasa ada dan tersedia, karena napas adalah teman sejati kita."
            ],
            "source": "Buntario Tigris, <em>1 Menit yang Mengubah Hidup Anda</em>, hal. 45"
        },
        "08": {
            "content": [
                "Pikiran egois membuat pikiranmu jadi kotor. Orang egois hanya mementingkan dirinya sendiri."
            ],
            "source": "Jing Yin – Ken Hudson - Yanfeng Liu, <em>Come and See 1 – Dhamma Untuk Anak</em>, hal. 28"
        },
        "09": {
            "content": [
                "Buddha meminta kita untuk bisa membedakan antara apa yang kita butuhkan dengan apa yang kita inginkan, dan hendaknya memperjuangkan kebutuhan kita serta membatasi keinginan kita."
            ],
            "source": "Shravasti Dhammika, <em>Good Question Good Answer</em>, hal. 27"
        },
        "10": {
            "content": [
                "Pikiran, jika dikembangkan dengan tepat, adalah piranti yang sangat kuat. Jika kita mampu belajar memusatkan energi mental dan memproyeksikannya kepada orang lain, pikiran bisa mendatangkan efek pada mereka."
            ],
            "source": "Shravasti Dhammika, <em>Good Question Good Answer</em>, hal. 75"
        },
        "11": {
            "content": [
                "Cukupi semua kebutuhan anak, tapi jangan turuti semua keinginan yang diminta mereka."
            ],
            "source": "Vijaya Samarawickrama, <em>Membesarkan Anak</em>, hal. 5"
        },
        "12": {
            "content": [
                "Anak-anak belajar dari kehidupan.",
                "Anak yang tumbuh dengan ejekan, belajar menjadi bodoh.<br />Anak yang tumbuh dengan penuh celaan, belajar menjadi pencela.<br />Anak yang tumbuh dengan tidak dipercaya, belajar menjadi pembohong.<br />Anak yang tumbuh dengan kekerasan, belajar menjadi kasar.<br />Anak yang tumbuh dengan kasih sayang, belajar untuk mencinta.<br />Anak yang tumbuh dengan dorongan, belajar menjadi percaya diri.<br />Anak yang tumbuh dengan kebenaran, belajar menjadi adil.<br />Anak yang tumbuh dengan pujian, belajar untuk menghargai.<br />Anak yang tumbuh dengan berbagi, belajar menjadi pengertian.<br />Anak yang tumbuh dengan pengetahuan, belajar menjadi bijaksana.<br />Anak yang tumbuh dengan kesabaran, belajar menjadi toleran.<br />Anak yang tumbuh dengan kebahagiaan, akan menemukan cinta dan keindahan."
            ],
            "source": "Vijaya Samarawickrama, <em>Membesarkan Anak</em>, hal. 67-68"
        },
        "13": {
            "content": [
                "Dalam kedalaman pikiran anda, anda memiliki kebijaksanaan yang akan menuntun anda tatkala berhadapan dengan sesuatu yang negatif. Anda tidak akan terhempas olehnya; anda dengan mudah bisa mengatasinya. Demikian pula, ketika sesuatu yang baik terjadi, anda juga mampu mengatasinya. Mengatasi hal-hal ini adalah kuncinya."
            ],
            "source": "H.H. Dalai Lama dan Daniel Goleman, <em>Dunia dalam Harmoni - Dialog tentang Aksi Belas Kasih</em>, hal. 82"
        },
        "14": {
            "content": [
                "Wajar saja mengejar kebahagiaan kita sendiri dan bekerja keras untuk mendapatkan saat ini. Tetapi jadi berbeda ketika pengejaran itu mengabaikan, menyangkal, dan bahkan mengorbankan kebahagiaan pihak lain. Ada perbedaan besar di antara keduanya."
            ],
            "source": "H.H. Dalai Lama dan Daniel Goleman, <em>Dunia dalam Harmoni - Dialog tentang Aksi Belas Kasih</em>, hal. 90"
        },
        "15": {
            "content": [
                "Pikiran terjadi satu demi satu, momen demi momen silih berganti. Mereka yang tidak mengamati kenyataan ini percaya bahwa ada satu pikiran dalam kehidupan atau keberadaan ini. Mereka tidak tahu bahwa pikiran yang baru selalu muncul pada setiap momen."
            ],
            "source": "Mahasi Sayadaw, <em>Meditasi Vipassana - Tuntunan Praktik &amp; Rujukan Tahap Pemurnian</em>, hal. 31"
        },
        "16": {
            "content": [
                "Dalam latihan meditasi vipassana, penting untuk mencontoh orang yang membuat api. Untuk membuat api pada masa sebelum ada korek api, orang harus secara terus menerus menggosok dua batang kayu secara bersamaan tanpa jeda sedikit pun dalam gerakannya. Ketika batang kayu menjadi semakin panas, lebih banyak usaha diperlukan, dan penggosokan harus dilakukan tanpa putus. Hanya ketika api telah dihasilkan, pembuatnya leluasa untuk istirahat. Sama halnya, seorang yogi harus bekerja keras, dengan demikian tidak ada jeda di antara pencatatan terdahulu dan yang mengikutinya, dan konsentrasi terdahulu dan yang mengikutinya. Ia harus kembali pada latihan dasar mencatat “timbul, tenggelam” setelah ia mencatat sensasi rasa sakit."
            ],
            "source": "Mahasi Sayadaw, <em>Meditasi Vipassana - Tuntunan Praktik &amp; Rujukan Tahap Pemurnian</em>, hal. 37"
        },
        "17": {
            "content": [
                "Putraku,<br />Informasi, komentar, kejayaan dan kehormatan tak pernah ada habisnya. Abdikan dirimu untuk bermeditasi, dan dengan cara ini capailah pencerahan sejati Ibu."
            ],
            "source": "Paul Reps &amp; Nyogen Senzaki, <em>101 Koan Zen</em>, hal. 41"
        },
        "18": {
            "content": [
                "“Yang memberilah yang seharusnya berterima kasih,” Seisetsu."
            ],
            "source": "Paul Reps &amp; Nyogen Senzaki, <em>101 Koan Zen</em>, hal. 97"
        },
        "19": {
            "content": [
                "Buddha berkata bahwa ada lima hal yang semestinya kita renungkan setiap hari:<br />Usia tua<br />Penyakit<br />Kematian<br />Ketidakkekalan<br />Hukum Sebab Akibat<br />Aku belum mengatasi usia tua, penyakit, dan kematian. Oleh karenanya, aku harus mempraktikkan Dharma dengan tekun demi meraih kebebasan sejati."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 1)</em>, hal. 75-76"
        },
        "20": {
            "content": [
                "Orang bijak memelihara moralitas, di mana dewasa ini telah terjadi kemerosotan moral secara luas di antara pria dan wanita yang menjadi munculnya begitu banyak penyakit baru dan aneh."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 1)</em>, hal. 89"
        },
        "21": {
            "content": [
                "Tetaplah waspada dan biarkanlah segala sesuatu berlalu secara alami, maka pikiran anda akan menjadi tenang dalam setiap situasi."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan – Meditasi dalam Segala Objek Kehidupan</em>, hal. 90"
        },
        "22": {
            "content": [
                "Kotoran batin bisa menjadi bermanfaat jika dimanfaatkan dengan terampil. Ibarat memindahkan kotoran ayam dan lembu ke tanah tempat pohon pepaya tumbuh. Kotoran adalah segala sesuatu yang busuk, tetapi ketika pohon menghasilkan buah, pepayanya terasa manis dan enak. Bilamana keragu-raguan muncul, misalnya, perhatikanlah, selidikilah. Ini akan membantu latihan anda tumbuh dan menghasilkan buah yang manis."
            ],
            "source": "Ajahn Chah, <em>Sebatang Pohon di Tengah Hutan – Meditasi dalam Segala Objek Kehidupan</em>, hal. 253"
        },
        "23": {
            "content": [
                "Pencarian jalan spiritual biasanya dimulai karena adanya penderitaan. Pencarian ini tidak dimulai  dengan gemerlap dan luapan kegembiraan, namun dengan terpaan keras dari derita, kekecewaan, dan kegalauan. Namun demikian, supaya penderitaan bisa mendorong usaha pencarian spiritual yang sejati, penderitaan ini haruslah lebih dari sekadar sesuatu yang diterima dari luar secara pasif. Penderitaan ini harus memicu suatu kesadaran batin, yaitu pencerapaan yang menembus rasa berpuas diri yang dangkal yang muncul dari hubungan kita seperti biasanya dengan dunia ini."
            ],
            "source": "Bhikkhu Bodhi, <em>Jalan Kebahagiaan Sejati</em>, hal. 1"
        },
        "24": {
            "content": [
                "Kita seharusnya paling mengasihi diri kita sendiri. Oleh sebab itu, kita seharusnya tidak menyakiti diri sendiri."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 2)</em>, hal. 19"
        },
        "25": {
            "content": [
                "Biarkanlah aku berdoa tidak untuk perlindungan dari mara bahaya, tetapi demi keberanian untuk menghadapinya."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 2)</em>, hal. 103"
        },
        "26": {
            "content": [
                "Perayaan ulang tahun adalah sebuah tanda dari umur anda. Anda harus mempersiapkan diri dengan berkata,“Saya sekarang setahun lebih dekat dengan kematian.”"
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 2)</em>, hal. 162"
        },
        "27": {
            "content": [
                "Orangtua, kakek-nenek, dan leluhurku hanya bisa menimbun kekayaan itu, namun tak sekeping emas pun dapat mereka bawa ketika mati."
            ],
            "source": "Bhikkhu Kusaladhamma, <em>Kronologi Hidup Buddha</em>, hal. 1"
        },
        "28": {
            "content": [
                "Tak sepercik pun kebahagiaan  dapat diperoleh melalui perseteruan tak berharga dan pertarungan sia-sia."
            ],
            "source": "Bhikkhu Kusaladhamma, <em>Kronologi Hidup Buddha</em>, hal. 283"
        },
        "29": {
            "content": [
                "Janganlah congkak dan lalai! Yang lalai tak akan terlahir di alam bahagia. Jika engkau tak lalai—engkau akan menuju ke alam bahagia."
            ],
            "source": "Bhikkhu Kusaladhamma, <em>Kronologi Hidup Buddha</em>, hal. 551"
        },
        "30": {
            "content": [
                "Kulit, urat otot, dan tulang bolehlah layu. Daging dan darah bolehlah kering dalam tubuh, tetapi sebelum mencapai penerangan sempurna, aku tidak akan meninggalkan tempat ini."
            ],
            "source": "E. Swarnasanti, <em>Riwayat Hidup Buddha Gautama</em>, hal. 38"
        },
        "31": {
            "content": [
                "Semua yang terbentuk akan terurai, berjuanglah dengan penuh kesadaran."
            ],
            "source": "E. Swarnasanti, <em>Riwayat Hidup Buddha Gautama</em>, hal. 86"
        }
    },
    "08": {
        "01": {
            "content": [
                "Dia yang melihat Hukum Sebab-musabab yang Saling Bergantungan melihat Dharma; dia yang melihat Dharma melihat Buddha."
            ],
            "source": "Mr. Zhao Pu Chu, <em>Tanya Jawab Mengenai Agama Buddha</em>, hal. 37"
        },
        "02": {
            "content": [
                "Setiap bentuk pikiran tidak kekal. Inilah Hukum Dharma mengenai kemunculan dan kelenyapan."
            ],
            "source": "Mr. Zhao Pu Chu, <em>Tanya Jawab Mengenai Agama Buddha</em>, hal. 44"
        },
        "03": {
            "content": [
                "Buddha bersabda, bahwa dunia ada di dalam diri anda. Saat anda mendisiplinkan diri anda, maka sebuah dunia pun terdisiplinkan dan kedamaian dipertahankan."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 3)</em>, hal. 4"
        },
        "04": {
            "content": [
                "Kesehatan adalah harta yang terbesar.<br />Kepuasan hati adalah kekayaan yang terbesar."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 3)</em>, hal. 46"
        },
        "05": {
            "content": [
                "Pikiran adalah pemimpin, setiap orang diciptakan oleh pikiran.<br />Pikiran dapat membuat seseorang sakit, pikiran pun dapat menyembuhkan."
            ],
            "source": "Dr. Phang Cheng Kar, <em>Don't Worry Be Healthy - Hidup Sehat Tanpa Cemas (Buku 3)</em>, hal. 159"
        },
        "06": {
            "content": [
                "Tanya: Siapakah Guanyin?",
                "Jawab: Guanyin adalah pengejawantahan dari Belas Kasih Universal. Mungkin ini terdengar terlalu abstrak, terlalu jauh. Beliau biasa disebut Bodhisattwa Belas Kasih. Atau dalam bahasa Sanskerta disebut Awalokiteswara, yang diterjemahkan sebagai “Pendengar Suara-suara Dunia” atau “Yang Mendengarkan Permohonan-permohonan dari Mereka yang sedang dalam Kesusahan.”"
            ],
            "source": "Teoh Eng Soon, <em>Guan Yin – Seratus Satu Pertanyaan</em>, hal. 1"
        },
        "07": {
            "content": [
                "Dia yang memberikan penghormatan kepada Awalokiteswara, bermeditasi dengan memusatkan pikiran pada-Nya, dan melantunkan Sutra Awalokiteswara pada saat dini hari akan memperoleh berkah di dunia ini dan dunia lain, dalam kelahiran ini dan semua kelahiran berikutnya."
            ],
            "source": "Teoh Eng Soon, <em>Guan Yin – Seratus Satu Pertanyaan</em>, hal. 35"
        },
        "08": {
            "content": [
                "Harap diingat bahwa tidaklah benar menghukum anak-anak ketika anda marah dan benci. Mereka melakukannya karena ketidaktahuan."
            ],
            "source": "Bhikkhu Panyananda, <em>Love Your Children the Right Way - Cintailah Anak Anda dengan Bijaksana</em>, hal. 79"
        },
        "09": {
            "content": [
                "Ketika anda menakuti mereka dengan gertakan, maka anda mengakhiri pengetahuan anak."
            ],
            "source": "Bhikkhu Panyananda, <em>Love Your Children the Right Way - Cintailah Anak Anda dengan Bijaksana</em>, hal. 159"
        },
        "10": {
            "content": [
                "Negara kita akan menjadi sebuah negara yang berdisiplin dan tertib, karena orangtua membantu anak-anaknya melakukan tugas dengan benar."
            ],
            "source": "Bhikkhu Panyananda, <em>Love Your Children the Right Way - Cintailah Anak Anda dengan Bijaksana</em>, hal. 209"
        },
        "11": {
            "content": [
                "Di dalam dunia ini, seorang ayah dan seorang ibu adalah mulia adanya."
            ],
            "source": "Yeshe Tsogyal, <em>Putra Teratai - Riwayat Hidup Padmasambhava</em>, hal. 12"
        },
        "12": {
            "content": [
                "Lahir di dalam tiga bidang samsara adalah penderitaan. Bahkan dilahirkan sebagai raja Dharma juga hanyalah kerepotan dan gangguan. Jika engkau tidak menyadari bahwa pikiranmu adalah Dharmakaya yang tak dilahirkan. Kelahiran kembali di dalam samsara tidak berakhir, dan engkau berputar tanpa henti. Lihatlah ke dalam hakikat dirimu yang sunya dan sadar ! Maka engkau segera mencapai pencerahan."
            ],
            "source": "Yeshe Tsogyal, <em>Putra Teratai - Riwayat Hidup Padmasambhava</em>, hal. 26"
        },
        "13": {
            "content": [
                "Buddha berkata, “Dharma ibarat sebuah rakit.” Beliau menggunakan kata rakit karena rakit di zaman tersebut lazim digunakan untuk menyeberangi sungai, sehingga penjelasan Dharma adalah rakit dapat diterima. Penjelasan ini artinya sangat mendalam. Seseorang tidak boleh melekat pada Dharma sehingga melupakan dirinya sendiri, bangga atas statusnya sebagai seorang guru atau seorang terpelajar. Jika ia lupa bahwa Dharma hanyalah sebuah rakit, sesuatu yang berbahaya mungkin terjadi. Dharma adalah rakit, sebuah alat transportasi untuk menyeberangkan kita dari satu sisi ke sisi yang lain. Setelah tiba dan menginjakkan kaki di daratan seberang, jangan begitu bodoh membawa rakit tersebut.” "
            ],
            "source": "Buddhadasa Bhikkhu, <em>The Truth of Nature – Tanya Jawab dengan Bhikkhu Buddhadasa tentang Ajaran Buddha</em>, hal. 24"
        },
        "14": {
            "content": [
                "“<em>Dukkha</em> tidak bisa diakhiri dengan bersembahyang di rumah, berdoa di wihara, menyepi di hutan atau pegunungan. <em>Dukkha</em> harus diakhiri dari akarnya. Kita harus menemukan akar <em>dukkha</em> yang ada di dalam diri kita yang menyebabkan kita menderita setiap hari. Setelah akar tersebut kita temukan, kemudian kita cabut. <em>Dukkha</em> yang kita rasakan kemarin sudah berlalu dan berakhir, tidak bisa kembali lagi. <em>Dukkha</em> hari ini adalah masalah yang sebenarnya. <em>Dukkha</em> masa yang akan datang belum ada dan belum menjadi masalah, tetapi <em>dukkha</em> yang dirasakan saat ini harus diatasi.” "
            ],
            "source": "Buddhadasa Bhikkhu, <em>The Truth of Nature – Tanya Jawab dengan Bhikkhu Buddhadasa tentang Ajaran Buddha</em>, hal. 114"
        },
        "15": {
            "content": [
                "<em>Dukkha</em> sering kali diterjemahkan sebagai “penderitaan”, meskipun hal itu meliputi hal yang lebih luas. Semua perasaan sakit, penyakit, frustrasi, kesulitan, stres, kemarahan, penderitaan, atau segala bentuk ketidaksenangan atau ketidakpuasan adalah <em>dukkha</em>. Bagaimana pun perasaan positif juga dikategorikan sebagai <em>dukkha</em>, karena perasaan itu berlalu dengan cepat, selalu berubah, dan tidak menghasilkan kebahagiaan yang permanen.<br /><em>Dukkha</em> dalam pengertian terluasnya adalah suatu keadaan dari ketidaksempurnaan untuk hadir di dunia ini sebagai individu yang tidak tercerahkan.”"
            ],
            "source": "Buddhadasa Bhikkhu, <em>Practical Buddhism - Warisan Bhikkhu Buddhadasa</em>, hal. 35"
        },
        "16": {
            "content": [
                "“<em>Kebijaksanaan</em> dalam agama Buddha adalah melihat dan mengerti kebenaran tertinggi. <em>Kebijaksanaan</em> terkumpul dengan memupuk pandangan yang mendalam, yaitu dengan melihat kebenaran dengan jelas dalam setiap situasi, begitu dalam sehingga tertanam dalam pengertian seseorang, tak akan pernah dilupakan. <em>Kebijaksanaan</em> adalah faktor kunci untuk pencerahan.”"
            ],
            "source": "Buddhadasa Bhikkhu, <em>Practical Buddhism - Warisan Bhikkhu Buddhadasa</em>, hal. 120"
        },
        "17": {
            "content": [
                "“Adikku, lihatlah pada pohon yang besar yang tumbang itu. Dia berdiri kokoh, menahan terjangan angin. Ketika angin bertiup terlalu keras, maka pohon-pohon itu akan tercabut dari akarnya. Nah, sekarang lihat rumput yang rendah yang kita gunakan untuk membuat sapu. Ketika angin bertiup, rumput terayun merunduk bersama angin. Ketika angin berhenti, rumput dengan mudah tegak kembali.”<br />Ini adalah pelajaran untukmu. Terlalu keras, kaku, keras hati, keras kepala tidak selamanya baik. Kadang kadang lebih baik untuk menjadi fleksibel, ramah, rendah hati, dan dapat berkompromi.<br />Kita harus mempertimbangkan waktu, orang- orang, peristiwa, dan menyesuaikan tindakan kita sesuai dengan situasi."
            ],
            "source": "Bhikkhu Panyananda, <em>Dharma Menerangi Jalan</em>, hal. 6"
        },
        "18": {
            "content": [
                "Umat Buddha mempunyai Buddha, Dharma, dan Sanggha sebagai pelindung mereka. Umat Buddha memercayai hukum sebab akibat “<em>karma</em>” sebagaimana: “<em>tham-dee dai dee; tham-chua dai chu</em>a” artinya: berbuat baik, menerima baik; berbuat jahat, menerima kejahatan. Umat Buddha membiarkan Buddha menyinari jalan kehidupannya. Mereka membiarkan Dharma membimbing untuk hidup baik, dan Sanggha menjadi teladan cara hidup. Umat Buddha tidak memuja apa pun. Umat Buddha tidak memercayai peramal. Umat Buddha percaya, setiap orang menentukan masa depannya sendiri."
            ],
            "source": "Bhikkhu Panyananda, Dharma Menerangi Jalan, hal 114"
        },
        "19": {
            "content": [
                "Kematian haruslah dibahas setiap hari, dan carilah kesempatan untuk saling mengingatkan satu sama lain sesering mungkin agar tetap waspada, bahwa hari ini bisa saja kita menemui ajal. Karenanya, apa saja yang memberi manfaat baik bagi kita, harus segera dilaksanakan secepat mungkin. Ungkapkan rasa sayang kepada orang-orang yang kita cintai."
            ],
            "source": "W. Vajiramedhi, <em>Memandang Kematian di Mata</em>, hal. 7"
        },
        "20": {
            "content": [
                "Hidup bagaikan embun pagi. Kita bangun dan berjalan secara cermat di atas permadani rumput. Kita melihat embun yang berkilauan jatuh di atas rerumputan. Tetapi, pada saat berjalan berbalik arah sekali lagi, tetesan embun tersebut telah hilang tanpa jejak."
            ],
            "source": "W. Vajiramedhi, <em>Memandang Kematian di Mata</em>, hal. 23"
        },
        "21": {
            "content": [
                "Salah satu pandangan dasar Buddha Dharma menekankan bahwa penderitaan merupakan suatu realitas, dan oleh sebab itu, sikap menghindar tidak akan menyelesaikan permasalahan. Apa yang justru harus kita kerjakan adalah menghadapi penderitaan, menatap dan menganalisanya, menguji, menelusuri sebab-sebab, serta mencari tahu cara yang terbaik bagi anda untuk menanggulanginya."
            ],
            "source": "H.H. Dalai Lama, <em>The Way to Freedom - Jalan Menuju Pembebasan</em>, hal. 80"
        },
        "22": {
            "content": [
                "Pentingnya disiplin moral ditekankan oleh guru Buddha sendiri. Menjelang <em>parinirwana</em>, guru Buddha ditanya tentang siapa yang akan menggantikan Dia, dan Ia berkata bahwa latihan moralitas harus menjadi pembimbing dan guru bagi segenap ajaran Buddhis. Ia menanamkan disiplin moral sebagai penggantinya."
            ],
            "source": "H.H. Dalai Lama, <em>The Way to Freedom - Jalan Menuju Pembebasan</em>, hal. 197"
        },
        "23": {
            "content": [
                "Sang Katak yang melambangkan nafsu keinginan rendah (<em>tanha</em>) ditelan oleh ular yang melambangkan kemelekatan (<em>upadana</em>) yang kemudian ditelan oleh burung (<em>bhava</em> atau penjelmaan); sedangkan burung bertengger di atas gelagah yang rapuh dan berongga tanpa memiliki inti kayu padat seperti halnya tubuh kita, tanpa roh yang kekal melambangkan kelahiran (<em>jati</em>). Akar dari gelagah sedang digerogoti oleh empat ekor tikus yang melambangkan kelahiran, penuaan, penyakit dan kematian, kejadian-kejadian yang menandai perjalanan hidup kita."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Mengajarkan Dharma Melalui Gambar</em>, hal. 32-33"
        },
        "24": {
            "content": [
                "Belajar tanpa praktik menghasilkan kaum terpelajar yang kosong yang baginya semua kebijaksanaan ditemukan dalam buku (hanya berupa teori). Praktik tanpa belajar, walaupun seringkali disertai dengan keyakinan yang mendalam tetapi buta, gampang terbawa ke jalan yang salah. Kedua hal ini merupakan aspek yang saling melengkapi dan jika salah satunya tidak ada, maka tidak mungkin seseorang akan mampu merealisasi Dharma atau mencapai penembusan (<em>pativedha</em>)."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Mengajarkan Dharma Melalui Gambar</em>, hal. 55-56"
        },
        "25": {
            "content": [
                "Menaklukkan pikiran dan membawanya ke arah pemahaman yang benar akan kesunyataan bukanlah hal yang mudah. Ini membutuhkan proses perlahan dan bertahap untuk menyimak dan membaca penjelasan dari pikiran dan sifat semua fenomena; menafsirkan dan dengan seksama menganalisis informasi ini; serta akhirnya mengubah pikiran melalui meditasi."
            ],
            "source": "Kathleen MacDonald, <em>Meditasi - Sebuah Petunjuk Praktis</em>, hal. 9"
        },
        "26": {
            "content": [
                "Meditasi bukanlah berkelana atau melarikan diri. Pada kenyataannya, meditasi adalah bersikap sepenuhnya jujur terhadap diri kita sendiri: memperhatikan diri kita dengan baik-baik dan mengolahnya untuk menjadi lebih positif dan bermanfaat, bagi diri kita sendiri dan orang lain."
            ],
            "source": "Kathleen MacDonald, <em>Meditasi - Sebuah Petunjuk Praktis</em>, hal. 11"
        },
        "27": {
            "content": [
                "Semangat menurut Dharma adalah melakukan segala aktivitas sehari-hari dengan penuh semangat dan perhatian, semangat dalam menjaga diri untuk tidak melakukan perbuatan jahat, semangat dalam memupuk berbagai perbuatan baik, dan semangat untuk melatih diri dalam meditasi."
            ],
            "source": "Tony Yoyo, <em>Bingkai Refleksi - Refleksi Keseharian dalam Bingkai Dharma</em>, hal. 23"
        },
        "28": {
            "content": [
                "Berani mengakui dan menerima masa lalu, menggunakannya sebagai batu pijakan untuk melangkah di saat sekarang menuju masa depan yang lebih baik, adalah cara terbaik."
            ],
            "source": "Tony Yoyo, <em>Bingkai Refleksi - Refleksi Keseharian dalam Bingkai Dharma</em>, hal. 92"
        },
        "29": {
            "content": [
                "Dharma ajaran Buddha merupakan ajaran tentang praktik, baik melalui pikiran, ucapan maupun perbuatan di keseharian hidup kita. Tanpa praktik atau ‘terjun langsung’ melakukan berbagai teori atau pengetahuan Dharma yang dimiliki, niscaya kita tidak bisa merasakan nikmat dan indahnya Dharma."
            ],
            "source": "Tony Yoyo, <em>Bingkai Refleksi - Refleksi Keseharian dalam Bingkai Dharma</em>, hal. 193"
        },
        "30": {
            "content": [
                "Anda bukan individu yang mempunyai keterbatasan dan kecemasan seperti yang anda yakini. Siapa saja guru Buddhis yang terlatih dapat memberitahukan anda dengan keyakinan yang berasal dari pengalamannya sendiri bahwa, sesungguhnya, anda memiliki kasih sayang, kesadaran, dan mempunyai kemampuan sempurna untuk mencapai yang terbaik, bukan hanya untuk diri anda, tetapi untuk semua orang dan semua hal yang bisa anda bayangkan. Satu-satunya masalah adalah anda tidak menyadari keistimewaan yang anda miliki ini. Dalam istilah ilmiah yang rumit yang saya pahami lewat sebuah percakapan dengan para ahli di Eropa dan Amerika Utara, kebanyakan orang memiliki keyakinan yang salah tentang siapa diri mereka sesungguhnya yang dibentuk oleh kebiasaan, dan juga yang dihasilkan oleh gambaran tentang diri mereka sendiri yang dihasilkan oleh neuron-neuron."
            ],
            "source": "Yongey Mingyur Rinpoche &amp; Eric Swanson, <em>The Joy of Living - Keceriaan Hidup, Mengungkap Rahasia dan Kunci Ilmiah Kebahagiaan</em>, hal. 71"
        },
        "31": {
            "content": [
                "Jika anda sungguh-sungguh ingin menemukan kebahagiaan dan kepuasaan abadi, anda harus belajar menenangkan pikiran anda. Hanya dengan menenangkan pikiran maka kualitas terbaik pikiran dapat muncul. Cara termudah untuk menjernihkan air yang kotor, karena dipenuhi lumpur dan sedimen yang lain, hanyalah membiarkan air itu tenang. Demikian juga, jika anda izinkan pikiran anda untuk tenang, ketidaktahuan/kebodohan, kemelekatan, ketidaksukaan dan semua noda batin lainnya perlahan-lahan akan mengendap. Kemudian kasih sayang, kejernihan, dan ekspansi tanpa batas dari hakikat sejati pikiran anda akan muncul."
            ],
            "source": "Yongey Mingyur Rinpoche &amp; Eric Swanson, <em>The Joy of Living - Keceriaan Hidup, Mengungkap Rahasia dan Kunci Ilmiah Kebahagiaan</em>, hal. 195"
        }
    },
    "09": {
        "01": {
            "content": [
                "Dalam hipnotis, pikiran anda selalu sadar dan sedang mengamati. Inilah sebabnya orang-orang yang sedang berada dalam hipnotis yang dalam dan terlibat secara aktif dalam rangkaian memori masa kecil atau kehidupan lampaunya, dapat menjawab pertanyaan terapis, berbicara dalam bahasa kehidupannya yang sekarang, mengetahui lokasi geografis yang sedang mereka lihat dan bahkan mengetahui tahun, yang biasanya melintas di hadapan mata sebelah dalam mereka atau hanya muncul di pikiran mereka."
            ],
            "source": "Dr. Brian L. Weiss, <em>Through Time Into Healing, Waktu yang Menyembuhkan</em>, hal. 21"
        },
        "02": {
            "content": [
                "Meditasi, suatu praktik yang sangat saya rekomendasikan, adalah suatu metode dasar lainnya untuk membuka kesadaran anda ke ingatan-ingatan dari kehidupan lampau. Meditasi menjernihkan pikiran dan ketika pikiran jernih, wawasan (<em>insight</em>) dan mungkin ingatan dari kehidupan lampau akan muncul secara spontan."
            ],
            "source": "Dr. Brian L. Weiss, <em>Through Time Into Healing, Waktu yang Menyembuhkan</em>, hal. 284"
        },
        "03": {
            "content": [
                "Mayoritas orang benar-benar teridentifikasi penuh dengan suara dalam kepala—arus tanpa henti dari pemikiran yang sulit dan keras serta emosi-emosi yang menyertainya—yang dapat kita gambarkan sebagai milik pikiran mereka. Selama anda tidak sepenuhnya menyadari hal ini, anda menganggap si pemikir sebagai diri anda. Ini adalah pikiran egois. Kita menyebutnya egois karena terdapat rasa diri, rasa Aku (ego), dalam setiap pemikiran—setiap memori, setiap interpretasi, pendapat, sudut pandang, reaksi, emosi. Secara spiritual ini dikatakan ketidaksadaran."
            ],
            "source": "Eckhart Tolle, <em>A New Earth, Dunia Baru – Kesadaran Akan Tujuan Hidup Anda</em>, hal. 63"
        },
        "04": {
            "content": [
                "Komponen ego emosional berbeda pada masing-masing orang. Pada beberapa ego, ada yang lebih besar dibanding yang lainnya. Pikiran yang memancing respon emosional dalam tubuh mungkin terkadang datang begitu cepat sehingga sebelum pikiran sempat menyuarakannya, tubuh sudah langsung merespon dengan emosi, dan emosi berubah menjadi reaksi."
            ],
            "source": "Eckhart Tolle, <em>A New Earth, Dunia Baru – Kesadaran Akan Tujuan Hidup Anda</em>, hal. 149"
        },
        "05": {
            "content": [
                "Hampir semua tubuh manusia berada di bawah rasa tekanan dan stres yang sangat besar bukan karena diancam oleh faktor luar tetapi dari dalam pikiran."
            ],
            "source": "Eckhart Tolle, <em>A New Earth, Dunia Baru – Kesadaran Akan Tujuan Hidup Anda</em>, hal. 150"
        },
        "06": {
            "content": [
                "Jangan mengira bahwa mempraktikkan ajaran Buddha itu sulit. Hal tersebut tidaklah sulit; suatu hal yang mudah. Bisakah kamu bernapas masuk dan keluar serta menyadari bahwa kamu sedang bernapas masuk dan keluar? Bernapas masuk dan bernapas keluar—itulah perhatian penuh kesadaran. Berlatihlah menjadi sadar terhadap napasmu terlebih dahulu, maka kemudian kamu akan menjadi sadar terhadap keadaan tubuh dan pikiranmu; kamu akan menjadi sadar terhadap segala sesuatu yang sedang terjadi di sekitarmu."
            ],
            "source": "Thich Nhat Hanh, <em>Di Bawah Pohon Jambu Air</em>, hal. 43"
        },
        "07": {
            "content": [
                "Meskipun sampah baunya tidak sedap dan tidak menyenangkan, jika kamu mengetahui bagaimana cara mengolahnya, kamu dapat mengubahnya kembali menjadi bunga."
            ],
            "source": "Thich Nhat Hanh, <em>Di Bawah Pohon Jambu Air</em>, hal. 57"
        },
        "08": {
            "content": [
                "Ketika kita melihat bunga di dalam diri kita, kita bahagia, tetapi kita perlu berhati-hati, apabila kita tidak merawat bunga dengan baik, bunga akan segera menjadi seonggok sampah. Oleh sebab itu, kita belajar cara merawatnya sehingga bunga akan tinggal bersama kita dalam waktu yang lebih lama. Ketika bunga membusuk menjadi sampah, kita tidak takut, karena kita tahu bagaimana mengubah sampah menjadi mawar lagi. Jadi, ketika kamu mengalami kekacauan perasaan tidak keruan, jika kamu melihat secara mendalam perasaan itu, kamu akan melihat secuil benih kebahagiaan dan pembebasan di dalamnya. Dengan demikian transformasi terjadi."
            ],
            "source": "Thich Nhat Hanh, <em>Di Bawah Pohon Jambu Air</em>, hal. 65-66"
        },
        "09": {
            "content": [
                "Meditasi bukanlah merasakan sesuatu yang istimewa. Meditasi adalah merasakan apa yang anda rasakan. Bukan membuat pikiran menjadi kosong atau diam, walaupun ketenangan bisa diperoleh pada saat meditasi dan dapat dikembangkan dengan cara yang sistematis. Yang terpenting, meditasi adalah melepaskan pikiran apa adanya dan mengetahui bagaimana keadaannya pada momen ini."
            ],
            "source": "Jon Kabat-Zinn, <em>Wherever You Go, There You Are – Meditasi Perhatian Murni dalam Keseharian</em>, hal. 28"
        },
        "10": {
            "content": [
                "Melepas ibarat membuka telapak tangan anda untuk melepaskan sesuatu yang sedang anda genggam."
            ],
            "source": "Jon Kabat-Zinn, <em>Wherever You Go, There You Are – Meditasi Perhatian Murni dalam Keseharian</em>, hal. 47"
        },
        "11": {
            "content": [
                "Kesadaran diri tidak menghakimi. Kesadaran diri berarti kita telah menyelami misteri pikiran kita dan menjelajahi semua sudut pemikiran dan perasaan kita. Tak ada pikiran atau emosi yg diasingkan atau dianggap bukan bagian dari diri kita. Saya tidak menyarankan kita untuk melakukan setiap pemikiran dan perasaan kita. Yang saya maksud adalah kita harus memahami sepenuhnya diri kita sendiri dan menjadi manusia yang sadar."
            ],
            "source": "Diana Winston, <em>Wide Awake, Sadar Sepenuhnya – Panduan Buddhis Bagi Remaja</em>, hal. 40"
        },
        "12": {
            "content": [
                "Namun, Buddha menekankan bahwa tiap kali kita membandingkan diri dengan orang lain, kita menciptakan pengertian diri yang tetap—yaitu mengunci diri sendiri dalam kepercayaan tentang siapa diri kita. Buddha mengenali bahwa manusia terobsesi dengan “diri” sendiri dan “ego”. Saat kita membandingkan diri dengan orang lain, kita memiliki perasaan yang kuat dari “aku” atau “milikku” yang superior maupun inferior dan akibatnya membandingkan akan memperkuat perasaan kita atas diri sendiri atau ego. Bahkan saat kita merasa setara, masih ada perasaan aku yang timbul. Walau membandingkan dengan wajar, masalah timbul karena kita tidak melihat kenyataan siapa sebenarnya diri kita."
            ],
            "source": "Diana Winston, <em>Wide Awake, Sadar Sepenuhnya – Panduan Buddhis Bagi Remaja</em>, hal. 169–170"
        },
        "13": {
            "content": [
                "Kalau kita damai, kalau kita bahagia,<br />kita bisa mekar seperti setangkai bunga,<br />dan setiap orang di dalam keluarga kita,<br />seluruh masyarakat kita, akan menerima manfaat dari kedamaian kita."
            ],
            "source": "Thich Nhat Hanh, <em>Senantiasa Damai</em>, hal. 1"
        },
        "14": {
            "content": [
                "Napas masuk, kutenangkan tubuhku.<br />Napas keluar, aku tersenyum.<br />Berdiam dalam kekinian<br />aku tahu ini adalah momen yang menakjubkan."
            ],
            "source": "Thich Nhat Hanh, <em>Senantiasa Damai</em>, hal. 6"
        },
        "15": {
            "content": [
                "“Apa yang kita sebut dengan ‘saya’ hanyalah sebuah pintu yang mengayun; ia bergerak ketika kita menarik napas masuk dan menghempuskan napas keluar."
            ],
            "source": "Shunryu Suzuki, <em>Zen Mind Beginner’s Mind - Pikiran Zen Pikiran Pemula</em>, hal. 14"
        },
        "16": {
            "content": [
                "Orang-orang yang hanya terikat kepada hasil dari usaha mereka tidak akan mendapatkan kesempatan untuk menikmati hasilnya karena hasil itu bagi mereka tidak pernah ada."
            ],
            "source": "Shunryu Suzuki, <em>Zen Mind Beginner’s Mind - Pikiran Zen Pikiran Pemula</em>, hal. 136"
        },
        "17": {
            "content": [
                "Ajaran-ajaran Thay Nhat Hanh selama retret musim panas diberikan untuk anak-anak dan dewasa guna menciptakan kedamaian dalam diri mereka dan dunia. Aspek yang unik dari Plum Village sebagai pusat retret adalah berfokus pada anak-anak. Kami tidak memaksa mereka, tetapi anak-anak diundang bergabung dengan kami untuk meditasi duduk, meditasi jalan, meditasi teh (anak-anak mendapat meditasi lemon) untuk waktu yang pendek dan makan makanan dengan diam, dengan kesadaran dan apresiasi. Sebagai akibatnya, banyak anak-anak berlatih penuh kesadaran lebih baik daripada orangtua mereka dan ketika mereka kembali ke rumah, mereka mengingatkan orangtua mereka untuk kembali pada kekinian dan menikmati banyaknya keajaiban yang ada di sekeliling mereka."
            ],
            "source": "Sister Chan Kong, <em>Learning True Love – Pengamalan Ajaran Buddha di Masa Tersulit</em>, hal. 343"
        },
        "18": {
            "content": [
                "Menjadi seorang biksuni di Barat, saya tidak menggendong bayi-bayi yang kurang makan di dalam pelukan saya tetapi anak-anak remaja dan orang dewasa menangis dalam diam ketika mereka berbagi cerita-cerita tentang masa kanak-kanak mereka yang penuh kesedihan dan pelecehan. Mendengarkan dengan penuh perhatian rasa sakit mereka dan menolong mereka memperbarui diri mereka, saya mampu membantu memulihkan banyak dari 'anak-anak' yang terluka ini dan ini sangat dekat dengan cita-cita saya untuk menggendong anak-anak kampung. Saya bersyukur mampu menolong dengan cara ini."
            ],
            "source": "Sister Chan Kong, <em>Learning True Love – Pengamalan Ajaran Buddha di Masa Tersulit</em>, hal. 379"
        },
        "19": {
            "content": [
                "Katakanlah saya mendedikasikan hidup saya untuk satu hal: “Bunga ini sangat indah. Selama bunga ini hidup, hidup saya akan berarti. Jika bunga ini mati, maka saya juga ingin mati”. Jika saya bersikap seperti ini, saya bodoh, bukan? Tentunya bunga hanyalah suatu contoh, tetapi begitulah pandangan ekstrem dari pikiran materialistik. Pendekatan yang lebih realistis adalah: “Ya, bunga ini indah, namun tidak langgeng. Hari ini hidup, besok bunganya akan mati. Kebahagiaan saya tidak hanya tergantung dari bunga itu dan saya tidak dilahirkan hanya menikmati bunga”."
            ],
            "source": "Lama Yeshe, <em>Setenang Dasar Lautan</em>, hal. 128"
        },
        "20": {
            "content": [
                "Jika kita ingin menjalankan Dharma, bermeditasi, atau mengikuti jalan spiritual – lakukanlah dengan pemahaman. Janganlah melakukan sesuatu jika kita tidak mengerti apa yang kita lakukan dan mengapa kita melakukannya."
            ],
            "source": "Lama Yeshe, <em>Setenang Dasar Lautan</em>, hal. 165"
        },
        "21": {
            "content": [
                "Kita mungkin tetap mengatakan, “Saya menjalankan ajaran agama; saya bermeditasi. Saya melakukan ini; saya melakukan itu, saya berdoa, saya membaca buka Dharma.” Siapa saja bisa mengatakan, “Saya menjalankan ini, saya menjalankan itu,” namun apakah ada hubungan antara apa yang kita jalankan dengan kesadaran kita? Itulah yang harus kita cek. Apakah praktik kita menyelesaikan masalah-masalah mental kita, dan membawa realisasi sempurna serta prajna universal? Jika jawabannya “ya”, itu berarti praktik kita benar."
            ],
            "source": "Lama Yeshe, <em>Setenang Dasar Lautan</em>, hal. 169"
        },
        "22": {
            "content": [
                "Kebodohan adalah faktor lain yang menyebabkan orang-orang berpikiran salah dan berkelakuan buruk. Hal ini merendahkan derajat mereka sehingga setara dengan binatang buas yang bertindak hanya berdasarkan insting, tanpa kesadaran dan kebijaksanaan. Perlu bagi mereka untuk menerapkan kebijaksanaan dalam mengatasi masalah-masalah kehidupan sehari-hari mereka. Kapan pun mereka bertemu dengan pengemis, mereka harus berlatih welas asih dan kebijaksanaan. Tolonglah mereka sebisanya. Kalau mereka tidak bisa membantu pengemis itu, paling sedikit bersikaplah yang baik dan jangan mengusir mereka seolah-olah mereka anjing jalanan. (Phra Rajsuddhitanamongkol, 1993)"
            ],
            "source": "Sucitra Onkom, <em>Menciptakan Kedamaian Dunia yang Berkelanjutan</em>, hal. 135"
        },
        "23": {
            "content": [
                "Meditasi vipassana akan menuntun kita keluar dari kegelapan dunia dan kebiasaan-kebiasaan yang tidak terlatih, serta membebaskan kita dari tirani ‘tanpa kesadaran’, dimana melalui kebodohan, kita menjadi terkurung. Sejak saat kita berlatih kesadaran, ibaratnya seperti sebatang lilin yang dinyalakan untuk menerangi hidup kita, lilin yang bersinar terang dan membuat kita bisa menerima dan memahami dengan terang."
            ],
            "source": "Sucitra Onkom, <em>Menciptakan Kedamaian Dunia yang Berkelanjutan</em>, hal. 170"
        },
        "24": {
            "content": [
                "Nirwana tidak lagi merupakan sesuatu yang dicari di luar Samsara, dikatakan bahwa Nirwana tidak berlawanan dengan Samsara. Ketika ide tentang Kedemikianan membuka wawasan yang lebih luas bagi kaum Mahayana, Samsara seperti juga Nirwana menemukan tempat mereka di dalam Kedemikianan itu sendiri, dan Nirwana adalah Samsara dan Samsara adalah Nirwana."
            ],
            "source": "Beatrice Lane Suzuki, <em>Agama Buddha Mahayana</em>, hal. 43"
        },
        "25": {
            "content": [
                "Buddha bukannya tiga tetapi Satu. Trikaya tidak lain merupakan aspek dari Satu Buddha. Jika dipandang dari aspek Yang Absolut dan Universal, Beliau adalah Dharmakaya yang transenden; jika ditinjau dari aspek Idealitas, Beliau sebagai manusia dijadikan Ilahi, sebagaimana apa adanya, Beliau adalah Sambhogakaya, memberi khotbah kepada para Bodhisattwa untuk menolong mereka bekerja menyelamatkan makhluk-makhluk hidup; jika ditinjau dari aspek manusiawi, Beliau adalah Nirmanakaya, Sakyamuni yang menyejarah, lahir di Kapilawastu, mencapai Pencerahan di bawah pohon Bodhi, dan merealisasi Parinirwana setelah menyelesaikan misi-Nya dalam kehidupan."
            ],
            "source": "Beatrice Lane Suzuki, <em>Agama Buddha Mahayana</em>, hal. 48-49"
        },
        "26": {
            "content": [
                "Menyembuhkan tubuh dan kesadaran mempunyai satu persamaan dengan gagasan dari para pecinta lingkungan. Misalnya; ketika sungai tercemar, mereka mungkin berupaya memasukkan zat-zat penawar untuk menetralisir airnya. Tetapi pendekatan yang lebih langsung dan masuk akal adalah menghentikan aliran polutan ke dalam sungai tersebut. Jika hal ini dilakukan, seiring dengan berjalannya waktu, air yang mengalir melalui tanah, bebatuan, dan tanaman akan memurnikan sungai sepenuhnya. Dengan cara yang sama, daripada memakai teknik pernapasan khusus, kita cukup menghentikan pikiran-pikiran dan emosi-emosi yang bergejolak. Tak lama kemudian, anda akan mendapati bahwa aliran napas yang wajar telah terpulihkan secara alami."
            ],
            "source": "B. Allan Wallace, Ph.D., <em>Merevolusi Ketajaman Perhatian - Menyingkap Mekanisme Kesadaran</em>, hal. 35"
        },
        "27": {
            "content": [
                "Dalam <em>samatha</em>, anda hampir tidak melakukan apa-apa. Secara pasif, anda memperhatikan sensasi-sensasi napas tanpa mengaturnya dengan cara apa pun. Ego anda hampir tidak ada selagi anda membiarkan tubuh bernapas dengan sendirinya secara wajar, dengan hanya mengerahkan upaya yang lebih halus untuk menyeimbangkan perhatian ketika perhatian terjatuh ke dalam keloyoan atau gejolak."
            ],
            "source": "B. Allan Wallace, Ph.D., <em>Merevolusi Ketajaman Perhatian - Menyingkap Mekanisme Kesadaran</em>, hal. 81"
        },
        "28": {
            "content": [
                "“Sebuah sistem ekonomi yang menghargai Bumi adalah juga sistem yang meningkatkan kualitas hidup manusia. (Pendidikan Alami) dari ekonomi memandang manusia dan Bumi bukan sebagai kompetitor melainkan sebagai partisipan dalam suatu hubungan yang saling mempertahankan kelangsungannya”."
            ],
            "source": "Lloyd Field, Ph.D, <em>Business and the Buddha – Berhasil dengan Berbuat Baik</em>, hal. 136"
        },
        "29": {
            "content": [
                "Menjalani hidup dengan mengurangi materialisme dan konsumerisme—hidup lebih sederhana—menghasilkan lebih sedikit kegelisahan mental dan lebih banyak waktu untuk proses yang tidak begitu mudah; mengembangkan pikiran."
            ],
            "source": "Lloyd Field, Ph.D, <em>Business and the Buddha – Berhasil dengan Berbuat Baik</em>, hal. 146"
        },
        "30": {
            "content": [
                "Kesalahan arah hidup secara massal ini menyebabkan kita menciptakan suatu masyarakat dunia yang terjangkit oleh penyakit-penyakit sosial: konflik, perang, terorisme global, pergolakan ekonomi besar-besaran, penebangan hutan, kepunahan satwa hingga pemanasan global. Akibatnya, setiap manusia, bersama dengan hewan dan tumbuhan di planet ini berbagi penderitaan yang sama."
            ],
            "source": "Supawan P. Panawong Green, <em>Panduan Hidup – Diet Moral</em>, hal. 13"
        }
    },
    "10": {
        "01": {
            "content": [
                "Secara alami, alam telah memberikan kita suatu hadiah yang berharga, yang disebut kesadaran. Alat penting ini secara alami mencegah kita melangkah melampaui batasan moral kita karena ia mematok pikiran kita pada landasan etika."
            ],
            "source": "Supawan P. Panawong Green, <em>Panduan Hidup – Diet Moral</em>, hal. 17"
        },
        "02": {
            "content": [
                "Semua makhluk hidup adalah sahabat penderitaan, yang rentan terhadap keadaan sulit. Terhadap setumpukan sampah perbuatan buruk dalam pikiran manusia pada umumnya; untungnya terdapat juga gudang kebajikan yang menunggu untuk dibuka. Pilihan mengembangkan kebajikan atau menyerah pada kejahatan benar-benar terletak di tangan sendiri."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Hidup Sukses dan Bahagia - Tanpa Takut dan Cemas</em>, hal. 75"
        },
        "03": {
            "content": [
                "Perubahan suasana hati tercermin di wajah kita. Kenali hal ini dengan memandangi cermin. Inilah cara yang baik mengobati diri kita akibat suasana hati yang terus menerus berubah. Cara ini membuat kita tertawa jika kita melihat buruknya wajah kita dalam keadaan marah. Sebaliknya, tersenyumlah dan otot wajah akan mengendur sehingga membangkitkan semangat dan membuat kita gembira."
            ],
            "source": "Dr. K. Sri Dhammananda, <em>Hidup Sukses dan Bahagia - Tanpa Takut dan Cemas</em>, hal. 97"
        },
        "04": {
            "content": [
                "Kita tidak tahu mengapa doa kadang-kadang efektif dan kadang-kadang tidak. Tetapi terlepas dari semua ini, timbul pertanyaan lainnya: “Apabila Tuhan atau kekuatan apa pun diri kita telah menentukan takdir, lalu apa gunanya berdoa?”"
            ],
            "source": "Thich Nhat Hanh, <em>Kekuatan Di Balik Energi Doa - Cara Memperdalam Praktik Spiritual Anda</em>, hal. 8"
        },
        "05": {
            "content": [
                "Kita dan Tuhan bukanlah dua eksistensi yang terpisah. Oleh karena itu, kehendak Tuhan adalah kehendak kita juga. Bila kita ingin berubah, Tuhan tidak akan menghalanginya."
            ],
            "source": "Thich Nhat Hanh, <em>Kekuatan Di Balik Energi Doa - Cara Memperdalam Praktik Spiritual Anda</em>, hal. 10"
        },
        "06": {
            "content": [
                "Bagaimanakah kita seharusnya berdoa? Kita berdoa dengan mulut dan pikiran kita, namun ini masih belum cukup. Kita hendaknya berdoa dengan segenap tubuh, ucapan, dan pikiran kita. Tubuh, ucapan, dan pikiran dapat menjadi satu dengan bantuan kekuatan perhatian penuh kesadaran. Dalam keadaaan kemanunggalan tiga komponen tersebut, kita sanggup membangkitkan energi keyakinan dan cinta kasih yang diperlukan untuk mengubah situasi sulit."
            ],
            "source": "Thich Nhat Hanh, <em>Kekuatan Di Balik Energi Doa - Cara Memperdalam Praktik Spiritual Anda</em>, hal. 29"
        },
        "07": {
            "content": [
                "“Biksu, ada dua orang yang saya katakan tidak mudah dibalas budinya. Siapakah kedua orang itu? Mereka adalah ayah dan ibumu. Bahkan jika seorang anak menggendong ibunya di satu bahu dan ayahnya di bahu yang lain, dan melayani mereka selama seratus tahun, memandikan mereka, memijati mereka, dan menerima kotoran serta air seni mereka saat duduk di bahunya, itu masih belum cukup untuk membayar utang budinya kepada mereka. Bahkan jika seorang anak mempersembahkan harta kekayaan yang demikian melimpah dan memberikan mereka kekuasaan atas satu kerajaan yang dipenuhi dengan tujuh harta berharga, itu masih belum cukup untuk membayar utang budinya. Mengapa demikian? Karena seorang ibu dan seorang ayah memiliki jasa budi yang sangat banyak; mereka membesarkan anak mereka dan mengajari mereka tentang hal-hal di dunia ini. Seorang anak yang memberi semangat, memberikan pengertian dan menyebabkan seorang ibu dan seorang ayah yang tidak memiliki keyakinan menjadi penuh keyakinan, yang memiliki perilaku moral buruk menjadi berperilaku moral yang baik... yang kikir menjadi murah hati... yang tidak memiliki kebijaksanaan menjadi penuh kebijaksanaan, dengan melakukan hal-hal itulah, dia telah membayar utangnya kepada orang tuanya.”"
            ],
            "source": "Suchitra Onkom, <em>Legenda Pak Tua Tiow - Berdasar Kisah Nyata</em>, hal. 37"
        },
        "08": {
            "content": [
                "Sesungguhnya, pencerahan itu cukup sederhana. Bayangkan mengenai sebuah kebiasaan berjalan melewati sebuah ruangan yang gelap, dan kita menabrak meja, kursi, dan perabotan-perabotan lainnya. Suatu hari, karena beruntung atau kebetulan, kita meraba-raba dinding dan menekan tombol yang menyalakan lampu. Tiba-tiba, kita melihat seluruh ruangan dan perabotan yang ada di dalamnya, dinding-dindingnya, karpetnya dan berpikir, “Lihat semua barang yang ada di sini! Pantas saja saya selalu menabrak sesuatu!” Dan ketika memperhatikan barang-barang tersebut, dengan sedikit rasa penasaran untuk pertama kalinya, kita menyadari bahwa tombol lampunya selama ini ternyata ada di sana. Kita hanya tidak mengetahuinya, atau mungkin kita tidak berpikir tentang kemungkinan apa saja menyangkut ruangan tersebut selain dari gelap. Itu adalah salah satu cara untuk menjelaskan pencerahan: menyalakan lampu di sebuah ruangan di mana kita menghabiskan hampir seluruh waktu kita berjalan di dalam kegelapan."
            ],
            "source": "Yongey Mingyur Rinpoche &amp; Eric Swanson, <em>Kebijaksanaan yang Membahagiakan</em>, hal. 41"
        },
        "09": {
            "content": [
                "Beberapa tahun yang lalu saya berjalan kaki di India, di mana banyak jalannya masih berbatu. Saya pergi terburu-buru sehingga saya melupakan sandal saya—sebuah keputusan yang kemudian saya sesali karena berjalan kaki tanpa alas kaki di jalanan berbatu bukanlah sebuah pengalaman yang menyenangkan. Tidak lama setelah itu, saya menjelaskan pengalaman saya ini kepada seorang dokter India.<br />“Oh, bagus sekali,” jawabnya.<br />Ketika saya menanyakan lebih lanjut apa artinya, ia menjelaskan bahwa menurut beberapa sistem pengobatan kuno, menekan beberapa poin di telapak kaki menstimulasi aktivitas dari berbagai sistem dan organ, sehingga hal ini mendukung kesehatan. Mereka-mereka yang mengenal refleksiologi mungkin sudah memahami potensi manfaat yang dihasilkan dari latihan ini; tetapi untuk saya, ini adalah sebuah gagasan yang baru. Setelah mendengarkan penjelasan si dokter, saya mulai lebih sering berjalan kaki tanpa alas kaki. Saya terkejut, bukannya merasa tidak nyaman, saya malah mulai merasakan kenikmatan dari sensasi batu-batu pada kaki saya.<br />Mengapa?<br />Batu-batunya tidak berubah. Kaki saya tidak berubah. Cara jalan saya juga tidak berubah.<br />Ketika saya merenungkannya, saya menyadari bahwa satu-satunya aspek pengalaman yang telah berubah adalah sudut pandang saya."
            ],
            "source": "Yongey Mingyur Rinpoche &amp; Eric Swanson, <em>Kebijaksanaan yang Membahagiakan</em>, hal. 76"
        },
        "10": {
            "content": [
                "Menyadari bahwa tubuh ini (rapuh) seperti tempayan; memperkuat pikiran ini (kokoh) seperti benteng kota; hendaklah ia menumpas Mara dengan senjata kebijaksanaan; hendaknya ia menjaga apa yang telah dicapainya, dan hidup tanpa ikatan lagi."
            ],
            "source": "Narada Mahathera, <em>Ilustrasi Dhammapada - Menelusuri Jejak Langkah Buddha</em>, hal. 55"
        },
        "11": {
            "content": [
                "Daripada seribu ucapan tak berguna, lebih baik sepatah kalimat bermanfaat, yang memberikan kedamaian bagi pendengarnya."
            ],
            "source": "Narada Mahathera, <em>Ilustrasi Dhammapada - Menelusuri Jejak Langkah Buddha</em>, hal. 137"
        },
        "12": {
            "content": [
                "Untuk membangkitkan pengertian secara nyata, anda harus berlatih memandang semua makhluk hidup dengan mata kasih sayang. Ketika anda mengerti, anda akan mengasihi."
            ],
            "source": "Thich Nhat Hanh, <em>Being Peace</em>, hal. 21"
        },
        "13": {
            "content": [
                "Meditasi adalah berusaha melihat segala sesuatu secara mendalam, melihat bagaimana kita bisa berubah, bagaimana kita bisa mengubah situasi. Mengubah situasi berarti mengubah pikiran. Mengubah pikiran berarti mengubah situasi, karena situasi adalah pikiran dan pikiran adalah situasi."
            ],
            "source": "Thich Nhat Hanh, <em>Being Peace - Senantiasa Damai</em>, hal. 73"
        },
        "14": {
            "content": [
                "Ketika kita bersentuhan dengan pikiran sejati kita, sumber pengertian dan kasih akan memancar keluar. Inilah dasar dari segalanya. Bersentuhan dengan pikiran sejati kita diperlukan demi kelangsungan karir yang telah dimulai para Buddha dan para Bodhisattwa."
            ],
            "source": "Thich Nhat Hanh, <em>Being Peace - Senantiasa Damai</em>, hal. 99"
        },
        "15": {
            "content": [
                "Pikiran seperti seekor monyet yang bergelantungan dari satu dahan ke dahan lain di dalam hutan, demikian dijelaskan di dalam Sutra. Agar tidak kehilangan jejak si monyet karena ia tiba-tiba bergerak, kita harus terus menerus mengawasi dia dan bahkan menyatu dengannya. Pikiran yang mengamati pikiran sama seperti sebuah objek dan bayangannya—si objek tidak bisa menghilangkan bayangannya. Keduanya adalah satu. Ke mana pun pikiran pergi, ia masih tetap menyatu dengan pikiran. Kadang-kadang di dalam Sutra menggunakan istilah “mengikat si monyet” untuk menjelaskan kesadaran terhadap pikiran.Tetapi bayangan monyet hanyalah sebuah contoh. Ketika pikiran sudah langsung dan senantiasa waspada terhadap dirinya sendiri, ia tidak lagi seperti seekor monyet. Tidak ada lagi dua pikiran, yang satu bergelayutan dari satu dahan ke dahan lain dan yang satu lagi mengikutinya untuk mengikatnya dengan seutas tali."
            ],
            "source": "Thich Nhat Hanh, <em>Keajaiban Hidup Sadar</em>, hal. 50"
        },
        "16": {
            "content": [
                "Ketika duduk diam dan bernapas dengan perlahan, bayangkan diri anda seperti sebuah batu kerikil yang jatuh ke aliran air jernih. Sambil tenggelam, tidak ada keinginan untuk menyetir gerakan. Tenggelam menuju titik istirahat total di dasar sungai berpasir halus. Teruskan menuju titik istirahat total di dasar sungai berpasir halus. Teruskan meditasi pada batu kerikil hingga pikiran dan tubuh anda istirahat total, seperti sebutir kerikil berdiam total di atas pasir. Pertahankan kedamaian dan kebahagiaan ini selama setengah jam sambil mengamati napas."
            ],
            "source": "Thich Nhat Hanh, <em>Keajaiban Hidup Sadar</em>, hal. 102"
        },
        "17": {
            "content": [
                "Seperti halnya kesadaran pikiran dan kesadaran indra, kesadaran terpendam pun mengonsumsi. Ketika kamu berada di tengah sekelompok orang, meskipun kamu ingin menjadi dirimu sendiri, kamu akan mengonsumsi cara-cara mereka, dan kamu juga mengonsumsi kesadaran terpendam mereka. Kesadaran kita juga diisi dengan kesadaran orang-orang lain. Cara kita membuat keputusan, kesukaan atau ketidaksukaan kita, tergantung pada cara kita bersama melihat sesuatu. Kamu mungkin melihat sesuatu itu tidak bagus, tetapi ketika banyak orang berpendapat bahwa sesuatu itu bagus, maka pelan-pelan kamu juga akan menerima bahwa hal itu memang bagus, karena kesadaran individu juga terbentuk oleh kesadaran kolektif."
            ],
            "source": "Thich Nhat Hanh, <em>Buddha Mind, Buddha Body - Pikiran Buddha, Tubuh Buddha – Melangkah Menuju Pencerahan</em>, hal. 11"
        },
        "18": {
            "content": [
                "Saat yang kita sebut kelahiran sebenarnya adalah saat kelanjutan. Karena sebelumnya dia telah ada, walau dalam bentuk yang berbeda. Oleh karena itu tidak ada yang akan mati, karena sesungguhnya tidak ada yang lahir. Pada saat pembuahan di rahim ibu kita bukanlah saat pertama kalinya kita ada. Kita telah ada sebelumnya dalam diri ayah dan ibu kita, dalam leluhur kita. Kita tidak berasal dari ketiadaan. Kita adalah suatu kelanjutan. Seperti aliran air di bumi ini, adalah kelanjutan dari awan di langit. Aliran air tidak pernah dilahirkan. Itu hanyalah kelanjutan dari awan.",
                "Ketika kita mendengar kata ‘mati’ banyak di antara kita merasa takut, karena kita mengira bahwa kematian adalah lenyapnya diri. Kematian diartikan dari ada kamu menjadi tiada, yang semula adalah kamu menjadi bukan siapa-siapa. Tetapi kita sebenarnya seperti awan, dan awan tidak mungkin mati. Awan mungkin berubah menjadi hujan atau salju, es, atau air. Tetapi tidak mungkin bagi awan untuk menjadi bukan apa-apa."
            ],
            "source": "Thich Nhat Hanh, <em>Buddha Mind, Buddha Body - Pikiran Buddha, Tubuh Buddha – Melangkah Menuju Pencerahan</em>, hal. 106-107"
        },
        "19": {
            "content": [
                "Mendengarkan secara mendalam, mendengarkan dengan welas asih bukanlah mendengarkan dengan tujuan untuk menganalisis atau bahkan mencari tahu apa yang telah terjadi di masa lalu. Pertama-tama kamu mendengarkan adalah untuk memberikan kelegaan kepada orang itu, suatu kesempatan untuk berbicara, untuk merasa bahwa seseorang akhirnya memahami dia."
            ],
            "source": "Thich Nhat Hanh, <em>Anger - Memadamkan Api Kemarahan Lewat Kearifan Buddhis</em>, hal. 90"
        },
        "20": {
            "content": [
                "Jika kamu tidak tahu bagaimana memperlakukan dirimu sendiri dengan welas asih, bagaimana kamu bisa memperlakukan orang lain dengan welas asih? Ketika kemarahan timbul, teruslah berlatih jalan dan napas berkesadaran untuk menghasilkan energi perhatian penuh kesadaran. Teruslah rangkul dengan lembut energi kemarahan di dalam dirimu. Kemarahan mungkin tetap ada di sana untuk beberapa lama, tetapi kamu sudah aman, karena Buddha ada di dalam dirimu, membantumu untuk merawat kemarahanmu dengan baik."
            ],
            "source": "Thich Nhat Hanh, <em>Anger - Memadamkan Api Kemarahan Lewat Kearifan Buddhis</em>, hal. 165"
        },
        "21": {
            "content": [
                "Kegembiraan dan kesenangan anak-anak di dunia ini sangatlah sepele dan pilihan serta kecenderungan mereka hanya menyingkapkan kebijaksanaan kecil. Mereka bersikap mirip anak kecil yang menemukan sebuah kelereng yang sangat cantik dengan bintik hijau di atasnya, dan karena sangat gembira serta untuk memastikan ia tidak akan kehilangan lagi, seketika itu juga ditelannya kelereng itu, dan akibatnya isi perutnya harus dikeluarkan. Lagi pula, siapa yang tak akan takut jika menyadari bahwa ia sendiri menyembunyikan segala penderitaan dan teror dengan memiliki tubuh jasmani! Penderitaan tanpa akhir dalam lingkaran kelahiran demi kelahiran kembali yang sia-sia (Samsara), inilah nasib orang biasa dan untuk mengubahnya, kita harus berpacu menuju keselamatan. Petapa-petapa Buddhis adalah orang yang takut akan kelahiran dan kematian, sehingga meninggalkan rumah untuk memperoleh penyelamatan."
            ],
            "source": "Edward Conze, <em>Sejarah Singkat Agama Buddha</em>, hal. 10"
        },
        "22": {
            "content": [
                "Dari sudut lain kita bisa mengatakan bahwa semua kesengsaraan kita berasal dari kebiasaan yang ingin mengambil bagian dari alam semesta ini sebagai “milik kita” sendiri, dan menyatakan sebanyak mungkin, bahwa “ini milikku, akulah ini, inilah diriku”."
            ],
            "source": "Edward Conze, <em>Sejarah Singkat Agama Buddha</em>, hal. 11"
        },
        "23": {
            "content": [
                "Selama kita menginginkan lebih banyak, lebih baik dan hal-hal yang berbeda, kita tidak akan pernah puas dengan apa pun yang kita miliki."
            ],
            "source": "Thubten Chodron, <em>Membuka Hati, Menjernihkan Pikiran</em>, hal. 27"
        },
        "24": {
            "content": [
                "Jika kita memang melakukan kesalahan dan seseorang menunjukkannya, mengapa harus marah? "
            ],
            "source": "Thubten Chodron, <em>Membuka Hati, Menjernihkan Pikiran</em>, hal. 46"
        },
        "25": {
            "content": [
                "Akui kelemahan dengan jujur dan berusahalah perbaiki."
            ],
            "source": "Thubten Chodron, <em>Membuka Hati, Menjernihkan Pikiran</em>, hal. 60"
        },
        "26": {
            "content": [
                "Setiap kali anda merasa tersesat, terasing, terbuang dari kehidupan atau dunia, setiap kali anda merasa putus asa, marah, atau terguncang, anda harus tahu bagaimana praktiknya pulang ke rumah. Bernapas dengan kesadaran adalah alat yang biasa anda gunakan untuk kembali ke rumah anda yang sejati di mana anda bertemu dengan Buddha, Dharma, dan Sanggha. Bernapas dengan kesadaran membuat anda ada di rumah, memompa energi kesadaran dalam diri anda. Kesadaran adalah substansi dari Buddha."
            ],
            "source": "Thich Nhat Hanh, <em>Going Home, Pulang ke Rumah – Membawa Kristus dan Buddha Bersama dalam Hidup Keseharian</em>, hal. 50"
        },
        "27": {
            "content": [
                "Kita belajar dalam agama Buddha bahwa sesuatu yang negatif berguna untuk menghasilkan yang positif. Seperti sampah. Jika anda tahu bagaimana mengelola sampah, anda akan mampu untuk menghasilkan bunga dan sayuran dari sampah tersebut. Sampah bisa dijadikan kompos, dan kompos amat esensial untuk bunga dan sayuran."
            ],
            "source": "Thich Nhat Hanh, <em>Going Home, Pulang ke Rumah – Membawa Kristus dan Buddha Bersama dalam Hidup Keseharian</em>, hal. 103"
        },
        "28": {
            "content": [
                "Akibat yang serupa terjadi dengan tindakan baik kita, yang akan menghasilkan respon yang baik pula dari dunia. Jika kita memperbesar perbuatan-perbuatan baik kita, hasil yang baik juga akan diperbesar. Untuk perbuatan baik, sistem pembesarannya adalah apa yang diistilahkan sebagai “Ladang Kebajikan”."
            ],
            "source": "Phra Bhasakorn Bhavilai &amp; David Freyer, <em>Karma – Wacana Baru Mengenai Konsep Sebab Akibat Buddhis</em>, hal. 43"
        },
        "29": {
            "content": [
                "Dan apakah sumber dari kemelekatan kita? Dia datang dari nilai salah yang kita tambahkan pada objek. Lalu, dari mana nilai yang salah itu muncul? Dari pandangan kita yang salah terhadap alam, ketidaktahuan kita, kegagalan kita untuk melihat kenyataan sebagaimana adanya. Ketidaktahuan adalah rantai utama yang menuntun ke arah penderitaan."
            ],
            "source": "Phra Bhasakorn Bhavilai &amp; David Freyer, <em>Karma – Wacana Baru Mengenai Konsep Sebab Akibat Buddhis</em>, hal. 85"
        },
        "30": {
            "content": [
                "Jika saya menaruh tangan saya di dalam api, batin sayalah, bukan tangan saya, yang merasakan sakit."
            ],
            "source": "Phra Bhasakorn Bhavilai &amp; David Freyer, <em>Karma – Wacana Baru Mengenai Konsep Sebab Akibat Buddhis </em>, hal. 87"
        },
        "31": {
            "content": [
                "Rasa sakit hati, lalu membalas perbuatan jahat yang dilakukan kepada kita, serta mengasihani diri sendiri, adalah reaksi yang bisa dipahami. Tetapi yang diperoleh dari semua itu, dalam tataran karma, akan mengakibatkan penderitaan lebih lanjut di masa depan. Sebaliknya, dengan mengendalikan diri terlepas dari sikap egoisme, maka kesulitan paling traumatik sekalipun dapat dihadapi dengan ketenangan, yang jauh lebih berharga."
            ],
            "source": "David Michie, <em>Menemukan Kebahagiaan di Dunia yang Tak Pasti – Agama Buddha untuk Orang Sibuk</em>, hal. 91"
        }
    },
    "11": {
        "01": {
            "content": [
                "Dengan kejujuran tak tergoyahkan, kita perlu mengunjungi kembali saat-saat tergelap dalam kehidupan kita dan bertanya pada diri sendiri, berapa banyak ketidakbahagiaan saya akibat terlalu banyak memikirkan diri sendiri? Akankah saya diuntungkan dengan terlibat secara positif dengan orang lain? Sebaliknya, bagaimana dengan masa-masa ketika saya mengalami kebahagiaan mendalam serta sepenuh hati? Apa yang menjadi kepedulian saya saat itu, dan dapatkah mencipta ulang keadaan itu? "
            ],
            "source": "David Michie, <em>Menemukan Kebahagiaan di Dunia yang Tak Pasti – Agama Buddha untuk Orang Sibuk</em>, hal. 182-183"
        },
        "02": {
            "content": [
                "Semakin anda reaktif, maka anda semakin terjerat atau terbelit dengan bentuk atau perwujudan. Semakin terindentifikasi dengan bentuk atau perwujudan, maka ego semakin kuat."
            ],
            "source": "Eckhart Tolle, <em>Harmoni dengan Segala Kehidupan - Koleksi Inspirasional dari A New Earth</em>, hal. 20"
        },
        "03": {
            "content": [
                "Kesadaran spiritual adalah melihat secara jernih bahwa apa yang saya persepsi, alami, pikir, atau rasakan, pada ujungnya bukanlah ‘siapa diri saya?’"
            ],
            "source": "Eckhart Tolle, <em>Harmoni dengan Segala Kehidupan - Koleksi Inspirasional dari A New Earth</em>, hal. 116"
        },
        "04": {
            "content": [
                "Saya dan kehidupan adalah satu. Tidak mungkin sebaliknya. Jadi bagaimana saya bisa kehilangan kehidupan saya? Bagaimana mungkin saya kehilangan sesuatu hal yang sejak awal tidak saya miliki? "
            ],
            "source": "Eckhart Tolle, <em>Harmoni dengan Segala Kehidupan - Koleksi Inspirasional dari A New Earth</em>, hal. 136"
        },
        "05": {
            "content": [
                "Dengan usahamu sendiri bangunkan dirimu, perhatikan dirimu, dan hiduplah dengan sukacita."
            ],
            "source": "Jack Maguire, <em>Bangun! Bangun! - Seminggu dalam Biara Zen</em>, hal. 173"
        },
        "06": {
            "content": [
                "Mempelajari jalan Buddha adalah mempelajari diri sendiri.<br />Mempelajari diri sendiri adalah melupakan diri sendiri.<br />Melupakan diri sendiri adalah dicerahkan oleh 10.000 hal lainnya."
            ],
            "source": "Jack Maguire, <em>Bangun! Bangun! - Seminggu dalam Biara Zen</em>, hal. 205"
        },
        "07": {
            "content": [
                "Memarahi orang yang memarahi kita, serupa dengan mengejar orang yang melemparkan api ke rumah kita yang berisi bensin. Setelah balik dari mengejar, rumah sudah habis terbakar."
            ],
            "source": "Gede Prama, <em>Setenang Pepohonan, Selembut Rerumputan</em>, hal. 38"
        },
        "08": {
            "content": [
                "Banyak sekali manusia yang menyukai perhiasan. Tapi perhiasan terindah bernama kesabaran. Terutama karena kesabaran membuat kehidupan jadi anggun menawan."
            ],
            "source": "Gede Prama, <em>Setenang Pepohonan, Selembut Rerumputan</em>, hal. 109"
        },
        "09": {
            "content": [
                "Kebajikan kadang diikuti kesialan.Tapi kesialan bukan alasan untuk menghentikan kebajikan. Terutama karena pencerahan memerlukan dua syarat, tabungan kebajikan berlimpah (<em>accumulation of merit</em>) serta simpanan kebijaksanaan yang tidak terhingga (<em>accumulation of wisdom</em>)."
            ],
            "source": "Gede Prama, <em>Setenang Pepohonan, Selembut Rerumputan</em>, hal. 206"
        },
        "10": {
            "content": [
                "Terikat pada orang yang dicintai akan membuat anda tertarik dalam pusaran air yang berputar. Membenci musuh anda akan membuat anda terbakar oleh api. Dalam gelapnya kebingungan, anda lupa apa yang harus dijalani dan apa yang tidak.",
                "Lepaskanlah kebiasaan anda - Ini adalah praktik Bodhisattwa."
            ],
            "source": "Thubten Chodron, <em>37 Praktik Boddhisattwa</em>, hal. 61"
        },
        "11": {
            "content": [
                "Terpenjara dalam lingkaran kelahiran kembali. Dewa mana di dunia yang dapat memberikan anda perlindungan?<br />Dengan demikian ketika anda mencari perlindungan, berlindunglah pada Tiga Permata yang tidak akan mengkhianatimu - Ini adalah praktik para bodhisattwa."
            ],
            "source": "Thubten Chodron, <em>37 Praktik Boddhisattwa</em>, hal. 105"
        },
        "12": {
            "content": [
                "Semua bentuk penderitaan adalah seperti menghadapi kematian anak di dalam mimpi.<br />Menggenggam penampakan yang sepertinya nyata akan membuat anda khawatir.<br />Oleh karena itu ketika anda menemukan keadaan yang tidak menyenangkan, pandanglah semua itu seperti suatu ilusi - Ini adalah praktik para bodhisattwa."
            ],
            "source": "Thubten Chodron, <em>37 Praktik Boddhisattwa</em>, hal. 219"
        },
        "13": {
            "content": [
                "Sulit sekali mendapatkan ketenteraman dan kekayaan,<br />Apalagi mendapat kelahiran sebagai manusia yang sangat berarti ini!<br />Kalau sekarang saya gagal untuk memanfaatkannya,<br />Bagaimana mungkin saya mendapatkan kesempatan semacam ini lagi?"
            ],
            "source": "Pema Codron, <em>Tiada Waktu Yang Boleh Tersia-siakan - Pedoman Menuju Jalan Praktik Bodhisattwa</em>, hal. 7"
        },
        "14": {
            "content": [
                "Memahami kehampaan, adalah memahami semua Dharma, memahami kehampaan adalah menyaksikan apa yang mendatangkan ketenteraman dan kedamaian, memahami kehampaan berarti mengetahui bahwa 'segala fenomena' adalah baik."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Inti-Pokok Pohon Bodhi - Ajaran Buddha tentang Sunyata</em>, hal. xiii"
        },
        "15": {
            "content": [
                "Bilamana pikiran hampa dari keserakahan, kebencian, dan delusi, dia akan benar-benar hampa, dan semua dukkha akan berakhir. Bahkan tindakan (Kamma) akan dengan sendirinya berakhir."
            ],
            "source": "Buddhadasa Bhikkhu, <em>Inti-Pokok Pohon Bodhi - Ajaran Buddha tentang Sunyata</em>, hal. 93"
        },
        "16": {
            "content": [
                "Pemikiran penghormatan yang diarahkan kepada objek mulia, adalah sebuah <em>kamma</em> berguna yang membangkitkan jasa kebaikan di dalam arus mental dari orang yang memunculkan pemikiran tersebut. Saat penghormatan tadi diarahkan kepada objek-objek puja yang mulia, yaitu Tiga Mestika, maka jasa kebajikan yang dibangkitkannya akan besar serta penuh kekuatan. Jasa kebajikan seperti itu, yang diakumulasi di dalam batin, akan memiliki kapasitas untuk melawan rintangan-rintangan menuju ke pemenuhan kewajiban suci seseorang, serta mendukung kesuksesan penyelesaiannya."
            ],
            "source": "Bhikkhu Bodhi dan U Rewata Dhamma, <em>Panduan Komprehensif tentang Abhidhamma – Etika, Psikologi, dan Filsafat Ajaran Buddha</em>, hal. 4"
        },
        "17": {
            "content": [
                "<em>Abhidhamma</em> tidak hanya memilah tipe-tipe kesadaran, tetapi yang lebih penting lagi, <em>Abhidhamma</em> memperagakan keteraturan <em>citta</em> di dalam sebuah kosmos (satu kesatuan yang saling-berjalin erat)."
            ],
            "source": "Bhikkhu Bodhi dan U Rewata Dhamma, <em>Panduan Komprehensif tentang Abhidhamma – Etika, Psikologi, dan Filsafat Ajaran Buddha</em>, hal. 13"
        },
        "18": {
            "content": [
                "Selama anda terus lari dari penderitaan, anda akan terus menderita."
            ],
            "source": "Thich Nhat Hanh, <em>Jawaban dari Hati - Jawaban Praktis terhadap Pertanyaan-pertanyaan Mendesak dalam Kehidupan</em>, hal. 2"
        },
        "19": {
            "content": [
                "Jika kedua orangtua berlatih perhatian penuh kesadaran dan welas asih dalam kehidupan sehari-hari, maka anak-anak secara alami akan belajar dari mereka."
            ],
            "source": "Thich Nhat Hanh, <em>Jawaban dari Hati - Jawaban Praktis terhadap Pertanyaan-pertanyaan Mendesak dalam Kehidupan</em>, hal. 29-30"
        },
        "20": {
            "content": [
                "Para biksu, inilah jalan langsung untuk mempurifikasi para makhluk, untuk mengatasi penderitaan dan ratapan, untuk menghilangkan <em>dukkha</em> dan penolakan, untuk memperoleh cara sesungguhnya, untuk merealisasi <em>Nibbana</em>, yakni empat <em>Satipatthana</em>."
            ],
            "source": "Bhikkhu Analayo, <em>Satipatthana – Jalan Langsung ke Tujuan</em>, hal. 39"
        },
        "21": {
            "content": [
                "Seandainya seseorang mengembangkan empat satipatthana ini sedemikian rupa selama tujuh tahun... enam tahun... lima tahun... empat tahun... tiga tahun... dua tahun... satu tahun... tujuh bulan... enam bulan... lima bulan... empat bulan... tiga bulan... dua bulan... satu bulan... setengah bulan... tujuh hari, makan salah satu dari kedua hasil dapat terealisasi yaitu: pengetahuan sempurna di sini dan sekarang atau Anagami."
            ],
            "source": "Bhikkhu Analayo, <em>Satipatthana – Jalan Langsung ke Tujuan</em>, hal. 363"
        },
        "22": {
            "content": [
                "Seperti halnya, induk ayam yang mengerami telur tanpa lelah suatu saat akan menyebabkan telur menetas, demikian pula praktik yang dijalankan terus-menerus suatu saat akan menghasilkan realisasi."
            ],
            "source": "Bhikkhu Analayo, <em>Satipatthana – Jalan Langsung ke Tujuan</em>, hal. 367"
        },
        "23": {
            "content": [
                "Gunung, sungai, bumi yang luas, sepuluh ribu fenomena–semuanya berbicara. Dapatkah anda mendengarkan mereka? Master Dongshan mengatakan, “Jika mendengarnya dengan telinga dan melihatnya dengan mata, anda tidak akan pernah menangkapnya. Hanya ketika anda mendengar dengan mata dan melihat dengan telinga, barulah anda bisa benar-benar memahaminya.” Bagaimana anda mendengar dengan mata? Bagaimana anda melihat dengan telinga? "
            ],
            "source": "John Doido Loori, <em>Sarang Macan – Praktik Nyata Zen dalam Wujud Pertarungan Dharma (Percakapan Dharma)</em>, hal. 52"
        },
        "24": {
            "content": [
                "Kalau anda terdelusi, maka apa yang harus dikerjakan selalu dilihat menyangkut diri individual dan berpusat pada diri. Kalau anda tidak terdelusi, maka apa yang harus dikerjakan itu berhubungan dengan setiap orang dan seluruh alam semesta. Kedua pandangan ini memberi hasil yang sangat berbeda. Gandhi dan Hitler, kedua-duanya melakukan apa yang mereka pikir harus dikerjakan. Akan tetapi, tindakan-tindakan mereka memengaruhi banyak orang yang tak terhitung jumlahnya, dengan cara yang berbeda secara dramatis."
            ],
            "source": "John Doido Loori, <em>Sarang Macan – Praktik Nyata Zen dalam Wujud Pertarungan Dharma (Percakapan Dharma)</em>, hal. 328"
        },
        "25": {
            "content": [
                "Pada titik tertentu kita menjadi sadar mengenai napas itu sendiri, seperti hembusan angin yang melalui kita, tanpa usaha keras. Napas menyentuh dengan ringan dalam hati, dan membuat kita terhubung secara alami dengan hangatnya mengembangkan kebaikan. Lucunya, hal-hal ini indah, namun tidak ada sesuatu yang istimewa."
            ],
            "source": "Ezra Bayda, <em>Intisari Zen - Nasihat Sederhana untuk Hidup dengan Penuh Kesadaran dan Welas Asih</em>, hal. 83"
        },
        "26": {
            "content": [
                "Saat kita merasa orang lain telah menyakiti kita, kecenderungan yang biasa kita lakukan adalah untuk menghakimi dan menyalahkan mereka. Kita juga jarang sadar bahwa alasan menyalahkan sebenarnya adalah usaha untuk menutupi rasa sakit kita sendiri, termasuk di dalamnya adalah rasa sakit melihat diri kita sendiri sebagai kurang, atau tidak mampu dalam beberapa hal. Jadi kita menjadi marah dan menunjukkan rasa sesal, dan kemudian fokus pada kekurangan dari mereka yang telah menyakiti kita."
            ],
            "source": "Ezra Bayda, <em>Intisari Zen - Nasihat Sederhana untuk Hidup dengan Penuh Kesadaran dan Welas Asih</em>, hal. 190"
        },
        "27": {
            "content": [
                "Buddha mengajarkan praktik meditasi cinta kasih sebagai penawar bagi rasa takut. Mengembangkan cinta kasih atau kebahagiaan, memperdalam pemahaman diri, atau mempelajari ajaran-ajaran kebijaksanaan sanggup menghadirkan keteguhan, kemantapan, serta penyembuhan tak langsung, sehingga menguatkan kita. Di kemudian hari, hal itu akan membantu kita menghadapi rasa sakit."
            ],
            "source": "Donald Rothberg, <em>The Engaged Spiritual Life - Kehidupan Spiritual yang Terjun Aktif, Mengubah Diri dan Dunia ke Arah yang Lebih Baik</em>, hal. 122"
        },
        "28": {
            "content": [
                "Begitu kita menyelam ia marah tanpa disertai kemelekatan ataupun penolakan, kita akan sanggup bersentuhan dengan aspek-aspek lebih murni – cinta kasih, kepedulian, kegembiraan, belas kasih, dan keseimbangan batin. Kemarahan kita–terkadang cukup mengejutkan–ternyata dapat ditransformasikan menjadi kualitas-kualitas mulia semacam ini. Ketika kita menghadapi kemarahan dengan cara ini, kita akan sanggup bertindak lebih bijak. Kita akan memperoleh beberapa wawasan terhadap permasalahan yang hadir dan sanggup membebaskan pandangan tersebut dari gejolak emosional, menyalahkan, serta niat mencelakai orang lain."
            ],
            "source": "Donald Rothberg, <em>The Engaged Spiritual Life - Kehidupan Spiritual yang Terjun Aktif, Mengubah Diri dan Dunia ke Arah yang Lebih Baik</em>, hal. 247"
        },
        "29": {
            "content": [
                "Jika anda hanya mendengar ajaran Dharma tanpa mempraktikkannya, anda ibarat sendok yang berada dalam mangkuk sop. Sendok itu berada dalam mangkuk sop setiap hari, tetapi tidak mengetahui rasa sop itu. Anda harus bercermin diri dan bermeditasi."
            ],
            "source": "Ajahn Chah, <em>A Tree in A Forest - Meditasi dalam Segala Objek Kehidupan</em>, hal. 70"
        },
        "30": {
            "content": [
                "Ketahuilah apa yang sedang terjadi di dalam pikiran anda—tidak gembira atau sedih karenanya, tidak melekat. Jika anda menderita, pahamilah, ketahuilah, dan kosongkan pikiran. Ibarat sepucuk surat—anda harus membukanya sebelum bisa mengetahui isinya."
            ],
            "source": "Ajahn Chah, <em>A Tree in A Forest - Meditasi dalam Segala Objek Kehidupan</em>, hal. 73"
        }
    },
    "12": {
        "01": {
            "content": [
                "Dharma ada di pikiran, bukan di hutan. Jangan percaya pada orang lain. Dengarlah apa kata pikiran anda. Anda tidak perlu pergi dan mencari ke tempat lain. Kebijaksanaan ada di dalam diri anda, bagaikan sebuah mangga matang yang manis yang berada di tengah-tengah buah mangga yang masih muda."
            ],
            "source": "Ajahn Chah, <em>A Tree in A Forest - Meditasi dalam Segala Objek Kehidupan</em>, hal. 252"
        },
        "02": {
            "content": [
                "Ketika kita berkonsentrasi, lima rintangan, kotoran batin kita, tidak mendapatkan kesempatan untuk muncul karena pikiran hanya dapat melakukan satu hal dalam satu waktu."
            ],
            "source": "Ayya Khema, <em>Being Nobody, Going Nowhere – Meditasi dengan Jalan Buddha</em>, hal. 11"
        },
        "03": {
            "content": [
                "Pikiran tidak dapat berkonsentrasi tanpa tiga pondasi yang terdiri atas kedermawanan, tindak moral, dan cinta kasih. Tiga pondasi inilah yang merupakan pilar dari meditasi, yang mendukung latihan meditasi. Cinta kasih, sebagai perasaan di dalam hati seseorang, sangat penting bagi konsentrasi karena cinta kasih menciptakan kedamaian dan ketenangan di dalam pikiran."
            ],
            "source": "Ayya Khema, <em>Being Nobody, Going Nowhere – Meditasi dengan Jalan Buddha</em>, hal. 61"
        },
        "04": {
            "content": [
                "Sudah tentu karma kita tidaklah sama. Namun akan menjadi lebih dari cukup bila kedamaian yang ada di dalam hati kita dapat kita teruskan kepada keluarga kita. Bukankah itu pun sudah merupakan pencapaian besar? Itu pun cukup. Jika diteruskan ke keluarga kita, setiap anggota keluarga kita, maka kemungkinan dapat kita teruskan ke tetangga kita. Pencapaian yang lebih besar lagi. Kita tidak harus menjadi Buddha untuk bisa memberi pengaruh yang bermanfaat bagi orang lain."
            ],
            "source": "Ayya Khema, <em>Being Nobody, Going Nowhere – Meditasi dengan Jalan Buddha</em>, hal. 97"
        },
        "05": {
            "content": [
                "Kita memiliki Sanggha untuk berlatih bersama. Kita memiliki ajaran-ajaran dari Buddha untuk dilaksanakan, dan sebuah lingkungan latihan di mana kita mengalami latihan. Memperoleh kondisi-kondisi yang menguntungkan ini untuk berlatih merupakan sesuatu yang luar biasa berharga. Tugas kita adalah menjaga dan mengembangkan kondisi-kondisi yang menguntungkan ini di mana saja kita berada."
            ],
            "source": "Thich Nhat Hanh, <em>Menemukan Rumah Kita yang Sebenarnya – Hidup di Tanah Suci di Sini dan Sekarang</em>, hal. 193"
        },
        "06": {
            "content": [
                "Ketika kita memiliki sebuah persepsi, kita berkecenderungan untuk membuat persepsi itu menjadi kebenaran yang absolut. Apabila kita terperangkap di dalamnya dan kita tidak dapat melepaskannya maka ini disebut pandangan yang terperangkap dalam pandangan."
            ],
            "source": "Thich Nhat Hanh, <em>Menemukan Rumah Kita yang Sebenarnya – Hidup di Tanah Suci di Sini dan Sekarang</em>, hal. 227"
        },
        "07": {
            "content": [
                "Keyakinan (<em>sraddha</em>) dan aspirasi (<em>pranidhana</em>) merupakan dua sumber energi yang dapat menolong kita mengurangi penderitaan dengan segera. Pada saat kita berada dalam situasi di mana kita merasa muak dan putus asa dengan dunia <em>saha</em>, kita sangat menderita. Sebaliknya, kita bisa berada di dunia <em>saha</em> penuh dengan energi keyakinan dan aspirasi dan kita merasa tenteram serta tidak menderita lagi."
            ],
            "source": "Thich Nhat Hanh, <em>Menemukan Rumah Kita yang Sebenarnya – Hidup di Tanah Suci di Sini dan Sekarang</em>, hal. 250"
        },
        "08": {
            "content": [
                "Sebelum kita mampu untuk mencintai dan merawat diri kita sendiri, kita tidak akan bisa berbuat banyak untuk menolong orang lain "
            ],
            "source": "Thich Nhat Hanh, <em>Teachings on Love – Ajaran Tentang Cinta Sejati</em>, hal. 32"
        },
        "09": {
            "content": [
                "Ketika anda sudah mengerti akar penderitaan anda—kemarahan, kepedihan, dan frustasi anda—hati anda akan menjadi damai, tenang, dan ringan."
            ],
            "source": "Thich Nhat Hanh, <em>Teachings on Love – Ajaran Tentang Cinta Sejati</em>, hal. 69"
        },
        "10": {
            "content": [
                "Rumah sejatimu di sini dan sekarang. Tidak dibatasi oleh waktu, ruang, kebangsaan, atau suku. Rumah sejatimu bukan gagasan abstrak; namun sesuatu yang dapat kamu sentuh dan rasakan setiap saat. Dengan sadar-penuh dan konsentrasi, energi Buddha, kamu dapat menemukan rumah sejatimu dalam relaksasi pikiran dan tubuh sepenuhnya dalam masa sekarang."
            ],
            "source": "Thich Nhat Hanh, <em>Rumah Sejatimu - Kebijaksanaan Sehari-hari dari Thich Nhat Hanh</em>, hal. 1"
        },
        "11": {
            "content": [
                "Kapan pun pikiranmu terpencar, gunakan napasmu sebagai alat untuk menggenggam pikiranmu lagi."
            ],
            "source": "Thich Nhat Hanh, <em>Rumah Sejatimu – Kebijaksanaan Sehari-hari dari Thich Nhat Hanh</em>, hal. 45"
        },
        "12": {
            "content": [
                "Biasanya ketika kita menderita, kita merasa sebagai satu-satunya orang yang menderita, dan bahwa orang lain sangat bahagia. Padahal pada kenyataannya, biasanya orang yang menyakiti kita juga mempunyai banyak penderitaan dan tidak tahu cara mengatasi emosinya yang kuat. Bernapas dengan kesadaran, menghasilkan energi sadar-penuh, dan kita bisa mendapatkan pemahaman mengenai cara mengatasi penderitaan kita dan penderitaan orang lain dengan belas kasih."
            ],
            "source": "Thich Nhat Hanh, <em>Rumah Sejatimu – Kebijaksanaan Sehari-hari dari Thich Nhat Hanh</em>, hal. 163"
        },
        "13": {
            "content": [
                "Merasa dipahami seringkali lebih penting dibandingkan mendapatkan apa yang dianggap kita inginkan."
            ],
            "source": "C.L. Claridge, <em>Menjadi Orangtua Berhati Buddha - Memperkaya Keluarga Anda dengan Kebijaksanaan dan Welas Asih Buddhis</em>, hal. 162"
        },
        "14": {
            "content": [
                "Kebanyakan masalah kita dianggap bermasalah karena kita berharap lingkungan luar, atau orang lain, berubah untuk memenuhi kebutuhan kita. Kita memiliki sedikit kendali terhadap lingkungan luar, tetapi kendali yang sepenuhnya adalah pada lingkungan di dalam diri kita, yaitu pikiran kita."
            ],
            "source": "C.L. Claridge, <em>Menjadi Orangtua Berhati Buddha - Memperkaya Keluarga Anda dengan Kebijaksanaan dan Welas Asih Buddhis</em>, hal. 197"
        },
        "15": {
            "content": [
                "Jika kita sadar bahwa kita berbicara pada anak kita dengan cara yang  tidak ingin kita lakukan pada teman kita, maka kita tidak terhubung dengan mereka."
            ],
            "source": "C.L. Claridge, <em>Menjadi Orangtua Berhati Buddha - Memperkaya Keluarga Anda dengan Kebijaksanaan dan Welas Asih Buddhis</em>, hal. 275"
        },
        "16": {
            "content": [
                "Untuk mentransformasi samsara menjadi nirwana, kita perlu belajar merenungkan serta melihat dengan jernih bahwa keduanya hanyalah manifestasi dari kesadaran kita sendiri. Benih-benih samsara, penderitaan, kebahagiaan, dan nirwana sudah ada di dalam kesadaran terpendam kita. Kita hanya perlu menyirami benih-benih kebahagiaan, serta menghindari menyiram benih-benih penderitaan."
            ],
            "source": "Thich Nhat Hanh, <em>Memahami Pikiran Kita</em>, hal. 8"
        },
        "17": {
            "content": [
                "Ketika jatuh cinta, biasanya kita jatuh cinta pada gambaran yang kita miliki tentang kekasih kita. Kita tidak bisa makan, tidur, atau berbuat apa pun karena gambaran yang ada dalam diri kita itu sangatlah kuat. Kekasih kita sangat rupawan bagi kita, tetapi gambaran kita tentang dia bisa jadi sesungguhnya jauh dari kenyataan. Kita tidak menyadari bahwa objek persepsi kita bukanlah ‘realitas dalam dirinya sendiri’, melainkan hanya sebuah gambaran yang kita ciptakan sendiri. Setelah menikah dan tinggal bersama kekasih kita selama dua atau tiga tahun, kita menyadari bahwa gambaran yang kita genggam dan membuat tidak bisa tidur di malam hari tersebut, sebagian besar ternyata salah."
            ],
            "source": "Thich Nhat Hanh, <em>Memahami Pikiran Kita</em>, hal. 44"
        },
        "18": {
            "content": [
                "Merasa berkecukupan adalah raja diraja dari semua kekayaan."
            ],
            "source": "Gede Prama, <em>Compassion – Menyembuhkan Pikiran, Menyejukkan Lingkungan</em>, hal. 146"
        },
        "19": {
            "content": [
                "Cukup menyatu rapi dengan setiap langkah yang sedang berjalan. Sambil jangan lupa bersyukur pada setiap berkah yang terjadi di saat ini."
            ],
            "source": "Gede Prama, <em>Compassion – Menyembuhkan Pikiran, Menyejukkan Lingkungan</em>, hal. 152"
        },
        "20": {
            "content": [
                "Tatkala dimaki, lihat ke dalam seberapa besar api amarah menyala, seberapa banyak energi belas kasih tersisa untuk orang yang sedang memaki."
            ],
            "source": "Gede Prama, <em>Compassion – Menyembuhkan Pikiran, Menyejukkan Lingkungan</em>, hal. 266"
        },
        "21": {
            "content": [
                "Warna putih tidak hanya mewakili agama, etika, dan nilai kebajikan namun juga kemurnian, kesucian jasmani maupun batin, beserta kejujuran nan gemilang. Oleh karenanya, strategi samudra putih tidak lain adalah pendekatan terhadap manajemen bisnis yang berpusat pada etika selaku jantung hati bagi motivasi kita, sehingga menyediakan pelindung yang transparan bagi setiap organisasi demi menjaganya dari berbagai hambatan."
            ],
            "source": "Danai Chanchaochai, <em>White Ocean Strategy, Strategi Samudra Putih</em>, hal. 10"
        },
        "22": {
            "content": [
                "Apa yang digunakan untuk menilai kehidupan seseorang bukanlah usianya, melainkan kebajikan yang ia tinggalkan setelah meninggal dunia dan bagaimana orang-orang merasa kehilangan dirinya."
            ],
            "source": "Danai Chanchaochai, <em>White Ocean Strategy, Strategi Samudra Putih</em>, hal. 148"
        },
        "23": {
            "content": [
                "Kita sebaiknya seperti gunung. Kita tidak seharusnya mengizinkan kondisi luar mengambil kebahagiaan dan kedamaian kita, sekuat apa pun petir menggelegar, sekencang apa pun angin bertiup. Kita semua memiliki inti yang tenang dan hening. Itu ada di sana kapan pun kita menginginkan atau membutuhkannya. Dengan melihat ke dalam, kita dapat merasakan kekuatan penyembuhan. Di dalam gunung sempurna - begitu pula kita."
            ],
            "source": "Dr. Brian L. Weiss, <em>Keajaiban – Transformasi Kekuatan Penyembuhan dari Ingatan Masa Lalu</em>, hal. 8"
        },
        "24": {
            "content": [
                "Kita kembali bersama mereka yang kita cintai berulang kali, berjumpa dalam siklus yang tidak terhingga jumlahnya. Tubuh kita berganti dalam setiap kesempatan, tetapi jiwa kita tetaplah sama. Ketika kita mengenali orang lain sebagai belahan jiwa yang kita kasihi, jurang pemisah di antara kita lenyap. Keyakinan dan rasa nyaman tumbuh. Kesedihan dan keputusasaan hilang."
            ],
            "source": "Dr. Brian L. Weiss, <em>Keajaiban – Transformasi Kekuatan Penyembuhan dari Ingatan Masa Lalu</em>, hal. 271"
        },
        "25": {
            "content": [
                "Dari semua noda batin, dikatakan bahwa noda batin ketidaktahuan (<em>avijjasava</em>) adalah yang paling buruk. Paling membahayakan dan kuat, dan merupakan acuan semua kejahatan. Ketidaktahuan (kebodohan batin) bukan hanya tidak ‘tahu’, tetapi mengetahui secara salah dan tertipu. Noda batin ketidaktahuan inilah yang menjadikan kejahatan nampak baik, berubah nampak tidak berubah, buruk nampak indah, tidak menyenangkan nampak menyenangkan, ketidakpuasan nampak kepuasan. Karena keduanya tidak terduga dan mudah menyebar, noda batin ini menimbulkan ilusi kekekalan dan seterusnya, dan oleh karenanya menjadikan makhluk hidup dalam perbudakan abadi. Dengan akal bulus noda batin ketidaktahuan, manusia dengan tetap bahagia tidak menyadari kenyataan menyakitkan dari <em>samsara</em>.",
                "Meraba-raba dalam kegelapan pekat ketidaktahuan, manusia diarahkan untuk merasa jijik pada sesuatu yang sesuai dengan cahaya kebijaksanaan."
            ],
            "source": "Acharya Buddharakkhita, <em>Batin, Mengatasi Nodanya – Suatu Pembelajaran Mendalam mengenai Kotoran Batin dalam Sudut Pandang Buddhis</em>, hal. 117"
        },
        "26": {
            "content": [
                "Seorang yang bijaksana seperti seorang penjaga hutan yang ahli memasuki hutan bambu belantara, ia tidak terluka dan selamat. Ia berjalan dengan memotong jeratan bambu tajam dan berduri dan kembali dengan tiang-tiang bambu terbaik.",
                "Orang duniawi yang tidak bijaksana memasuki hutan bambu belantara hanya untuk terjerat, terluka seluruhnya, dan bahkan tersesat atau menemui ajalnya, tidak (mungkin) kembali dengan selamat apalagi dengan membawa tiang bambu."
            ],
            "source": "Acharya Buddharakkhita, <em>Batin, Mengatasi Nodanya – Suatu Pembelajaran Mendalam mengenai Kotoran Batin dalam Sudut Pandang Buddhis</em>, hal. 166"
        },
        "27": {
            "content": [
                "Empat Kebenaran Mulia pada dasarnya membentuk inti dari pengalaman para yogi, pandangan terang yang menembus realitas segala sesuatu. Empat Kebenaran Mulia menandakan aktualisasi kebenaran hidup, kebenaran dunia, dan kebenaran adidunia. Dan aktualisasi ini direfleksikan dalam perubahan (<em>transmutasi</em>) pikiran, dalam transformasi kepribadian, dalam keadaan yang melampaui ketidaktahuan menuju pencerahan, dari keterbatasan akan keberadaan yang terikat oleh ego menuju keadaan tanpa batas dari Nibbana, dari ketidaksempurnaan perbudakan <em>samsara</em> menuju kesempurnaan kesadaran adiduniawi (<em>lokuttara citta</em>)."
            ],
            "source": "Acharya Buddharakkhita, <em>Batin, Mengatasi Nodanya – Suatu Pembelajaran Mendalam mengenai Kotoran Batin dalam Sudut Pandang Buddhis</em>, hal. 168"
        },
        "28": {
            "content": [
                "Retret yang sesungguhnya adalah mengistirahatkan pikiran, jasmani, dan ucapan dari hal yang tidak bermanfaat."
            ],
            "source": "Thubten Chodron, <em>Don’t Believe Everything You Think – Hidup dengan Kebijaksanaan dan Welas Asih</em>, hal. 55"
        },
        "29": {
            "content": [
                "Jelaslah, bahwa bahkan jika kita telah mencapai kesuksesan duniawi, ketakutan akan kehilangan menghantui kita."
            ],
            "source": "Thubten Chodron, <em>Don’t Believe Everything You Think – Hidup dengan Kebijaksanaan dan Welas Asih</em>, hal. 194"
        },
        "30": {
            "content": [
                "Saat kita menilik kehidupan berlatih, ada satu kata yang terus muncul, yaitu mengalami. Apa sesungguhnya diartikan ‘mengalami’? Dapatkah kita mendefinisikannya? Dapatkah kita menggambarkannya?"
            ],
            "source": "Ezra Bayda, <em>Being Zen, Maujud Zen – Mengaplikasikan Meditasi dalam Kehidupan</em>, hal. 28"
        },
        "31": {
            "content": [
                "Pembelajaran dalam berlatih tidak akan didapatkan hanya dengan membaca atau memikirkannya. Untuk membangkitkan ketercerahan berdasarkan pemahaman yang autentik, kita perlu belajar dari pengalaman kita sendiri."
            ],
            "source": "Ezra Bayda, <em>Being Zen, Maujud Zen – Mengaplikasikan Meditasi dalam Kehidupan</em>, hal. 46"
        }
    }
}