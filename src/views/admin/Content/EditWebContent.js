import React, { Component } from 'react';
import WebContentForm from './WebContentForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import { getWebContent, updateWebContent} from '../../../actions/Content'; 
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return{
        webContentData: state.content.webContentData,
        succeed: state.content.succeed,
        error: state.content.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getWebContent: () => {
            dispatch(getWebContent())
        },
        updateWebContent: (data) => {
            dispatch(updateWebContent(data))
        }
    }
}

class EditWebContent extends Component {
    constructor(){
        super();
        this.state = {
            webContentData: {
                description: '',
                vision: '',
                mission: '',
                iframe: '',
                image: null
            },
            successMessage: '',
            errorMessage: '',
            imageUrl: null
        }
    }

    componentWillMount(){
        this.props.getWebContent();
    }

    componentWillReceiveProps(newProps){
        if(this.state.webContentData != newProps.webContentData){
					this.setState({
						webContentData: newProps.webContentData,
						imageUrl: `${IMAGE_URL}/public/images/contents/${newProps.webContentData.image}?t=${new Date().getTime()}`
					})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/about');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleGetWebContentData(data){
        console.log(data);
        this.props.updateWebContent(data);
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Get Web Content</div>
                    </div>
                </div>
                <WebContentForm data = {this.state.webContentData} imageUrl={this.state.imageUrl} getWebContentData={this.handleGetWebContentData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

EditWebContent = connect(mapStateToProps, mapDispatchToProps)(EditWebContent);
export default EditWebContent;