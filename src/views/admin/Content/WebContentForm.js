import React, { Component } from 'react';
import update from 'react-addons-update';

class WebContentForm extends Component {
    constructor(){
        super();
        this.state = {
            webContentData: {},
            imageUrl: ''
        }
    }

    componentWillMount(){
        console.log(this.props.data)
        this.setState({
            webContentData: this.props.data
        })
    }

    componentWillReceiveProps(newProps){
        if(this.state.webContentData != newProps.data){
            this.setState({
                webContentData: newProps.data
            })
        }
        if(this.state.imageUrl != newProps.imageUrl){
            this.setState({
                imageUrl: newProps.imageUrl
            })
        }
    }
    
    handleInputOnChange(event){
        let name = event.target.name
        let currWebContent = this.state.webContentData;
        this.setState({
            webContentData: update(currWebContent, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleFileOnChange(data){
        var reader = new FileReader();
        var imageBase64 = "";
        reader.onload = function(e){
            imageBase64 = e.target.result;
        }
        reader.readAsDataURL(data.target.files[0]);
        setTimeout(() => { this.setState({imageUrl: imageBase64}); }, 100);

        let currWebContentData = this.state.webContentData;
        this.setState({
            webContentData: update(currWebContentData, {
                image: {$set: data.target.files[0]}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getWebContentData(this.state.webContentData);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="form-group">
                            <label>Gambar</label>
                            <div className="img__upload">
                                { !this.state.imageUrl ? <label>IMAGE</label> : null }
                                { this.state.imageUrl ? <img src={this.state.imageUrl} /> : null}
                            </div>
                            <label className="btn--file-upload">Browse here <input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)} /></label>
                        </div>
                        <div className="form-group">
                            <label>Deskripsi Vihara</label>
                            <textarea type="text" rows="20" name="description" value={this.state.webContentData.description} className="form-control" onChange={ this.handleInputOnChange.bind(this) }></textarea>
                        </div>

                        <div className="form-group">
                            <label>Visi</label>
                            <textarea type="text" rows="10" name="vision" value={this.state.webContentData.vision} className="form-control" onChange={ this.handleInputOnChange.bind(this) }></textarea>
                        </div>

                        <div className="form-group">
                            <label>Misi</label>
                            <textarea type="text" rows="10" name="mission" value={this.state.webContentData.mission} className="form-control" onChange={ this.handleInputOnChange.bind(this) }></textarea>
                        </div>

                        <div className="form-group">
                            <label>Iframe</label>
                            <input type="text" name="iframe" value={this.state.webContentData.iframe} className="form-control" onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-6">
                        <a role="button" href="/admin/ebook/list" className="btn btn-block btn-danger">Cancel</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>                
            </form> 
        );
    }
}

export default WebContentForm;