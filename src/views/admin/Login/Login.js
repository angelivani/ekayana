import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'react-addons-update';
import { userLogin } from '../../../actions/User';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';

const mapStateToProps = (state) => {
    return{
      user: state.user.user,
      error: state.user.error,
      fetched: state.user.fetched,
      accessToken: state.user.accessToken
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        doLogin: (loginData) => {
            dispatch(userLogin(loginData))
        }
    }
}

class Login extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(){
        super();
        this.state = {
            loginData: {
                username: '',
                password: ''
            }
        }
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.doLogin(this.state.loginData);
    }

    handleInputOnChange(event){
        let currLoginData = this.state.loginData;
        let name = event.target.name;

        this.setState({
            loginData: update(currLoginData, {
                [name]: {$set: event.target.value }
            })
        })
    }

    componentWillReceiveProps(newProps){
        if(newProps.fetched && newProps.accessToken){
            this.props.history.push("/admin/buddhavacana/list");
        }
    }

    render() {
        const {loginData} = this.state;
        return (
            <div className="d-flex justify-content-center align-items-center login__wrapper">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="card login__card">
                        <img src="http://www.ink.co.id/files.documents/clients/Ekayana.png" width="250px" height="auto" />
                        {/* <span className="logo">Ekayana</span> */}
                        
                        <div className="form-group mt-3">
                            <label>Username</label>
                            <input type="text" name="username" value={loginData.username} className="form-control" placeholder="Username" onChange = {this.handleInputOnChange.bind(this) } />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" name="password" value={loginData.password} className="form-control" placeholder="Password" onChange = {this.handleInputOnChange.bind(this) } />
                        </div>
                        <button className="btn btn-block btn--base" type="submit">Login</button>
                    </div>
                </form>
            </div>
        );
    }
}

Login = connect(mapStateToProps, mapDispatchToProps)(Login);
Login = withCookies(Login);
export default Login;