import React, { Component } from 'react';
import {connect} from 'react-redux';
import {getAlbumnDetail, updateAlbumn} from '../../../actions/Albumn';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import AlbumnForm from './AlbumnForm';

const mapStateToProps = (state) => {
    return{
        albumnData: state.albumn.albumnData,
        succeed: state.albumn.succeed,
        error: state.albumn.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getAlbumnDetail: (id) => {
            dispatch(getAlbumnDetail(id))
        },
        updateAlbumn: (data) => {
            dispatch(updateAlbumn(data))
        }
    }
}

class EditAlbumn extends Component {
    constructor(){
        super();
        this.state = {
            albumnData: {
                name: 'aa',
                description: 'lorem ipsum'
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        console.log('AA');
        let data = {
            id: this.props.match.params.id,
            limit: 10,
            offset: 0
        } 
        this.props.getAlbumnDetail(data);
    }

   
    componentWillReceiveProps(newProps){
   
        if(newProps.albumnData){
            this.setState({albumnData: newProps.albumnData})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetAlbumnData(data){
        console.log(data);
        this.props.updateAlbumn(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/albumn/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ''});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Edit Albumn</div>
                    </div>
                </div>
                <AlbumnForm data={this.state.albumnData} getAlbumnData={this.handleGetAlbumnData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

EditAlbumn = connect(mapStateToProps, mapDispatchToProps)(EditAlbumn);
export default EditAlbumn;