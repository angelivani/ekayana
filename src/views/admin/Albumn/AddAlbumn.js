import React, { Component } from 'react';
import AlbumnForm from './AlbumnForm';
import {connect} from 'react-redux';
import {addAlbumn} from '../../../actions/Albumn';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      succeed: state.albumn.succeed,
      error: state.albumn.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addAlbumn: (data) => {
            dispatch(addAlbumn(data))
        }
    }
}

class AddAlbumn extends Component {
    constructor(){
        super();
        this.state = {
            albumnData: {
                name: '',
                description: ''
            }
        }
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.succeed){
            this.setState({
                successMessage: newProps.succeed
            })
        }
        else if(newProps.error){
            this.setState({
                errorMessage: newProps.error
            })
        }
    }

    handleGetAlbumnData(data){
        console.log(data);
        this.props.addAlbumn(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/albumn/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Albumn</div>
                    </div>
                </div>
                <AlbumnForm data={this.state.albumnData} getAlbumnData={this.handleGetAlbumnData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

AddAlbumn = connect(mapStateToProps, mapDispatchToProps)(AddAlbumn);
export default AddAlbumn;