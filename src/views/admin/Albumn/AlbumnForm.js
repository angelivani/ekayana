import React, { Component } from 'react';
import update from 'react-addons-update';

class AlbumnForm extends Component {
    constructor(){
        super();
        this.state = {
            albumnData: {}
        }
    }

    componentWillMount(){
        this.setState({ albumnData: this.props.data })
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.data != this.props.data){
            this.setState({albumnData: this.props.data});
        }
    }

    handleInputOnChange(event){
        let name = event.target.name;
        let currAlbumn = this.state.albumnData;
        this.setState({
            albumnData: update(currAlbumn, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getAlbumnData(this.state.albumnData);
    }

    render() {
        const {albumnData} = this.state;
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nama Album</label>
                            <input type="text" className="form-control" name="name" value={albumnData.name} onChange={ this.handleInputOnChange.bind(this) } />
                        </div>

                        <div className="form-group">
                            <label>Deskripsi Albumn</label>
                            <textarea className="form-control" name="description" value={albumnData.description} rows="4" onChange={ this.handleInputOnChange.bind(this) } ></textarea>
                        </div>
                    </div>
                </div>
                <div className="mt-3">
                    <div className="row">
                        <div className="col-md-6">
                            <a href="/admin/albumn/list" role="button" className="btn btn-block btn-danger">Cancel</a>
                        </div>
                        <div className="col-md-6">
                            <button className="btn btn-block btn-success" type="submit">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            
        );
    }
}

export default AlbumnForm;