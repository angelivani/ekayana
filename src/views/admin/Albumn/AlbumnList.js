import React, { Component } from 'react';
import AlbumnCard from '../../../component/client/albumn/AlbumnCard';

import {getAlbumnList, deleteAlbumn} from '../../../actions/Albumn';
import {connect} from 'react-redux';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      albumnList: state.albumn.albumnList,
      albumnSucceed: state.albumn.succeed,
      albumnError: state.albumn.error
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        getAlbumnList: () => {
            dispatch(getAlbumnList())
        },
        deleteAlbumn: (id) => {
            dispatch(deleteAlbumn(id))
        }
    }
}

class AlbumnList extends Component {
    constructor(){
        super();
        this.state = {
            albumnList: null,
            successMessage: '',
            errorMessage: '',
            albumnToDelete: -1,
            showConfirmationModal: false
        }
    }

    componentWillMount(){
        this.props.getAlbumnList();
    }

    componentDidUpdate(prevState){
        if(prevState.albumnList != this.props.albumnList){
            this.setState({
                albumnList: this.props.albumnList
            })
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.albumnSucceed){
            this.setState({
                successMessage: newProps.albumnSucceed
            })
        }
        if(newProps.albumnError){
            this.setState({
                errorMessage: newProps.albumnError
            })
        }
    }

    handleDeleteOnClicked(id){
        console.log(id);
        
        this.setState({
            showConfirmationModal: true,
            albumnToDelete: id
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteAlbumn(this.state.albumnToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getAlbumnList();
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/albumn/add">
                        <i className="fas fa-plus mr-2"></i> Album
                    </a>
                </div>
                <div className="section--title border-bottom">List Album</div>
                <div className="row mt-5">
                    {this.state.albumnList && this.state.albumnList.map((albumn, index) =>
                        (
                            <div className="col-lg-3 col-md-3 mb-3">
                                <AlbumnCard albumn={albumn} />
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-info col-6 mr-2" onClick={() => this.props.history.push(`/admin/albumn/edit/${albumn.id}`)}>Edit</button>
                                    <button className="btn btn-danger col-6" onClick={ () => this.handleDeleteOnClicked(albumn.id)}>Delete</button>
                                </div>
                            </div>
                            
                        )
                    )}
                </div>

                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus album ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }

            </div>
        );
    }
}

AlbumnList = connect(mapStateToProps, mapDispatchToProps)(AlbumnList);
export default AlbumnList;