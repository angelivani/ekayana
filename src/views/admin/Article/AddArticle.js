import React, {Component} from 'react';
import ArticleForm from './ArticleForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { connect } from 'react-redux';
import { addArticle } from '../../../actions/Article'; 

const mapStateToProps = (state) => {
    return{
      succeed: state.article.succeed,
      error: state.article.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addArticle: (data) => {
            dispatch(addArticle(data))
        }
    }
}

class AddArticle extends Component {
    constructor(){
        super();
        this.state = {
            articleData: {
                name: '',
                description: '',
                image: null
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetArticleData(data){
        console.log(data);
        this.props.addArticle(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/article/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(            
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Article</div>
                    </div>
                </div>
                <ArticleForm articleData = {this.state.articleData} getArticleData={this.handleGetArticleData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
            
        )
    }
}

AddArticle = connect(mapStateToProps, mapDispatchToProps)(AddArticle);
export default AddArticle;
