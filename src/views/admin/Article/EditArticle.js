import React, {Component} from 'react';
import ArticleForm from './ArticleForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { getArticleById, updateArticle } from '../../../actions/Article';
import { connect } from 'react-redux';
import {IMAGE_URL} from '../../../constants/ActionTypes';

const mapStateToProps = (state) => {
    return{
      succeed: state.article.succeed,
      error: state.article.error,
      articleData: state.article.articleData
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        getArticleById: (id) => {
            dispatch(getArticleById(id))
        },
        updateArticle: (data) => {
            dispatch(updateArticle(data))
        }
    }
}

class EditArticle extends Component {
    constructor(){
        super();
        this.state = {
            articleData: {},
            imageUrl: null,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getArticleById(id);
    }

    componentWillReceiveProps(newProps){
        if(this.state.articleData != newProps.articleData){
            this.setState({ articleData: newProps.articleData })
        }

        if(this.state.imageUrl != newProps.articleData.image){
            this.setState({imageUrl: `${IMAGE_URL}/public/images/article/${newProps.articleData.image}`}
            )
        }
        console.log(newProps);
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetArticleData(data){
        console.log(data);
        this.props.updateArticle(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/article/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }
    
    render(){
        return(
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Edit Article</div>
                    </div>
                </div>
                <ArticleForm articleData = {this.state.articleData} imageUrl={this.state.imageUrl} getArticleData={this.handleGetArticleData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        )
    }
}

EditArticle = connect(mapStateToProps, mapDispatchToProps)(EditArticle);
export default EditArticle;