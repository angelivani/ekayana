import React, { Component } from 'react';
import update from 'react-addons-update';

class ArticleForm extends Component {
    constructor(){
        super();
        this.state = {
            articleData: {},
            imageUrl: ''
        }
    }

    componentWillMount(){
        this.setState({
            articleData: this.props.articleData
        })
    }

    componentDidUpdate(prevState){
        if(prevState.articleData != this.props.articleData){
            this.setState({
                articleData: this.props.articleData
            })
        }
        
        if(prevState.imageUrl != this.props.imageUrl){
            this.setState({
                imageUrl: this.props.imageUrl
            })
        }
    }

    handleInputOnChange(event){
        let name = event.target.name
        let currArticle = this.state.articleData;
        this.setState({
            articleData: update(currArticle, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleFileOnChange(data){
        var reader = new FileReader();
        var imageBase64 = "";
        reader.onload = function(e){
            console.log(e.target.result);
            imageBase64 = e.target.result;
        }
        reader.readAsDataURL(data.target.files[0]);
        setTimeout(() => { this.setState({imageUrl: imageBase64}); }, 100);
        
       
        let currArticle = this.state.articleData;
        this.setState({
            articleData: update(currArticle, {
                image: {$set: data.target.files[0]}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getArticleData(this.state.articleData);
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nama</label>
                            <input type="text" className="form-control" name="name" value={this.state.articleData.name} onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label>Deskripsi</label>
                            <textarea className="form-control" name="description" value={this.state.articleData.description} rows="5" onChange={this.handleInputOnChange.bind(this)}></textarea>
                        </div>
                        
                        <div className="form-group">
                            <label>Gambar Artikel</label>
                            <div className="img__upload">
                                {!this.state.imageUrl ? <label>ARTICLE IMAGE</label> : null }
                                {this.state.imageUrl ? <img src={this.state.imageUrl} /> : null }
                            </div>
                        </div>
                        <label className="btn--file-upload">Browse here<input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)}/></label>    
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/article/list" className="btn btn-block btn-danger" role="button">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        )
    }
}

export default ArticleForm;