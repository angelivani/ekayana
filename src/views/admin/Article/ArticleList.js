import React, { Component } from 'react';
import ArticleCard from '../../../component/client/article/ArticleCard';
import { getArticleList, deleteArticle } from '../../../actions/Article'; 
import { connect } from 'react-redux';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import DefaultPagination from '../../../component/shared/pagination/DefaultPagination';

const mapStateToProps = (state) => {
    return {
        articleList: state.article.articleList,
        totalData: state.article.totalData,
        succeed: state.article.succeed,
        error: state.article.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getArticleList: (data) => {
            dispatch(getArticleList(data))
        },
        deleteArticle: (id) => {
            dispatch(deleteArticle(id))
        }
    }
}

class ArticleList extends Component {
    constructor(){
        super();
        this.state = {
            limit: 10,
            offset: 0,
            articleList: null,
            totalData: null,
            showConfirmationModal: false,
            successMessage: '',
            errorMessage: '',
            articleToDelete: 0
        }
    }

    componentWillMount(){
        this.props.getArticleList({
            limit: this.state.limit, 
            offset: this.state.offset
        })
    }

    componentWillReceiveProps(newProps){
        if(this.state.articleList != newProps.articleList) {        
            this.setState({ 
                articleList: newProps.articleList 
            })
        }
        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData })
        }

        if(this.state.successMessage != newProps.succeed){
            this.setState({ successMessage: newProps.succeed })
        }
        if(this.state.errorMessage != newProps.error){
            this.setState({ errorMessage: newProps.error })
        }
    }

    handleDeleteOnClicked(id){
        this.setState({
            showConfirmationModal: true,
            articleToDelete: id
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteArticle(this.state.articleToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getArticleList({
            limit: this.state.limit, 
            offset: this.state.offset
        })
    }

    getDataOnPageClicked(data){
        console.log(data);
    }

    handleGetDataOnPageClicked(data){
        let offset = data * this.state.limit - this.state.limit;
        this.props.getArticleList({
            limit: this.state.limit,
            offset: offset
        });
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a href="/admin/article/add" className="btn btn--base">
                        <i className="fas fa-plus mr-2"></i>
                        <span className="font-weight-bold">Artikel</span>
                    </a>
                </div>

                <div className="section--title border-bottom">List Artikel</div>

                <div className="row mt-3">
                    {this.state.articleList && this.state.articleList.map((article, index) => 
                        (
                            <div className="col-md-3 col-lg-4 col-xl-3 mb-2">
                                <ArticleCard article={article} />
                                <div className="d-flex justify-content-center m-1">
                                    <button className="btn btn-info col-6 mr-2" onClick={() => this.props.history.push(`/admin/article/edit/${article.id}`)}>Edit</button>
                                    <button className="btn btn-danger col-6" onClick={ () => this.handleDeleteOnClicked(article.id)}>Delete</button>
                                </div>
                            </div>
                        )
                    )}
                </div>
                {this.state.totalData && <DefaultPagination totalData={this.state.totalData} limit={this.state.limit} getDataOnPageClicked={this.handleGetDataOnPageClicked.bind(this)} />}
                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus artikel ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

ArticleList = connect(mapStateToProps, mapDispatchToProps)(ArticleList);
export default ArticleList;