import React, { Component } from 'react';

class SessionExpired extends Component {
    render() {
        return (
            <div className="d-flex align-items-center justify-content-center">
                <h1>Opps your session already expired</h1>
            </div>
        );
    }
}

export default SessionExpired;