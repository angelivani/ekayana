import React, {Component} from 'react';
import update from 'react-addons-update'; 

class SpecialActivityForm extends Component {
    constructor(){
        super();
        this.state = {
            specialActivity: {
                name: '',
                description: '',
                location: '',
                time: '',
                cpList: [
                    {name: '', phone: ''}
                ]
            }
        }
    }

    addContactPerson(){
        console.log('a')
        let data = {
            name: '',
            phone: ''
        }
        let currActData = this.state.specialActivity;
        this.setState({
            specialActivity: update(currActData, {
                cpList: {$push: [data]}
            })
        })
    }

    render(){
        const {specialActivity} = this.state;

        return(
            <form>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="name" className="form-control" placeholder="Input Judul" />
                        </div>

                        <div className="form-group">
                            <label>Deskripsi Kegiatan</label>
                            <textarea name="description" className="form-control" rows="5" placeholder="Input Deskripsi Kegiatan"></textarea>
                        </div>

                        <div className="form-group">
                            <label>Lokasi Kegiatan</label>
                            <input type="text" name="location" className="form-control" placeholder="Input lokasi kegiatan" />
                        </div>

                        <div className="form-group">
                            <label>Waktu Kegiatan</label>
                            <input type="text" name="time" className="form-control" placeholder="Input hari pelaksanaan" />
                        </div>

                        <div className="form-group">
                            <label>Contact Person</label>
                            {this.state.specialActivity.cpList.map((cp, index) => 
                                (
                                    <div className="row" key={index}>
                                        <div className="col-md-4 mb-3" style={{ marginLeft: '0', paddingLeft: '0'}}>
                                            <input type="text" value={cp.name} className="form-control" placeholder="Nama" />
                                        </div>
                                        <div className="col-md-7 mb-3" style={{ marginRight: '0', paddingRight: '0'}}>
                                            <input type="text" value={cp.phone} className="form-control" placeholder="No Telepon" />
                                        </div>
                                        
                                    </div>
                                )
                            )}
                            
                            <button type="button" className="btn btn--base" onClick={this.addContactPerson.bind(this)}>Tambah</button>
                            
                        </div>

                        <div className="row d-flex justify-content-end mt-4">
                            <button className="btn btn-danger btn--large mr-3">Hapus</button>
                            <button className="btn btn--base btn--large">Simpan</button>
                        </div>  
                    </div>
                </div>
            </form>
            
        )
    }
}

export default SpecialActivityForm;