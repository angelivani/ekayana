import React, { Component } from 'react';
import SpecialActivityForm from './SpecialActivityForm'; 

class AddActivity extends Component {
    render() {
        return (
            <div className="form__wrapper">
                <h1>Kegiatan Khusus</h1>
                <div className="pt-4">
                    <SpecialActivityForm />
                </div>
                
            </div>
        );
    }
}

export default AddActivity;