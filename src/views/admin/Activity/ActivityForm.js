import React, { Component } from 'react';
import TimePicker from 'react-time-picker';
import update from 'react-addons-update'; 
import Select from 'react-select';

const getListOfTime = () => {
    let timeList = [];
    for(var i=0; i < 23; i++){
        let numb = (i+1).toString();
        if(numb.length == 1) { 
            numb = "0" + numb+":00";    
        }
        else{
            numb += ":00";
        }
        timeList.push({value: numb, label: numb, index: i})
    }
    timeList.push({value: "00:00", label: "00:00", index: 23});
    return timeList;
}

const getListOfDays = () => {
    let days = [
        {value: "", label: '-'},
        {value: 'Senin', label: 'Senin'},
        {value: 'Selasa', label: 'Selasa'},
        {value: 'Rabu', label: 'Rabu'},
        {value: 'Kamis', label: 'Kamis'},
        {value: 'Jumat', label: 'Jumat'},
        {value: 'Sabtu', label: 'Sabtu'},
        {value: 'Minggu', label: 'Minggu'},
    ]
    return days;
}

class ActivityForm extends Component {
    constructor(){
        super();
        this.state = {
            activityData: {},
            timeList: [],
            dayList: []
        }
    }

    componentWillMount(){
        let myTimeList = getListOfTime();
        let dayList = getListOfDays();

        this.setState({
            activityData: this.props.data,
            timeList: myTimeList,
            dayList: dayList
        })
    }

    bindingTimeIntoComboBox(selectedParamName, paramName){
        let timeList = this.state.timeList;
        let currActData = this.state.activityData;
        let schedules = this.state.activityData.schedules;
        

        for(var i=0; i < schedules.length; i++){            
            for(var j=0; j < timeList.length; j++){
                
                if(schedules[i][`${paramName}`] == timeList[j].value){
                    this.setState({
                        activityData: update(currActData, {
                            schedules: {
                                [i]: {
                                    $merge: {[selectedParamName]: timeList[j]}
                                }
                            }
                        })
                    })          
                }
            }
        }
    }

    bindingDaysIntoComboBox(){
        let dayList = this.state.dayList;
        let schedules = this.state.activityData.schedules;
        let currActData = this.state.activityData;
        console.log(dayList);
        console.log(schedules);

        for(var i=0; i < schedules.length; i++){
            for(var j=0; j < dayList.length; j++){
                if(schedules[i].day == dayList[j].value){
                    console.log(dayList[j]);
                    this.setState({
                        activityData: update(currActData, {
                            schedules: {
                                [i]: { $merge: {selectedDays: dayList[j]} }
                            }
                        })
                    })
                }
            }
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.data != this.props.data){
            this.setState({activityData: this.props.data}, () => {
                this.bindingTimeIntoComboBox("selectedTimeStart", "time_from");

                setTimeout(() => {
                    this.bindingTimeIntoComboBox("selectedTimeEnd", "time_to");
                }, 120);

                setTimeout(() => {
                    this.bindingDaysIntoComboBox();
                }, 150);

            })            
        }
    }

    addSchedule(){
        let data = {
            selectedDays: {value: 'Monday', label: 'Senin'},
            selectedTimeStart: {value: '06:00', label: '06:00'},
            selectedTimeEnd: {value: '07:00', label: '07:00'},
            day: '',
            time_from: '06:00',
            time_to: '07:00'
        }
        
        let currActData = this.state.activityData;

        this.setState({
            activityData: update(currActData, {
                schedules: {$push: [data]}
            })
        })
    }

    // handleTimeChange(paramName, selectedParamName, index, selectedData)  {
    //     console.log(paramName)
    //     console.log(index);
    //     console.log(selectedData);
    //     let currActData = this.state.activityData;

    //     this.setState({
    //         activityData: update(currActData, {
    //             schedules: {
    //                 [index]: {
    //                     [paramName]: {$set: selectedData.value},
    //                     [selectedParamName]: {$set: selectedData}
    //                 }    
    //             }
    //         })
    //     })
    // }

    handleDaysChange(index, selectedData){
        let currActData = this.state.activityData;
        this.setState({
            activityData: update(currActData, {
                schedules: {
                    [index]: {
                        day: {$set: selectedData.value},
                        selectedDays: {$set: selectedData}
                    }
                }
            })
        }, () => { console.log(this.state.activityData.schedules[`${index}`])})
    }

    handleInputOnChange(event){
        let name = event.target.name;
        let currActivityData = this.state.activityData;
        this.setState({
            activityData: update(currActivityData, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleTimeChange(index, event){
        let paramName = event.target.name;        
        let currActivityData = this.state.activityData;
        console.log(paramName);
        this.setState({
            activityData: update(currActivityData, {
                schedules: {
                    [index]: {
                        [paramName]: {$set: event.target.value}
                    }
                }
            })
        }, () => { console.log(this.state)})
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getActivityData(this.state.activityData)
    }

    render() {
        const {activityData} = this.state;
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row mb-5">
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="section--subtitle mb-4">Kegiatan</div>
                        <div className="form-group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="name" value={activityData.name} className="form-control" onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label>Lokasi</label>
                            <input type="text" name="location" value={activityData.location} className="form-control" onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                    </div>
                    <div className="col-md-6">
                    
                        <div className="section--subtitle mb-4">Jadwal Kegiatan</div>
                        {activityData.schedules.map((schedule, index) => 
                            (
                                <div>
                                    <div className="row">
                                        <div className="col-md-4 pl-0">
                                            <div className="form-group">
                                                <label>Hari Kegiatan</label>      
                                                <Select key={index} value={schedule.selectedDays} options={this.state.dayList} onChange={this.handleDaysChange.bind(this, index)} />
                                            </div>
                                            
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label>Jam Mulai</label>
                                                {/* <Select key={index} value={schedule.selectedTimeStart} options={this.state.timeList} onChange={ this.handleTimeChange.bind(this, 'time_from', 'selectedTimeStart', index)} /> */}
                                                <input type="text" name="time_from" value={schedule.time_from} className="form-control" onChange={this.handleTimeChange.bind(this, index)} />
                                            </div>
                                        </div>
                                        <div className="col-md-4 pr-0">
                                            <div className="form-group">
                                                <label>Jam Selesai</label>
                                                {/* <Select key={index} value={schedule.selectedTimeEnd} options={this.state.timeList} onChange={ this.handleTimeChange.bind(this, 'time_to', 'selectedTimeEnd', index)} /> */}
                                                <input type="text" name="time_to" value={schedule.time_to} className="form-control" onChange={this.handleTimeChange.bind(this, index)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        )}
                        <div className="row">
                            <button className="btn btn--base mb-5" type="button" onClick={this.addSchedule.bind(this)}>
                                <i className="fas fa-plus" style={{ fontSize: "12px", marginRight: "5px" }}></i>
                                <span style={{ fontSize: "15px"}}>Kegiatan</span>
                            </button>
                        </div>
                    </div>
                    
                </div>
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/activity/list" role="button" className="btn btn-block btn-danger">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        );
    }
}

export default ActivityForm;