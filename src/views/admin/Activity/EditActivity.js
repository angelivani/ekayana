import React, { Component } from 'react';
import ActivityForm from './ActivityForm';
import {connect} from 'react-redux';
import {getDetailActivity, updateActivity} from '../../../actions/Activity';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return {
        activityData: state.activity.activityData,
        succeed: state.activity.succeed,
        error: state.activity.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        getDetailActivity: (id) => {
            dispatch(getDetailActivity(id))
        },
        updateActivity: (data) => {
            dispatch(updateActivity(data))
        }
    }
}

class EditActivity extends Component {
    constructor(){
        super();
        this.state = {
            activityData: {
                name: '',
                location: '',
                schedules: [
                    {
                        selectedDays: {value: 'Monday', label: 'Senin'},
                        selectedTimeStart: {value: '06:00', label: '06:00'},
                        selectedTimeEnd: {value: '07:00', label: '07:00'},
                        day: 'Monday',
                        time_from: '06:00',
                        time_to: '07:00'
                    }
                ]
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getDetailActivity(id);
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.activityData){
            this.setState({activityData: newProps.activityData})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }        
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetActivityData(data){
        console.log(data);
        this.props.updateActivity(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/activity/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ''});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Update Kegiatan</div>
                    </div>
                </div>

                <div className="pt-4">
                    <ActivityForm data={this.state.activityData} getActivityData={this.handleGetActivityData.bind(this)} />
                    {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                    {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
                </div>
            </div>
        );
    }
}

EditActivity = connect(mapStateToProps, mapDispatchToProps)(EditActivity);
export default EditActivity;