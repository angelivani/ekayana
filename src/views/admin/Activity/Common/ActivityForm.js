import React, { Component } from 'react';
import update from 'react-addons-update'; 
import Select from 'react-select';

const getListOfTime = () => {
    let timeList = [];
    for(var i=0; i < 23; i++){
        let numb = (i+1).toString();
        if(numb.length == 1) { 
            numb = "0" + numb+":00";    
        }
        else{
            numb += ":00";
        }
        timeList.push({value: numb, label: numb, index: i})
    }
    timeList.push({value: "00:00", label: "00:00", index: 23});
    return timeList;
}

const getListOfDays = () => {
    let days = [
        {value: 'Monday', label: 'Senin'},
        {value: 'Tuesday', label: 'Selasa'},
        {value: 'Wednesday', label: 'Rabu'},
        {value: 'Thursday', label: 'Kamis'},
        {value: 'Friday', label: 'Jumat'},
        {value: 'Saturday', label: 'Sabtu'},
        {value: 'Sunday', label: 'Minggu'},
    ]
    return days;
}

class ActivityForm extends Component {
    constructor(){
        super();
        this.state = {
            activityData: {
                name: '',
                location: '',
                scheduleList: [
                    {
                        selectedDays: {value: 'Monday', label: 'Senin'},
                        selectedTimeStart: {value: '06:00', label: '06:00'},
                        selectedTimeEnd: {value: '07:00', label: '07:00'},
                        day: '',
                        timeStart: '06:00',
                        timeEnd: '07:00'
                    }
                ]
            },
            timeList: [],
            dayList: []
        }
    }

    componentWillMount(){
        let myTimeList = getListOfTime();
        let dayList = getListOfDays();

        this.setState({
            timeList: myTimeList,
            dayList: dayList
        })
    }

    handleTimeChange(index, name, time){
        console.log(index);
        console.log(name);
        console.log(time);

        // this.setState({
        //     activityData: update(currActData, {
        //         scheduleList: {
        //             [index]: {
        //                 [name]: {$set: time} 
        //             }    
        //         }
        //     })
        // }, () => {
        //     console.log(this.state);
        // })
    }

    
    addSchedule(){
        let data = {
            selectedTimeStart: {value: '06:00', label: '06:00'},
            selectedTimeEnd: {value: '07:00', label: '07:00'},
            day: '',
            timeStart: '06:00',
            timeEnd: '07:00'
        }
        
        let currActData = this.state.activityData;

        this.setState({
            activityData: update(currActData, {
                scheduleList: {$push: [data]}
            })
        })
    }

    handleTimeChange(paramName, selectedParamName, index, selectedData)  {
        console.log(paramName)
        console.log(index);
        console.log(selectedData);
        let currActData = this.state.activityData;

        this.setState({
            activityData: update(currActData, {
                scheduleList: {
                    [index]: {
                        [paramName]: {$set: selectedData.value},
                        [selectedParamName]: {$set: selectedData}
                    }    
                }
            })
        })   
    }

    render() {
        const {activityData} = this.state;
        return (
            <div>
                <div className="row mb-5">
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="section--subtitle mb-4">Kegiatan</div>
                        <div className="form-group">
                            <label>Nama Kegiatan</label>
                            <input type="text" value={activityData.name} className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Lokasi</label>
                            <input type="text" value={activityData.location} className="form-control" />
                        </div>
                    </div>
                    <div className="col-md-6">
                    
                        <div className="section--subtitle mb-4">Jadwal Kegiatan</div>
                        {activityData.scheduleList.map((schedule, index) => 
                            (
                                <div>
                                    <div className="row">
                                        <div className="col-md-4 pl-0">
                                            <div className="form-group">
                                                <label>Hari Kegiatan</label>      
                                                <Select key={index} value={schedule.selectedDays} options={this.state.dayList} />
                                            </div>
                                            
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label>Jam Mulai</label>
                                                <Select key={index} value={schedule.selectedTimeStart} options={this.state.timeList} onChange={ this.handleTimeChange.bind(this, 'timeStart', 'selectedTimeStart', index)} />
                                            </div>
                                        </div>
                                        <div className="col-md-4 pr-0">
                                            <div className="form-group">
                                                <label>Jam Selesai</label>
                                                <Select key={index} value={schedule.selectedTimeEnd} options={this.state.timeList} onChange={ this.handleTimeChange.bind(this, 'timeEnd', 'selectedTimeEnd', index)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        )}
                        <div className="row">
                            <button className="btn btn--base mb-5" onClick={this.addSchedule.bind(this)}>
                                <i className="fas fa-plus" style={{ fontSize: "12px", marginRight: "5px" }}></i>
                                <span style={{ fontSize: "15px"}}>Kegiatan</span>
                            </button>
                        </div>
                    </div>
                    
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <button className="btn btn-block btn-danger">Cancel</button>
                    </div>
                    <div className="col-md-6">
                        <button className="btn btn-block btn-success">Simpan</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActivityForm;