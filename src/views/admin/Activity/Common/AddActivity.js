import React, { Component } from 'react';
import ActivityForm from './ActivityForm';

class AddActivity extends Component {
    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Buat Kegiatan</div>
                    </div>
                </div>

                <div className="pt-4">
                    <ActivityForm />
                </div>
                
                
            </div>
        );
    }
}

export default AddActivity;