import React, {Component} from 'react';
import ActivityCard from '../../../component/client/activity_card/ActivityCard';

import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';

import {getActivityList, deleteActivity} from '../../../actions/Activity';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return{
      activityList: state.activity.activityList,
      succeed: state.activity.succeed,
      error: state.activity.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getActivityList: (data) => {
            dispatch(getActivityList(data))
        },
        deleteActivity: (id) => {
            dispatch(deleteActivity(id))
        }
    }
}

class ActivityList extends Component{
    constructor(){
        super();
        this.state = {
            limit: 20,
            offset: 0,
            activityList: null,

            successMessage: '',
            errorMessage: '',
            confirmationMsg: 'Apakah Anda yakin ingin menghapus data ini?',
            showConfirmationModal: false,
            idToDelete: -1
        }
    }

    componentWillMount(){
        this.fetchActivityListByOffset(0);
    }

    componentWillReceiveProps(newProps){
        if(this.state.activityList != newProps.activityList){
            this.setState({
                activityList: newProps.activityList
            })
        }
        console.log(this.state.successMessage);
        console.log(newProps.succeed)
        if(this.state.successMessage !== newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
    }

    fetchActivityListByOffset(offset){
        let data = {
            limit: this.state.limit,
            offset: offset
        }
        this.props.getActivityList(data);
    }

    handleEditBtnClicked(id){
        this.props.history.push(`/admin/activity/edit/${id}`)
    }

    handleDeleteBtnClicked(id){
        this.setState({showConfirmationModal: true, idToDelete: id});
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleSuccessModalClose(){
        console.log('handle success close')
        this.setState({successMessage: ''});
        this.fetchActivityListByOffset(0);
    }

    handleConfirmationModalClose(btnType){
        this.setState({showConfirmationModal: false});
        
        if(btnType == "yes"){
            this.props.deleteActivity(this.state.idToDelete);
        }
    }

    render(){
        return(
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/activity/add">
                        <i className="fas fa-plus mr-2"></i> Aktivitas
                    </a>
                </div>                
                <div className="section--title border-bottom">Activity List</div>                
                <div className="mt-4">
                    {
                        this.state.activityList ?  
                        <div className="row">
                            {this.state.activityList.map((activity, index) => 
                                (
                                    <div className="col-sm-4 col-xs-12 m-bottom-10">
                                        <ActivityCard card={activity}>
                                            <div className="d-flex justify-content-end">
                                                <button className="btn btn-info mr-2" onClick={() => this.handleEditBtnClicked(activity.id)}>
                                                    <i className="fas fa-pencil-alt"></i>
                                                </button>
                                                <button className="btn btn-danger" onClick={() => this.handleDeleteBtnClicked(activity.id)}>
                                                    <i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </ActivityCard>
                                    </div>
                                )
                            )}                           
                        </div> : "Loading"
                    }
                </div>
                { this.state.showConfirmationModal ? <ConfirmationModal confirmationMessage={this.state.confirmationMsg} whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> : null }
                { this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> : null }
                { this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
                
            </div>
        )
    }
}

ActivityList = connect(mapStateToProps, mapDispatchToProps)(ActivityList);
export default ActivityList;
