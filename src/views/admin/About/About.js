import React, { Component } from 'react';
import { aboutConstant } from '../../../constants/AboutEkayana';

class About extends Component { 
    constructor(){
        super();
        this.aboutMenuList = [
            {name: 'WIHARA', url: 'general-desc/edit'},
            {name: 'PIMPINAN', url: 'leader/list'},
            {name: 'SEJARAH SINGKAT', url: 'history/update'},
            {name: 'PENDIDIKAN DAN PELATIHAN', url: 'education-training/update'},
            {name: 'KEBAKTIAN DAN PELAYANAN UMAT', url: 'prayer/update'},
            {name: 'PELAYANAN SOSIAL', url: 'social-service/update'},
            {name: 'MEDIA DAN UNIT USAHA', url: 'media/update'},
            {name: 'GENERASI MUDA, SENI BUDAYA, DAN OLAHRAGA', url:'generation-art-culture/update'},
            {name: 'FASILITAS', url: 'facility/list'}
        ]
    }

    handleOnClick(menu){
        this.props.history.push(`/admin/${menu}`);
    }

    render(){
        return (
            <div className="d-flex justify-content-center page__section" style={{ backgroundColor: '#FF8C00'}}>
                <div className="d-flex flex-wrap about_menu_section">
                    {this.aboutMenuList.map((menu, index) => 
                        (
                            <div className="d-flex justify-content-center align-items-center tile" key={index} onClick={ () => {this.handleOnClick(menu.url)} }>
                                <span>{menu.name}</span>
                                <div className="tile-underline"></div>
                            </div>
                        )
                    )}
                </div>
            </div>
        )
    }
}

export default About;