import React, {Component} from 'react';
import update from 'react-addons-update'; 
import DatePicker from 'react-datepicker';
import moment from 'moment';


export default class EventForm extends Component {
    constructor(){
        super();
        this.state = {
            eventData: {},
            imageUrl: ''
        }
    }

    componentWillMount() {
        this.setState({
            eventData: this.props.eventData,
            imageUrl: this.props.iamgeUrl
        });
    }

    componentDidUpdate(prevProps){
        if(prevProps.eventData != this.props.eventData){
            this.setState({
                eventData: this.props.eventData
            })
        }
        
        if(prevProps.imageUrl != this.props.imageUrl){
            this.setState({
                imageUrl: this.props.imageUrl
            })
        }
    }

    handleInputOnChange(event){
        let name = event.target.name
        let currEvent = this.state.eventData;
        this.setState({
            eventData: update(currEvent, {
                [name]: {$set: event.target.value}
            })
        })
    }

    onDateChanged(data){
        console.log(moment(data).format('YYYY-MM-DD'));

        let currEvent = this.state.eventData;
        this.setState({
            eventData: update(currEvent, {
                date: {$set: data},
                event_date: {$set: moment(data).format('YYYY-MM-DD')}
            })
        }, () => {
            console.log(this.state.eventData);
        })
    }

    handleFileOnChange(data){
        console.log(data.target.files[0]);
        

        var reader = new FileReader();
        var imageBase64 = "";
        reader.onload = function(e){
            console.log(e.target.result);
            imageBase64 = e.target.result;
        }
        reader.readAsDataURL(data.target.files[0]);
        setTimeout(() => { this.setState({imageUrl: imageBase64}); }, 100);
        
        let images = [];
        images.push(data.target.files[0]);

        let currEvent = this.state.eventData;
        this.setState({
            eventData: update(currEvent, {
                image: {$set: images}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getEventData(this.state.eventData);
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">  
                        <div className="form-group">
                            <label>Nama Event</label>
                            <input type="text" name="name" className="form-control" value={this.state.eventData.name} onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                        
                        <div className="form-group">
                            <label>Deskripsi Event</label>
                            <textarea className="form-control serve__whitespace" name="description" rows="10" value={this.state.eventData.description} onChange={this.handleInputOnChange.bind(this)}></textarea>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tanggal Event</label> 
                            <DatePicker className="form-control" onChange={this.onDateChanged.bind(this)} selected={this.state.eventData.date} dateFormat="YYYY-MM-DD" />
                        </div>
                        <div className="form-group">
                            <label>Gambar Event</label>
                            <div className="img__upload">
                                {!this.state.imageUrl ? <label>EVENT IMAGE</label> : null }
                                {this.state.imageUrl ? <img src={this.state.imageUrl} /> : null }
                            </div>
                            
                        </div>
                        <label className="btn--file-upload">Browse here<input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)}/></label>    
                        
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/event/list" className="btn btn-block btn-danger" role="button">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        )
    }
}