import React, {Component} from 'react';
import EventForm from './EventForm';
import moment from 'moment';

import {connect} from 'react-redux';
import {editEvent, getEventDetail} from '../../../actions/Event';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import {API_URL, IMAGE_URL} from '../../../constants/ActionTypes';

const mapStateToProps = (state) => {
    return{
        eventData: state.event.eventData,
        succeed: state.event.succeed,
        error: state.event.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        editEvent: (data) => {
            dispatch(editEvent(data))
        },
        getEventDetail: (id) => {
            dispatch(getEventDetail(id))
        }
    }
}

class EditEvent extends Component {
    constructor(){
        super();
        this.state = {
            eventData: {
                name: '',
                description: '',
                date: moment(),
                event_date: moment().format('YYYY-MM-DD'),
                image: [],
            },
            imageUrl: '',
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getEventDetail(id)
    }

    componentWillReceiveProps(newProps){
        console.log(newProps.eventData);
        if(newProps.eventData){
            let newEvent = {
                id: newProps.eventData.id,
                name: newProps.eventData.name,
                event_date: moment(newProps.eventData.event_date).format('YYYY-MM-DD'),
                date: moment(newProps.eventData.event_date),
                description: newProps.eventData.description
            }
            let imageUrl;
            if(newProps.eventData.images.length > 0){
                imageUrl = `${IMAGE_URL}/public/images/event/${newProps.eventData.images[0].image}`;
            } 
            else { imageUrl = '' }
            

            this.setState({
                eventData: newEvent,
                imageUrl: imageUrl
            });
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetEventData(data){
        console.log(data);
        this.props.editEvent(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});    
        this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(
            <div className="form__wrapper">
                <div className="row border-bottom">
                    <div className="col-md-12">
                        <div className="section--title">Edit Event</div>
                    </div>
                </div>
                <div className="pt-4">
                    <EventForm eventData = {this.state.eventData} getEventData={this.handleGetEventData.bind(this)} imageUrl={this.state.imageUrl} />
                </div>
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
            
        )
    }
}

EditEvent = connect(mapStateToProps, mapDispatchToProps)(EditEvent);
export default EditEvent;