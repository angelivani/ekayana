import React, { Component } from 'react';
import EventCard from '../../../component/client/event/EventCard';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

import {getEventList, deleteEvent, pinningEvent, unpinEvent} from '../../../actions/Event';
import {connect} from 'react-redux';
import DefaultPagination from '../../../component/shared/pagination/DefaultPagination';

const mapStateToProps = (state) => {
    return{
        eventList: state.event.eventList,
        totalData: state.event.totalData,
        succeed: state.event.succeed,
        error: state.event.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getEventList: (data) => {
            dispatch(getEventList(data))
        },
        deleteEvent: (id) => {
            dispatch(deleteEvent(id))
        },
        pinningEvent: (data) => {
            dispatch(pinningEvent(data))
        },
        unpinEvent: (data) => {
            dispatch(unpinEvent(data))
        }
    }
}

class EventList extends Component {
    constructor(){
        super();
        this.state = {
            eventList: null,
            totalData: 0,
            limit: 10,
            showConfirmationModal: false,
            eventIdToDelete: null,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        this.props.getEventList({
            limit: this.state.limit,
            offset: 0
        });
    }

    componentWillReceiveProps(newProps){ 
        console.log(newProps);
        if(this.state.eventList != newProps.eventList){
            this.setState({
                eventList: newProps.eventList
            }, () => {
                console.log(this.state.eventList);
            })
        }
        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData }, () => { console.log(this.state)})
        }
        if(this.state.successMessage != newProps.succeed){
            this.setState({ successMessage: newProps.succeed}, () => {console.log(this.state) });
        }
        
        if(this.state.errorMessage != newProps.error){
            this.setState({ errorMessage: newProps.error });
        }
    }

    handleDeleteBtnClicked(eventId){
        this.setState({
            showConfirmationModal: true,
            eventIdToDelete: eventId
        })
    } 
    
    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteEvent(this.state.eventIdToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getEventList({
            limit: this.state.limit,
            offset: 0
        });
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handlePinEvent(eventId){
        let data = {
            id: eventId
        }
        this.props.pinningEvent(data);
    }

    handleGetDataOnPageClicked(data){
        let offset = data * this.state.limit - this.state.limit;
        this.props.getEventList({
            limit: this.state.limit,
            offset: offset
        });
    }

    handleUnpinEvent(eventId){
        let data = {
            id: eventId
        }
        this.props.unpinEvent(data);
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/event/add">
                        <i className="fas fa-plus mr-2"></i> Event
                    </a>
                </div>
                <div className="section--title border-bottom">List Event</div>
                
               

                {this.state.eventList ? 
                    <div>
                        {
                            this.state.eventList.map((event, index) => (
                                <EventCard event = {event} key={index+4}>
                                    <div className="row m-0" key={index + 3}>
                                        <div className="col-md-12 m-0 p-0" key={index + 2}>
                                            <a href={`/admin/event/edit/${event.id}`} role="button" className="btn btn-info mr-2" key={index + 1}>Edit</a>                                            
                                            <button className="btn btn-danger mr-2" key = {index} onClick={() => this.handleDeleteBtnClicked(event.id)} >Hapus</button>
                                            {event.pinned == 1 ?                                                  
                                                <button className="btn btn-dark" key={index} onClick={() => this.handleUnpinEvent(event.id) }>Unpin</button> :
                                                <button className="btn btn--base" key={index} onClick={() => this.handlePinEvent(event.id) }>Pin</button>
                                            }
                                            
                                        </div>
                                    </div>
                                </EventCard>
                            ))
                        }
                    </div> 
                : null }
                
               {this.state.totalData && <DefaultPagination totalData={this.state.totalData} limit={this.state.limit} getDataOnPageClicked={this.handleGetDataOnPageClicked.bind(this)} /> } 
               {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus event ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
               {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
               {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

EventList = connect(mapStateToProps, mapDispatchToProps)(EventList);
export default EventList;