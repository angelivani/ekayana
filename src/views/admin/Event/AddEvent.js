import React, {Component} from 'react';
import EventForm from './EventForm';
import moment from 'moment';

import {connect} from 'react-redux';
import {addEvent} from '../../../actions/Event';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      succeed: state.event.succeed,
      error: state.event.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addEvent: (data) => {
            dispatch(addEvent(data))
        }
    }
}

class AddEvent extends Component {
    constructor(){
        super();
        this.state = {
            eventData: {
                name: '',
                description: '',
                date: moment(),
                event_date: moment().format('YYYY-MM-DD'),
                image: []
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetEventData(data){
        console.log(data);
        this.props.addEvent(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(
            <div className="form__wrapper">
                <div className="row border-bottom">
                    <div className="col-md-12">
                        <div className="section--title">Tambah Event</div>
                    </div>
                </div>
                <div className="pt-4">
                    <EventForm eventData = {this.state.eventData} getEventData={this.handleGetEventData.bind(this)} />
                </div>
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
            
        )
    }
}

AddEvent = connect(mapStateToProps, mapDispatchToProps)(AddEvent);
export default AddEvent;