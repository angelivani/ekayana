import React, { Component } from 'react';
import Select from 'react-select';
import Modal from 'react-modal';

const customStyles = {
	overlay: {
		position: 'fixed',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: 'rgba(0, 0, 0, 0.75)'
	},
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		borderRadius: '4px',
		padding: '10px'
	}
};

class UserModal extends Component {
	constructor() {
		super();
		this.state = {
			isModalOpen: false,
			userData: null,
			userStatusList: [
				{ value: '1', label: 'Aktif' },
				{ value: '0', label: 'Non Aktif' }
			],
			selectedUserStatus: null
		}	
	}

	componentWillMount() {
		const userData = this.props.userData;
		let selectedStatus = null;

		for (let user of this.state.userStatusList) {
			if (parseInt(user.value) === userData.active) {
				selectedStatus = user;
				break;
			}
		}

		this.setState({
			isModalOpen: true,
			userData: userData,
			selectedUserStatus: selectedStatus
		})
	}

	componentWillReceiveProps(newProps) {
		this.setState({
			userData: newProps.userData
		})
	}

	handleStatusChange(data) {
		let userData = this.state.userData;
		userData = Object.assign(userData, { active: parseInt(data.value) });
		this.setState({
			selectedUserStatus: data,
			userData: userData
		});
	}

	hideModal() {
		this.setState({ isModalOpen: false });
	}

	handleSubmit(e) {
		e.preventDefault();
		this.setState({ isModalOpen: false });
		this.props.getUserDataUpdated(this.state.userData);
	}

	handleInputOnChange(event) {
		let name = event.target.name;
		let currUserData = this.state.userData;
		currUserData = Object.assign(currUserData, { [name]: event.target.value });
		this.setState({
			userData: currUserData
		})
	}

	render() {
		return (
			<Modal isOpen={this.state.isModalOpen} style={customStyles}>
				<div className="d-flex justify-content-end">
					<button className="btn btn-danger" onClick={() => this.hideModal()}>X</button>	
				</div>
				<form className="pl-5 pr-5 pb-5 pt-2" onSubmit={this.handleSubmit.bind(this)}>
					<h3>Update User</h3>	
					<hr />
					<div className="form-group">
						<label>Username</label>
						<input type="text" disabled name="username" value={this.state.userData.username} className="form-control" />
					</div>
					<div className="form-group">
						<label>Select</label>
						<Select value={this.state.selectedUserStatus} options={this.state.userStatusList} onChange={this.handleStatusChange.bind(this)} />
					</div>
					<div className="form-group">
						<label>Old Password</label>
						<input type="password" name="old_password" className="form-control" onChange={this.handleInputOnChange.bind(this)} />
					</div>
					<div className="form-group">
						<label>New Password</label>
						<input type="password" name="new_password" className="form-control" onChange={this.handleInputOnChange.bind(this)} />
					</div>
					<div className="row">
						<div className="col-12">
							<button className="btn btn-block btn-success">Save</button>
						</div>
					</div>
				</form>
				
			</Modal>
		);
	}
}

export default UserModal;