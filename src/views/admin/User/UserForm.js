import React, { Component } from 'react';
import update from 'react-addons-update'; 

class UserForm extends Component {
    constructor(){
        super();
        this.state = {
            userData: {}
        }
    }

    componentWillMount(){
        this.setState({
            userData: this.props.data
        })
    }
    
    componentDidUpdate(prevProps, prevState){
        if(prevProps.data != this.props.data){
            this.setState({userData: this.props.data});
        }
    }

    handleInputOnChange(event){
        let name = event.target.name;
        let currUserData = this.state.userData;
        this.setState({
            userData: update(currUserData, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getUserData(this.state.userData);
    }
    
    render(){
        const { userData } = this.state;
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name="full_name" value={userData.full_name} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                        <div className="form-group">
                            <label>Username</label>
                            <input type="text" name="username" value={userData.username} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" name="password" value={userData.password} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Email</label>
                            <input type="email" name="email" value={userData.email} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                        <div className="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" value={userData.phone} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                    </div>
                </div>
                <div className="mt-3">
                    <div className="row">
                        <div className="col-6">
                            <a href="/" className="btn btn-block btn-danger">Cancel</a>
                        </div>
                        <div className="col-6">
                            <button className="btn btn-block btn-success">Simpan</button>
                        </div>
                    </div>
                </div>
                
            </form> 
        )
    }
}

export default UserForm;