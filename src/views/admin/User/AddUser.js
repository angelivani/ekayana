import React, {Component} from 'react';

import {addUser} from '../../../actions/User';
import {connect} from 'react-redux';
import UserForm from './UserForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      succeed: state.user.succeed,
      error: state.user.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addUser: (data) => {
            dispatch(addUser(data))
        }
    }
}

class AddUser extends Component {
    constructor(){
        super();
        this.state = {
            userData: {
                username: '',
                password: '',
                full_name: '',
                email: '',
                phone: ''
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({
                successMessage: newProps.succeed
            })
        }
        else if(newProps.error){
            this.setState({
                errorMessage: newProps.error
            })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        // this.props.history.push('/admin/albumn/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleGetUserData(data){
        console.log(data);
        this.props.addUser(data);
    }

    render(){
        return(
        <div className="form__wrapper">
            <div className="row">
                <div className="col-md-12">
                    <div className="section--title">Add User</div>
                </div>
            </div>
            <div className="pt-4">
                <UserForm data={this.state.userData} getUserData={this.handleGetUserData.bind(this)} />
            </div>
            {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
            {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
        </div>
        )
    }
}

AddUser = connect(mapStateToProps, mapDispatchToProps)(AddUser);
export default AddUser;
