import React, { Component } from 'react';
import UserModal from './Modal/UserModal';
import { getUserAdminList, updateUserAdmin, resetUserProps } from '../../../actions/User';
import { connect } from 'react-redux';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
	return {
		userList: state.user.userList,
		succeed: state.user.succeed,
		error: state.user.error,
	}
}

const mapDispatchToProps = (dispatch, props) => {
	return {
		getUserAdminList: () => {
			dispatch(getUserAdminList())
		},
		updateUserAdmin: (data) => {
			dispatch(updateUserAdmin(data))
		},
		resetUserProps: () => {
			dispatch(resetUserProps())
		}
	}
}

class UserList extends Component {
	constructor() {
		super();
		this.state = {
			userList: null,
			popUserModal: false,
			selectedUser: null,
			successMessage: null,
			errorMessage: null
		}
	}

	componentWillMount() {
		this.props.getUserAdminList();
	}

	componentWillReceiveProps(newProps) {
		console.log('succeed props', newProps.succeed);
		if (this.state.userList != newProps.userList) {
			this.setState({
				userList: newProps.userList
			})
		}
		if (this.state.successMessage !== newProps.succeed) {
			this.setState({
				successMessage: newProps.succeed
			})
		}
		if (this.state.errorMessage !== newProps.error) {
			this.setState({
				errorMessage: 'Ada ketidakcocokan dengan password lama user'
			})
		}
	}

	showUserModal(user) {
		this.setState({
			popUserModal: true,
			selectedUser: user
		})
	}

	handleSuccessModalClose() {
		this.setState({ successMessage: null });
		this.props.resetUserProps();
		this.props.history.push('/admin/user/list');
	}

	handleWarningModalClose() {
		this.setState({ errorMessage: null });
		this.props.resetUserProps();
	}

	handleUserDataUpdated(data) {
		this.setState({ popUserModal: false });
		console.log('user updated', data);
		this.props.updateUserAdmin(data);
	}

	render() {
		return (
			<div className="base__wrapper">
				<div className="d-flex justify-content-end">
					<a href="/admin/user/add" className="btn btn--base">
						<i className="fas fa-plus mr-2"></i>
						<span className="font-weight-bold">User</span>
					</a>
				</div>
				<table className="table">
					<thead>
						<tr>
							<th>Username</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>	
					<tbody>
						{this.state.userList && this.state.userList.map((user, index) =>
							(
								<tr>
									<td>{user.username}</td>
									<td>{user.active === 1 ? 'Aktif' : 'Non Aktif'}</td>
									<td>
										<button className="btn btn-warning clr-white" onClick={() => this.showUserModal(user)}>Update</button>
									</td>
								</tr>
							)
						)}
					</tbody>
				</table>
				{this.state.popUserModal && <UserModal userData={this.state.selectedUser} getUserDataUpdated={this.handleUserDataUpdated.bind(this)} />}
				{this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> : null}
				{this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null}
			</div>
		);
	}
}
UserList = connect(mapStateToProps, mapDispatchToProps)(UserList);
export default UserList;