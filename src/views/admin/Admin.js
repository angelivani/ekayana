import React, { Component } from 'react';
import {Route, Redirect} from 'react-router-dom';

import Login from './Login/Login';
import PrivateRoute from '../../router/PrivateRoute';

import BuddhavacanaList from './Buddhavacana/BuddhavacanaList';
import AddBuddhavacana from './Buddhavacana/AddBuddhavacana';
import EditBuddhavacana from './Buddhavacana/EditBuddhavacana';

import ActivityList from './Activity/ActivityList';
import AddActivity from './Activity/AddActivity';
import EditActivity from './Activity/EditActivity';

import Sidenav from '../../component/admin/sidenav/Sidenav';
import MainHeader from '../../component/admin/header/MainHeader';

import EventList from './Event/EventList';
import AddEvent from './Event/AddEvent';
import EditEvent from './Event/EditEvent';

import AlbumnList from './Albumn/AlbumnList';
import AddAlbumn from './Albumn/AddAlbumn';
import EditAlbumn from './Albumn/EditAlbumn';

import AddPhotos from './Photos/AddPhotos';
import PhotosList from './Photos/PhotosList';

import EbookList from './Ebook/EbookList';
import AddEbook from './Ebook/AddEbook';
import EditEbook from './Ebook/EditEbook';

import AddUser from './User/AddUser';

import AddArticle from './Article/AddArticle';
import EditArticle from './Article/EditArticle';
import ArticleList from './Article/ArticleList';

import AddFacility from './Facility/AddFacility';
import EditFacility from './Facility/EditFacility';

import LeaderList from './Leader/LeaderList';
import AddLeader from './Leader/AddLeader';
import EditLeader from './Leader/EditLeader';

import EditWebContent from './Content/EditWebContent';
import FacilityList from './Facility/FacilityList';

import { withCookies, Cookies } from 'react-cookie';
import EditDynamicContent from './DynamicContent/EditDynamicContent';
import About from './About/About';
import UserList from './User/UserList';
import EditUser from './User/EditUser';

class Admin extends Component {
	constructor(){
		super();
		this.state = {
			toggleStatus: true
		}
	}

	renderClass(){
		if (this.props.cookies.get('ek_accessToken')) {
			if(this.state.toggleStatus) return 'admin-content-wrapper-open';
			else return 'admin-content-wrapper-close';
		}
		else {
			return 'default__wrapper';
		}
	}

	handleGetToggleState(toggleState){
		console.log(toggleState);
		this.setState({
			toggleStatus: toggleState
		})
	}

	hideSidenav(){
		this.setState({toggleStatus: false})
	}

	render() {
		const {match} = this.props;

		return(
			<div>
				<div className={`${this.state.toggleStatus ? 'masking' : ''}`} onClick={this.hideSidenav.bind(this)}></div>
					<div>
						{ this.props.cookies.get("ek_accessToken") ? <Sidenav sidenavExpand={this.state.toggleStatus} /> : null }
						<div className={this.renderClass()}>
							{ this.props.cookies.get("ek_accessToken") ? <MainHeader toggleStatus={this.state.toggleStatus} getToggleState={this.handleGetToggleState.bind(this)} /> : null }

						<Route path={`${match.path}/login`} component={Login} />
						<PrivateRoute path={`${match.path}/user/list`} component={UserList} />
						<PrivateRoute path={`${match.path}/user/add`} component={AddUser} />
						<PrivateRoute path={`${match.path}/user/edit/:id`} component={EditUser} />

						<PrivateRoute path={`${match.path}/buddhavacana/list`} component={BuddhavacanaList} />

						<PrivateRoute path={`${match.path}/buddhavacana/add`} component={AddBuddhavacana} />
						<PrivateRoute path={`${match.path}/buddhavacana/edit/:id`} component={EditBuddhavacana} />

						<PrivateRoute path={`${match.path}/event/list`} component={EventList} />
						<PrivateRoute path={`${match.path}/event/add`} component={AddEvent} />
						<PrivateRoute path={`${match.path}/event/edit/:id`} component={EditEvent} />

						<PrivateRoute path={`${match.path}/activity/list`} component={ActivityList} />
						<PrivateRoute path={`${match.path}/activity/add`} component={AddActivity} />
						<PrivateRoute path={`${match.path}/activity/edit/:id`} component={EditActivity} />

						<PrivateRoute path={`${match.path}/albumn/list`} component={AlbumnList} />
						<PrivateRoute path={`${match.path}/albumn/add`}  component={AddAlbumn} />
						<PrivateRoute path={`${match.path}/albumn/edit/:id`} component={EditAlbumn} />

						<PrivateRoute path={`${match.path}/photos/list`} component={PhotosList} />
						<PrivateRoute path={`${match.path}/photos/add`} component={AddPhotos} />

						<PrivateRoute path={`${match.path}/ebook/list`} component={EbookList} />
						<PrivateRoute path={`${match.path}/ebook/add`} component={AddEbook} />
						<PrivateRoute path={`${match.path}/ebook/edit/:id`} component={EditEbook} />

						<PrivateRoute path={`${match.path}/article/list`} component={ArticleList} />
						<PrivateRoute path={`${match.path}/article/add`} component={AddArticle} />
						<PrivateRoute path={`${match.path}/article/edit/:id`} component={EditArticle} />

						<PrivateRoute path={`${match.path}/facility/list`} component={FacilityList} />
						<PrivateRoute path={`${match.path}/facility/add`} component={AddFacility} />
						<PrivateRoute path={`${match.path}/facility/edit/:id`} component={EditFacility} />

						<PrivateRoute path={`${match.path}/leader/list`} component={LeaderList} />
						<PrivateRoute path={`${match.path}/leader/add`} component={AddLeader} />
						<PrivateRoute path={`${match.path}/leader/edit/:id`} component={EditLeader} />

						<PrivateRoute exact path={`${match.path}/about`} component={About} />
						<PrivateRoute exact path={`${match.path}/general-desc/edit`} component={EditWebContent} />

						<PrivateRoute exact path={`${match.path}/:type/update`} component={EditDynamicContent} />
					</div>
				</div>
			</div>
		)        
	}
}

Admin = withCookies(Admin);
export default Admin;