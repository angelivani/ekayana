import React, { Component } from 'react';
import LeaderForm from './LeaderForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import { getLeaderDetail, editLeader } from '../../../actions/Leader';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return {
        leaderData: state.leader.leaderData,
        succeed: state.leader.succeed,
        error: state.leader.error,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        getLeaderDetail: (id) => {
            dispatch(getLeaderDetail(id))
        },
        editLeader: (data) => {
            dispatch(editLeader(data))
        }
    }
}

class EditLeader extends Component {
    constructor(){
        super();
        this.state = {
            leaderData: {
                name: '',
                position: '',
                description: '',
                image: null
            },
            imageUrl: '',
            successMessage: null,
            errorMessage: null
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getLeaderDetail(id);
    }

    componentWillReceiveProps(newProps){
        if(this.state.leaderData != newProps.leaderData){
           this.setState({leaderData: newProps.leaderData}) 
        }

        if(this.state.imageUrl != newProps.leaderData.image){
            this.setState({imageUrl: `${IMAGE_URL}/public/images/leader/${newProps.leaderData.image}`})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetLeaderData(data){
        console.log(data);
        this.props.editLeader(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/leader/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Leader</div>
                    </div>
                </div>
                <LeaderForm leaderData={this.state.leaderData} imageUrl={this.state.imageUrl} getLeaderData={this.handleGetLeaderData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

EditLeader = connect(mapStateToProps, mapDispatchToProps)(EditLeader);
export default EditLeader;