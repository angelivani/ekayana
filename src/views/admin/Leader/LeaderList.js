import React, { Component } from 'react';
import { getLeaderList, deleteLeader } from '../../../actions/Leader';
import { IMAGE_URL } from '../../../constants/ActionTypes';

import EmbossCard from '../../../component/client/emboss_card/EmbossCard';
import { connect } from 'react-redux';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import DefaultPagination from '../../../component/shared/pagination/DefaultPagination';

const mapStateToProps = (state) => {
    return {
        leaderList: state.leader.leaderList,
        totalData: state.leader.totalData,
        succeed: state.leader.succeed,
        error: state.leader.error,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getLeaderList: (data) => {
            dispatch(getLeaderList(data))
        },
        deleteLeader: (id) => {
            dispatch(deleteLeader(id))
        }
    }
}

class LeaderList extends Component {
    constructor(){
        super();
        this.state = {
            limit: 10,
            offset: 0,
            leaderList: [],
            totalData: null,
            showConfirmationModal: false,
            leaderIdToDelete: null,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        this.props.getLeaderList({
            limit: this.state.limit,
            offset: this.state.offset
        })
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        this.setState({ leaderList: newProps.leaderList })

        if(this.state.leaderList != newProps.leaderList){
            this.setState({ leaderList: newProps.leaderList })
        }
        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData })
        }
        if(this.state.successMessage != newProps.succeed){
            this.setState({successMessage: newProps.succeed}, () => {console.log(this.state)});
        }
        
        if(this.state.errorMessage != newProps.error){
            this.setState({errorMessage: newProps.error});
        }

    }

    handleDeleteOnClicked(id){
        this.setState({
            showConfirmationModal: true,
            leaderIdToDelete: id
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteLeader(this.state.leaderIdToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getLeaderList({
            limit: 10,
            offset: 0
        });
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }
    
    handleGetDataOnPageClicked(data){
        let offset = data * this.state.limit - this.state.limit;
        this.props.getLeaderList({
            limit: this.state.limit,
            offset: offset
        });
    }

    render(){
        return(
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/leader/add">
                        <i className="fas fa-plus mr-2"></i> Leader
                    </a>
                </div>
                <div className="section--title border-bottom">List Leader</div>

                <div className="row mt-5">
                    {this.state.leaderList && this.state.leaderList.map((leader, index) => 
                        (
                        <div className="col-md-3 mb-5">
                            <EmbossCard image={`${IMAGE_URL}/public/images/leader/${leader.image}`} title={leader.name} subtitle={leader.position} description={leader.description} />
                                            
                            <div className="d-flex justify-content-center m-1">
                                <button className="btn btn-info col-6 mr-2" onClick={() => this.props.history.push(`/admin/leader/edit/${leader.id}`)}>Edit</button>
                                <button className="btn btn-danger col-6" onClick={ () => this.handleDeleteOnClicked(leader.id)}>Delete</button>
                            </div>
                        </div>
                        )
                    )}
                </div>
                
                {this.state.totalData > 0 && <DefaultPagination totalData={this.state.totalData} limit={this.state.limit} getDataOnPageClicked={this.handleGetDataOnPageClicked.bind(this)} />}
                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus leader ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }

            </div>
        )
    }
}
LeaderList = connect(mapStateToProps, mapDispatchToProps)(LeaderList);
export default LeaderList;