import React, { Component } from 'react';
import LeaderForm from './LeaderForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { addLeader } from '../../../actions/Leader';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return {
        succeed: state.leader.succeed,
        error: state.leader.error,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        addLeader: (data) => {
            dispatch(addLeader(data))
        }
    }
}

class AddLeader extends Component {
    constructor(){
        super()
        this.state = {
            leaderData: {
                name: '',
                position: '',
                description: '',
                image: null
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetLeaderData(data){
        console.log(data);
        this.props.addLeader(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/leader/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Leader</div>
                    </div>
                </div>
                <LeaderForm leaderData={this.state.leaderData} getLeaderData={this.handleGetLeaderData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

AddLeader = connect(mapStateToProps, mapDispatchToProps)(AddLeader);
export default AddLeader;