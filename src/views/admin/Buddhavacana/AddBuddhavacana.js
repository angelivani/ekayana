import React, { Component } from 'react';
import BuddhavacanaForm from './BuddhavacanaForrm';
import {connect} from 'react-redux';
import {addBuddhavacana} from '../../../actions/Buddhavacana';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      succeed: state.buddhavacana.succeed,
      error: state.buddhavacana.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addBuddhavacana: (data) => {
            dispatch(addBuddhavacana(data))
        }
    }
}

class AddBuddhavacana extends Component {
    constructor(){
        super();
        this.state = {
            buddhavacanaData: {
                source: '',
                title: '',
                description: ''
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    handleGetBuddhavacanaData(data){
        console.log(data);
        this.props.addBuddhavacana(data);
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.succeed){
            this.setState({
                successMessage: newProps.succeed
            })
        }
        else if(newProps.error){
            this.setState({
                errorMessage: newProps.error
            })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/buddhavacana/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Buddhavacana</div>
                    </div>
                </div>
                <div className="pt-4">
                    <BuddhavacanaForm data = {this.state.buddhavacanaData} getBuddhavacanaData = {this.handleGetBuddhavacanaData.bind(this)} />
                </div>
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

AddBuddhavacana = connect(mapStateToProps, mapDispatchToProps)(AddBuddhavacana);
export default AddBuddhavacana;