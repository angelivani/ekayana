import React, { Component } from 'react';
import BuddhavacanaForm from './BuddhavacanaForrm';
import {connect} from 'react-redux';
import {getBuddhavacanaDetail, updateBuddhavacana} from '../../../actions/Buddhavacana';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
        buddhavacanaData: state.buddhavacana.buddhavacanaData,
        succeed: state.buddhavacana.succeed,
        error: state.buddhavacana.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        getBuddhavacanaDetail: (id) => {
            dispatch(getBuddhavacanaDetail(id))
        },
        updateBuddhavacana: (data) => {
            dispatch(updateBuddhavacana(data))
        }
    }
}

class EditBuddhavacana extends Component {
    constructor(){
        super();
        this.state = {
            buddhavacanaData: {
                id: 0,
                source: '',
                title: '',
                description: ''
            }
        }
    }
    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getBuddhavacanaDetail(id);
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.buddhavacanaData){
            this.setState({buddhavacanaData: newProps.buddhavacanaData})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetBuddhavacanaData(data){
        console.log(data);
        this.props.updateBuddhavacana(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/buddhavacana/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <h1>Edit Buddhavacana</h1>
                <BuddhavacanaForm data = {this.state.buddhavacanaData} getBuddhavacanaData = {this.handleGetBuddhavacanaData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}
EditBuddhavacana = connect(mapStateToProps, mapDispatchToProps)(EditBuddhavacana);
export default EditBuddhavacana;