import React, { Component } from 'react';
import update from 'react-addons-update'; 

class BuddhavacanaForm extends Component {
    constructor(){
        super();
        this.state = {
            buddhavacanaData: {}
        }
    }

    componentWillMount(){
        this.setState({
            buddhavacanaData: this.props.data
        })
    }
    
    componentDidUpdate(prevProps, prevState){
        if(prevProps.data != this.props.data){
            this.setState({buddhavacanaData: this.props.data});
        }
    }

    handleInputOnChange(event){
        let name = event.target.name;
        let currBuddhavacana = this.state.buddhavacanaData;
        this.setState({
            buddhavacanaData: update(currBuddhavacana, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getBuddhavacanaData(this.state.buddhavacanaData);
    }
    
    render(){
        const { buddhavacanaData } = this.state;
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="form-group">
                            <label>Judul</label>
                            <input type="text" name="title" value={buddhavacanaData.title} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                        <div className="form-group">
                            <label>Sumber</label>
                            <input type="text" name="source" value={buddhavacanaData.source} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Deskripsi</label>
                            <textarea className="form-control" name="description" value={buddhavacanaData.description} rows="4" onChange={ this.handleInputOnChange.bind(this) } ></textarea>
                        </div>
                    </div>
                </div>
                <div className="mt-3">
                    <div className="row">
                        <div className="col-md-6">
                            <a href="/admin/buddhavacana/list" role="button" className="btn btn-block btn-danger">Cancel</a>
                        </div>
                        <div className="col-md-6">
                            <button className="btn btn-block btn-success" type="submit">Simpan</button>
                        </div>
                    </div>
                </div>
                
            </form> 
        )
    }
}

export default BuddhavacanaForm;