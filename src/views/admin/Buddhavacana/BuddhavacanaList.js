import React, { Component } from 'react';
import BuddhavacanaCard from '../../../component/client/buddhavacana/BuddhavacanaCard';

import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';

import {getBuddhavacanaList, deleteBuddhavacana} from '../../../actions/Buddhavacana';
import {connect} from 'react-redux';
import DefaultPagination from '../../../component/shared/pagination/DefaultPagination';

const mapStateToProps = (state) => {
    return{
        buddhavacanaList: state.buddhavacana.buddhavacanaList,
        totalData: state.buddhavacana.totalData,
        succeed: state.buddhavacana.succeed,
        error: state.buddhavacana.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getBuddhavacanaList: (data) => {
            dispatch(getBuddhavacanaList(data))
        },
        deleteBuddhavacana: (data) => {
            dispatch(deleteBuddhavacana(data))
        }
    }
}

class BuddhavacanaList extends Component {
    constructor(){
        super();
        this.state = {
            limit: 10,
            offset: 0,
            totalData: null,
            buddhavacanaList: null,
            inAdminPage: false,
            showConfirmationModal: false,
            idToDelete: -1,
            successMessage: "",
            errorMessage: "",
            confirmationMsg: "Apakah Anda yakin ingin menghapus data ini ?"
        }   
    }

    componentWillMount(){
        this.fetchBuddhavacanaListByOffset(0);
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(this.state.buddhavacanaList != newProps.buddhavacanaList){
            this.setState({ buddhavacanaList: newProps.buddhavacanaList })
        }
        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData });
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
    }

    fetchBuddhavacanaListByOffset(offset){
        let data = {
            limit: this.state.limit,
            offset: offset
        }

        this.props.getBuddhavacanaList(data);
    }
    handleEditClicked(id){
        this.props.history.push(`/admin/buddhavacana/edit/${id}`)
    }

    handleDeleteClicked(id){
        this.setState({showConfirmationModal: true, idToDelete: id});
    }

    handleConfirmationModalClose(btnType){
        this.setState({showConfirmationModal: false});
        console.log(btnType);
        if(btnType == "yes"){
            this.props.deleteBuddhavacana(this.state.idToDelete);
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ""});
        this.fetchBuddhavacanaListByOffset(0);
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleGetDataOnPageClicked(page){
        let offset = page * this.state.limit - this.state.limit;
        this.props.getBuddhavacanaList({
            limit: this.state.limit,
            offset: offset
        });
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a href="/admin/buddhavacana/add" className="btn btn--base">
                        <i className="fas fa-plus mr-2"></i>
                        <span className="font-weight-bold">Buddhavacana</span>
                    </a>
                </div>

                <div className="row border-bottom">
                    <div className="col-12">
                        <p className="section--title">Buddhavacana List</p>
                    </div>
                </div>

                <div>
                    {this.state.buddhavacanaList ? 
                        <div className="d-flex flex-column m-top-20">
                            {this.state.buddhavacanaList.map((buddhavacana, index) =>
                                (
                                    <BuddhavacanaCard buddhavacana={buddhavacana} inAdminPage={true}>
                                        <div className="col-md-2 d-flex align-items-center">
                                            <button className="btn btn-info mr-2" onClick={ () => this.handleEditClicked(buddhavacana.id)}>
                                                Edit
                                            </button>
                                            <button className="btn btn-danger" onClick={() => this.handleDeleteClicked(buddhavacana.id)}>
                                                Delete
                                            </button>
                                        </div>
                                    </BuddhavacanaCard>
                                )
                            )}    
                        </div>  : "Loading"
                    }
                    { this.state.totalData && <DefaultPagination totalData={this.state.totalData} limit={this.state.limit} getDataOnPageClicked={this.handleGetDataOnPageClicked.bind(this)} /> }
                    { this.state.showConfirmationModal && <ConfirmationModal confirmationMessage={this.state.confirmationMsg} whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                    { this.state.successMessage && <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> }
                    { this.state.errorMessage && <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> }
                </div>
            </div>
        );
    }
}

BuddhavacanaList = connect(mapStateToProps, mapDispatchToProps)(BuddhavacanaList);
export default BuddhavacanaList;
