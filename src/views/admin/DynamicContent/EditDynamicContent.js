import React, { Component } from 'react';
import DynamicContentForm from './DynamicContentForm';
import { connect } from 'react-redux';
import { getWebContentByType, updateWebContentBasedOnType } from '../../../actions/DynamicContent';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { aboutConstant } from '../../../constants/AboutEkayana';

const mapStateToProps = (state) => {
    return {
        contentData: state.dynamiccontent.contentData,
        succeed: state.dynamiccontent.succeed,
        error: state.dynamiccontent.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getWebContentByType: (type) => {
            dispatch(getWebContentByType(type))
        },
        updateWebContentBasedOnType: (data) => {
            dispatch(updateWebContentBasedOnType(data))
        }
    }
}

class EditDynamicContent extends Component {
    constructor(){
        super();
        this.state = {
            contentType: '',
            contentData: {
                image: null,
                description: ""
            },
            imageUrl: null,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        let type = this.props.match.params.type;        

        switch(type){
            case 'history': {
                this.setState({
                    contentType: aboutConstant.sejarah,
                    title: 'SEJARAH SINGKAT'
                }, () => { this.props.getWebContentByType(aboutConstant.sejarah) } );
                break;
            }
            case 'education-training': {
                this.setState({
                    contentType: aboutConstant.pendidikan,
                    title: 'PENDIDIKAN DAN PELATIHAN'
                }, () => { this.props.getWebContentByType(aboutConstant.pendidikan) } );
                break;
            }
            case 'prayer': {
                this.setState({
                    contentType: aboutConstant.kebaktian,
                    title: 'KEBAKTIAN DAN PELAYANAN UMAT'
                }, () => { this.props.getWebContentByType(aboutConstant.kebaktian) } );
                break;
            }
            case 'social-service': {
                this.setState({
                    contentType: aboutConstant.pelayanan,
                    title: 'PELAYANAN SOSIAL'
                }, () => { this.props.getWebContentByType(aboutConstant.pelayanan) } );
                break;
            }
            case 'media': {
                this.setState({
                    contentType: aboutConstant.mediaunit,
                    title: 'MEDIA DAN UNIT USAHA'
                }, () => { this.props.getWebContentByType(aboutConstant.mediaunit) } );
                break;
            }
            case 'generation-art-culture': {
                this.setState({
                    contentType: aboutConstant.generasimuda,
                    title: 'GENERASI MUDA'
                }, () => { this.props.getWebContentByType(aboutConstant.generasimuda) } );
                break;
            }
            default: {
                this.props.history.push('/404');
            }
        }
    }
    
    componentWillReceiveProps(newProps){
        if( newProps.contentData != this.state.contentData ){
            this.setState({
                contentData: newProps.contentData
            })
        }

        if( newProps.contentData.image != this.state.imageUrl ){
            this.setState({imageUrl: `${IMAGE_URL}/public/images/contents/${newProps.contentData.image}`})
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetContentData(data){
        console.log(data);
        data.type = this.state.contentType;
        console.log('TYPE', data.type);
        setTimeout(() => { this.props.updateWebContentBasedOnType(data) }, 90);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        let type = this.props.match.params.type;
        this.props.getWebContentByType(this.state.contentType);
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Edit <span style={{ textTransform: 'capitalize' }}>{this.state.contentType}</span></div>
                    </div>
                </div>
                <DynamicContentForm contentData={this.state.contentData} imageUrl={this.state.imageUrl} getContentData={this.handleGetContentData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>    
        )
     }
}

EditDynamicContent = connect(mapStateToProps, mapDispatchToProps)(EditDynamicContent);
export default EditDynamicContent;
