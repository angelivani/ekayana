import React, { Component } from 'react';
import update from 'react-addons-update';

class DynamicContentForm extends Component {
    constructor(){
        super();
        this.state = {
            contentData: {},
            imageUrl: ''
        }
    }

    componentDidUpdate(prevState){
        if(prevState.contentData != this.props.contentData){
            this.setState({
                contentData: this.props.contentData
            })
        }
        if(prevState.imageUrl != this.props.imageUrl){
            this.setState({
                imageUrl: this.props.imageUrl
            })
        }
    }

    handleInputOnChange(event){
        let name = event.target.name;
        let currContentData = this.state.contentData;
        this.setState({
            contentData: update(currContentData, {
                [name]: { $set: event.target.value }
            })
        })
    }

    handleFileOnChange(data){
        var reader = new FileReader();
        var imageBase64 = "";
        reader.onload = function(e){
            imageBase64 = e.target.result;
        }
        reader.readAsDataURL(data.target.files[0]);
        setTimeout(() => { this.setState({imageUrl: imageBase64}); }, 100);

        let currContentData = this.state.contentData;
        this.setState({
            contentData: update(currContentData, {
                image: {$set: data.target.files[0]}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getContentData(this.state.contentData)
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Gambar</label>
                            <div className="img__upload">
                                { !this.state.imageUrl ? <label>IMAGE</label> : null }
                                { this.state.imageUrl ? <img src={this.state.imageUrl} /> : null}
                            </div>
                            <label className="btn--file-upload">Browse here <input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)} /></label>
                        </div>
                        <div className="form-group">
                            <label>Deskripsi</label>
                            <textarea className="form-control" rows="20" name="description" value={this.state.contentData.description} onChange={this.handleInputOnChange.bind(this)} ></textarea>
                        </div>                        
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/about" className="btn btn-block btn-danger" role="button">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        )
    }
}

export default DynamicContentForm;