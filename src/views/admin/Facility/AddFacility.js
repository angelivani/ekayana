import React, { Component } from 'react';
import FacilityForm from './FacilityForm';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import { addFacility } from '../../../actions/Facility'; 
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return{
      succeed: state.facility.succeed,
      error: state.facility.error,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        addFacility: (data) => {
            dispatch(addFacility(data))
        }
    }
}

class AddFacility extends Component {
    constructor(){
        super();
        this.state = {
            facilityData: {
                name: '',
                description: '',
                image: ''
            },
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetFacilityData(data){
        console.log(data);
        this.props.addFacility(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/facility/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Facility</div>
                    </div>
                </div>

                <FacilityForm facilityData = {this.state.facilityData} getFacilityData={this.handleGetFacilityData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        )
    }
}

AddFacility = connect(mapStateToProps, mapDispatchToProps)(AddFacility);
export default AddFacility;