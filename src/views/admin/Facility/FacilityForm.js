import React, { Component } from 'react';
import update from 'react-addons-update';

class FacilityForm extends Component {
    constructor(){
        super();
        this.state = {
            facilityData: {},
            imageUrl: ''
        }
    }

    componentWillMount(){
        this.setState({
            facilityData: this.props.facilityData
        })
    }

    componentDidUpdate(prevState){
        if(prevState.facilityData != this.props.facilityData){
            this.setState({
                facilityData: this.props.facilityData
            })
        }
        
        if(prevState.imageUrl != this.props.imageUrl){
            this.setState({
                imageUrl: this.props.imageUrl
            })
        }
    }

    handleInputOnChange(event){
        let name = event.target.name
        let currFacility = this.state.facilityData;
        this.setState({
            facilityData: update(currFacility, {
                [name]: {$set: event.target.value}
            })
        })
    }

    handleFileOnChange(data){
        var reader = new FileReader();
        var imageBase64 = "";
        reader.onload = function(e){
            console.log(e.target.result);
            imageBase64 = e.target.result;
        }
        reader.readAsDataURL(data.target.files[0]);
        setTimeout(() => { this.setState({imageUrl: imageBase64}); }, 100);
        
       
        let currFacility = this.state.facilityData;
        this.setState({
            facilityData: update(currFacility, {
                image: {$set: data.target.files[0]}
            })
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getFacilityData(this.state.facilityData);
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nama</label>
                            <input type="text" className="form-control" name="name" value={this.state.facilityData.name} onChange={this.handleInputOnChange.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label>Deskripsi</label>
                            <textarea className="form-control" name="description" value={this.state.facilityData.description} rows="8" onChange={this.handleInputOnChange.bind(this)}></textarea>
                        </div>
                        
                        <div className="form-group">
                            <label>Gambar Artikel</label>
                            <div className="img__upload">
                                {!this.state.imageUrl ? <label>FACILITY IMAGE</label> : null }
                                {this.state.imageUrl ? <img src={this.state.imageUrl} /> : null }
                            </div>
                        </div>
                        <label className="btn--file-upload">Browse here<input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)}/></label>    
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/facility/list" className="btn btn-block btn-danger" role="button">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        )
    }
}

export default FacilityForm;