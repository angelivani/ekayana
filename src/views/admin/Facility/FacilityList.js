import React, { Component } from 'react';
import { getFacilityList, deleteFacility } from '../../../actions/Facility';
import { IMAGE_URL } from '../../../constants/ActionTypes';

import EmbossCard from '../../../component/client/emboss_card/EmbossCard';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import DefaultPagination from '../../../component/shared/pagination/DefaultPagination';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return {
        facilityList: state.facility.facilityList,
        totalData: state.facility.totalData,
        succeed: state.facility.succeed,
        error: state.facility.error,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getFacilityList: (data) => {
            dispatch(getFacilityList(data))
        },
        deleteFacility: (id) => {
            dispatch(deleteFacility(id))
        }
    }
}

class FacilityList extends Component {
    constructor(){
        super();
        this.state = {            
            limit: 2,
            offset: 0,
            totalData: null,
            facilityList: [],
            totalData: null,
            facilityIdToDelete: null,
            showConfirmationModal: false,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        this.props.getFacilityList({
            limit: this.state.limit,
            offset: 0
        })
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        this.setState({ facilityList: newProps.facilityList })

        if(this.state.facilityList != newProps.facilityList){
            this.setState({ facilityList: newProps.facilityList })
        }
        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData })
        }

        if(this.state.totalData != newProps.totalData){
            this.setState({ totalData: newProps.totalData })
        }

        if(this.state.successMessage != newProps.succeed){
            this.setState({successMessage: newProps.succeed}, () => {console.log(this.state)});
        }
        
        if(this.state.errorMessage != newProps.error){
            this.setState({errorMessage: newProps.error});
        }
    }

    handleDeleteOnClicked(id){
        this.setState({
            showConfirmationModal: true,
            facilityIdToDelete: id
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteFacility(this.state.facilityIdToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getFacilityList({
            limit: this.state.limit,
            offset: 0
        });
        // this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleGetDataOnPageClicked(page){
        let offset = page * this.state.limit - this.state.limit;
        this.props.getFacilityList({
            limit: this.state.limit,
            offset: offset
        });
    }

    render(){
        return(
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/facility/add">
                        <i className="fas fa-plus mr-2"></i> Fasilitas
                    </a>
                </div>
                <div className="section--title border-bottom">List Fasilitas</div>

                <div className="row mt-5">
                    {this.state.facilityList && this.state.facilityList.map((facility, index) => 
                        (
                        <div className="col-md-3 mb-5">
                            <EmbossCard image={`${IMAGE_URL}/public/images/facility/${facility.image}`} title={facility.name} description={facility.description} />
                                            
                            <div className="d-flex justify-content-center m-1">
                                <button className="btn btn-info col-6 mr-2" onClick={() => this.props.history.push(`/admin/facility/edit/${facility.id}`)}>Edit</button>
                                <button className="btn btn-danger col-6" onClick={ () => this.handleDeleteOnClicked(facility.id)}>Delete</button>
                            </div>
                        </div>
                        )
                    )}
                </div>

                {this.state.totalData && <DefaultPagination totalData={this.state.totalData} limit={this.state.limit} getDataOnPageClicked={this.handleGetDataOnPageClicked.bind(this)} />}
                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus fasilitas ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }

            </div>
        )
    }
}

FacilityList = connect(mapStateToProps, mapDispatchToProps)(FacilityList);
export default FacilityList;