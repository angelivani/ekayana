import React, { Component } from 'react';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';
import FacilityForm from './FacilityForm';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import { getFacilityDetail, editFacility } from '../../../actions/Facility'; 
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        facilityData: state.facility.facilityData,
        succeed: state.facility.succeed,
        error: state.facility.error,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        getFacilityDetail: (id) => {
            dispatch(getFacilityDetail(id))
        },
        editFacility: (data) => {
            dispatch(editFacility(data))
        }
    }
}

class EditFacility extends Component {
    constructor(){
        super();
        this.state = {
            facilityData: {},
            imageUrl: null,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getFacilityDetail(id);
    }

    componentWillReceiveProps(newProps){
        if(this.state.facilityData != newProps.facilityData){
            this.setState({ facilityData: newProps.facilityData })
        }

        if(this.state.imageUrl != newProps.facilityData.image){
            this.setState({imageUrl: `${IMAGE_URL}/public/images/facility/${newProps.facilityData.image}`}
            )
        }

        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed})
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error})
        }
    }

    handleGetFacilityData(data){
        console.log(data);
        this.props.editFacility(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        // this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render(){
        return(
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Edit Facility</div>
                    </div>
                </div>
                <FacilityForm facilityData = {this.state.facilityData} imageUrl={this.state.imageUrl} getFacilityData={this.handleGetFacilityData.bind(this)} />
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        )
    }
}

EditFacility = connect(mapStateToProps, mapDispatchToProps)(EditFacility);
export default EditFacility;