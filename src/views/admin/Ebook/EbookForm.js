import React, { Component } from 'react';
import update from 'react-addons-update'; 

class EbookForm extends Component {
	constructor(){
			super();
			this.state = {
				ebookData: {},
				ebook_name: '',
				addEbook: false
			}
	}

	componentWillMount(){
		this.setState({
			ebookData: this.props.data,
			addEbook: this.props.addEbook
		}, () => {
				console.log(this.state.ebookData);
		})
	}
	
	componentWillReceiveProps(newProps) {
		this.setState({
			addEbook: newProps.addEbook
		})
	}

	componentDidUpdate(prevProps, prevState){
		if(prevProps.data != this.props.data){
			this.setState({ebookData: this.props.data, ebook_name: this.props.data.file});
		}
	}

	handleInputOnChange(event){		
		let name = event.target.name;
		let currEbook = this.state.ebookData;
		console.log(currEbook);
		this.setState({
				ebookData: update(this.state.ebookData, {
						[name]: {$set: event.target.value}
				})
		})
	}

	handleFileOnChange(data){
		let currEbook = this.state.ebookData;
		this.setState({
				ebookData: update(currEbook, {
						file: {$set: data.target.files[0]}
				})
		})
		this.setState({ ebook_name: data.target.files[0].name })
	}

	handleSubmit(e){
		e.preventDefault();
		this.props.getEbookData(this.state.ebookData);
	}

	render(){
		return(
			<form onSubmit={this.handleSubmit.bind(this)}>
				<div className="row">
						<div className="col-md-6 col-sm-12 col-xs-12">
								<div className="form-group">
										<label>Judul</label>
										<input type="text" name="name" value={this.state.ebookData.name} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
								</div>
								<div className="form-group">
										<label>Sumber</label>
										<input type="text" name="source" value={this.state.ebookData.source} className="form-control" onChange={ this.handleInputOnChange.bind(this) } />
								</div>
						</div>
					<div className="col-md-6">
						<div className="form-group">
							<label>Upload Ebook</label><br />
							{this.state.addEbook && <label className="btn--file-upload">Upload File<input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)} /></label> } <br /> 
							{this.state.ebook_name && <button className="btn btn-light">{this.state.ebook_name}</button>}
						</div>
					</div>
				</div>
				
				<div className="row">
						<div className="col-6">
								<a role="button" href="/admin/ebook/list" className="btn btn-block btn-danger">Cancel</a>
						</div>
						<div className="col-6">
								<button className="btn btn-block btn-success" type="submit">Simpan</button>
						</div>
				</div>						
			</form> 
		)
	}
}

export default EbookForm;