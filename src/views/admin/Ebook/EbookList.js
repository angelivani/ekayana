import React, { Component } from 'react';
import Title from '../../../component/shared/title/default_title_center/Title';

import { getEbookList, deleteEbook } from '../../../actions/Ebook';
import { connect } from 'react-redux';
import EbookCard from '../../../component/client/ebook/EbookCard';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return {
        ebookList: state.ebook.ebookList,
        succeed: state.ebook.succeed,
        error: state.ebook.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getEbookList: (data) => {
            dispatch(getEbookList(data))
        },
        deleteEbook: (id) => {
            dispatch(deleteEbook(id))
        }
    }
}

class EbookList extends Component {
    constructor(){
        super();
        this.state = {
            limit: 10,
            ebookList: [],
            ebookToDelete: -1,
            showConfirmationModal: false,
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillMount(){
        this.props.getEbookList({
            limit: this.state.limit, offset: 0
        })
    }

    componentDidUpdate(){
        setTimeout(() => {
            window.scrollTo(0,0)
        }, 90)
    }

    componentWillReceiveProps(newProps){ 
        
        if(this.state.successMessage != newProps.succeed){
            this.setState({successMessage: newProps.succeed});
        }
        
        if(this.state.errorMessage != newProps.error){
            this.setState({errorMessage: newProps.error});
        }
    }

    componentDidUpdate(prevState){
        if(prevState.ebookList != this.props.ebookList){
            this.setState({
                ebookList: this.props.ebookList
            })
        }
    }

    handleDeleteOnClicked(id){
        console.log(id);
        
        this.setState({
            showConfirmationModal: true,
            ebookToDelete: id
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({showConfirmationModal: false});
            this.props.deleteEbook(this.state.ebookToDelete);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.getEbookList({
            limit: this.state.limit,
            offset: 0
        });
        // this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/ebook/add">
                        <i className="fas fa-plus mr-2"></i> Ebook
                    </a>
                </div>
                <div className="section--title border-bottom">List Ebook</div>

                <div className="row mt-5">
                    {this.state.ebookList && this.state.ebookList.map((ebook, index) => 
                        (
                        <div className="col-md-3 ebook__wrapper mb-5">
                            <EbookCard ebook={ebook} />  
                                            
                            <div className="d-flex justify-content-center">
                                <button className="btn btn-info col-6 mr-2" onClick={() => this.props.history.push(`/admin/ebook/edit/${ebook.id}`)}>Edit</button>
                                <button className="btn btn-danger col-6" onClick={ () => this.handleDeleteOnClicked(ebook.id)}>Delete</button>
                            </div>
                        </div>
                        )
                    )}
                </div>
                
                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus ebook ini?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }

            </div>
            
        );
    }
}

EbookList = connect(mapStateToProps, mapDispatchToProps)(EbookList);
export default EbookList;