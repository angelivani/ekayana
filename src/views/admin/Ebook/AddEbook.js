import React, { Component } from 'react';
import update from 'react-addons-update'; 
import EbookForm from './EbookForm';

import {connect} from 'react-redux';
import {addEbook} from '../../../actions/Ebook';
import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

const mapStateToProps = (state) => {
    return{
      succeed: state.ebook.succeed,
      error: state.ebook.error,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        addEbook: (data) => {
            dispatch(addEbook(data))
        }
    }
}

class AddEbook extends Component {
    constructor(){
			super();
			this.state = {
				ebookData: {
					name: '',
					source: '',
					file: null
				},
				addEbook: true,
				successMessage: '',
				errorMessage: ''
			}
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.succeed){
            this.setState({
                successMessage: newProps.succeed
            })
        }
        else if(newProps.error){
            this.setState({
                errorMessage: newProps.error
            })
        }
    }

    handleGetEbookData(data){
        this.props.addEbook(data);
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/ebook/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section--title">Add Ebook</div>
                    </div>
                </div>
                <div className="pt-4">
								<EbookForm addEbook={this.state.addEbook} data = {this.state.ebookData} getEbookData = {this.handleGetEbookData.bind(this)} />
                </div>
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }

}

AddEbook = connect(mapStateToProps, mapDispatchToProps)(AddEbook);
export default AddEbook;