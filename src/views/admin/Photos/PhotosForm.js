import React, { Component } from 'react';
import Select from 'react-select';
import update from 'react-addons-update';

import {getAlbumnList} from '../../../actions/Albumn';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return{
        albumnList: state.albumn.albumnList,
        error: state.albumn.error
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getAlbumnList: () => {
            dispatch(getAlbumnList())
        }
    }
}

class PhotosForm extends Component {
    constructor(){
        super();
        this.state = {
            albumn_id: 0,
            albumnList: [{value: 0, label: 'Please Select Albumn'}],
            selectedAlbumn: null,
            base64images: [],
            listImage: []
        }
    }

    componentWillMount(){
        this.props.getAlbumnList();
    }

    componentDidUpdate(prevState){
        
        if(prevState.albumnList != this.props.albumnList){
            let albumnList = this.props.albumnList;
            let newList = [];

            for(var i=0; i < albumnList.length; i++) {
                newList.push({
                    value: albumnList[i].id,
                    label: albumnList[i].name
                })    
            }
            
            this.setState({
                albumn_id: newList[0].value,
                albumnList: newList,
                selectedAlbumn: newList[0]
            }, () => {
                console.log(this.state);
            })
            console.log(newList)
        }
        
    }

    
    handleAlbumnChanged(data){
        let currPhotos = this.state.photosData;

        this.setState({
            albumn_id: data.value,
            selectedAlbumn: data
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.getPhotosData(this.state);
    }

    handleFileOnChange(data){
        let files = data.target.files;
        let base64List = [];
        let listImages = [];

        for(var i=0; i < files.length; i++){
            listImages.push(files[i]);
            var reader = new FileReader();
            reader.onload = function(e){
                console.log(e.target.result);
                base64List.push(e.target.result)
            
            }
            reader.readAsDataURL(files[i]);
        }

        setTimeout(() => {
            this.setState({
                listImage: listImages,
                base64images: base64List
            })
        }, 200);
    }

    handleRemovePhoto(removedIndex){
        let base64images = this.state.base64images;
        base64images.splice(removedIndex, 1);

        let listImages = this.state.listImage; 
        listImages.splice(removedIndex, 1);

        setTimeout(() => {
            let currPhotos = this.state.photosData;

            this.setState({
                base64images: base64images
            }, () => {
                console.log(this.state);
            })    
        }, 120);
        

    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Albumn</label>
                            <Select value={this.state.selectedAlbumn} options={this.state.albumnList} onChange={this.handleAlbumnChanged.bind(this)} /> 
                        </div>
                        <div className="form-group">
                            <label>Upload Photos</label>
                            <div>
                                <label className="btn--file-upload">Browse here<input type="file" className="hidden" onChange={this.handleFileOnChange.bind(this)} multiple /></label>    
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div className="d-flex flex-wrap">
                    
                        {this.state.base64images.length > 0 && <div className="form-group">
                            <label>Uploaded File</label>
                            <div className="d-flex flex-wrap">
                                {this.state.base64images.map((base64img, index) => 
                                    (
                                        <div className="img__box m-2" key={index+2}>
                                            <img key={index+1} src={base64img} />
                                            <button key={index} className="btn__selection__delete selection__for_delete" onClick={() => this.handleRemovePhoto(index)} type="button">x</button>
                                        </div>
                                    )
                                )}
                            </div>
                            
                        </div>}
                    
                </div>
                <div className="row">
                    <div className="col-6">
                        <a href="/admin/photos/list" className="btn btn-block btn-danger" role="button">Kembali</a>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-block btn-success" type="submit">Simpan</button>
                    </div>
                </div>
                
            </form>
        );
    }
}

PhotosForm = connect(mapStateToProps, mapDispatchToProps)(PhotosForm);
export default PhotosForm;