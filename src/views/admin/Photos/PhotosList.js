import React, { Component } from 'react';
import Select from 'react-select';

import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

import {IMAGE_URL} from '../../../constants/ActionTypes';
import {getPhotosInAlbumn, deletePhotosInAlbumn} from '../../../actions/Photos';
import {getAlbumnList} from '../../../actions/Albumn';
import {connect} from 'react-redux';
import ConfirmationModal from '../../../component/shared/modal/confirmation_modal/ConfirmationModal';

const mapStateToProps = (state) => {
    return{
        photosList: state.photos.photosList,
        albumnList: state.albumn.albumnList,
        error: state.albumn.error,
        photoSucceed: state.photos.succeed,
        photoError: state.photos.error
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getAlbumnList: () => {
            dispatch(getAlbumnList())
        },
        getPhotosInAlbumn: (data) => {
            dispatch(getPhotosInAlbumn(data))
        },
        deletePhotosInAlbumn: (data) => {
            dispatch(deletePhotosInAlbumn(data))
        }
    }
}

class PhotosList extends Component {
    constructor(){
        super();
        this.state = {
            photosList: null,
            albumn_id: 0,
            albumnList: [{value: 0, label: 'Please Select Albumn'}],
            selectedAlbumn: null,
            photosAreSelected: false,
            listPhotosToDelete: [],
            successMessage: '',
            errorMessage: '',
            showConfirmationModal: false
        }
    }

    componentWillMount(){
        this.props.getAlbumnList();
    }

    componentWillReceiveProps(newProps){
        if(newProps.photoSucceed){
            this.setState({
                successMessage: newProps.photoSucceed
            })
        }
        else if(newProps.photoError){
            this.setState({
                errorMessage: newProps.photoError
            })
        }
    }

    componentDidUpdate(prevState){
        
        if(prevState.albumnList != this.props.albumnList){
            let albumnList = this.props.albumnList;
            let newList = [];

            for(var i=0; i < albumnList.length; i++) {
                newList.push({
                    value: albumnList[i].id,
                    label: albumnList[i].name
                })    
            }
            newList.splice(0, 0, {value: 0, label: 'Please Select Albumn'});

            this.setState({
                albumn_id: newList[0].value,
                albumnList: newList,
                selectedAlbumn: newList[0]
            }, () => {
                console.log(this.state);
            })
            console.log(newList)
        }

        if(prevState.photosList != this.props.photosList){
            this.setState({
                photosList: this.props.photosList
            })
        }
    }

    handleAlbumnChanged(data){
        let currPhotos = this.state.photosData;

        this.setState({
            albumn_id: data.value,
            selectedAlbumn: data
        })
        this.props.getPhotosInAlbumn({
            albumn_id: data.value,
            limit: 10,
            offset: 0
        })
    }

    makePhotosBecomeSelected(){
        this.setState({
            photosAreSelected: true
        })
    }

    handlePhotosChecked(index, photoId){
        console.log('index of photos: '+ index);
        console.log('PHTOOS ID: '+ photoId);
        
        let isPhotoExists = false;
        let indexToRemove = -1;
        let lastIndex = 0;
        
        if( (this.state.listPhotosToDelete.length - 1) > 0){
            lastIndex = this.state.listPhotosToDelete.length - 1;
        }
        
        for(var i=0; i < this.state.listPhotosToDelete.length; i++){
            if(photoId == this.state.listPhotosToDelete[i]){
                indexToRemove = i;
                isPhotoExists = true;
                break;
            }
        } 
        

        setTimeout(() => {
            if(!isPhotoExists) {
                this.state.listPhotosToDelete.push(photoId);
                console.log(this.state.listPhotosToDelete);
            }
            else {
                this.state.listPhotosToDelete.splice(indexToRemove, 1);
                console.log(this.state.listPhotosToDelete);
            }
        }, 150);
        
    }

    cancelPhotosToDelete(){
        this.setState({
            photosAreSelected: false,
            listPhotosToDelete: []
        })
    }

    deleteSelectedPhotos(){
        this.setState({
            showConfirmationModal: true
        })
    }

    handleConfirmationModalClose(btnType){
        if(btnType == 'yes'){
            this.setState({
                showConfirmationModal: false,
                photosAreSelected: false
            });
            let data = {
                albumn_id: this.state.albumn_id,
                
            }
            this.props.deletePhotosInAlbumn(this.state);
        }
        else if(btnType == 'cancel'){
            this.setState({ showConfirmationModal: false })
        }
    }    

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        // this.props.history.push('/admin/event/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    render() {
        return (
            <div className="base__wrapper">
                <div className="d-flex justify-content-end">
                    <a role="button" className="btn btn--base" href="/admin/photos/add">
                        <i className="fas fa-plus mr-2"></i> Foto
                    </a>
                </div>
                
                <div className="section--title border-bottom">List Foto dalam Album</div>
                <div className="row mt-3">
                    <div className="col-md-6">
                        <Select value={this.state.selectedAlbumn} options={this.state.albumnList} onChange={this.handleAlbumnChanged.bind(this)} /> 
                    </div>
                </div>
                
                { this.state.photosList && !this.state.photosAreSelected && <button className="btn btn-info mt-3 mb-3" onClick={this.makePhotosBecomeSelected.bind(this)}>Select</button> }

                { this.state.photosList && this.state.photosAreSelected && <div className="d-flex mt-3 mb-3">
                    <button className="btn btn-warning mr-2" onClick={this.cancelPhotosToDelete.bind(this)}>Cancel</button>
                    <button className="btn btn-danger" onClick={this.deleteSelectedPhotos.bind(this)}>Delete</button>
                </div> }

                {this.state.photosList ? 
                    
                    <div className="d-flex flex-wrap">
                        {this.state.photosList.map((photos, index) => 
                            (
                                <div className="img__box mr-2">
                                    <img src={`${IMAGE_URL}/public/images/photos/${photos.image}`} />
                                    {this.state.photosAreSelected && <input type="checkbox" className="selection" onClick={ () => this.handlePhotosChecked(index, photos.id)} /> }
                                </div>
                            )
                        )}
                    </div>
                : null }

                {this.state.showConfirmationModal && <ConfirmationModal confirmationMessage="Apakah Anda yakin ingin menghapus foto berikut?"  whenConfirmationModalClose={this.handleConfirmationModalClose.bind(this)} /> }
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

PhotosList = connect(mapStateToProps, mapDispatchToProps)(PhotosList);
export default PhotosList;