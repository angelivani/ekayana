import React, { Component } from 'react';
import PhotosForm from './PhotosForm';

import SuccessModal from '../../../component/shared/modal/success_modal/SuccessModal';
import WarningModal from '../../../component/shared/modal/warning_modal/WarningModal';

import {addPhotosIntoAlbumn} from '../../../actions/Photos';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return{
        succeed: state.photos.succeed,
        error: state.photos.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        addPhotosIntoAlbumn: (data) => {
            dispatch(addPhotosIntoAlbumn(data))
        }
    }
}

class AddPhotos extends Component {
    constructor(){
        super();
        this.state = {
            successMessage: '',
            errorMessage: ''
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({
                successMessage: newProps.succeed
            })
        }
        else if(newProps.error){
            this.setState({
                errorMessage: newProps.error
            })
        }
    }

    handleSuccessModalClose(){
        this.setState({successMessage: ''});
        this.props.history.push('/admin/photos/list');
    }

    handleWarningModalClose(){
        this.setState({errorMessage: ""});
    }

    handleGetPhotosData(data){
        console.log(data);
        this.props.addPhotosIntoAlbumn(data);
    }

    render() {
        return (
            <div className="form__wrapper">
                <div className="row border-bottom">
                    <div className="col-md-12">
                        <div className="section--title">Upload Photo</div>
                    </div>
                </div>
                <div className="pt-4">
                    <PhotosForm getPhotosData={this.handleGetPhotosData.bind(this)} />
                </div>
                {this.state.successMessage ? <SuccessModal successMessage={this.state.successMessage} whenSuccessModalClose={this.handleSuccessModalClose.bind(this)} /> :  null}
                {this.state.errorMessage ? <WarningModal errorMessage={this.state.errorMessage} whenWarningModalClose={this.handleWarningModalClose.bind(this)} /> : null }
            </div>
        );
    }
}

AddPhotos = connect(mapStateToProps, mapDispatchToProps)(AddPhotos);
export default AddPhotos;