import React, {Component} from 'react';

class PageNotFound extends Component {
    render(){
        return(
            <div className="page__not__found__wrapper">
                <div className="page__not__found__box">
                    <h1>404</h1>
                    <h2 className="desc">Oops...Halaman yang Anda cari tidak dapat ditemukan</h2>
                </div>                
            </div>
        )
    }
}
export default PageNotFound;