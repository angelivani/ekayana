import React, { Component } from 'react';
import Title from '../../../component/shared/title/default_title_center/Title';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import { getEbookListForPublic } from '../../../actions/Ebook';
import { connect } from 'react-redux';
import EbookCard from '../../../component/client/ebook/EbookCard';

const mapStateToProps = (state) => {
    return {
        ebookList: state.ebook.ebookList,
        succeed: state.ebook.succeed,
        error: state.ebook.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getEbookList: (data) => {
            dispatch(getEbookListForPublic(data))
        }
    }
}

class Ebook extends Component {
    constructor(){
        super();
        this.state = {
            ebookList: []
        }
    }

    componentWillMount(){
        this.props.getEbookList({limit: 10, offset: 0})
    }

    componentDidUpdate(){
        setTimeout(() => {
            window.scrollTo(0,0)
        }, 90)
    }

    componentDidUpdate(prevState){
        if(prevState.ebookList != this.props.ebookList){
            this.setState({
                ebookList: this.props.ebookList
            })
        }
    }

    render() {
        return (
            <div className="container min-height-small">
                <Title title="Ebook" />
                <div className="page__section pl-3 pr-3 min-height-small">
                    <div className="row">
                        {this.state.ebookList && this.state.ebookList.map((ebook, index) => 
                            (
                            <div className="col-md-3 ebook__wrapper mb-5">
                                <EbookCard ebook={ebook}>  
                                    <a target="_blank" href={`${IMAGE_URL}/public/pdf/${ebook.file}`} role="button" className="btn btn--base">Download E-Book</a>
                                </EbookCard>
                            </div>
                            )
                        )}
                    </div>
                </div>
            </div>
            
        );
    }
}

Ebook = connect(mapStateToProps, mapDispatchToProps)(Ebook);
export default Ebook;