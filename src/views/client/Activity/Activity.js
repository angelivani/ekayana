import React, { Component } from 'react';
import ActivityCard from '../../../component/client/activity_card/ActivityCard';
import Title from '../../../component/shared/title/default_title_center/Title';

import {getActivityListForPublic} from '../../../actions/Activity';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return{
      activityList: state.activity.activityList,
      error: state.activity.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getActivityList: (data) => {
            dispatch(getActivityListForPublic(data))
        }
    }
}

class DailyActivity extends Component {
    constructor(){
        super();
        this.state = {
            limit: 20,
            offset: 0,
            dailyActivity: null
        }
    }

    componentWillMount(){
        let data = {
            limit: this.state.limit,
            offset: this.state.offset
        }
        this.props.getActivityList(data);
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(newProps.activityList){
            this.setState({
                dailyActivity: newProps.activityList
            })
        }
    }

    setActivePage(btnId){
        this.setState({currentPage: btnId})
    }

    render() {
        return (
            
            <div className="default__wrapper">
                <Title title="Kegiatan Rutin" />
                <div className="activity__content__wrapper min-height-small">
                    {
                       this.state.dailyActivity ?  
                        <div className="row">
                            {this.state.dailyActivity.map((dailyAct, index) => 
                            (
                                <div className="col-sm-4 col-xs-12 m-bottom-10">
                                    <ActivityCard card={dailyAct} />
                                </div>
                            )
                            )}                           
                        </div> : "Loading"
                    }
                                        
                </div>
            </div>
        );
    }
}

DailyActivity = connect(mapStateToProps, mapDispatchToProps)(DailyActivity);
export default DailyActivity;