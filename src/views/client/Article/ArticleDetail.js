import React, { Component } from 'react';
import { getArticleById } from '../../../actions/Article';
import {connect} from 'react-redux';
import Title from '../../../component/shared/title/default_title_center/Title';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import moment from 'moment';

const mapStateToProps = (state) => {
    return{
        articleData: state.article.articleData,
        succeed: state.article.succeed,
        error: state.article.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getArticleById: (id) => {
            dispatch(getArticleById(id))
        }
    }
}

class ArticleDetail extends Component {
    constructor(){
        super();
        this.state = {
            articleData: {},
            imageUrl: null
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getArticleById(id);
    }

    componentWillReceiveProps(newProps){
        if(this.state.articleData != newProps.articleData){
            setTimeout(() => {
                this.setState({
                    articleData: newProps.articleData,
                    imageUrl: `${IMAGE_URL}/public/images/article/${newProps.articleData.image}`
                })
            }, 100)
        }
    }

    backToPreviousPage(){
        this.props.history.push('/id/article')
    }

    render(){
        const { articleData } = this.state;
        return(
            <div className="container">
                
                <Title title="Article Detail" />
                <div className="text-center">
                    {   
                        this.state.imageUrl && 
                        <img className="event__detail__img text" src={this.state.imageUrl} /> 
                    }
                    
                    <h1>{articleData.name}</h1>
                    <h2 className="my__event__date mb-3">{moment(articleData.created_at).format('LL')}</h2>

                    {   
                        articleData.description && 
                        <p className="event__detail__desccription">{ articleData.description }</p> 
                    }
                </div>

                <div className="d-flex justify-content-center mt-4 mb-3">
                    <button className="btn btn-md btn-outline-dark" onClick={this.backToPreviousPage.bind(this)}>Kembali</button>
                </div>
            </div>
        )
    }
}

ArticleDetail = connect(mapStateToProps, mapDispatchToProps)(ArticleDetail);
export default ArticleDetail;