import React, {Component} from 'react';

import { getArticleList } from '../../../actions/Article';
import { connect } from 'react-redux';
import ArticleCard from '../../../component/client/article/ArticleCard';

const mapStateToProps = (state) => {
    return{
        articleList: state.article.articleList,
        error: state.article.error
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getArticleList: (data) => {
            dispatch(getArticleList(data))
        }
    }
}

class Article extends Component {
    constructor(){
        super();
        this.state = {
            error: false,
            hasMore: true,
            articleList: [],
            isLoading: false,
            currPage: 1,
            limit: 8
        }
        // this.onScroll = this.onScroll.bind(this);
    }

    componentWillMount(){   
        this.props.getArticleList({
            limit: this.state.limit, 
            offset: (this.state.currPage * this.state.limit) - this.state.limit
        });
    } 

    componentDidMount(){
        setTimeout(() => {
            window.scrollTo(0,0);
        }, 90);  
    }

    // componentDidMount(){
    //     this.refs.iScroll.addEventListener("scroll", this.onScroll.bind(this));
    // }

    componentWillReceiveProps(newProps){
        if( (this.state.articleList != newProps.articleList) && this.state.hasMore && newProps.articleList.length > 0){
            let listArticle = this.state.articleList;
            for(var i=0; i < newProps.articleList.length; i++){
                listArticle.push(newProps.articleList[i])
            }
            
            let currPage = this.state.currPage + 1;
            
            setTimeout(() => {
                this.setState({
                    articleList: listArticle,
                    isLoading: false,
                    currPage: currPage
                }, () => {
                    console.log(this.state);
                })
            }, 90)
        }
        else{
            this.setState({ hasMore: false, isLoading: false });
        }
    }

    // onScroll(){
    //     if (this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight >=this.refs.iScroll.scrollHeight){
    //         this.loadArticle();
    //       }
    // }

    loadArticle = () => {
        if (this.state.hasMore) {            
            this.setState({ isLoading: true }, () => {
                this.props.getArticleList({
                    limit: this.state.limit, 
                    offset: (this.state.currPage * this.state.limit) - this.state.limit
                });
            })
        }
        else { this.setState({ isLoading: false }) }
    }

    render(){
        return(
            <div className="p-3">
                <div className="row">
                    {this.state.articleList.map((article, index) => 
                        (
                            <div className="col-md-3 mb-2">
                                <ArticleCard article={article} />
                            </div>
                        )
                    )}

                    {
                        this.state.isLoading && 
                        <div style={{ width: "100%", textAlign: "center" }}>
                            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div> 
                        </div>
                    }
                </div>

                {   this.state.hasMore && 
                    <div className="d-flex justify-content-center mb-2 mt-2">
                        <button className="btn btn-outline-dark" onClick={() => {this.loadArticle()}}>Lihat Selebihnya</button>
                    </div> 
                }
            </div>
        )
    }
}

Article = connect(mapStateToProps, mapDispatchToProps)(Article);
export default Article;