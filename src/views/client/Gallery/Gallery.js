import React, { Component } from 'react';
import Title from '../../../component/shared/title/default_title_center/Title';

import AlbumnCard from '../../../component/client/albumn/AlbumnCard';
import { getAlbumnList } from '../../../actions/Albumn';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        albumnList: state.albumn.albumnList,
        error: state.albumn.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getAlbumnList: () => {
            dispatch(getAlbumnList())
        }
    }
}

class Gallery extends Component {
    constructor(){
        super();
        this.state = {
            albumnList: null
        }
    }

    componentWillMount(){
        
        this.props.getAlbumnList();
    }

    componentDidUpdate(prevState){
        if(prevState.albumnList != this.props.albumnList){
            console.log(this.props.albumnList)
            this.setState({
                albumnList: this.props.albumnList
            })
        }
    }

    render() {
        return (
            <div className="container">
                <Title title="GALERI" />
                <div className="page__section">
                
                    <div className="row mb-5 mt-5">
                        {this.state.albumnList && this.state.albumnList.map((albumn, index) =>
                            (
                                <div className="col-lg-3 col-md-3 mb-3" onClick={() => this.props.history.push(`/id/gallery/${albumn.id}`)}>
                                    <AlbumnCard albumn={albumn} />
                                </div>
                            )
                        )}    
                    </div>
                </div>
            </div>
            
        );
    }
}

Gallery = connect(mapStateToProps, mapDispatchToProps)(Gallery);
export default Gallery;