import React, { Component } from 'react';
import Title from '../../../component/shared/title/default_title_center/Title';

import {getBuddhavacanaListForPublic} from '../../../actions/Buddhavacana';
import {connect} from 'react-redux';
import BuddhavacanaCard from '../../../component/client/buddhavacana/BuddhavacanaCard';

const mapStateToProps = (state) => {
    return{
        buddhavacanaList: state.buddhavacana.buddhavacanaList,
        error: state.buddhavacana.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getBuddhavacanaList: (data) => {
            dispatch(getBuddhavacanaListForPublic(data))
        }
    }
}

class Buddhavacana extends Component {
    constructor(){
        super();
        this.state = {
            limit: 2,
            offset: 0,
            hasMore: true,
            isLoading: false,
            buddhavacanaList: [],
            inAdminPage: false,
            currPage: 1
        }
    }

    componentWillMount(){
        let data = {
            limit: this.state.limit,
            offset: this.state.offset
        }

        this.props.getBuddhavacanaList(data);
    }

    componentWillReceiveProps(newProps){
        if( (this.state.buddhavacanaList != newProps.buddhavacanaList) && this.state.hasMore && newProps.buddhavacanaList.length > 0){
            let listBuddhavacana = this.state.buddhavacanaList;
            for(var i=0; i < newProps.buddhavacanaList.length; i++){
                listBuddhavacana.push(newProps.buddhavacanaList[i])
            }
            
            let currPage = this.state.currPage + 1;
            
            setTimeout(() => {
                this.setState({
                    buddhavacanaList: listBuddhavacana,
                    isLoading: false,
                    currPage: currPage
                })
            }, 90)
        }
        else{
            this.setState({ hasMore: false, isLoading: false });
        }
    }

    loadBuddhavacana(){
        if (this.state.hasMore) {            
            this.setState({ isLoading: true }, () => {
                this.props.getBuddhavacanaList({
                    limit: this.state.limit, 
                    offset: (this.state.currPage * this.state.limit) - this.state.limit
                });
            })
        }
        else { this.setState({ isLoading: false }) }
    }

    render() {
        return (
            <div ref="iScroll" className="container">
                <Title title="Buddhavacana"/>
                <div className="row p-3">
                    
                    <div className="d-flex flex-column m-top-20">
                        {this.state.buddhavacanaList.map((buddhavacana, index) =>
                            (
                                <BuddhavacanaCard buddhavacana={buddhavacana} inAdminPage={false} />
                            )
                        )}
                    </div>
                    
                    { this.state.isLoading && 
                        <div style={{ width: "100%", textAlign: "center" }}>
                            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div> 
                        </div>
                    }                        
                </div>

                { this.state.hasMore && <div className="d-flex justify-content-center mb-3">
                    <button className="btn btn-outline-dark" onClick={() => {this.loadBuddhavacana()}}>Lihat Selebihnya</button>
                </div> }
            </div>
        );
    }
}

Buddhavacana = connect(mapStateToProps, mapDispatchToProps)(Buddhavacana);
export default Buddhavacana;