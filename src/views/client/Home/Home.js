import React, { Component } from 'react';
import AboutOverview from '../../../component/client/about_ekayana/AboutOverview';

import { getEventListForPublic } from '../../../actions/Event';
import { getAlbumnList } from '../../../actions/Albumn';
import { getWebContent } from '../../../actions/Content';

import SimpleEventCard from '../../../component/client/event/SimpleEventCard';
import Title from '../../../component/shared/title/default_title_center/Title';
import CirclePagination from '../../../component/shared/pagination/CirclePagination';
import AlbumnCard from '../../../component/client/albumn/AlbumnCard';
import DailyReflection from '../../../component/client/reflections/DailyReflections';

import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return{
        webContentData: state.content.webContentData,
        eventList: state.event.eventList,
        totalData: state.event.totalData,
        eventError: state.activity.error,
        albumnList: state.albumn.albumnList,
        albumnError: state.albumn.error
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return{
        getEventList: (data) => {
            dispatch(getEventListForPublic(data))
        },
        getAlbumnList: () => {
            dispatch(getAlbumnList())
        },
        getWebContent: () => {
            dispatch(getWebContent())
        }
    }
}

class Home extends Component {
    constructor(){
        super();
        this.state = {
            eventList: null,
            eventTotalData: 0,
            eventLimit: 3,
            albumnList: null,
            webContentData: null
        }
    }

    componentWillMount(){
        this.props.getEventList({
            limit: this.state.eventLimit,
            offset: 0
        })
        this.props.getAlbumnList();
        this.props.getWebContent();
    }

    componentDidUpdate(prevState){
        if(prevState.eventList != this.props.eventList){
            this.setState({
                eventList: this.props.eventList,
                eventTotalData: this.props.totalData
            })
        }

        if(prevState.albumnList != this.props.albumnList){
            this.setState({
                albumnList: this.props.albumnList
            })
            console.log(this.props);
        }
        
        // if(prevState.webContentData != this.props.webContentData){
        //     this.setState({
        //         webContentData: this.props.webContentData
        //     }, () => {
        //         console.log(this.state.webContentData);
        //     })
        // }
    }

    componentWillReceiveProps(newProps){
        if(newProps.eventError){
            console.log(newProps.eventError)
        }

        if(newProps.albumnError){
            console.log(newProps.albumnError)
        }
        if(newProps.webContentData){
            this.setState({ webContentData: newProps.webContentData })
        }
    }

    handleEventOnPageClicked(offset){
        this.props.getEventList({
            limit: this.state.eventLimit,
            offset: offset
        })
    }

    handleAlbumnPageOnClicked(offset){
        this.props.getEventList({
            limit: this.state.eventLimit,
            offset: offset
        })
    }

    render() {
        return (
            <div>
                { this.state.webContentData && <AboutOverview webContentData={this.state.webContentData} /> }
                
                { this.state.webContentData && <div className="d-flex justify-content-center mt-5 mb-5" dangerouslySetInnerHTML={{ __html: this.state.webContentData.iframe }}></div> }

                <DailyReflection />
                
                <div className="base__wrapper">
                    <Title title="NEXT EVENT" />
                    <div className="row">
                        {this.state.eventList && this.state.eventList.map( (event, index) => (
                            <div className="col-md-4 col-sm-12 col-xs-12 mb-3">
                                <SimpleEventCard card={event} key={index} />
                            </div>
                        )) }
                    </div>

                    <div className="d-flex justify-content-end align-items-center">
                        <CirclePagination totalData={this.state.eventTotalData} limit={this.state.eventLimit} getDataOnPageClicked={this.handleEventOnPageClicked.bind(this)} />
                        <a role="button" className="btn btn--base ml-4" href="/id/event">Lihat Semua</a>
                    </div>
                </div>
                
                <div className="base__wrapper">
                    <Title title="Dokumentasi" />
                    <div className="row">
                        {this.state.albumnList && this.state.albumnList.map((albumn, index) =>
                            (
                                <div className="col-lg-3 col-md-3 mb-3" onClick={() => this.props.history.push(`/id/gallery/${albumn.id}`)}>
                                    <AlbumnCard albumn={albumn} />
                                </div>
                            )
                        )}
                    </div>
                </div>
                                
            </div>
        );
    }
}

Home = connect(mapStateToProps, mapDispatchToProps)(Home);
export default Home;