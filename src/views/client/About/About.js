import React, { Component } from 'react';
import TitleCenter from '../../../component/shared/title/title_center/TitleCenter';

class About extends Component {
    constructor(){
        super();
        this.aboutMenuList = [
            {name: 'WIHARA', url: 'ekayana'},
            {name: 'PIMPINAN', url: 'leader'},
            {name: 'SEJARAH SINGKAT', url: 'history'},
            {name: 'PENDIDIKAN DAN PELATIHAN', url: 'education-training'},
            {name: 'KEBAKTIAN DAN PELAYANAN UMAT', url: 'prayer'},
            {name: 'PELAYANAN SOSIAL', url: 'social-service'},
            {name: 'MEDIA DAN UNIT USAHA', url: 'media'},
            {name: 'GENERASI MUDA, SENI BUDAYA, DAN OLAHRAGA', url:'generation-art-culture'},
            {name: 'FASILITAS', url: 'facility'}
        ]
    }

    handleOnClick(url){
        this.props.history.push(`/id/about/${url}`);
    }

    render() {
        return (
            <div className="default__wrapper bg-orange">
                <TitleCenter title="Tentang Ekayana" />
                
                <div className="d-flex justify-content-center page__section">
                    <div className="d-flex flex-wrap about_menu_section">
                        {this.aboutMenuList.map((menu, index) => 
                            (
                                <div className="d-flex justify-content-center align-items-center tile" key={index} onClick={ () => {this.handleOnClick(menu.url)} }>
                                    <span>{menu.name}</span>
                                    <div className="tile-underline"></div>
                                </div>
                            )
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default About;