import React, { Component } from 'react';
import TitleCenter from '../../../../../component/shared/title/title_center/TitleCenter';
import { aboutConstant } from '../../../../../constants/AboutEkayana';
import { connect } from 'react-redux';
import { getWebContentByType } from '../../../../../actions/DynamicContent';
import { IMAGE_URL } from '../../../../../constants/ActionTypes';

const mapStateToProps = (state) => {
    return {
        contentData: state.dynamiccontent.contentData
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getWebContentByType: (data) => {
            dispatch(getWebContentByType(data))
        }
    }
}

class DynamicContent extends Component {
    constructor(){
        super();
        this.state = {
            contentData: { },
            imageUrl: null,
            title: ''
        }
    }

    componentWillMount(){
        let type = this.props.match.url.split('/');
        console.log(type[3])
        switch(type[3]){
            case 'history': {
                this.props.getWebContentByType(aboutConstant.sejarah);
                this.setState({ title: 'SEJARAH SINGKAT' })
                break;
            }
            case 'education-training': {
                this.props.getWebContentByType(aboutConstant.pendidikan);
                this.setState({ title: 'PENDIDIKAN DAN PELATIHAN' })
                break;
            }
            case 'prayer': {
                this.props.getWebContentByType(aboutConstant.kebaktian);
                this.setState({ title: 'KEBAKTIAN DAN PELAYANAN UMAT' })
                break;
            }
            case 'social-service': {
                this.props.getWebContentByType(aboutConstant.pelayanan);
                this.setState({ title: 'PELAYANAN SOSIAL' })
                break;
            }
            case 'media': {
                this.props.getWebContentByType(aboutConstant.mediaunit);
                this.setState({ title: 'MEDIA DAN UNIT USAHA' })
                break;
            }
            case 'generation-art-culture': {
                this.props.getWebContentByType(aboutConstant.generasimuda);
                this.setState({ title: 'GENERASI MUDA' })
                break;
            }
            default: {
                this.props.history.push('/404');
            }
        }
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        this.setState({
            contentData: newProps.contentData,
            imageUrl: `${IMAGE_URL}/public/images/contents/${newProps.contentData.image}`
        })
    }

    render(){
        return(
            <div className="dynamic__content__wrapper">
                <TitleCenter title={this.state.title} />
                <div className="row mt-3">
                    <div className="col-md-3">
                        <img className="img-fluid" src={this.state.imageUrl} />
                    </div>
                    <div className="col-md-9 serve__whitespace mt-2">
                        <p>{this.state.contentData.description}</p>
                    </div>
                </div>
            </div>
        )
    }
}

DynamicContent = connect(mapStateToProps, mapDispatchToProps)(DynamicContent);
export default DynamicContent;