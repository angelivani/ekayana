import React, { Component } from 'react';
import TitleCenter from '../../../../../component/shared/title/title_center/TitleCenter';
import { getWebContent } from '../../../../../actions/Content';
import { connect } from 'react-redux';
import { IMAGE_URL } from '../../../../../constants/ActionTypes';

const mapStateToProps = (state) => {
    return {
        contentData: state.content.webContentData
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getWebContent: () => {
            dispatch(getWebContent())
        }
    }
}

class AboutEkayana extends Component {
    constructor() {
        super();
        this.state = { contentData: {} }
    }

    componentWillMount() {
        this.props.getWebContent();
    }

    componentWillReceiveProps(newProps) {
        console.log(newProps);
        this.setState({ contentData: newProps.contentData })
    }

    render() {
        const { contentData } = this.state;
        return (
            <div className="about_ekayana_wrapper">
                <TitleCenter title="About" />
                <div className="row mt-3">
                    <div className="col-md-3">
										<img className="img-fluid" src={`${IMAGE_URL}/public/images/contents/${contentData.image}`} />
                    </div>
                    <div className="col-md-9 pt-2">
                        <p>{ contentData.description }</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 vision_mission_title border-bottom">
                        VISI DAN MISI
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="col center-block">
                            <div className="icon_wrapper">
                                <i className=" far fa-lightbulb"></i>
                                <div>VISI</div>
                            </div>
                            <div className="pd-10">{ contentData.vision }</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="col center-block">
                            <div className="icon_wrapper">
                                <i className=" fas fa-rocket"></i>
                                <div>MISI</div>
                            </div>
                            <div className="pd-10" style={{ whiteSpace: 'pre-wrap' }}>{ contentData.mission }</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

AboutEkayana = connect(mapStateToProps, mapDispatchToProps) (AboutEkayana);
export default AboutEkayana;