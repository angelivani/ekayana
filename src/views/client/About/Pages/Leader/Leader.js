import React, { Component } from 'react';
import TitleCenter from '../../../../../component/shared/title/title_center/TitleCenter';
import EmbossCard from '../../../../../component/client/emboss_card/EmbossCard';
import { getLeaderList } from '../../../../../actions/Leader';
import { IMAGE_URL } from '../../../../../constants/ActionTypes';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return {
        leaderList: state.leader.leaderList,
        succeed: state.leader.succeed,
        error: state.leader.error,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getLeaderList: (data) => {
            dispatch(getLeaderList(data))
        }
    }
}

class Leader extends Component {
    constructor(){
        super();
        this.state = {
            leaderList: [],
            limit: 10,
            offset: 0
        }
    }

    componentWillMount(){
        this.props.getLeaderList({
            limit: this.state.limit,
            offset: this.state.offset
        })
    }

    componentWillReceiveProps(newProps){
        this.setState({ leaderList: newProps.leaderList })

    }

    render() {
        return (
            <div className="leader_wrapper">
                <TitleCenter title="PIMPINAN" />
                <div className="row">
                    {this.state.leaderList.map((leader, index) => 
                        (
                            <div className="col-md-4 mt-3">
                                <EmbossCard image={`${IMAGE_URL}/public/images/leader/${leader.image}`} 
                                    title={leader.name} 
                                    subtitle={leader.position} 
                                    description={leader.description} 
                                />
                            </div>
                        )
                    )}
                </div>                
            </div>
        );
    }
}

Leader = connect(mapStateToProps, mapDispatchToProps)(Leader);
export default Leader;