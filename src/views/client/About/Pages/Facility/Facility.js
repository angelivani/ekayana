import React, { Component } from 'react';
import TitleCenter from '../../../../../component/shared/title/title_center/TitleCenter';
import EmbossCard from '../../../../../component/client/emboss_card/EmbossCard';
import { getFacilityList } from '../../../../../actions/Facility';
import { IMAGE_URL } from '../../../../../constants/ActionTypes';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        facilityList: state.facility.facilityList,
        succeed: state.facility.succeed,
        error: state.facility.error,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        getFacilityList: (data) => {
            dispatch(getFacilityList(data))
        }
    }
}

class Facility extends Component {
    constructor(){
        super()
        this.state = {
            facilityList: [],
            limit: 10,
            offset: 0
        }
    }

    componentWillMount(){
        this.props.getFacilityList({
            limit: this.state.limit,
            offset: this.state.offset
        })
    }

    componentWillReceiveProps(newProps){
        console.log(newProps);
        if(this.state.facilityList != newProps.facilityList){
            this.setState({ facilityList: newProps.facilityList })
        }
    }

    render(){
        return(
            <div className="facility_wrapper">
                <TitleCenter title="FASILITAS" />
                <div className="row">
                    {this.state.facilityList.map((facility, index) => 
                        (
                            <div className="col-md-4 mt-3">
                                <EmbossCard image={`${IMAGE_URL}/public/images/facility/${facility.image}`} title={facility.name} description={facility.description} />
                            </div>
                        )
                    )}
                </div>                
            </div>
        )
    }
}

Facility = connect(mapStateToProps, mapDispatchToProps)(Facility);
export default Facility;