import React, {Component} from 'react';
import { getEventDetailForPublic } from '../../../actions/Event';
import {connect} from 'react-redux';
import Title from '../../../component/shared/title/default_title_center/Title';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import moment from 'moment';

const mapStateToProps = (state) => {
    return{
        eventData: state.event.eventData,
        succeed: state.event.succeed,
        error: state.event.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getEventDetailForPublic: (id) => {
            dispatch(getEventDetailForPublic(id))
        }
    }
}

class EventDetail extends Component {
    constructor(){
        super();
        this.state = {
            eventData: {},
            imageUrl: null
        }
    }

    componentWillMount(){
        let id = this.props.match.params.id;
        this.props.getEventDetailForPublic(id);
    }

    componentWillReceiveProps(newProps){
        if(this.state.eventData != newProps.eventData){
            console.log(newProps);


          setTimeout(() => {
            this.setState({
                eventData: newProps.eventData,
                imageUrl: `${IMAGE_URL}/public/images/event/${newProps.eventData.images[0].image}`
            }, () => { 
                console.log(this.state.eventData.images[0].image)
             })
          }, 100);
            
        }
    }

    backToPreviousPage(){
        this.props.history.push('/id/event')
    }

    render(){
        const { eventData } = this.state;
        return(
            <div className="container">
                
                <Title title="Event Detail" />
                <div className="text-center">
                    {   
                        this.state.imageUrl && 
                        <img className="event__detail__img text" src={this.state.imageUrl} /> 
                    }
                    
                    <h1>{eventData.name}</h1>
                    <h2 className="my__event__date mb-3">{moment(eventData.event_date).format('LL')}</h2>

                    {   
                        eventData.description && 
                        <p className="event__detail__desccription">{ eventData.description }</p> 
                    }

                </div>
                <div className="d-flex justify-content-center mt-4 mb-3">
                    <button className="btn btn-md btn-outline-dark" onClick={this.backToPreviousPage.bind(this)}>Kembali</button>
                </div>
            </div>
        )
    }
}


EventDetail = connect(mapStateToProps, mapDispatchToProps)(EventDetail);

export default EventDetail;