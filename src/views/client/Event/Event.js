import React, {Component} from 'react';
import EventCard from '../../../component/client/event/EventCard';
import Title from '../../../component/shared/title/default_title_center/Title';
import {getEventListForPublic, getPinnedEventList} from '../../../actions/Event';
import {connect} from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import {
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton
  } from 'react-share';
  

const mapStateToProps = (state) => {
    return{
        eventPinnedList: state.event.eventPinnedList,
        eventList: state.event.eventList,
        totalData: state.event.totalData,
        succeed: state.event.succeed,
        error: state.event.error,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return{
        getEventList: (data) => {
            dispatch(getEventListForPublic(data))
        },
        getPinnedEventList: () => {
            dispatch(getPinnedEventList())
        }
    }
}

class EventList extends Component {
    constructor(){
        super();
        this.state = {
            eventPinnedList: [],
            eventList: [],
            limit: 10,
            hasMore: true,
            isLoading: false,
            currPage: 1,
            currentPage: 1
        }
    }

    componentWillMount(){
        this.props.getEventList({
            limit: this.state.limit, 
            offset: (this.state.currPage * this.state.limit) - this.state.limit
        });
    }

    componentDidMount(){
        setTimeout(() => {
            window.scrollTo(0,0);
        }, 90);  
    }

    componentWillReceiveProps(newProps){
        console.log(newProps)

        if( (this.state.eventList.length != newProps.eventList.length) && this.state.hasMore && newProps.eventList.length > 0){
            let listEvent = this.state.eventList;
            let listPinnedEvent = this.state.eventPinnedList;

            for(var i=0; i < newProps.eventList.length; i++){
                if(newProps.eventList[i].pinned == 1){
                    listPinnedEvent.push(newProps.eventList[i])
                }
                else{
                    listEvent.push(newProps.eventList[i])
                }
            }
            
            let currPage = this.state.currPage + 1;
            
            setTimeout(() => {
                this.setState({
                    eventList: listEvent,
                    eventPinnedList: listPinnedEvent,
                    isLoading: false,
                    currPage: currPage
                })
            }, 90)
        }
        else{
            this.setState({ hasMore: false, isLoading: false });
        }
    }

    loadEvent(){
        if (this.state.hasMore) {            
            this.setState({ isLoading: true }, () => {
                this.props.getEventList({
                    limit: this.state.limit, 
                    offset: (this.state.currPage * this.state.limit) - this.state.limit
                });
            })
        }
        else { this.setState({ isLoading: false }) }
    }

    render(){
        return(
            <div>
                <Title title="Event" />
                
                <div className="container min-height-small">
                    {this.state.eventPinnedList && this.state.eventPinnedList.map((pinnedEvent, index) => (                            
                        <EventCard event={pinnedEvent} key={index+7} /> 
                    ))}                                        

                    {this.state.eventList && this.state.eventList.map((event, index) =>(
                        <EventCard event={event} key={index+7} />   
                    ))}
                    
                    
                    {this.state.isLoading &&                         
                        <div style={{ width: "100%", textAlign: "center" }}>
                            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div> 
                        </div>
                    }

                    {this.state.hasMore && 
                        <div className="d-flex justify-content-center mb-3 mt-2">
                            <button className="btn btn-outline-dark" onClick={() => {this.loadEvent()}}>Lihat Selebihnya</button>
                        </div> 
                    }
                </div>
            </div>
        )
    }
}

EventList = connect(mapStateToProps, mapDispatchToProps)(EventList);
export default EventList;