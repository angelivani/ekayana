import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import Home from './Home/Home';
import About from './About/About';
import Footer from '../../component/client/footer/Footer.js';
import AboutEkayana from './About/Pages/AboutEkayana/AboutEkayana';
import Leader from './About/Pages/Leader/Leader';
import Activity from './Activity/Activity';
import Header from '../../component/client/header/Header';
import Buddhavacana from './Buddhavacana/Buddhavacana';
import Gallery from './Gallery/Gallery';
import Ebook from './Ebook/Ebook';
import EventList from './Event/Event';
import EventDetail from './Event/EventDetail';
import Photos from '../../component/client/photos/Photos';
import Facility from './About/Pages/Facility/Facility';
import DynamicContent from './About/Pages/DynamicContent/DynamicContent';
import Article from './Article/Article';
import ArticleDetail from './Article/ArticleDetail';

class Client extends Component {
    render() {
        const {match} = this.props;
        return (
            <div>
                <Header />
                <div className="page__wrapper">
                    <Route path={`${match.path}/home`} component={Home} />
                    <Route exact path={`${match.path}/about`} component={About} />

                    <Route exact path={`${match.path}/activity`} component={Activity} />
                    <Route exact path={`${match.path}/event`} component={EventList} />
                    <Route path={`${match.path}/event/:id`} component={EventDetail} />

                    <Route path={`${match.path}/about/ekayana`} component={AboutEkayana} />
                    <Route path={`${match.path}/about/leader`} component={Leader} />
                    <Route path={`${match.path}/about/facility`} component={Facility} />

                    <Route path={`${match.path}/about/history`} component={DynamicContent} />
                    <Route path={`${match.path}/about/education-training`} component={DynamicContent} />
                    <Route path={`${match.path}/about/prayer`} component={DynamicContent} />
                    <Route path={`${match.path}/about/social-service`} component={DynamicContent} />
                    <Route path={`${match.path}/about/media`} component={DynamicContent} />
                    <Route path={`${match.path}/about/generation-art-culture`} component={DynamicContent} />
                    
                    <Route path={`${match.path}/buddhavacana`} component={Buddhavacana} />

                    <Route exact path={`${match.path}/gallery`} component={Gallery} />
                    <Route path={`${match.path}/gallery/:id`} component={Photos} />
                    
                    <Route path={`${match.path}/ebook`} component={Ebook} />
                    <Route exact path={`${match.path}/article`} component={Article} />
                    <Route path={`${match.path}/article/:id`} component={ArticleDetail} />
                </div>
                <Footer />
            </div>
        );
    }
}

export default Client;