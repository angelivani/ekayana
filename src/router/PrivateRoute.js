import React, {Component} from 'react';
import {Route, Redirect} from 'react-router-dom';
import { withCookies} from 'react-cookie';
import BuddhavacanaList from '../views/admin/Buddhavacana/BuddhavacanaList';

const e = React.createElement;

class PrivateRoute extends Component {

    render(){
        return(
            this.props.path && <Route path={this.props.path} render={(props) => (
                this.props.cookies.get('ek_accessToken') ? e(this.props.component, props, null) : <Redirect to="/admin/login" />
            )} />
        )
    }
}

PrivateRoute = withCookies(PrivateRoute);
export default PrivateRoute;