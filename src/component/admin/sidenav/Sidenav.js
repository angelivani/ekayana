import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router';
import UserProfile from './UserProfile';

import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';

class Sidenav extends Component {
    constructor(){
        super();
        this.state = {
					listMenu: [
								{name: 'User', link: "user/list", icon: "fas fa-user"},
                {name: "About Ekayana", link: "about", icon: "fas fa-building"},
                {name: "Buddhavacana", link: "buddhavacana/list", icon: "fas fa-gem"},
                {name: "Kegiatan", link: "activity/list", icon: "fas fa-chess-rook"},
                {name: "Event", link: "event/list", icon: "far fa-calendar-alt"},
                {name: "Album", link: "albumn/list", icon: "fas fa-book"},
                {name: "Foto", link: "photos/list", icon: "fas fa-images"},
                {name: "Ebook", link: "ebook/list", icon: "fas fa-book-open"},
                {name: "Artikel", link: "article/list", icon: "far fa-newspaper"}
            ],
            sidenavExpand: true,
            currentPath: ''
        }
        console.log(this.props);
    }

    componentWillMount(){
        this.setState({
            currentPath: this.props.location.pathname
        }, () => {
            console.log(this.state);
        })
    }

    componentDidUpdate(){
        if(this.props.sidenavExpand !== this.state.sidenavExpand){
            this.setState({ sidenavExpand: this.props.sidenavExpand })
        }
        if(this.props.location.pathname !== this.state.currentPath){
            this.setState({currentPath: this.props.location.pathname})
        }
    }
    
    render() {
        const {sidenavExpand} = this.state;
        if(this.props.cookies.get('ek_accessToken')) {
            console.log('STILL EXISTS');
            return (<SidenavComponent {...this.state} />);
        } 
        else {
            return(null)
        } 
    }
}

const SidenavComponent = ({sidenavExpand, listMenu, currentPath}) => {
    return(
        <ul className={`admin-sidenav ${sidenavExpand ? 'admin-sidenav-open' : 'admin-sidenav-close'}`}>
            <UserProfile />
            {listMenu.map((menu, index) => 
                (<li key={index} className={ currentPath.includes(menu.link) ? 'active-menu': '' }>
                    <i className={`sidenav-icon ${menu.icon}`}></i>
                    <Link key={index} className="sidenav-menu" to={`/admin/${menu.link}`}>
                         {menu.name}
                    </Link>
                </li>)
            )}
           
        </ul>
    )
}


Sidenav = withRouter(Sidenav);
Sidenav = withCookies(Sidenav);
export default Sidenav;