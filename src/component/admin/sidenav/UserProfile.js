import React, { Component } from 'react';
import { withRouter } from 'react-router';

import Cookies from 'universal-cookie';

class UserProfile extends Component {
    doLogout(){
        console.log('DO LOGOUT');
        const cookies = new Cookies();
        cookies.remove('ek_accessToken');
        setTimeout(() => { this.props.history.push('/admin/login'); }, 200);        
    }

    render() {
        return (
            <div className="display-flex flex-dir-column">
                <div className="display-flex flex-dir-column flex-justify-center">
                    <div className="display-flex flex-justify-center pd-10">
                        <img className="admin-image" src="https://www.adorama.com/alc/wp-content/uploads/2016/10/shutterstock_267775931-1366x800.jpg" />
                    </div>
                    <div className="admin-name">John Doe</div>
                    <div className="text-align-center pd-10">
                        <span className="admin-position">Admin</span>
                    </div>
                </div>
                <div className="display-flex admin-settings-wrapper">
                    <button>
                        Settings
                    </button>
                    <button onClick={this.doLogout.bind(this)}>
                        Logout
                    </button>
                </div>
            </div>
        );
    }
}

UserProfile = withRouter(UserProfile);

export default UserProfile;