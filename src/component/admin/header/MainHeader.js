import React, { Component } from 'react';

class MainHeader extends Component {
    constructor(){
        super();
        this.state = {
            toggleStatus: true
        }
    }

    componentDidUpdate(){
        if(this.props.toggleStatus !== this.state.toggleStatus){
            this.setState({toggleStatus: this.props.toggleStatus})
        }
    }

    toggle(){
        this.props.getToggleState(!this.state.toggleStatus);
    }

    render() {
        return (
            <div className="admin-main-header">
                <button className="button-icon m-right-10" onClick={this.toggle.bind(this)}>
                    <i className="fas fa-bars menu-bar-icon"></i>
                </button>
                <span className="company-name">VIHARA EKAYANA ARAMA</span>
            </div>
        );
    }
}

export default MainHeader;