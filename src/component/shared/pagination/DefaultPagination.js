import React, { Component } from 'react';

class DefaultPagination extends Component {
    constructor(){
        super();
        this.state = {
            start_page: 0,
            end_page: 0,
            total_data: 0,
            total_page: 0,
            limit: 0,
            list_page: [],
            active: 0,
            show_arrow_left: false,
            show_arrow_right: false
        }
    }
    componentWillMount(){
        var totalPage = 0;
        var listPage = [];

        if(this.state.limit != this.props.limit){
            this.setState({limit: this.props.limit})
        }

        if(this.props.totalData <= this.props.limit){
           totalPage = 1;
        }
        else {

            if( (this.props.totalData % this.props.limit) === 0){
                totalPage = parseInt(this.props.totalData) / parseInt(this.props.limit);
            }
            else{
                totalPage = parseInt( parseInt(this.props.totalData) / parseInt(this.props.limit) )  + 1;
            }
        }
        
        for(var i=0; i < totalPage; i++){
            if(i < this.props.limit){
                listPage.push({
                    page: i+1,
                    show: true
                });    
            }
            else{
                
                listPage.push({
                    page: i+1,
                    show: false
                });
            }
            console.log(i+1);
        }

        this.setState({
            ...this.state,
            end_page: (totalPage > this.props.limit) ? this.props.limit : totalPage,
            total_data: parseInt(this.props.totalData),
            limit: parseInt(this.props.limit),
            total_page: totalPage,
            list_page: listPage,
            show_arrow_right: (totalPage > this.props.limit) ? true : false,
        }, () => {
            console.log(this.state);
        })
    }

    handlePageClicked(data){
        this.setState({
            active: data.page - 1
        }, () => {
            this.props.getDataOnPageClicked(data.page)
        })
    }

    handleNextPageClicked(){
        var listPage = this.state.list_page;
        var startPage = this.state.end_page + 1;
        console.log('start page', startPage);
        var showArrowLeft = true;
        var showArrowRight = this.state.show_arrow_right;
        var endPage;
        console.log('next page clicked')
        console.log(this.state.total_page);
        
        console.log(listPage);
        if(this.state.total_page > (this.state.end_page + this.state.limit)){
            endPage = this.state.end_page + this.state.limit;
        }
        else{
            endPage = this.state.total_page;
            showArrowRight = false;
        }

        
        
        for(var i= startPage-1; i < endPage; i++){
            listPage[i].show = true;
        }
        for(var i=(startPage-(this.state.limit + 1)); i < (startPage-1); i++){
            listPage[i].show = false;
        }

        this.setState({
            ...this.state,
            active: startPage - 1,
            start_page: startPage,
            end_page: endPage,
            list_page: listPage,
            show_arrow_left: showArrowLeft,
            show_arrow_right: showArrowRight
        }, () => {
            this.props.getDataOnPageClicked(startPage);
        })

    }

    handlePageBeforeClicked(){
        
        var startPage = this.state.start_page - this.state.limit;
        var endPage = this.state.start_page - 1;
        var listPage = this.state.list_page;
        var showArrowLeft = this.state.show_arrow_left;
        var showArrowRight = this.state.show_arrow_right;
        var totalPage = this.state.total_page;

        if(startPage === 1){
            showArrowLeft = false;
        }
        if(totalPage > endPage){
            showArrowRight = true;
        }

        for(var i= (startPage-1) ; i < endPage; i++){
            listPage[i].show = true;
        }

        for(var i= (this.state.start_page - 1); i < this.state.end_page; i++){
            listPage[i].show = false;
        }

        this.setState({
            ...this.state,
            active: startPage - 1,
            start_page: startPage,
            end_page: endPage,
            list_page: listPage,
            show_arrow_left: showArrowLeft,
            show_arrow_right: showArrowRight
        }, () => {
            this.props.getDataOnPageClicked(this.state.active + 1);
        })
    }

    render() {
        return (
            <div className="d-flex justify-content-center">
                {this.state.show_arrow_left ? <button onClick={this.handlePageBeforeClicked.bind(this)}><i className="fas fa-chevron-left"></i></button> : null}

                {this.state.list_page.map((pagination, index) => (
                    pagination.show ? <button key={index} className={`page-numb ${this.state.active === index ? 'bg-soft-cyan clr-white' : 'bg-grey'}`} onClick={() => this.handlePageClicked(pagination)}> {pagination.page} </button> : null
                ))}

                {this.state.show_arrow_right ? <button onClick={ () => this.handleNextPageClicked() }><i className="fas fa-chevron-right"></i></button> : null }
            </div>
        );
    }
}

export default DefaultPagination;