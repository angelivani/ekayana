import React, { Component } from 'react';

class CirclePagination extends Component {
    constructor(){
        super();
        this.state = {
            total_data: 0,
            total_page: 0,
            limit: 0,
            list_page: [],
            active: 0
        }
    }
    componentWillReceiveProps(newProps){
        var totalPage = 0;
        var listPage = [];
        if( (this.state.total_data != newProps.totalData) && ( (newProps.totalData % newProps.limit) === 0) ){
            totalPage = parseInt(newProps.totalData) / parseInt(newProps.limit);
        }
        else{
            totalPage = parseInt( parseInt(newProps.totalData) / parseInt(newProps.limit) )  + 1;
        }

        for(var i=0; i < totalPage; i++){
            if(i < 5){
                listPage.push({
                    page: i+1,
                    show: true
                });
            }
            else{
                listPage.push({
                    page: i+1,
                    show: false
                });
            }
        }

        this.setState({
            total_data: parseInt(newProps.totalData),
            limit: parseInt(newProps.limit),
            total_page: totalPage,
            list_page: listPage
        })
    }

    handlePageClicked(data){
        
        this.setState({
            active: data.page - 1
        }, () => {
            this.props.getDataOnPageClicked(data.page * this.state.limit - this.state.limit)
        })
        
    }

    render() {
        return (

            <div className="display-flex flex-justify-center">
                {this.state.list_page.map((page, index) => (
                    <div className={`circle-pagination mr-1 ${this.state.active === index ? 'bg-orange' : 'bg-grey'}`}  key={index} onClick={() => this.handlePageClicked(page)}></div>
                ))}
                
            </div>
        );
    }
}

export default CirclePagination;