import React, { Component } from 'react';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '4px',
        padding               : '0'
    }
};

class SuccessModal extends Component {
    constructor(){
        super();
        this.state = {
            isModalOpen: false
        }
    }

    componentWillMount(){
        this.setState({isModalOpen: true})
    }

    hideModal(){
        this.setState({ isModalOpen: false });
        this.props.whenSuccessModalClose();
    }

    render() {
        return (
            <Modal isOpen={this.state.isModalOpen} style={customStyles}>
                <div className="success-modals-wrapper">
                    <div className="display-flex flex-dir-column flex-justify-center align-item-center modals-body">
                        <i className="fa fa-check-circle"></i>
                        <p className="modals-type">Success</p>
                        <p>{ this.props.successMessage }</p>
                    </div>
                    <div className="display-flex flex-justify-center align-item-center modals-footer">
                        <button className="confirm-button" onClick={this.hideModal.bind(this)}>OK</button>
                    </div>
                </div>
            </Modal>
            
        );
    }
}

export default SuccessModal;