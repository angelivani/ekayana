import React, { Component } from 'react';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '4px',
        padding               : '0'
    }
};

class WarningModal extends Component {
    constructor(){
        super();
        this.state = {
            isModalOpen: false
        }
    }

    componentWillMount(){
        this.setState({isModalOpen: true})
    }

    hideModal(){
        this.setState({ isModalOpen: false })
        this.props.whenWarningModalClose();
    }
    
    render() {
        return (
            <Modal isOpen={this.state.isModalOpen} style={customStyles}>
                <div className="warning-modals-wrapper">
                    <div className="display-flex flex-dir-column flex-justify-center align-item-center modals-body">
                        <i className="fa fa-times-circle"></i>
                        <p className="modals-type">Error</p>
                        <p>{ this.props.errorMessage }</p>
                    </div>
                    <div className="display-flex flex-justify-center align-item-center modals-footer">
                        <button className="button" onClick={this.hideModal.bind(this)}>BACK</button>
                    </div>
                </div>
            </Modal>
        );
    }
}


export default WarningModal;