import React, { Component } from 'react';

class Title extends Component {
    render() {
        return (
            <div className="d-flex align-items-center justify-content-center default_title_wrapper">
                <span>{this.props.title}</span>
                <div className="title_decorator"></div>
            </div>
        );
    }
}

export default Title;