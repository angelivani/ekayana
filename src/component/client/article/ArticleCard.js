import React, { Component } from 'react';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import { withRouter } from 'react-router';
import moment from 'moment';

class ArticleCard extends Component {
    render() {
        const {article, history} = this.props; 
        return(
            <div>
                <div className="card article__card">
                    <img className="article__img" src={`${IMAGE_URL}/public/images/article/${article.image}`} alt="Card image cap" />
                    <div className="card-body" onClick={ () => history.push(`/id/article/${article.id}`) }>
                        <h5 className="card-title">{article.name}</h5>
                        <h3 className="card-subtitle mb-2 text-muted">{moment(article.created_at).format('LL')}</h3>
                        <p className="card-text">{article.description}</p>
                    </div>
                </div>
            </div>
        )
    }
}

ArticleCard = withRouter(ArticleCard);
export default ArticleCard;