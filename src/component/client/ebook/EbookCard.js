import React, { Component } from 'react';
import {IMAGE_URL} from '../../../constants/ActionTypes';

class EbookCard extends Component {
    render() {
        const {ebook} = this.props;
        return (
            <div className="d-flex flex-column ebook__tile">
                <img src={`${IMAGE_URL}/public/images/pdf_thumb/${ebook.thumbnail}` } />
                <span className="title">{ebook.name}</span>
                <span className="subtitle">{ebook.source}</span>
                { this.props.children }
            </div>
        );
    }
}

export default EbookCard;