import React, { Component } from 'react';

class Footer extends Component {
    constructor(){
        super();
        this.state = {
            addressList: [
                {
                    placeName: "Wihara Ekayana Arama",
                    address: "Jl. Mangga II No. 8 Tanjung Duren Barat - Green Ville",
                    area: "Duri Kepa - Jakarta Barat - 11510"
                }
            ],
            emailList: [
                {name: 'admin@ekayana.co.id'}
            ],
            phoneList: [
                {number: '021-5687921'},
                {number: '021-5687922'}
            ],
            faxList: [
                {number: '021-5687923'}
            ]
        }
    }
    render() {
        return (
            <div className="footer">
                <div className="footer-title-wrapper">
                    <div className="clr-orange">CONTACT INFO</div>
                    <div className="footer-title-underline"></div>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-4 col-sm-12 ">
                        <div className="pd-top-10">
                            <span className="pd-left-20 title">Alamat</span>
                            {this.state.addressList.map((address, index) => (
                                <div className="display-flex flex-dir-column padding" key={index + 3} style={{ padding: "10px 20px" }}>
                                    <span key={index + 2}>{address.placeName}</span>
                                    <span key={index + 1}>{address.address}</span>
                                    <span key={index}>{address.area}</span>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-12">
                        <div className="display-flex flex-dir-column m-bottom-10 padding-10-20">
                            <span className="title">Email</span>
                            {this.state.emailList.map((emailData, index) => 
                                <span key={index}>{emailData.name}</span>
                            )}    
                        </div>
                        
                        <div className="display-flex flex-dir-column m-bottom-10 padding-10-20">
                            <span className="title">Nomor Telepon</span>
                            {this.state.phoneList.map((phoneData, index) => 
                                <span key={index}>{phoneData.number}</span>
                            )}    
                        </div>

                        <div className="display-flex flex-dir-column m-bottom-10 padding-10-20">
                            <span className="title">Fax</span>
                            {this.state.faxList.map((faxData, index) => 
                                <span key={index}>{faxData.number}</span>
                            )}    
                        </div>                        
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-12">
                        <div className="pd-top-10">
                            <span className="pd-left-20 title">Social Media</span>
                            <div className="display-flex flex-dir-column m-bottom-10 padding-10-20 social-media-wrapper">
                                <a href="https://twitter.com/EkayanaArama/" target="_blank"><i className="fab fa-twitter-square clr-light-blue m-right-10"></i>Wihara Ekayana Arama</a>
                                <a href="https://web.facebook.com/WiharaEkayana/" target="_blank"><i className="fab fa-facebook-square clr-soft-blue m-right-10"></i>Wihara Ekayana Arama</a>
                                <a href="https://www.instagram.com/ekayanaarama" target="_blank"><i className="fab fa-instagram clr-pink m-right-10"></i>@ekayanaarama</a>
                                <a href="https://www.youtube.com/user/ekayanabudhist" target="_blank"><i className="fab fa-youtube clr-red m-right-10"></i>Ekayana Arama</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;