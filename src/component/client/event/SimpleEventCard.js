import React, { Component } from 'react';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import moment from 'moment';
import {withRouter} from 'react-router';

class SimpleEventCard extends Component {
    render() {
        const {card} = this.props;
        return (
            <div className="card" key={card.id + 6}>
                <img className="card-img-top event-card-img" key={card.id + 5} src={`${IMAGE_URL}/public/images/event/${card.images[0].image}`} alt="Card image cap" />
                <div className="card-body" key={card.id + 4} onClick={ () => this.props.history.push(`/id/event/${card.id}`)}>
                    <h5 className="card-title" key={card.id + 3}>{card.name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted" key={card.id + 2}>{moment(card.event_date).format('LL')}</h6>
                    <p className="card-text event__ek__desc" key={card.id + 1}>
                        {card.description }
                    </p>
                </div>
            </div>
        );
    }
}

SimpleEventCard = withRouter(SimpleEventCard);
export default SimpleEventCard;