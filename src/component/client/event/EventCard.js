import React, {Component} from 'react';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import moment from 'moment';
import {withRouter} from 'react-router';

class EventCard extends Component {
    constructor(){
        super();
        moment.locale();
    }

    render(){
        const {event, history} = this.props;
        return(
            <div>
                <div className="event__card row mb-3">
                    <div className="col-md-4">
                        <div className="block__event">
                            <img src={`${IMAGE_URL}/public/images/event/${event.images[0].image}`} />
                        </div>                        
                        <div className="row">
                            <div className="col-4 d-flex align-items-center border-right">
                                <div className="event__date">{moment(event.event_date).format('LL')}</div>
                            </div>
                            <div className="col-8 d-flex align-items-center" onClick={ () => history.push(`/id/event/${event.id}`) }>
                                <span className="event__title">{event.name}</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="block__event">
                            <div className="box">
                                <div className="wrapper">
                                    <p className="event__ek__desc">{event.description}</p>
                                </div>
                            </div>
                        </div>
                        
                        {this.props.children}
                        
                    </div>
                </div>
            </div>
            
        )
    }
}

EventCard = withRouter(EventCard);
export default EventCard;