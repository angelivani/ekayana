import React, { Component } from 'react';

class LeaderCard extends Component {
    render() {
        const {article} = this.props; 
        return(
            <div>
                <div className="card article__card" style={{ border: 'none', boxShadow: "2px 2px black" }}>
                    <img className="article__img" src="https://pbs.twimg.com/media/CbRqC7dXEAASfuC.jpg" alt="Card image cap" />
                    <div className="card-body">
                        <h5 className="card-title">{leader.name}</h5>
                        <h3 className="card-subtitle mb-2 text-muted">{leader.position}</h3>
                        <p className="card-text">{leader.description}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default LeaderCard;