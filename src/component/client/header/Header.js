import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

class Header extends Component {
	constructor() {
		super();
		this.state = {
			menu_list: [],
			toggle: true
		}
	}
	componentWillMount() {
		this.setState({
			menu_list: [
				{
					name: 'Beranda',
					path: this.props.match.url + '/home'
				},
				{
					name: 'Tentang Ekayana',
					path: this.props.match.url + '/about'
				},
				{
					name: 'Kegiatan',
					path: this.props.match.url + '/activity'
				},
				{
					name: 'Event',
					path: this.props.match.url + '/event'
				},
				{
					name: 'Artikel',
					path: this.props.match.url + '/article'
				},
				{
					name: 'Buddhavacana',
					path: this.props.match.url + '/buddhavacana'
				},
				{
					name: 'Galeri',
					path: this.props.match.url + '/gallery'
				},
				{
					name: 'E-Book Buddhis',
					path: this.props.match.url + '/ebook'
				}
			]
		})
	}

	toggleHeader() {
		this.setState({
			toggle: !this.state.toggle
		})
	}

	render() {
		return (
			<nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light main__header">
				<a className="navbar-brand company__name" href="#">Wihara Ekayana Arama</a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>

				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav ml-auto">
						{this.state.menu_list.map((menu, index) =>
							(
								<li className={`nav-item mr-2 menu_text_decoration ${this.props.location.pathname.includes(menu.path) ? 'menu__active' : ''}`}>
									<a className="nav-link" href={menu.path}>{menu.name}</a>
								</li>
							)
						)}
					</ul>
				</div>
			</nav>
		);
	}
}

Header = withRouter(Header);
export default Header;