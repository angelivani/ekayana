import React, { Component } from 'react';

class DailyCard extends Component {
    render() {
        const {card} = this.props;
        return (
            <div className="activity__card">
                <div className="card__title">
                    {card.name}
                    <div className="card_title_decorator"></div>
                </div>
                
                <div className="row p-0 m-0">
                    <div className="col-xs-1 mr-3">
                        <i className="fas fa-map-pin"></i>
                    </div>
                    <div className="col-xs-2 mr-3">
                        Lokasi
                    </div>
                    <div className="col-xs-1 mr-3">
                        :
                    </div>
                    <div className="col-xs-8">
                        {card.location}
                    </div>
                </div>
                <div className="mb-2"></div>
                <div className="row p-0 m-0">
                    <div className="col-xs-1 mr-3">
                        <i className="fas fa-calendar-alt"></i>
                    </div>
                    <div className="col-xs-2 mr-3">
                        Jadwal
                    </div>
                    <div className="col-xs-1 mr-3">
                        :
                    </div>
                    <div className="col-xs-12 mr-3">
                        
                        {card.schedules.map( (schedule, index) => 
                            (   
                                <div className="ml-3">
                                    <div className="row">
                                        <div className="col-xs-12">
                                            {`${schedule.day}${schedule.day == '' ? '' : ','} ${schedule.time_from} - ${schedule.time_to}`}
                                        </div>
                                        
                                    </div>
                                    <div className="mb-1"></div>
                                </div>
                                
                            )
                        )}
                    </div>
                </div>
                {this.props.children}
        </div>
        );
    }
}

export default DailyCard;