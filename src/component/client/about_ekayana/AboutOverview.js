import React, { Component } from 'react';
import SuccessModal from '../../shared/modal/success_modal/SuccessModal';

class AboutOverview extends Component {
    render() {
        const {webContentData} = this.props;
        return (
            <div className="display-flex flex-justify-center align-item-center flex-dir-column about-overview-wrapper">
                <div className="about-overview-box">
                    <p className="company-name">Wihara Ekayana Arama</p>
                    <p>{webContentData.description}</p>
                </div>
                <div className="underline-decorator"></div>
                
            </div>
        );
    }
}

export default AboutOverview;