import React, { Component } from 'react';

class Card extends Component {
    render() {
        const {card} = this.props;
        return (
            <div className="card">
                <img className="card-img-top custom-image" src={card.image} alt="Card image cap" />
                <div className="card-body">
                    <p className="custom-card-title">{card.title}</p>
                    <p className="card-subtitle mb-2 text-muted">{card.subtitle}</p>
                    <p className="card-text">{card.description}</p>
                </div>
            </div>
        );
    }
}

export default Card;