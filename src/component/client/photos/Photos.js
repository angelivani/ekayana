import React, { Component } from 'react';

import { getPhotosInAlbumn } from '../../../actions/Photos';
import { IMAGE_URL } from '../../../constants/ActionTypes';
import { connect } from 'react-redux';
import Title from '../../shared/title/default_title_center/Title';
import { Lightbox } from 'react-modal-image';
import moment from 'moment';

const mapStateToProps = (state) => {
	return {
		albumn: state.photos.albumn,
		photosList: state.photos.photosList,
		albumnList: state.albumn.albumnList,
		error: state.albumn.error,
		photoSucceed: state.photos.succeed,
		photoError: state.photos.error
	}
}

const mapDispatchToProps = (dispatch, props) => {
	return {
		getPhotosInAlbumn: (data) => {
			dispatch(getPhotosInAlbumn(data))
		}
	}
}

class Photos extends Component {
	constructor() {
		super();
		this.state = {
			albumn: null,
			photosList: null,
			openModalImage: false,
			urlImgToShow: ''
		}
	}

	componentWillMount() {
		let id = this.props.match.params.id;
		this.props.getPhotosInAlbumn({
			albumn_id: id,
			limit: 10,
			offset: 0
		})
	}

	componentDidUpdate(prevState) {
		if (prevState.albumn != this.props.albumn) {
			console.log(this.props.albumn);
			this.setState({
				albumn: this.props.albumn
			})
		}
		if (prevState.photosList != this.props.photosList) {
			this.setState({
				photosList: this.props.photosList
			})
		}
	}

	handleImgOnClick(e) {
		console.log(e.target.src);
		this.setState({ urlImgToShow: e.target.src, openModalImage: true });
	}

	closeLightbox() {
		this.setState({ openModalImage: false });
	}

	render() {
		return (
			<div className="container">

				<div className="text-center">
					{this.state.albumn &&
						<div>
							<h1 style={{ letterSpacing: "1px", fontSize: "17px", fontWeight: "bold" }}>{this.state.albumn.name}</h1>
							<p style={{ color: 'grey' }}>{moment(this.state.albumn.created_at).format('LL')}</p>
							<hr />
						</div>
					}

				</div>

				<div className="row">
					{this.state.photosList && this.state.photosList.map((photos, index) =>
						(
							<div className="col-lg-3 col-md-4 mb-3" onClick={this.handleImgOnClick.bind(this)}>
								<img className="img__fitdiv" src={`${IMAGE_URL}/public/images/photos/${photos.image}`} />
							</div>
						)
					)}
				</div>

				{this.state.openModalImage && <Lightbox large={this.state.urlImgToShow} onClose={() => this.closeLightbox()} />}


			</div>
		);
	}
}

Photos = connect(mapStateToProps, mapDispatchToProps)(Photos);
export default Photos;