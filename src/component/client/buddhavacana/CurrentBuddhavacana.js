import React, { Component } from 'react';
import moment from 'moment';

class CurrentBuddhavacana extends Component {
    constructor(){
        super();
        moment().locale;
        this.state = {
            buddhavacana: {
                buddhavacana_id: 1,
                buddhavacana_desc: 'Dg 4 buah cara, seseorang yg pandai dan bijaksana berusaha agar tdk runtuh (moralnya), selalu sadar, tdk disalahkan, dan tdk tercela. Apakah ke 4 buah cara itu? Ke 4 buah cara itu adl melaksanakan Perbuatan Benar, Ucapan Benar, dan Pikiran Benar, serta bersyukur atas keadaannya sekarang',
                buddhavacana_date: moment().format('LL')
            }
        }
    }

    render() {
        const { buddhavacana } = this.state
        return (
            <div className="display-flex flex-justify-center align-item-center flex-dir-column buddhavacana-overview-wrapper">
                <div className="display-flex flex-justify-center align-item-center flex-dir-column buddhavacana-box">
                    <div className="title-wrapper">
                        Renungan Harian
                    </div>
                    <p className="title"></p>
                    
                        
                    <div className="buddhavacana-desc"> 
                        <span className="pd-left-20 pd-right-20">{buddhavacana.buddhavacana_desc}</span>    
                    </div>
                              
                    <div className="date-wrapper">
                        <span>{buddhavacana.buddhavacana_date}</span>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default CurrentBuddhavacana;