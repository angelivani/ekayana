import React, { Component } from 'react';

class BuddhavacanaCard extends Component {
    constructor(){
        super();
    }

    componentWillReceiveProps(newProps){
        if(newProps.succeed){
            this.setState({successMessage: newProps.succeed});
        }
        else if(newProps.error){
            this.setState({errorMessage: newProps.error});
        }
    }

    handleSuccessModalClose(){
       
    }
    render() {
        const {buddhavacana, inAdminPage } = this.props;
        return (
            <div>
                <div className="row">
                    <div className="col-md-4 d-flex align-items-center mb-3">
                        <div className="buddhavacana__source">
                            {buddhavacana.source}
                            <div className="text-decorator"></div>
                        </div>
                    </div>
                    
                    <div className={inAdminPage ? "col-md-6" : "col-md-8"}>
                        <p className="font-weight-bold">{buddhavacana.title}</p>
                        <p class="serve__whitespace">{buddhavacana.description}</p>
                    </div>
                    {this.props.children}
                </div>
                <hr />
            </div>
        );
    }
}

export default BuddhavacanaCard;