import React, { Component } from 'react';
import DefaultImage from '../../../assets/default_image.jpg';
import {IMAGE_URL} from '../../../constants/ActionTypes';
import moment from 'moment';

class AlbumnCard extends Component {
    render() {
        const {albumn} = this.props;
        return (
            // <div className="col-lg-3 mb-3">
                <div className="d-flex flex-column albumn__tile">
                    <img src={albumn.thumbnail ?  `${IMAGE_URL}/public/images/photos/${albumn.thumbnail}` : DefaultImage } />
                    <span className="albumn__title mt-2">{albumn.name}</span>
                    <span className="albumn__date">{ moment(albumn.created_at).format('LL') }</span>
                </div>
            // </div>
        );
    }
}

export default AlbumnCard;