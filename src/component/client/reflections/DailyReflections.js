import React, { Component } from 'react';
import moment from 'moment';
import { reflections as DailyRef } from '../../../constants/Reflections';

class DailyReflections extends Component {
    constructor(){
        super();
        this.state = {
            todayReflection: null,
            currentDate: moment().format('LL')
        }
    }

    componentWillMount(){        
        let currDate = (moment().date()).toString();
        let currMonth = (moment().month() + 1).toString();
        if(currDate.length == 1){
            currDate = "0" + currDate;
        }
        if(currMonth.length == 1){
            currMonth = "0" + currMonth;
        }
        
        this.setState({
            todayReflection: DailyRef[currMonth][currDate]
        })
    }

    render(){
        return(
            <div className="daily__reflections__wrapper">
                <div className="daily__reflections__section">
                    <p className="title-wrapper">
                        Renungan Harian
                    </p>
                    <div className="underline__decorator"></div>
                    <div className="reflections__desc">                       
                        {this.state.todayReflection.content.map((content, index) =>
                            (<p className="pd-left-20 pd-right-20" key={index} dangerouslySetInnerHTML={{ __html: content }}></p>)
                        )}
                    </div>
                    <div className="underline__decorator"></div>                              
                    <div className="date-wrapper">
                        <span>{this.state.currentDate}</span>
                    </div>
                </div>                
            </div>
        )
    }
}

export default DailyReflections;