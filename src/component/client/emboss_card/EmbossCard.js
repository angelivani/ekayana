import React, { Component } from 'react';

class EmbossCard extends Component {
    render() {
        const {image, title, subtitle, description} = this.props;
        return (
            <div className="card emboss_card">
                <img className="card-img-top custom-image" src={image} alt="Card image cap" />
                <div className="card-body">
                    <p className="custom-card-title">{title}</p>
                    { subtitle && <p className="card-subtitle mb-2 text-muted">{subtitle}</p> }
                    <p className="card-text event__ek__desc">{description}</p>
                </div>
            </div>
        );
    }
}

export default EmbossCard;