import React, { Component } from 'react';
import DocumentationCard from './DocumentationCard';

class DocumentationOverview extends Component {
    constructor(){
        super();
        this.state = {
            listUpcomingEvent: [
                {
                    id: 1,
                    image: 'https://media.istockphoto.com/photos/abstract-network-connection-background-picture-id509731276?k=6&m=509731276&s=612x612&w=0&h=C8_3Gb8V7DHKZnO1BP-BHYKYfTvxxqJAM29OtvaC7Qs=',
                    title: 'Lorem ipsum dolor',
                    date: '25 Jan 2018',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley'
                },
                {
                    id: 2,
                    image: 'https://media.istockphoto.com/photos/abstract-network-connection-background-picture-id509731276?k=6&m=509731276&s=612x612&w=0&h=C8_3Gb8V7DHKZnO1BP-BHYKYfTvxxqJAM29OtvaC7Qs=',
                    title: 'Why do we use it?',
                    date: '25 Jan 2018',
                    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here '
                },
                {
                    id: 3,
                    image: 'https://media.istockphoto.com/photos/abstract-network-connection-background-picture-id509731276?k=6&m=509731276&s=612x612&w=0&h=C8_3Gb8V7DHKZnO1BP-BHYKYfTvxxqJAM29OtvaC7Qs=',
                    title: 'Where can I get some?',
                    date: '25 Jan 2018',
                    description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you'
                }
            ]
        }
    }

    render() {
        return (
            <div className="d-flex justify-content-center align-items-center">
                <div className="row">
                        {this.state.listUpcomingEvent.map( (event, index) => (
                            <div className="col-md-4 col-sm-12 col-xs-12 mb-3">
                                <DocumentationCard card={event} />
                            </div>
                        )) }
                </div>
            </div>
        );
    }
}

export default DocumentationOverview;