import React, { Component } from 'react';

class DocumentationCard extends Component {
    render() {
        const {card} = this.props;
        return (
            <div className="card" key={card.id}>
                <img className="card-img-top" key={card.id} src={card.image} alt="Card image cap" />
                <div className="card-body">
                    <h5 className="card-title" key={card.id}>{card.title}</h5>
                    { card.date ? <h6 className="card-subtitle mb-2 text-muted" key={card.id}>{card.date}</h6> : null }
                    <p className="card-text" key={card.id}>{card.description}</p>
                </div>
            </div>
        );
    }
}

export default DocumentationCard;