import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    ADD_FACILITY_SUCCEED,
    UPDATE_FACILITY_SUCCEED,
    DELETE_FACILITY_SUCCEED,
    GET_FACILITY_LIST_SUCCEED,
    GET_FACILITY_DETAIL_SUCCEED,
    FAILED_PROCESS_FACILITY
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function getFacilityList(data){
    return function(dispatch){
        axios.get(`${API_URL}/facility?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_FACILITY_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_FACILITY, payload: err.response.data.message})
        })
    }
}

export function getFacilityDetail(id){
    return function(dispatch){
        axios.get(`${API_URL}/facility/${id}`)
        .then(res => {
            dispatch({type: GET_FACILITY_DETAIL_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_FACILITY, payload: err.response.data.message})
        })
    }
} 

export function addFacility(data){
    var formData = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);    
    formData.append('image', data.image);
    
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/facility?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: ADD_FACILITY_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_FACILITY, payload: err});
        })
    }
}

export function editFacility(data){
    var formData = new FormData();
    formData.append('id', data.id);
    formData.append('name', data.name);
    formData.append('description', data.description);    
    formData.append('image', data.image);

    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/facility?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: UPDATE_FACILITY_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_FACILITY, payload: err.response.data.message});
        })
    }
}

export function deleteFacility(id){
    return function(dispatch){
        axios.delete(`${API_URL}/facility/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: DELETE_FACILITY_SUCCEED, payload: res})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_FACILITY, payload: err.response.data.message})
        })
    }
}