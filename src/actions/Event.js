import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    GET_PINNED_EVENT_SUCCEED,
    ADD_EVENT_SUCCEED, 
    UPDATE_EVENT_SUCCEED,
    DELETE_EVENT_SUCCEED,
    GET_EVENT_DETAIL_SUCCEED,
    GET_EVENT_LIST_SUCCEED,
    PIN_EVENT_SUCCEED,
    UNPIN_EVENT_SUCCEED,
    FAILED_PROCESS_EVENT
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function getPinnedEventList(){
    return function(dispatch){
        axios.get(`${API_URL}/event/pinned`)
        .then(res => {
            dispatch({type: GET_PINNED_EVENT_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err})
        })
    }
}

export function addEvent(data){
    var formData = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('event_date', data.event_date);
    formData.append('image[0]', data.image[0]);

    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/event?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: ADD_EVENT_SUCCEED, payload: "Event berhasil ditambahkan"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err})
        })
    }
}

export function editEvent(data){
    // let dataToSend = null;
    // console.log(data);
    var formData = new FormData();
    formData.append('id', data.id);
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('event_date', data.event_date);

    if(typeof data.image == 'object') {
        formData.append('image[0]', data.image[0]);
        // dataToSend = formData;
    }
    // else{
    //     dataToSend = {
    //         id: data.id,
    //         name: data.name,
    //         description: data.description,
    //         event_date: data.event_date
    //     }
    // }
    
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/event?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: UPDATE_EVENT_SUCCEED, payload: "Event berhasil diupdate"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}

export function deleteEvent(id){
    return function(dispatch){
        axios.delete(`${API_URL}/event?accessToken=${access_token}&id=${id}`)
        .then(res => {
            dispatch({type: DELETE_EVENT_SUCCEED, payload: "Event berhasil dihapus"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}



export function getEventDetail(id){
    return function(dispatch){
        
        axios.get(`${API_URL}/event/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: GET_EVENT_DETAIL_SUCCEED, payload: res.data.data[0]})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}


export function getEventList(data){
    return function(dispatch){
        axios.get(`${API_URL}/event?accessToken=${access_token}&limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_EVENT_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err})
        })
    }
}

export function getEventListForPublic(data){
    return function(dispatch){
        axios.get(`${API_URL}/event/public?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_EVENT_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err})
        })
    }
}

export function getEventDetailForPublic(id){
    return function(dispatch){
        
        axios.get(`${API_URL}/event/public/${id}`)
        .then(res => {
            dispatch({type: GET_EVENT_DETAIL_SUCCEED, payload: res.data.data[0]})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}

export function pinningEvent(data){
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/event/pin?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: PIN_EVENT_SUCCEED, payload: "Event berhasil dipin"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}

export function unpinEvent(data){
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/event/unpin?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: UNPIN_EVENT_SUCCEED, payload: "Event berhasil di-unpin"});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EVENT, payload: err.response.data.message})
        })
    }
}