import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL, 
    GET_USER_LIST,
    USER_LOGIN_SUCCEED, 
    USER_LOGIN_FAILED,
    ADD_USER_SUCCEED,
	FAILED_PROCESS_USER,
	UPDATE_USER_LIST_SUCCEED,
	RESET_USER_PROPS,
	FAILED_UPDATE_USER
} from '../constants/ActionTypes';

export function userLogin(data){
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/user/authentication`,
            data: data
        })
        .then(res => {
            const cookies = new Cookies();
            cookies.set('ek_accessToken', res.data.data.accessToken);
            dispatch({type: USER_LOGIN_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: USER_LOGIN_FAILED, payload: err.response.data.message});
        })
    }
}

export function addUser(data){
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/user/signup`,
            data: data
        })
        .then(res => {
            dispatch({type: ADD_USER_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_USER, payload: err.response.data.message});
        })
    } 
}

export function getUserAdminList() {
    const cookies = new Cookies();
    const access_token = cookies.get('ek_accessToken');
    return function(dispatch) {
        axios.get(`${API_URL}/user/list?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: GET_USER_LIST, payload: res.data.data });
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_USER, payload: err});
        })
    }
}

export function updateUserAdmin(data) {
    const cookies = new Cookies();
    const access_token = cookies.get('ek_accessToken');
    return function(dispatch) {
        axios({
          method: 'PUT',
					url: `${API_URL}/user/update-password?accessToken=${access_token}`,
					data: data
        })
        .then(res => {
            dispatch({type: UPDATE_USER_LIST_SUCCEED, payload: res });
        })
        .catch(err => {
            dispatch({type: FAILED_UPDATE_USER, payload: err});
        })
    }
}

export function resetUserProps() {
	return function (dispatch) {
		dispatch({ type: RESET_USER_PROPS });
	}
}