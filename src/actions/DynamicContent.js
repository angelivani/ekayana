import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    UPDATE_DYNAMIC_WEB_CONTENT_SUCCEED,
    GET_DYNAMIC_WEB_CONTENT_SUCCEED,
    FAILED_PROCESS_DYNAMIC_WEB_CONTENT
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function updateWebContentBasedOnType(data){
    console.log('DATA DI ACTIONS', data);
    var formData = new FormData();
    formData.append('image', data.image);
    formData.append('description', data.description);

    return function(dispatch) {
        axios({
            method: 'PUT',
            url: `${API_URL}/web_contents/${data.type}?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({ type: UPDATE_DYNAMIC_WEB_CONTENT_SUCCEED, payload: res.data })
        })
        .catch(err => {
            dispatch({ type: FAILED_PROCESS_DYNAMIC_WEB_CONTENT, payload: err })
        })
    }
}

export function getWebContentByType(type){
    return function(dispatch) {
        axios.get(`${API_URL}/web_contents/${type}`)
        .then(res => {
            dispatch({type: GET_DYNAMIC_WEB_CONTENT_SUCCEED, payload: res.data })
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_DYNAMIC_WEB_CONTENT, payload: err })
        })
    }
}
