import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    GET_PHOTOS_SUCCEED,
    ADD_PHOTOS_SUCCEED,
    DELETE_PHOTOS_SUCCEED,
    FAILED_PROCESS_PHOTOS
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function getPhotosInAlbumn(data){
    return function(dispatch){
        axios.get(`${API_URL}/album/${data.albumn_id}?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_PHOTOS_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_PHOTOS, payload: err.response.data.message})
        })
    }
}

export function addPhotosIntoAlbumn(data){
    var formData = new FormData();
    for(var i=0; i < data.listImage.length; i++){
        formData.append(`image[${i}]`, data.listImage[i])
        console.log(data.listImage[i])
    }
    
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/album/${data.albumn_id}/upload?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: ADD_PHOTOS_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_PHOTOS, payload: err.response.data.message})
        })
    }
}

export function deletePhotosInAlbumn(data){
    let listId = {
        id: data.listPhotosToDelete
    }
    console.log(listId);

    return function(dispatch){
        axios({
            method: 'DELETE',
            url: `${API_URL}/album/${data.albumn_id}/delete-photos?accessToken=${access_token}`,
            data: listId  
        })
        .then(res => {
            dispatch({ type: DELETE_PHOTOS_SUCCEED, payload: res.data });
        })
        .catch(err => {
            dispatch({ type: FAILED_PROCESS_PHOTOS, payload: err.response.data.message })
        })
    }
}