import axios from 'axios';
import Cookies from 'universal-cookie';

import {
  API_URL,
  USER_SUBSCRIBE,
  USER_UNSUBSCRIBE,
  FAILED_SUBSCRIBE,
  FAILED_UNSUBSCRIBE 
} from '../constants/ActionTypes';

export function userSubscribe(email) {
  return function(dispatch) {
    axios.post(`${API_URL}/user/subscribe/${email}`)
    .then(res => {
      dispatch({type: USER_SUBSCRIBE, payload: res})
    })
    .catch(err => {
      dispatch({type: FAILED_SUBSCRIBE, payload: err})
    })
  }
}

export function userUnsubscribe() {
  return function(dispatch) {
    axios.delete(`${API_URL}/user/subscribe/${email}`)
    .then(res => {
      dispatch({ ype: USER_UNSUBSCRIBE, payload: res })
    })
    .catch(err => {
      dispatch({ type: FAILED_UNSUBSCRIBE, payload: err })
    })
  }
}
