import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL, 
    GET_EBOOK_DETAIL_SUCCEED, 
    GET_EBOOK_LIST_SUCCEED, 
    ADD_EBOOK_SUCCEED, 
    UPDATE_EBOOK_SUCCEED, 
    DELETE_EBOOK_SUCCEED, 
    FAILED_PROCESS_EBOOK
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function addEbook(data){
    var formData = new FormData();
    formData.append('name', data.name);
    formData.append('source', data.source);
    formData.append('file', data.file);

    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/ebook?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: ADD_EBOOK_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err.response.data.message});
        })
    }
}


export function editEbook(data){
    var formData = new FormData();
    formData.append('name', data.name);
    formData.append('source', data.source);
    
    formData.append('id', data.id);

    if(typeof data.file == 'object'){
        formData.append('file', data.file);
    }
    
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/ebook?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: UPDATE_EBOOK_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err});
        })
    }
}

export function deleteEbook(id){
    return function(dispatch){
        axios.delete(`${API_URL}/ebook?accessToken=${access_token}&id=${id}`)
        .then(res => {
            dispatch({type: DELETE_EBOOK_SUCCEED})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err.response.data.message})
        })
    }
}

export function getEbookDetail(id){
    return function(dispatch){
        axios.get(`${API_URL}/ebook/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: GET_EBOOK_DETAIL_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err.response.data.message})
        })
    }
} 

export function getEbookList(data){
    return function(dispatch){
        axios.get(`${API_URL}/ebook?accessToken=${access_token}&limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_EBOOK_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err.response.data.message})
        })
    }
}

export function getEbookListForPublic(data){
    return function(dispatch){
        axios.get(`${API_URL}/ebook/public?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_EBOOK_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_EBOOK, payload: err})
        })
    }
}






