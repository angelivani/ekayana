import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL, 
    GET_BUDDHAVACANA_LIST, 
    ADD_BUDDHAVACANA_SUCCEED, 
    UPDATE_BUDDHAVACANA_SUCCEED, 
    GET_BUDDHAVACANA_DETAIL, 
    DELETE_BUDDHAVACANA_SUCCEED,
    FAILED_PROCESS_BUDDHAVACANA
} from '../constants/ActionTypes';


const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function deleteBuddhavacana(id){
    return function(dispatch){
        axios.delete(`${API_URL}/buddhavacana?accessToken=${access_token}&id=${id}`)
        .then(res => {
            dispatch({type: DELETE_BUDDHAVACANA_SUCCEED})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message})
        })
    }
}

export function getBuddhavacanaList(data){
    return function(dispatch){
        axios.get(`${API_URL}/buddhavacana?accessToken=${access_token}&limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_BUDDHAVACANA_LIST, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message})
        })
    }
}

export function getBuddhavacanaListForPublic(data){
    return function(dispatch){
        axios.get(`${API_URL}/buddhavacana/public?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_BUDDHAVACANA_LIST, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message})
        })
    }
}

export function addBuddhavacana(data){
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/buddhavacana?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: ADD_BUDDHAVACANA_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message})
        })
    }
}

export function updateBuddhavacana(data){
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/buddhavacana?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: UPDATE_BUDDHAVACANA_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message})
        })
    }
}

export function getBuddhavacanaDetail(id){
    return function(dispatch){
        axios.get(`${API_URL}/buddhavacana/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: GET_BUDDHAVACANA_DETAIL, payload: res.data.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_BUDDHAVACANA, payload: err.response.data.message});
        })
    }
}