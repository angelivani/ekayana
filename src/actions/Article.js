import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    ADD_ARTICLE_SUCCEED,
    UPDATE_ARTICLE_SUCCEED,
    DELETE_ARTICLE_SUCCEED,
    GET_ARTICLE_DETAIL_SUCCEED,
    GET_ARTICLE_LIST_SUCCEED,
    FAILED_PROCESS_ARTICLE
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function addArticle(data) {
	console.log('FILE IMAGE', data.image);
    var formData = new FormData();
    formData.append('image', data.image);
    formData.append('name', data.name);
    formData.append('description', data.description);
    
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/article?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({ type: ADD_ARTICLE_SUCCEED, payload: res.data })
        })
        .catch(err => {
            console.log(err);
            dispatch({ type: FAILED_PROCESS_ARTICLE, payload: err })
        })
    }
}

export function updateArticle(data){
    console.log(data.image);
    var formData = new FormData();
    formData.append('id', data.id);
    formData.append('image', data.image);
    formData.append('name', data.name);
    formData.append('description', data.description);
    
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/article?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({ type: UPDATE_ARTICLE_SUCCEED, payload: res.data })
        })
        .catch(err => {
            dispatch({ type: FAILED_PROCESS_ARTICLE, payload: err })
        })
    }
}

export function deleteArticle(id){
    return function(dispatch){
        axios.delete(`${API_URL}/article/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: DELETE_ARTICLE_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({ type: FAILED_PROCESS_ARTICLE, payload: err});
        })
    }
}

export function getArticleById(id){
    return function(dispatch){
        axios.get(`${API_URL}/article/${id}`)
        .then(res => {
            dispatch({ type: GET_ARTICLE_DETAIL_SUCCEED, payload: res.data.data })
        })
        .catch(err => {
            dispatch({ type: FAILED_PROCESS_ARTICLE, payload: err });
        })
    }
}

export function getArticleList(data){
    return function(dispatch){
        axios.get(`${API_URL}/article?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_ARTICLE_LIST_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ARTICLE, payload: err});
        })
    }
}