import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL, 
    GET_ACTIVITY_LIST_SUCCEED, 
    GET_ACTIVITY_DETAIL_SUCCEED, 
    ADD_ACTIVITY_SUCCEED, 
    UPDATE_ACTIVITY_SUCCEED, 
    DELETE_ACTIVITY_SUCCEED, 
    FAILED_PROCESS_ACTIVITY
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function addActivity(data){
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/activity?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: ADD_ACTIVITY_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message});
        })
    }
}

export function updateActivity(data){
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/activity?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: UPDATE_ACTIVITY_SUCCEED, payload: res.data});
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message});
        })
    }
}

export function getDetailActivity(id){
    return function(dispatch){
        axios.get(`${API_URL}/activity/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: GET_ACTIVITY_DETAIL_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message});
        })
    }
}

export function getActivityList(data){
    return function(dispatch){
        axios.get(`${API_URL}/activity?accessToken=${access_token}&limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_ACTIVITY_LIST_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message})
        })
    } 
}

export function getActivityListForPublic(data){
    return function(dispatch){
        axios.get(`${API_URL}/activity/public?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_ACTIVITY_LIST_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message})
        })
    } 
}

export function deleteActivity(id){
    return function(dispatch){
        axios.delete(`${API_URL}/activity?accessToken=${access_token}&id=${id}`)
        .then(res => {
            dispatch({type: DELETE_ACTIVITY_SUCCEED})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ACTIVITY, payload: err.response.data.message})
        })
    }
}