import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    GET_LEADER_LIST_SUCCEED,
    GET_LEADER_DETAIL_SUCCEED,
    ADD_LEADER_SUCCEED,
    UPDATE_LEADER_SUCCEED,
    DELETE_LEADER_SUCCEED,
    FAILED_PROCESS_LEADER
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function getLeaderList(data){
    return function(dispatch){
        axios.get(`${API_URL}/leader?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_LEADER_LIST_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_LEADER, payload: err.response.data.message})
        })
    }
}

export function getLeaderDetail(id){
    return function(dispatch){
        axios.get(`${API_URL}/leader/${id}`)
        .then(res => {
            dispatch({type: GET_LEADER_DETAIL_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_LEADER, payload: err.response.data.message})
        })
    }
}

export function deleteLeader(id){
    return function(dispatch){
        axios.delete(`${API_URL}/leader/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: DELETE_LEADER_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_LEADER, payload: err.response.data.message})
        })
    }
}

export function addLeader(data){
    var formData = new FormData();
    formData.append('name', data.name);
    formData.append('position', data.position);
    formData.append('description', data.description);
    formData.append('image', data.image);

    return function(dispatch){
        axios({
            url: `${API_URL}/leader?accessToken=${access_token}`,
            method: 'POST',
            data: formData
        })
        .then(res => {
            dispatch({type: ADD_LEADER_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_LEADER, payload: err.response.data.message})
        })
    }
}

export function editLeader(data){
    var formData = new FormData();
    formData.append('id', data.id);
    formData.append('name', data.name);
    formData.append('position', data.position);
    formData.append('description', data.description);
    formData.append('image', data.image);

    return function(dispatch){
        axios({
            url: `${API_URL}/leader?accessToken=${access_token}`,
            method: 'PUT',
            data: formData
        })
        .then(res => {
            dispatch({type: UPDATE_LEADER_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_LEADER, payload: err.response.data.message})
        })
    }
}