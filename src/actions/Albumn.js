import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    GET_ALBUMN_LIST_SUCCEED,
    GET_ALBUMN_DETAIL_SUCCEED,
    ADD_ALBUMN_SUCCEED, 
    UPDATE_ALBUMN_SUCCEED,
    DELETE_ALBUMN_SUCCEED,
    FAILED_PROCESS_ALBUMN
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function addAlbumn(data){
    console.log(data);
    return function(dispatch){
        axios({
            method: 'POST',
            url: `${API_URL}/album?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            console.log(res);
            dispatch({type: ADD_ALBUMN_SUCCEED, payload: "Albumn berhasil ditambahkan"})
        })
        .catch(err => {
            console.log(err);
            // dispatch({type: FAILED_PROCESS_ALBUMN, payload: err})
        })
    }
}

export function updateAlbumn(data){
    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/album?accessToken=${access_token}`,
            data: data
        })
        .then(res => {
            dispatch({type: UPDATE_ALBUMN_SUCCEED, payload: "Albumn berhasil diupdate"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ALBUMN, payload: err})
        })
    }
}

export function deleteAlbumn(id){
    return function(dispatch){
        axios.delete(`${API_URL}/album/${id}?accessToken=${access_token}`)
        .then(res => {
            dispatch({type: DELETE_ALBUMN_SUCCEED, payload: "Albumn berhasil dihapus"})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ALBUMN, payload: err})
        })
    }
}

export function getAlbumnDetail(data){
    return function(dispatch){
        axios.get(`${API_URL}/album/${data.id}?limit=${data.limit}&offset=${data.offset}`)
        .then(res => {
            dispatch({type: GET_ALBUMN_DETAIL_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ALBUMN, payload: err})
        })
    }
}

export function getAlbumnList(){
    return function(dispatch){
        axios.get(`${API_URL}/album`)
        .then(res => {
            dispatch({type: GET_ALBUMN_LIST_SUCCEED, payload: res.data.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_ALBUMN, payload: err})
        })
    }
}