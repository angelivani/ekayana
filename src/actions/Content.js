import axios from 'axios';
import Cookies from 'universal-cookie';

import {
    API_URL,
    UPDATE_WEB_CONTENT_SUCCEED,
    GET_WEB_CONTENT_SUCCEED,
    FAILED_PROCESS_WEB_CONTENT
} from '../constants/ActionTypes';

const cookies = new Cookies();
const access_token = cookies.get('ek_accessToken');

export function updateWebContent(data){
    let formData = new FormData();
    formData.append('description', data.description);
    formData.append('vision', data.vision);
    formData.append('mission', data.mission);
    formData.append('iframe', data.iframe);
    formData.append('image', data.image);

    return function(dispatch){
        axios({
            method: 'PUT',
            url: `${API_URL}/web_contents/about?accessToken=${access_token}`,
            data: formData
        })
        .then(res => {
            dispatch({type: UPDATE_WEB_CONTENT_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_WEB_CONTENT, payload: err})
        })
    }
}

export function getWebContent(){
    return function(dispatch){
        axios.get(`${API_URL}/web_contents/about`)
        .then(res => {
            dispatch({type: GET_WEB_CONTENT_SUCCEED, payload: res.data})
        })
        .catch(err => {
            dispatch({type: FAILED_PROCESS_WEB_CONTENT, payload: err.response})
        })
    }
}