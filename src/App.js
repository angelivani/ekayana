import React, { Component } from 'react';

import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import {Provider} from 'react-redux';

import Admin from './views/admin/Admin';
import Client from './views/client/Client';
import PageNotFound from './views/PageNotFound';

import Default from './Default';
import store from './store';

import './App.css';

import { instanceOf } from 'prop-types';
import { CookiesProvider, withCookies, Cookies } from 'react-cookie';


class App extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };
  
  render() {
    return (
      <CookiesProvider>
        <Provider store={store}>
          <Router>
            <div style={{ width: "100%", height: "100px"}}>
              <Route exact path="/" component={Default} />
              <Route path="/id" component={Client} />
              <Route path="/admin" component={Admin} />
              <Route path="/404" component={PageNotFound} />
            </div>
          </Router>
        </Provider>
      </CookiesProvider>      
    );
  }
}

App = withCookies(App);
export default App;
