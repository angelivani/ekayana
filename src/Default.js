import React, {Component} from 'react';
import {Redirect} from 'react-router';

class Default extends Component {
    render(){
        return(
            <Redirect to="/id/home" />
        )
    }
}

export default Default;